package com.core136.service.party;

import com.core136.bean.party.PartyDues;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyDuesMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyDuesService {
    private PartyDuesMapper partyDuesMapper;

    @Autowired
    public void setPartyDuesMapper(PartyDuesMapper partyDuesMapper) {
        this.partyDuesMapper = partyDuesMapper;
    }

    public int insertPartyDues(PartyDues partyDues) {
        return partyDuesMapper.insert(partyDues);
    }

    public int deletePartyDues(PartyDues partyDues) {
        return partyDuesMapper.delete(partyDues);
    }

    public int updatePartyDues(Example example, PartyDues partyDues) {
        return partyDuesMapper.updateByExampleSelective(partyDues, example);
    }

    public PartyDues selectOnePartyDues(PartyDues partyDues) {
        return partyDuesMapper.selectOne(partyDues);
    }

    /**
     * @param orgId
     * @param onTime
     * @param duesType
     * @param year
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyDeusList
     * @Description:  获取党员缴费列表
     */
    public List<Map<String, String>> getPartyDeusList(String orgId, String onTime, String duesType, String year, String search) {
        return partyDuesMapper.getPartyDeusList(orgId, onTime, duesType, year, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param onTime
     * @param duesType
     * @param year
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyDeusList
     * @Description:  获取党员缴费列表
     */
    public PageInfo<Map<String, String>> getPartyDeusList(PageParam pageParam, String onTime, String duesType, String year) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyDeusList(pageParam.getOrgId(), onTime, duesType, year, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param memberId
     * @param year
     * @return List<Map < String, String>>
     * @Title: getPartyDeusList
     * @Description:  获取党员缴费列表
     */
    public List<Map<String, String>> getMyPartyDeusList(String orgId, String memberId, String year) {
        if (StringUtils.isBlank(year)) {
            year = SysTools.getTime("yyyy");
        }
        return partyDuesMapper.getMyPartyDeusList(orgId, memberId, year);
    }


}
