package com.core136.service.party;

import com.core136.bean.party.PartyChangeTerm;
import com.core136.mapper.party.PartyChangeTermMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class PartyChangeTermService {

    private PartyChangeTermMapper partyChangeTermMapper;

    @Autowired
    public void setPartyChangeTermMapper(PartyChangeTermMapper partyChangeTermMapper) {
        this.partyChangeTermMapper = partyChangeTermMapper;
    }

    public int insertPartyChangeTerm(PartyChangeTerm partyChangeTerm) {
        return partyChangeTermMapper.insert(partyChangeTerm);
    }

    public int deletePartyChangeTerm(PartyChangeTerm partyChangeTerm) {
        return partyChangeTermMapper.delete(partyChangeTerm);
    }

    public int updatePartyChangeTerm(Example example, PartyChangeTerm partyChangeTerm) {
        return partyChangeTermMapper.updateByExampleSelective(partyChangeTerm, example);
    }

    public PartyChangeTerm selectOnePartyChangeTerm(PartyChangeTerm partyChangeTerm) {
        return partyChangeTermMapper.selectOne(partyChangeTerm);
    }

}
