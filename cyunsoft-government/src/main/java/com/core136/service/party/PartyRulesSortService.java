package com.core136.service.party;

import com.core136.bean.party.PartyRulesSort;
import com.core136.mapper.party.PartyRulesSortMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyRulesSortService {

    private PartyRulesSortMapper partyRulesSortMapper;

    @Autowired
    public void setPartyRulesSortMapper(PartyRulesSortMapper partyRulesSortMapper) {
        this.partyRulesSortMapper = partyRulesSortMapper;
    }

    public int insertPartyRulesSort(PartyRulesSort partyRulesSort) {
        return partyRulesSortMapper.insert(partyRulesSort);
    }

    public int deletePartyRulesSort(PartyRulesSort partyRulesSort) {
        return partyRulesSortMapper.delete(partyRulesSort);
    }

    public int updatePartyRulesSort(Example example, PartyRulesSort partyRulesSort) {
        return partyRulesSortMapper.updateByExampleSelective(partyRulesSort, example);
    }

    public PartyRulesSort selectOnePartyRulesSort(PartyRulesSort partyRulesSort) {
        return partyRulesSortMapper.selectOne(partyRulesSort);
    }

    /**
     * @param orgId
     * @param sortLevel
     * @return List<Map < String, String>>
     * @Title: getRulesSortTree
     * @Description:  获取制度分类树结构
     */

    public List<Map<String, String>> getRulesSortTree(String orgId, String sortLevel) {
        return partyRulesSortMapper.getRulesSortTree(orgId, sortLevel);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        return partyRulesSortMapper.isExistChild(orgId, sortId);
    }
}
