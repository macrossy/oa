package com.core136.service.party;

import com.core136.bean.file.Attach;
import com.core136.bean.party.PartyAlbumVideo;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.common.video.VideoUtil;
import com.core136.mapper.party.PartyAlbumVideoMapper;
import com.core136.service.file.AttachService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.io.File;
import java.util.List;
import java.util.Map;

@Service
public class PartyAlbumVideoService {
    @Value("${app.attachpath}")
    private String attachpath;
    private PartyAlbumVideoMapper partyAlbumVideoMapper;

    @Autowired
    public void setPartyAlbumVideoMapper(PartyAlbumVideoMapper partyAlbumVideoMapper) {
        this.partyAlbumVideoMapper = partyAlbumVideoMapper;
    }

    private AttachService attachService;

    @Autowired
    public void setAttachService(AttachService attachService) {
        this.attachService = attachService;
    }


    public int insertPartyAlbumVideo(PartyAlbumVideo partyAlbumVideo) {
        return partyAlbumVideoMapper.insert(partyAlbumVideo);
    }

    public int deletePartyAlbumVideo(PartyAlbumVideo partyAlbumVideo) {
        return partyAlbumVideoMapper.delete(partyAlbumVideo);
    }

    public int updatePartyAlbumVideo(Example example, PartyAlbumVideo partyAlbumVideo) {
        return partyAlbumVideoMapper.updateByExampleSelective(partyAlbumVideo, example);
    }

    public PartyAlbumVideo selectOnePartyAlbumVideo(PartyAlbumVideo partyAlbumVideo) {
        return partyAlbumVideoMapper.selectOne(partyAlbumVideo);
    }

    /**
     * @param partyAlbumVideo int
     * @Title: setAlbumVideoClickCount
     * @Description:  设置播放次数
     */
    public int setAlbumVideoClickCount(PartyAlbumVideo partyAlbumVideo) {
        partyAlbumVideo = selectOnePartyAlbumVideo(partyAlbumVideo);
        partyAlbumVideo.setClickCount(partyAlbumVideo.getClickCount() + 1);
        Example example = new Example(PartyAlbumVideo.class);
        example.createCriteria().andEqualTo("orgId", partyAlbumVideo.getOrgId()).andEqualTo("videoId", partyAlbumVideo.getVideoId());
        return updatePartyAlbumVideo(example, partyAlbumVideo);
    }

    /**
     * 获取热榜TOP10
     * @param orgId
     * @param memberId
     * @return
     */
    public List<Map<String, String>> getAlbumVideoForHomeTop(String orgId, String memberId) {
        return partyAlbumVideoMapper.getAlbumVideoForHomeTop(orgId, memberId);
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getSendVideoUserList
     * @Description:   获取所有发布视频的人员列表前10
     */
    public List<Map<String, String>> getSendVideoUserList(String orgId) {
        return partyAlbumVideoMapper.getSendVideoUserList(orgId);
    }

    /**
     * @param orgId
     * @param albumType
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getAlbumVideoForHome
     * @Description:  获取专辑首页视频列表
     */
    public List<Map<String, String>> getAlbumVideoForHome(String orgId, String albumType, String memberId) {
        return partyAlbumVideoMapper.getAlbumVideoForHome(orgId, albumType, memberId);
    }

    /**
     * @param orgId
     * @param albumType
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getAlbumVideoByAlbumType
     * @Description:  获了按分类视频
     */
    public List<Map<String, String>> getAlbumVideoByAlbumType(String orgId, String albumType, String memberId) {
        return partyAlbumVideoMapper.getAlbumVideoByAlbumType(orgId, albumType, memberId);
    }

    /**
     * @param pageParam
     * @param albumType
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getAlbumVideoByAlbumType
     * @Description:  获了按分类视频
     */
    public PageInfo<Map<String, String>> getAlbumVideoByAlbumType(PageParam pageParam, String albumType, String memberId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAlbumVideoByAlbumType(pageParam.getOrgId(), albumType, memberId);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param partyAlbumVideo
     * @return RetDataBean
     * @Title: upLoadVideo
     * @Description:
     */
    public RetDataBean upLoadVideo(PartyAlbumVideo partyAlbumVideo) {
        try {
            Attach attach = new Attach();
            attach.setAttachId(partyAlbumVideo.getAttach());
            attach.setOrgId(partyAlbumVideo.getOrgId());
            attach = attachService.selectOne(attach);
            String picName = SysTools.randFileName(attach.getOldName());
            picName = picName.substring(0, picName.lastIndexOf(".") + 1) + "jpg";
            String picPath = attachpath+SysTools.getUploadFilePath("videopic") + File.separator + attach.getAttachId() + File.separator + picName;
            File video = new File(attachpath+attach.getPath());
            Long time = VideoUtil.getVideoDuration(video);
            int date = time.intValue();
            partyAlbumVideo.setVideoDuration(SysTools.getDate(date));
            VideoUtil.getVideoPic(video, picPath, 5);
            partyAlbumVideo.setPics(picName);
            if(attach!=null) {
                partyAlbumVideo.setVideoDuration(SysTools.getDate(date));
                partyAlbumVideo.setFileSize((double) attach.getFileSize() / (1024 * 1024));
            }else
            {
                partyAlbumVideo.setVideoDuration("0秒");
                partyAlbumVideo.setFileSize(0.0);
            }
            insertPartyAlbumVideo(partyAlbumVideo);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param albumType
     * @param createUser
     * @param search
     * @return List<Map < String, String>>
     * @Title: getAlbumVideoForApprovel
     * @Description:  获取待审批专辑列表
     */
    public List<Map<String, String>> getAlbumVideoForApprovel(String orgId, String accountId, String beginTime, String endTime, String albumType, String createUser, String search) {
        return partyAlbumVideoMapper.getAlbumVideoForApprovel(orgId, accountId, beginTime, endTime, albumType, createUser, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param albumType
     * @param createUser
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getAlbumVideoForApprovel
     * @Description:  获取待审批专辑列表
     */
    public PageInfo<Map<String, String>> getAlbumVideoForApprovel(PageParam pageParam, String beginTime, String endTime, String albumType, String createUser) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAlbumVideoForApprovel(pageParam.getOrgId(), pageParam.getAccountId(), beginTime, endTime, albumType, createUser, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    public List<Map<String, String>> getAlbumVideoForManage(String orgId, String accountId, String beginTime, String endTime, String albumType, String createUser, String status,String search) {
        return partyAlbumVideoMapper.getAlbumVideoForManage(orgId, accountId, beginTime, endTime, albumType, createUser, status,"%" + search + "%");
    }

    public PageInfo<Map<String, String>> getAlbumVideoForManage(PageParam pageParam, String beginTime, String endTime, String albumType, String createUser,String status) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAlbumVideoForManage(pageParam.getOrgId(), pageParam.getAccountId(), beginTime, endTime, albumType, createUser, status,pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @param status
     * @param beginTime
     * @param endTime
     * @param albumType
     * @param createUser
     * @param search
     * @return List<Map < String, String>>
     * @Title: getAlbumVideoForApprovelOld
     * @Description:  获取历史审批记录
     */
    public List<Map<String, String>> getAlbumVideoForApprovelOld(String orgId, String accountId, String status, String beginTime, String endTime, String albumType, String createUser, String search) {
        return partyAlbumVideoMapper.getAlbumVideoForApprovelOld(orgId, accountId, status, beginTime, endTime, albumType, createUser, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param status
     * @param beginTime
     * @param endTime
     * @param albumType
     * @param createUser
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getAlbumVideoForApprovelOld
     * @Description:  获取历史审批记录
     */
    public PageInfo<Map<String, String>> getAlbumVideoForApprovelOld(PageParam pageParam, String status, String beginTime, String endTime, String albumType, String createUser) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAlbumVideoForApprovelOld(pageParam.getOrgId(), pageParam.getAccountId(), status, beginTime, endTime, albumType, createUser, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getAlbumVideoByCreateUser
     * @Description:  获取当前用户最近10条视频
     */
    public List<Map<String, String>> getAlbumVideoByCreateUser(String orgId, String accountId) {
        return partyAlbumVideoMapper.getAlbumVideoByCreateUser(orgId, accountId);
    }

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getAlbumVideoByHistory
     * @Description:  获取当前用户历史记录
     */
    public List<Map<String, String>> getAlbumVideoByHistory(String orgId, String accountId) {
        return partyAlbumVideoMapper.getAlbumVideoByHistory(orgId, accountId);
    }

}
