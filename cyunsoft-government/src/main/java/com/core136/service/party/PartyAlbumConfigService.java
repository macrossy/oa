package com.core136.service.party;

import com.core136.bean.account.Account;
import com.core136.bean.party.PartyAlbumConfig;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.mapper.party.PartyAlbumConfigMapper;
import com.core136.service.account.UserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class PartyAlbumConfigService {
    private PartyAlbumConfigMapper partyAlbumConfigMapper;

    @Autowired
    public void setPartyAlbumConfigMapper(PartyAlbumConfigMapper partyAlbumConfigMapper) {
        this.partyAlbumConfigMapper = partyAlbumConfigMapper;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    public int insertPartyAlbumConfig(PartyAlbumConfig partyAlbumConfig) {
        return partyAlbumConfigMapper.insert(partyAlbumConfig);
    }

    public int deletePartyAlbumConfig(PartyAlbumConfig partyAlbumConfig) {
        return partyAlbumConfigMapper.delete(partyAlbumConfig);
    }

    public int updatePartyAlbumConfig(Example example, PartyAlbumConfig partyAlbumConfig) {
        return partyAlbumConfigMapper.updateByExampleSelective(partyAlbumConfig, example);
    }

    public PartyAlbumConfig selectOnePartyAlbumConfig(PartyAlbumConfig partyAlbumConfig) {
        return partyAlbumConfigMapper.selectOne(partyAlbumConfig);
    }

    /**
     * @param account
     * @return List<Map < String, String>>
     * @Title: getApprovelUserList
     * @Description:  获取审批人员列表
     */
    public List<Map<String, String>> getApprovelUserList(Account account) {
        PartyAlbumConfig partyAlbumConfigTemp = new PartyAlbumConfig();
        partyAlbumConfigTemp.setOrgId(account.getOrgId());
        partyAlbumConfigTemp = selectOnePartyAlbumConfig(partyAlbumConfigTemp);
        if (partyAlbumConfigTemp == null) {
            return null;
        } else if (StringUtils.isNotBlank(partyAlbumConfigTemp.getApprovelUser())) {
            String approvelUserStr = partyAlbumConfigTemp.getApprovelUser();
            String[] userArr = approvelUserStr.split(",");
            List<String> list = Arrays.asList(userArr);
            return userInfoService.getAllUserInfoByAccountList(account.getOrgId(), list);
        } else {
            return null;
        }
    }


    /**
     * @param partyAlbumConfig
     * @return RetDataBean
     * @Title: setAlbumConfig
     * @Description:  设置专辑参数
     */
    public RetDataBean setAlbumConfig(PartyAlbumConfig partyAlbumConfig) {
        PartyAlbumConfig partyAlbumConfigTemp = new PartyAlbumConfig();
        partyAlbumConfigTemp.setOrgId(partyAlbumConfig.getOrgId());
        int count = partyAlbumConfigMapper.selectCount(partyAlbumConfigTemp);
        if (count == 0) {
            insertPartyAlbumConfig(partyAlbumConfig);
        } else {
            Example example = new Example(PartyAlbumConfig.class);
            example.createCriteria().andEqualTo("orgId", partyAlbumConfig.getOrgId());
            updatePartyAlbumConfig(example, partyAlbumConfig);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS);
    }

}
