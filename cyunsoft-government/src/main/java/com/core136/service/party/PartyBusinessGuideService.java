package com.core136.service.party;

import com.core136.bean.party.PartyBusinessGuide;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.party.PartyBusinessGuideMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyBusinessGuideService {

    private PartyBusinessGuideMapper partyBusinessGuideMapper;

    @Autowired
    public void setPartyBusinessGuideMapper(PartyBusinessGuideMapper partyBusinessGuideMapper) {
        this.partyBusinessGuideMapper = partyBusinessGuideMapper;
    }

    public int insertPartyBusinessGuide(PartyBusinessGuide partyBusinessGuide) {
        return partyBusinessGuideMapper.insert(partyBusinessGuide);
    }

    public int deletePartyBusinessGuide(PartyBusinessGuide partyBusinessGuide) {
        return partyBusinessGuideMapper.delete(partyBusinessGuide);
    }

    public int updatePartyBusinessGuide(Example example, PartyBusinessGuide partyBusinessGuide) {
        return partyBusinessGuideMapper.updateByExampleSelective(partyBusinessGuide, example);
    }

    public PartyBusinessGuide selectOnePartyBusinessGuide(PartyBusinessGuide partyBusinessGuide) {
        return partyBusinessGuideMapper.selectOne(partyBusinessGuide);
    }

    /**
     * @param orgId
     * @param guideType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyBusinessGuideList
     * @Description:  获取业务指南列表
     */
    public List<Map<String, String>> getPartyBusinessGuideList(String orgId, String guideType, String beginTime, String endTime, String search) {
        return partyBusinessGuideMapper.getPartyBusinessGuideList(orgId, guideType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param guideType
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyBusinessGuideList
     * @Description:  获取业务指南列表
     */
    public PageInfo<Map<String, String>> getPartyBusinessGuideList(PageParam pageParam, String guideType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyBusinessGuideList(pageParam.getOrgId(), guideType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
