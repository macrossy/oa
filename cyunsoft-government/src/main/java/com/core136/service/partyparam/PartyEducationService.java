package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyEducation;
import com.core136.mapper.partyparam.PartyEducationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyEducationService {
    private PartyEducationMapper partyEducationMapper;

    @Autowired
    public void setPartyEducationMapper(PartyEducationMapper partyEducationMapper) {
        this.partyEducationMapper = partyEducationMapper;
    }

    public int insertPartyEducation(PartyEducation partyEducation) {
        return partyEducationMapper.insert(partyEducation);
    }

    public int deletePartyEducation(PartyEducation partyEducation) {
        return partyEducationMapper.delete(partyEducation);
    }

    public int updatePartyEducation(Example example, PartyEducation partyEducation) {
        return partyEducationMapper.updateByExampleSelective(partyEducation, example);
    }

    public PartyEducation selectOnePartyEducation(PartyEducation partyEducation) {
        return partyEducationMapper.selectOne(partyEducation);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getGovEducationTree
     * @Description:  获取学历分类树
     */
    public List<Map<String, String>> getGovEducationTree(String orgId, String levelId) {
        return partyEducationMapper.getGovEducationTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        return partyEducationMapper.isExistChild(orgId, sortId);
    }
}
