package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyUnitType;
import com.core136.mapper.partyparam.PartyUnitTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnitTypeService {
    private PartyUnitTypeMapper partyUnitTypeMapper;

    @Autowired
    public void setPartyUnitTypeMapper(PartyUnitTypeMapper partyUnitTypeMapper) {
        this.partyUnitTypeMapper = partyUnitTypeMapper;
    }

    public int insertPartyUnitType(PartyUnitType partyUnitType) {
        return partyUnitTypeMapper.insert(partyUnitType);
    }

    public int deletePartyUnitType(PartyUnitType partyUnitType) {
        return partyUnitTypeMapper.delete(partyUnitType);
    }

    public int updatePartyUnitType(Example example, PartyUnitType partyUnitType) {
        return partyUnitTypeMapper.updateByExampleSelective(partyUnitType, example);
    }

    public PartyUnitType selectOnePartyUnitType(PartyUnitType partyUnitType) {
        return partyUnitTypeMapper.selectOne(partyUnitType);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getUnitTypeTree
     * @Description:  获取党组织工作分类
     */
    public List<Map<String, String>> getUnitTypeTree(String orgId, String levelId) {
        return partyUnitTypeMapper.getUnitTypeTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        //  Auto-generated method stub
        return partyUnitTypeMapper.isExistChild(orgId, sortId);
    }


}
