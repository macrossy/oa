package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyPostLevel;
import com.core136.mapper.partyparam.PartyPostLevelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyPostLevelService {
    private PartyPostLevelMapper partyPostLevelMapper;

    @Autowired
    public void setPartyPostLevelMapper(PartyPostLevelMapper partyPostLevelMapper) {
        this.partyPostLevelMapper = partyPostLevelMapper;
    }

    public int insertPartyPostLevel(PartyPostLevel partyPostLevel) {
        return partyPostLevelMapper.insert(partyPostLevel);
    }

    public int deletePartyPostLevel(PartyPostLevel partyPostLevel) {
        return partyPostLevelMapper.delete(partyPostLevel);
    }

    public int updatePartyPostLevel(Example example, PartyPostLevel partyPostLevel) {
        return partyPostLevelMapper.updateByExampleSelective(partyPostLevel, example);
    }

    public PartyPostLevel selectOnePartyPostLevel(PartyPostLevel partyPostLevel) {
        return partyPostLevelMapper.selectOne(partyPostLevel);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getPostLevelTree
     * @Description:  获取职务级别树
     */
    public List<Map<String, String>> getPostLevelTree(String orgId, String levelId) {
        return partyPostLevelMapper.getPostLevelTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        //  Auto-generated method stub
        return partyPostLevelMapper.isExistChild(orgId, sortId);
    }

}
