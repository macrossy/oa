package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyAppSitu;
import com.core136.mapper.partyparam.PartyAppSituMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyAppSituService {
    private PartyAppSituMapper partyAppSituMapper;

    @Autowired
    public void setPartyAppSituMapper(PartyAppSituMapper partyAppSituMapper) {
        this.partyAppSituMapper = partyAppSituMapper;
    }

    public int insertPartyAppSitu(PartyAppSitu partyAppSitu) {
        return partyAppSituMapper.insert(partyAppSitu);
    }

    public int deletePartyAppSitu(PartyAppSitu partyAppSitu) {
        return partyAppSituMapper.delete(partyAppSitu);
    }

    public int updatePartyAppSitu(Example example, PartyAppSitu partyAppSitu) {
        return partyAppSituMapper.updateByExampleSelective(partyAppSitu, example);
    }

    public PartyAppSitu selectOnePartyAppSitu(PartyAppSitu partyAppSitu) {
        return partyAppSituMapper.selectOne(partyAppSitu);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getAppSituTree
     * @Description:  评议奖惩情况类别树
     */
    public List<Map<String, String>> getAppSituTree(String orgId, String levelId) {
        return partyAppSituMapper.getAppSituTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        //  Auto-generated method stub
        return partyAppSituMapper.isExistChild(orgId, sortId);
    }
}
