package com.core136.service.partyparam;

import com.core136.bean.partyparam.PartyUnitSubo;
import com.core136.mapper.partyparam.PartyUnitSuboMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnitSuboService {
    private PartyUnitSuboMapper partyUnitSuboMapper;

    @Autowired
    public void setPartyUnitSuboMapper(PartyUnitSuboMapper partyUnitSuboMapper) {
        this.partyUnitSuboMapper = partyUnitSuboMapper;
    }

    public int insertPartyUnitSubo(PartyUnitSubo partyUnitSubo) {
        return partyUnitSuboMapper.insert(partyUnitSubo);
    }

    public int deletePartyUnitSubo(PartyUnitSubo partyUnitSubo) {
        return partyUnitSuboMapper.delete(partyUnitSubo);
    }

    public int updatePartyUnitSubo(Example example, PartyUnitSubo partyUnitSubo) {
        return partyUnitSuboMapper.updateByExampleSelective(partyUnitSubo, example);
    }

    public PartyUnitSubo selectOnePartyUnitSubo(PartyUnitSubo partyUnitSubo) {
        return partyUnitSuboMapper.selectOne(partyUnitSubo);
    }

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getUnitSuboTree
     * @Description:  获取单位隶属关系树
     */
    public List<Map<String, String>> getUnitSuboTree(String orgId, String levelId) {
        return partyUnitSuboMapper.getUnitSuboTree(orgId, levelId);
    }

    /**
     * @param orgId
     * @param sortId
     * @return int
     * @Title: isExistChild
     * @Description:  判断是否还有子集
     */

    public int isExistChild(String orgId, String sortId) {
        return partyUnitSuboMapper.isExistChild(orgId, sortId);
    }

}
