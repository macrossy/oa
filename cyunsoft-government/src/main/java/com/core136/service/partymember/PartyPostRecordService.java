package com.core136.service.partymember;

import com.core136.bean.partymember.PartyPostRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyPostRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyPostRecordService {

    private PartyPostRecordMapper partyPostRecordMapper;

    @Autowired
    public void setPartyPostRecordMapper(PartyPostRecordMapper partyPostRecordMapper) {
        this.partyPostRecordMapper = partyPostRecordMapper;
    }

    public int insertPartyPostRecord(PartyPostRecord partyPostRecord) {
        return partyPostRecordMapper.insert(partyPostRecord);
    }

    public int deletePartyPostRecord(PartyPostRecord partyPostRecord) {
        return partyPostRecordMapper.delete(partyPostRecord);
    }

    public int updatePartyPostRecord(Example example, PartyPostRecord partyPostRecord) {
        return partyPostRecordMapper.updateByExampleSelective(partyPostRecord, example);
    }

    public PartyPostRecord selectOnePartyPostRecord(PartyPostRecord partyPostRecord) {
        return partyPostRecordMapper.selectOne(partyPostRecord);
    }

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyPartyPostList
     * @Description:  获取个人党内任职记录
     */
    public List<Map<String, String>> getMyPartyPostList(String orgId, String memberId) {
        return partyPostRecordMapper.getMyPartyPostList(orgId, memberId);
    }

    /**
     * @param orgId
     * @param postType
     * @param admPost
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyPostList
     * @Description:  获取党内任职记录列表
     */
    public List<Map<String, String>> getPartyPostList(String orgId, String postType, String takeType, String beginTime, String endTime, String search) {
        return partyPostRecordMapper.getPartyPostList(orgId, postType, takeType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param postType
     * @param takeType
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyPostList
     * @Description:  获取党内任职记录列表
     */
    public PageInfo<Map<String, String>> getPartyPostList(PageParam pageParam, String postType, String takeType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyPostList(pageParam.getOrgId(), postType, takeType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
