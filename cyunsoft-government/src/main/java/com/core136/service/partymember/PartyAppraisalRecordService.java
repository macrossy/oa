package com.core136.service.partymember;

import com.core136.bean.partymember.PartyAppraisalRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyAppraisalRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyAppraisalRecordService {
    private PartyAppraisalRecordMapper partyAppraisalRecordMapper;

    @Autowired
    public void setPartyAppraisalRecordMapper(PartyAppraisalRecordMapper partyAppraisalRecordMapper) {
        this.partyAppraisalRecordMapper = partyAppraisalRecordMapper;
    }

    public int insertPartyAppraisalRecord(PartyAppraisalRecord partyAppraisalRecord) {
        return partyAppraisalRecordMapper.insert(partyAppraisalRecord);
    }

    public int deletePartyAppraisalRecord(PartyAppraisalRecord partyAppraisalRecord) {
        return partyAppraisalRecordMapper.delete(partyAppraisalRecord);
    }

    public int updatePartyAppraisalRecord(Example example, PartyAppraisalRecord partyAppraisalRecord) {
        return partyAppraisalRecordMapper.updateByExampleSelective(partyAppraisalRecord, example);
    }

    public PartyAppraisalRecord selectOnePartyAppraisalRecord(PartyAppraisalRecord partyAppraisalRecord) {
        return partyAppraisalRecordMapper.selectOne(partyAppraisalRecord);
    }


    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyAppraisalList
     * @Description:  获取个人民主评议记录
     */
    public List<Map<String, String>> getMyAppraisalList(String orgId, String memberId) {
        return partyAppraisalRecordMapper.getMyAppraisalList(orgId, memberId);
    }

    /**
     * @param orgId
     * @param appResult
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getAppraisalList
     * @Description:  获取民主评议列表
     */
    public List<Map<String, String>> getAppraisalList(String orgId, String appResult, String beginTime, String endTime, String search) {
        return partyAppraisalRecordMapper.getAppraisalList(orgId, appResult, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param appResult
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getAppraisalList
     * @Description:  获取民主评议列表
     */
    public PageInfo<Map<String, String>> getAppraisalList(PageParam pageParam, String appResult, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAppraisalList(pageParam.getOrgId(), appResult, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
