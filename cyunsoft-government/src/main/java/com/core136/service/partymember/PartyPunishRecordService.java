package com.core136.service.partymember;

import com.core136.bean.partymember.PartyPunishRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyPunishRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyPunishRecordService {

    private PartyPunishRecordMapper partyPunishRecordMapper;

    @Autowired
    public void setPartyPunishRecordMapper(PartyPunishRecordMapper partyPunishRecordMapper) {
        this.partyPunishRecordMapper = partyPunishRecordMapper;
    }

    public int insertPartyPunishRecord(PartyPunishRecord partyPunishRecord) {
        return partyPunishRecordMapper.insert(partyPunishRecord);
    }

    public int deletePartyPunishRecord(PartyPunishRecord partyPunishRecord) {
        return partyPunishRecordMapper.delete(partyPunishRecord);
    }

    public int updatePartyPunishRecord(Example example, PartyPunishRecord partyPunishRecord) {
        return partyPunishRecordMapper.updateByExampleSelective(partyPunishRecord, example);
    }

    public PartyPunishRecord selectOnePartyPunishRecord(PartyPunishRecord partyPunishRecord) {
        return partyPunishRecordMapper.selectOne(partyPunishRecord);
    }

    /**
     * @param orgId
     * @param beginTime
     * @param rewardLevel
     * @param memberId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMemberPunishList
     * @Description:  获取党员惩戒列表
     */
    public List<Map<String, String>> getMemberPunishList(String orgId, String beginTime, String endTime, String punishLevel, String memberId, String search) {
        return partyPunishRecordMapper.getMemberPunishList(orgId, beginTime, endTime, punishLevel, memberId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param rewardLevel
     * @param memberId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMemberPunishList
     * @Description:  获取党员惩戒列表
     */
    public PageInfo<Map<String, String>> getMemberPunishList(PageParam pageParam, String beginTime, String endTime, String punishLevel, String memberId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMemberPunishList(pageParam.getOrgId(), beginTime, endTime, punishLevel, memberId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyMemberPunishList
     * @Description:  获取个人惩戒记录列表
     */
    public List<Map<String, String>> getMyMemberPunishList(String orgId, String memberId) {
        return partyPunishRecordMapper.getMyMemberPunishList(orgId, memberId);
    }

}
