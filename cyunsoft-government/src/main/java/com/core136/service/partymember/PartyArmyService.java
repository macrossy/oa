package com.core136.service.partymember;

import com.core136.bean.partymember.PartyArmy;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyArmyMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyArmyService {
    private PartyArmyMapper partyArmyMapper;

    @Autowired
    public void setPartyArmyMapper(PartyArmyMapper partyArmyMapper) {
        this.partyArmyMapper = partyArmyMapper;
    }

    public int insertPartyArmy(PartyArmy partyArmy) {
        return partyArmyMapper.insert(partyArmy);
    }

    public int deletePartyArmy(PartyArmy partyArmy) {
        return partyArmyMapper.delete(partyArmy);
    }

    public int updatePartyArmy(Example example, PartyArmy partyArmy) {
        return partyArmyMapper.updateByExampleSelective(partyArmy, example);
    }

    public PartyArmy selectOnePartyArmy(PartyArmy partyArmy) {
        return partyArmyMapper.selectOne(partyArmy);
    }

    /**
     * @param orgId
     * @param userType
     * @param education
     * @param partyMember
     * @param search
     * @return List<Map < String, String>>
     * @Title: getArmyUserList
     * @Description:  获取统战人员列表
     */
    public List<Map<String, String>> getArmyUserList(String orgId, String userType, String education, String partyMember, String armyRank, String search) {
        return partyArmyMapper.getArmyUserList(orgId, userType, education, partyMember, armyRank, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param userType
     * @param education
     * @param partyMember
     * @param armyRank
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getArmyUserList
     * @Description:  获取统战人员列表
     */
    public PageInfo<Map<String, String>> getArmyUserList(PageParam pageParam, String userType, String education, String partyMember, String armyRank) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArmyUserList(pageParam.getOrgId(), userType, education, partyMember, armyRank, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
