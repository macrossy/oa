package com.core136.service.partymember;

import com.core136.bean.partymember.PartyTargetTrain;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyTargetTrainMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyTargetTrainService {

    private PartyTargetTrainMapper partyTargetTrainMapper;

    @Autowired
    public void setPartyTargetTrainMapper(PartyTargetTrainMapper partyTargetTrainMapper) {
        this.partyTargetTrainMapper = partyTargetTrainMapper;
    }

    public int insertPartyTargetTrain(PartyTargetTrain partyTargetTrain) {
        return partyTargetTrainMapper.insert(partyTargetTrain);
    }

    public int deletePartyTargetTrain(PartyTargetTrain partyTargetTrain) {
        return partyTargetTrainMapper.delete(partyTargetTrain);
    }

    public int updatePartyTargetTrain(Example example, PartyTargetTrain partyTargetTrain) {
        return partyTargetTrainMapper.updateByExampleSelective(partyTargetTrain, example);
    }

    public PartyTargetTrain selectOnePartyTargetTrain(PartyTargetTrain partyTargetTrain) {
        return partyTargetTrainMapper.selectOne(partyTargetTrain);
    }

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param partyOrgId
     * @param jobStatus
     * @param search
     * @return List<Map < String, String>>
     * @Title: getTargetTrainList
     * @Description:  获取发展对象培训记录列表
     */
    public List<Map<String, String>> getTargetTrainList(String orgId, String beginTime, String endTime, String partyOrgId, String jobStatus, String search) {
        return partyTargetTrainMapper.getTargetTrainList(orgId, beginTime, endTime, partyOrgId, jobStatus, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getTargetTrainList
     * @Description:  获取发展对象培训记录列表
     */
    public PageInfo<Map<String, String>> getTargetTrainList(PageParam pageParam, String beginTime, String endTime, String partyOrgId, String jobStatus) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getTargetTrainList(pageParam.getOrgId(), beginTime, endTime, partyOrgId, jobStatus, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
