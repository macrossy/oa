package com.core136.service.partymember;

import com.core136.bean.partymember.PartyMemberLose;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyMemberLoseMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyMemberLoseService {
    private PartyMemberLoseMapper partyMemberLoseMapper;

    @Autowired
    public void setPartyMemberLoseMapper(PartyMemberLoseMapper partyMemberLoseMapper) {
        this.partyMemberLoseMapper = partyMemberLoseMapper;
    }

    public int insertPartyMemberLose(PartyMemberLose partyMemberLose) {
        return partyMemberLoseMapper.insert(partyMemberLose);
    }

    public int deletePartyMemberLose(PartyMemberLose partyMemberLose) {
        return partyMemberLoseMapper.delete(partyMemberLose);
    }

    public int updatePartyMemberLose(Example example, PartyMemberLose partyMemberLose) {
        return partyMemberLoseMapper.updateByExampleSelective(partyMemberLose, example);
    }

    public PartyMemberLose selectOnePartyMemberLose(PartyMemberLose partyMemberLose) {
        return partyMemberLoseMapper.selectOne(partyMemberLose);
    }

    public List<Map<String, String>> getMemberLoseList(String orgId, String loseType, String beginTime, String endTime, String search) {
        return partyMemberLoseMapper.getMemberLoseList(orgId, loseType, beginTime, endTime, "%" + search + "%");
    }


    public PageInfo<Map<String, String>> getMemberLoseList(PageParam pageParam, String loseType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMemberLoseList(pageParam.getOrgId(), loseType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
