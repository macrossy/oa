package com.core136.service.partymember;

import com.core136.bean.partymember.PartyAdmRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyAdmRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyAdmRecordService {
    private PartyAdmRecordMapper partyAdmRecordMapper;

    @Autowired
    public void setPartyAdmRecordMapper(PartyAdmRecordMapper partyAdmRecordMapper) {
        this.partyAdmRecordMapper = partyAdmRecordMapper;
    }

    public int insertPartyAdmRecord(PartyAdmRecord partyAdmRecord) {
        return partyAdmRecordMapper.insert(partyAdmRecord);
    }

    public int deletePartyAdmRecord(PartyAdmRecord partyAdmRecord) {
        return partyAdmRecordMapper.delete(partyAdmRecord);
    }

    public int updatePartyAdmRecord(Example example, PartyAdmRecord partyAdmRecord) {
        return partyAdmRecordMapper.updateByExampleSelective(partyAdmRecord, example);
    }


    public PartyAdmRecord selectOnePartyAdmRecord(PartyAdmRecord partyAdmRecord) {
        return partyAdmRecordMapper.selectOne(partyAdmRecord);
    }

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyAdmRecordList
     * @Description:  获取个人行政任职记录列表
     */
    public List<Map<String, String>> getMyAdmRecordList(String orgId, String memberId) {
        return partyAdmRecordMapper.getMyAdmRecordList(orgId, memberId);
    }

    /**
     * @param orgId
     * @param postType
     * @param admPost
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getAdmRecordList
     * @Description:  获取行政任职记录列表
     */
    public List<Map<String, String>> getAdmRecordList(String orgId, String postType, String admPost, String beginTime, String endTime, String search) {
        return partyAdmRecordMapper.getAdmRecordList(orgId, postType, admPost, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param postType
     * @param admPost
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getAdmRecordList
     * @Description:  获取行政任职记录列表
     */
    public PageInfo<Map<String, String>> getAdmRecordList(PageParam pageParam, String postType, String admPost, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAdmRecordList(pageParam.getOrgId(), postType, admPost, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
