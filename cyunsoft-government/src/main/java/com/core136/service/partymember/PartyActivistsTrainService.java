package com.core136.service.partymember;

import com.core136.bean.partymember.PartyActivistsTrain;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyActivistsTrainMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyActivistsTrainService {

    private PartyActivistsTrainMapper partyActivistsTrainMapper;

    @Autowired
    public void setPartyActivistsTrainMapper(PartyActivistsTrainMapper partyActivistsTrainMapper) {
        this.partyActivistsTrainMapper = partyActivistsTrainMapper;
    }

    public int insertPartyActivistsTrain(PartyActivistsTrain partyActivistsTrain) {
        return partyActivistsTrainMapper.insert(partyActivistsTrain);
    }

    public int deletePartyActivistsTrain(PartyActivistsTrain partyActivistsTrain) {
        return partyActivistsTrainMapper.delete(partyActivistsTrain);
    }

    public int updatePartyActivistsTrain(Example example, PartyActivistsTrain partyActivistsTrain) {
        return partyActivistsTrainMapper.updateByExampleSelective(partyActivistsTrain, example);
    }

    public PartyActivistsTrain selectOnePartyActivistsTrain(PartyActivistsTrain partyActivistsTrain) {
        return partyActivistsTrainMapper.selectOne(partyActivistsTrain);
    }

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param partyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getActivistsTrainList
     * @Description:  获取积极份子培训记录
     */
    public List<Map<String, String>> getActivistsTrainList(String orgId, String beginTime, String endTime, String partyOrgId, String jobStatus, String search) {
        return partyActivistsTrainMapper.getActivistsTrainList(orgId, beginTime, endTime, partyOrgId, jobStatus, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getActivistsTrainList
     * @Description:  获取积极份子培训记录
     */
    public PageInfo<Map<String, String>> getActivistsTrainList(PageParam pageParam, String beginTime, String endTime, String partyOrgId, String jobStatus) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getActivistsTrainList(pageParam.getOrgId(), beginTime, endTime, partyOrgId, jobStatus, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
