package com.core136.service.partymember;


import com.core136.bean.partymember.PartyMemberDifficulty;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyMemberDifficultyMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyMemberDifficultyService {

    private PartyMemberDifficultyMapper partyMemberDifficultyMapper;

    @Autowired
    public void setPartyMemberDifficultyMapper(PartyMemberDifficultyMapper partyMemberDifficultyMapper) {
        this.partyMemberDifficultyMapper = partyMemberDifficultyMapper;
    }

    public int insertPartyMemberDifficulty(PartyMemberDifficulty partyMemberDifficulty) {
        return partyMemberDifficultyMapper.insert(partyMemberDifficulty);
    }

    public int deletePartyMemberDifficulty(PartyMemberDifficulty partyMemberDifficulty) {
        return partyMemberDifficultyMapper.delete(partyMemberDifficulty);
    }

    public int updatePartyMemberDifficulty(Example example, PartyMemberDifficulty partyMemberDifficulty) {
        return partyMemberDifficultyMapper.updateByExampleSelective(partyMemberDifficulty, example);
    }

    public PartyMemberDifficulty selectOnePartyMemberDifficulty(PartyMemberDifficulty partyMemberDifficulty) {
        return partyMemberDifficultyMapper.selectOne(partyMemberDifficulty);
    }

    /**
     * @param orgId
     * @param difType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMemberDifficultyList
     * @Description:  获取困难党员列表
     */
    public List<Map<String, String>> getMemberDifficultyList(String orgId, String difType, String beginTime, String endTime, String search) {
        return partyMemberDifficultyMapper.getMemberDifficultyList(orgId, difType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param difType
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMemberDifficultyList
     * @Description:  获取困难党员列表
     */
    public PageInfo<Map<String, String>> getMemberDifficultyList(PageParam pageParam, String difType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMemberDifficultyList(pageParam.getOrgId(), difType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
