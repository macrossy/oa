package com.core136.service.partymember;

import com.core136.bean.partymember.PartyMember;
import com.core136.bean.partymember.PartyMemberFormal;
import com.core136.bean.partymember.PartyMemberJoin;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partymember.PartyMemberFormalMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyMemberFormalService {
    private PartyMemberFormalMapper partyMemberFormalMapper;

    @Autowired
    public void setPartyMemberFormalMapper(PartyMemberFormalMapper partyMemberFormalMapper) {
        this.partyMemberFormalMapper = partyMemberFormalMapper;
    }

    private PartyMemberJoinService partyMemberJoinService;

    @Autowired
    public void setPartyMemberJoinService(PartyMemberJoinService partyMemberJoinService) {
        this.partyMemberJoinService = partyMemberJoinService;
    }

    private PartyMemberService partyMemberService;

    @Autowired
    public void setPartyMemberService(PartyMemberService partyMemberService) {
        this.partyMemberService = partyMemberService;
    }

    public int insertPartyMemberFormal(PartyMemberFormal partyMemberFormal) {
        return partyMemberFormalMapper.insert(partyMemberFormal);
    }

    public int deletePartyMemberFormal(PartyMemberFormal partyMemberFormal) {
        return partyMemberFormalMapper.delete(partyMemberFormal);
    }

    public int updatePartyMemberFormal(Example example, PartyMemberFormal partyMemberFormal) {
        return partyMemberFormalMapper.updateByExampleSelective(partyMemberFormal, example);
    }

    public PartyMemberFormal selectOnePartyMemberFormal(PartyMemberFormal partyMemberFormal) {
        return partyMemberFormalMapper.selectOne(partyMemberFormal);
    }

    /**
     * @param orgId
     * @param status
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getFormalRecordList
     * @Description:  获取党员转正记录
     */
    public List<Map<String, String>> getFormalRecordList(String orgId, String status, String partyOrgId, String beginTime, String endTime, String search) {
        return partyMemberFormalMapper.getFormalRecordList(orgId, status, partyOrgId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param status
     * @param partyOrgId
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getFormalRecordList
     * @Description:  获取党员转正记录
     */
    public PageInfo<Map<String, String>> getFormalRecordList(PageParam pageParam, String status, String partyOrgId, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getFormalRecordList(pageParam.getOrgId(), status, partyOrgId, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param partyMemberFormal
     * @return RetDataBean
     * @Title: setPartyMemberFormal
     * @Description:  预备党员转正
     */
    @Transactional(value = "generalTM")
    public RetDataBean setPartyMemberFormal(PartyMemberFormal partyMemberFormal) {
        if (StringUtils.isBlank(partyMemberFormal.getJoinRecordId())) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        } else {
            if (partyMemberFormal.getStatus().equals("1")) {
                PartyMemberJoin partyMemberJoin = new PartyMemberJoin();
                partyMemberJoin.setJoinTime3(partyMemberFormal.getJoinTime());
                partyMemberJoin.setStatusFlag(4);
                Example example = new Example(PartyMemberJoin.class);
                example.createCriteria().andEqualTo("orgId", partyMemberFormal.getOrgId()).andEqualTo("recordId", partyMemberFormal.getJoinRecordId());
                partyMemberJoinService.updatePartyMemberJoin(example, partyMemberJoin);

                PartyMemberJoin partyMemberJoin1 = new PartyMemberJoin();
                partyMemberJoin1.setRecordId(partyMemberFormal.getJoinRecordId());
                partyMemberJoin1.setOrgId(partyMemberFormal.getOrgId());
                partyMemberJoin1 = partyMemberJoinService.selectOnePartyMemberJoin(partyMemberJoin1);

                if (StringUtils.isNotBlank(partyMemberJoin1.getMemberId())) {
                    PartyMember partyMember = new PartyMember();
                    partyMember.setMemberId(partyMemberJoin1.getMemberId());
                    partyMember.setPartyStatus("1");
                    partyMember.setOrgId(partyMemberJoin1.getOrgId());
                    Example example2 = new Example(PartyMember.class);
                    example2.createCriteria().andEqualTo("orgId", partyMemberJoin1.getOrgId()).andEqualTo("memberId", partyMemberJoin1.getMemberId());
                    partyMemberService.updatePartyMember(example2, partyMember);
                }

                return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertPartyMemberFormal(partyMemberFormal));
            } else {
                PartyMemberJoin partyMemberJoin = new PartyMemberJoin();
                partyMemberJoin.setJoinTime1(partyMemberFormal.getJoinTime());
                partyMemberJoin.setStatusFlag(100);
                Example example = new Example(PartyMemberJoin.class);
                example.createCriteria().andEqualTo("orgId", partyMemberFormal.getOrgId()).andEqualTo("recordId", partyMemberFormal.getJoinRecordId());
                partyMemberJoinService.updatePartyMemberJoin(example, partyMemberJoin);
                return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertPartyMemberFormal(partyMemberFormal));
            }
        }
    }

}
