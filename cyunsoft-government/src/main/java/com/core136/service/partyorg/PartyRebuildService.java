package com.core136.service.partyorg;

import com.core136.bean.partyorg.PartyRebuild;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyorg.PartyRebuildMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyRebuildService {
    private PartyRebuildMapper partyRebuildMapper;

    @Autowired
    public void setPartyRebuildMapper(PartyRebuildMapper partyRebuildMapper) {
        this.partyRebuildMapper = partyRebuildMapper;
    }

    public int insertPartyRebuild(PartyRebuild partyRebuild) {
        return partyRebuildMapper.insert(partyRebuild);
    }

    public int deletePartyRebuild(PartyRebuild partyRebuild) {
        return partyRebuildMapper.delete(partyRebuild);
    }

    public int updatePartyRebuild(Example example, PartyRebuild partyRebuild) {
        return partyRebuildMapper.updateByExampleSelective(partyRebuild, example);
    }

    public PartyRebuild selectOnePartyRebuild(PartyRebuild partyRebuild) {
        return partyRebuildMapper.selectOne(partyRebuild);
    }

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyRebuildList
     * @Description:  获取党组织改建记录列表
     */
    public List<Map<String, String>> getPartyRebuildList(String orgId, String beginTime, String endTime, String search) {
        return partyRebuildMapper.getPartyRebuildList(orgId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyRebuildList
     * @Description:  获取党组织改建记录列表
     */
    public PageInfo<Map<String, String>> getPartyRebuildList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyRebuildList(pageParam.getOrgId(), beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
