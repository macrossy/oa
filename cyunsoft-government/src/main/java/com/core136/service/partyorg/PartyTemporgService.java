package com.core136.service.partyorg;

import com.core136.bean.partyorg.PartyTemporg;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyorg.PartyTemporgMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyTemporgService {
    private PartyTemporgMapper partyTemporgMapper;

    @Autowired
    public void setPartyTemporgMapper(PartyTemporgMapper partyTemporgMapper) {
        this.partyTemporgMapper = partyTemporgMapper;
    }

    public int insertPartyTemporg(PartyTemporg partyTemporg) {
        return partyTemporgMapper.insert(partyTemporg);
    }

    public int deletePartyTemporg(PartyTemporg partyTemporg) {
        return partyTemporgMapper.delete(partyTemporg);
    }

    public int updatePartyTemporg(Example example, PartyTemporg partyTemporg) {
        return partyTemporgMapper.updateByExampleSelective(partyTemporg, example);
    }

    public PartyTemporg selectOnePartyTemporg(PartyTemporg partyTemporg) {
        return partyTemporgMapper.selectOne(partyTemporg);
    }

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyTemporgList
     * @Description:  获取临时党组织列表
     */
    public List<Map<String, String>> getPartyTemporgList(String orgId, String beginTime, String endTime, String search) {
        return partyTemporgMapper.getPartyTemporgList(orgId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyTemporgList
     * @Description:  获取临时党组织列表
     */
    public PageInfo<Map<String, String>> getPartyTemporgList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyTemporgList(pageParam.getOrgId(), beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
