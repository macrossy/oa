package com.core136.service.partyorg;

import com.core136.bean.partyorg.PartyLesMeet;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyorg.PartyLesMeetMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyLesMeetService {
    private PartyLesMeetMapper partyLesMeetMapper;

    @Autowired
    public void setPartyLesMeetMapper(PartyLesMeetMapper partyLesMeetMapper) {
        this.partyLesMeetMapper = partyLesMeetMapper;
    }

    public int insertPartyLesMeet(PartyLesMeet partyLesMeet) {
        return partyLesMeetMapper.insert(partyLesMeet);
    }

    public int deletePartyLesMeet(PartyLesMeet partyLesMeet) {
        return partyLesMeetMapper.delete(partyLesMeet);
    }

    public int updatePartyLesMeet(Example example, PartyLesMeet partyLesMeet) {
        return partyLesMeetMapper.updateByExampleSelective(partyLesMeet, example);
    }

    public PartyLesMeet selectOnePartyLesMeet(PartyLesMeet partyLesMeet) {
        return partyLesMeetMapper.selectOne(partyLesMeet);
    }

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param meetType
     * @param meetModel
     * @param search
     * @return List<Map < String, String>>
     * @Title: getLesMeetList
     * @Description:  获取三会一课记录列表
     */
    public List<Map<String, String>> getLesMeetList(String orgId, String beginTime, String endTime, String meetType, String meetModel, String search) {
        return partyLesMeetMapper.getLesMeetList(orgId, beginTime, endTime, meetType, meetModel, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param meetType
     * @param meetModel
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getLesMeetList
     * @Description:  获取三会一课记录列表
     */
    public PageInfo<Map<String, String>> getLesMeetList(PageParam pageParam, String beginTime, String endTime, String meetType, String meetModel) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getLesMeetList(pageParam.getOrgId(), beginTime, endTime, meetType, meetModel, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
