package com.core136.service.partyorg;

import com.core136.bean.partyorg.PartyLifeMeet;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyorg.PartyLifeMeetMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyLifeMeetService {

    private PartyLifeMeetMapper partyLifeMeetMapper;

    @Autowired
    public void setPartyLifeMeetMapper(PartyLifeMeetMapper partyLifeMeetMapper) {
        this.partyLifeMeetMapper = partyLifeMeetMapper;
    }

    public int insertPartyLifeMeet(PartyLifeMeet partyLifeMeet) {
        return partyLifeMeetMapper.insert(partyLifeMeet);
    }

    public int deletePartyLifeMeet(PartyLifeMeet partyLifeMeet) {
        return partyLifeMeetMapper.delete(partyLifeMeet);
    }

    public int updatePartyLifeMeet(Example example, PartyLifeMeet partyLifeMeet) {
        return partyLifeMeetMapper.updateByExampleSelective(partyLifeMeet, example);
    }

    public PartyLifeMeet selectOnePartyLifeMeet(PartyLifeMeet partyLifeMeet) {
        return partyLifeMeetMapper.selectOne(partyLifeMeet);
    }

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param meetType
     * @param partyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getLifeMeetList
     * @Description:  获取组织生活会记录列表
     */
    public List<Map<String, String>> getLifeMeetList(String orgId, String beginTime, String endTime, String meetType, String partyOrgId, String search) {
        return partyLifeMeetMapper.getLifeMeetList(orgId, beginTime, endTime, meetType, partyOrgId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param meetType
     * @param partyOrgId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getLifeMeetList
     * @Description:  获取组织生活会记录列表
     */
    public PageInfo<Map<String, String>> getLifeMeetList(PageParam pageParam, String beginTime, String endTime, String meetType, String partyOrgId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getLifeMeetList(pageParam.getOrgId(), beginTime, endTime, meetType, partyOrgId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
