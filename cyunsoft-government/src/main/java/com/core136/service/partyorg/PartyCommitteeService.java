package com.core136.service.partyorg;

import com.core136.bean.partyorg.PartyCommittee;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyorg.PartyCommitteeMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyCommitteeService {
    private PartyCommitteeMapper partyCommitteeMapper;

    @Autowired
    public void setPartyCommitteeMapper(PartyCommitteeMapper partyCommitteeMapper) {
        this.partyCommitteeMapper = partyCommitteeMapper;
    }

    public int insertPartyCommittee(PartyCommittee partyCommittee) {
        return partyCommitteeMapper.insert(partyCommittee);
    }

    public int deletePartyCommittee(PartyCommittee partyCommittee) {
        return partyCommitteeMapper.delete(partyCommittee);
    }

    public int updatePartyCommittee(Example example, PartyCommittee partyCommittee) {
        return partyCommitteeMapper.updateByExampleSelective(partyCommittee, example);
    }

    public PartyCommittee selectOnePartyCommittee(PartyCommittee partyCommittee) {
        return partyCommitteeMapper.selectOne(partyCommittee);
    }

    /**
     * @param orgId
     * @param electionRecordId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getCommitteeList
     * @Description:  获取当前届期委员列表
     */
    public List<Map<String, String>> getCommitteeList(String orgId, String electionRecordId, String search) {
        return partyCommitteeMapper.getCommitteeList(orgId, electionRecordId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param electionRecordId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getCommitteeList
     * @Description:  获取当前届期委员列表
     */
    public PageInfo<Map<String, String>> getCommitteeList(PageParam pageParam, String electionRecordId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getCommitteeList(pageParam.getOrgId(), electionRecordId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param partyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getNowCommitteeList
     * @Description:  获取当前届次委员列表
     */
    public List<Map<String, String>> getNowCommitteeList(String orgId, String partyOrgId, String search) {
        return partyCommitteeMapper.getNowCommitteeList(orgId, partyOrgId, SysTools.getTime("yyyy-MM-dd"), "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param partyOrgId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getNowCommitteeList
     * @Description:  获取当前届次委员列表
     */
    public PageInfo<Map<String, String>> getNowCommitteeList(PageParam pageParam, String partyOrgId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getNowCommitteeList(pageParam.getOrgId(), partyOrgId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
