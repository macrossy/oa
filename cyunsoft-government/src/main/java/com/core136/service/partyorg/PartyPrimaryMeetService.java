package com.core136.service.partyorg;

import com.core136.bean.partyorg.PartyPrimaryMeet;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyorg.PartyPrimaryMeetMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyPrimaryMeetService {
    private PartyPrimaryMeetMapper partyPrimaryMeetMapper;

    @Autowired
    public void setPartyPrimaryMeetMapper(PartyPrimaryMeetMapper partyPrimaryMeetMapper) {
        this.partyPrimaryMeetMapper = partyPrimaryMeetMapper;
    }

    public int insertPartyPrimaryMeet(PartyPrimaryMeet partyPrimaryMeet) {
        return partyPrimaryMeetMapper.insert(partyPrimaryMeet);
    }

    public int deletePartyPrimaryMeet(PartyPrimaryMeet partyPrimaryMeet) {
        return partyPrimaryMeetMapper.delete(partyPrimaryMeet);
    }

    public int updatePartyPrimaryMeet(Example example, PartyPrimaryMeet partyPrimaryMeet) {
        return partyPrimaryMeetMapper.updateByExampleSelective(partyPrimaryMeet, example);
    }

    public PartyPrimaryMeet selectOnePartyPrimaryMeet(PartyPrimaryMeet partyPrimaryMeet) {
        return partyPrimaryMeetMapper.selectOne(partyPrimaryMeet);
    }

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param meetType
     * @param partyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPrimaryMeetList
     * @Description:  获取主题党日活动列表
     */
    public List<Map<String, String>> getPrimaryMeetList(String orgId, String beginTime, String endTime, String meetType, String partyOrgId, String search) {
        return partyPrimaryMeetMapper.getPrimaryMeetList(orgId, beginTime, endTime, meetType, partyOrgId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @param meetType
     * @param partyOrgId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPrimaryMeetList
     * @Description:  获取主题党日活动列表
     */
    public PageInfo<Map<String, String>> getPrimaryMeetList(PageParam pageParam, String beginTime, String endTime, String meetType, String partyOrgId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPrimaryMeetList(pageParam.getOrgId(), beginTime, endTime, meetType, partyOrgId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
