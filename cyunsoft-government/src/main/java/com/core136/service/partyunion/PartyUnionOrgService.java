package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionOrg;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyunion.PartyUnionOrgMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class PartyUnionOrgService {
    private PartyUnionOrgMapper partyUnionOrgMapper;

    @Autowired
    public void setPartyUnionOrgMapper(PartyUnionOrgMapper partyUnionOrgMapper) {
        this.partyUnionOrgMapper = partyUnionOrgMapper;
    }

    public int insertPartyUnionOrg(PartyUnionOrg partyUnionOrg) {
        return partyUnionOrgMapper.insert(partyUnionOrg);
    }

    public int deletePartyUnionOrg(PartyUnionOrg partyUnionOrg) {
        return partyUnionOrgMapper.delete(partyUnionOrg);
    }

    public int updatePartyUnionOrg(Example example, PartyUnionOrg partyUnionOrg) {
        return partyUnionOrgMapper.updateByExampleSelective(partyUnionOrg, example);
    }

    public PartyUnionOrg selectOnePartyUnionOrg(PartyUnionOrg partyUnionOrg) {
        return partyUnionOrgMapper.selectOne(partyUnionOrg);
    }

    public PartyUnionOrg getPartyUnionOrg(String orgId) {
        String nowTime = SysTools.getTime("yyyy-MM-dd");
        return partyUnionOrgMapper.getPartyUnionOrg(orgId, nowTime);
    }

    public List<PartyUnionOrg> getAllPartyUnionOrgList(String orgId) {
        PartyUnionOrg partyUnionOrg = new PartyUnionOrg();
        partyUnionOrg.setOrgId(orgId);
        return partyUnionOrgMapper.select(partyUnionOrg);
    }

    public PageInfo<PartyUnionOrg> getAllPartyUnionOrgList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<PartyUnionOrg> datalist = getAllPartyUnionOrgList(pageParam.getOrgId());
        PageInfo<PartyUnionOrg> pageInfo = new PageInfo<PartyUnionOrg>(datalist);
        return pageInfo;
    }

}
