package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionTip;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyunion.PartyUnionTipMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnionTipService {

    private PartyUnionTipMapper partyUnionTipMapper;

    @Autowired
    public void setPartyUnionTipMapper(PartyUnionTipMapper partyUnionTipMapper) {
        this.partyUnionTipMapper = partyUnionTipMapper;
    }

    public int insertPartyUnionTip(PartyUnionTip partyUnionTip) {
        return partyUnionTipMapper.insert(partyUnionTip);
    }

    public int deletePartyUnionTip(PartyUnionTip partyUnionTip) {
        return partyUnionTipMapper.delete(partyUnionTip);
    }

    public int updatePartyUnionTip(Example example, PartyUnionTip partyUnionTip) {
        return partyUnionTipMapper.updateByExampleSelective(partyUnionTip, example);
    }

    public PartyUnionTip selectOnePartyUnionTip(PartyUnionTip partyUnionTip) {
        return partyUnionTipMapper.selectOne(partyUnionTip);
    }

    /**
     * 获取举报记录列表
     *
     * @param orgId
     * @param tipType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionTipList(String orgId, String tipType, String beginTime, String endTime, String search) {
        return partyUnionTipMapper.getPartyUnionTipList(orgId, tipType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取举报记录列表
     *
     * @param pageParam
     * @param tipType
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getPartyUnionTipList(PageParam pageParam, String tipType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyUnionTipList(pageParam.getOrgId(), tipType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
