package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionActivity;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyunion.PartyUnionActivityMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnionActivityService {
    private PartyUnionActivityMapper partyUnionActivityMapper;

    @Autowired
    public void setPartyUnionActivityMapper(PartyUnionActivityMapper partyUnionActivityMapper) {
        this.partyUnionActivityMapper = partyUnionActivityMapper;
    }

    public int insertPartyUnionActivity(PartyUnionActivity partyUnionActivity) {
        return partyUnionActivityMapper.insert(partyUnionActivity);
    }

    public int deletePartyUnionActivity(PartyUnionActivity partyUnionActivity) {
        return partyUnionActivityMapper.delete(partyUnionActivity);
    }

    public int updatePartyUnionActivity(Example example, PartyUnionActivity partyUnionActivity) {
        return partyUnionActivityMapper.updateByExampleSelective(partyUnionActivity, example);
    }

    public PartyUnionActivity selectOnePartyUnionActivity(PartyUnionActivity partyUnionActivity) {
        return partyUnionActivityMapper.selectOne(partyUnionActivity);
    }

    /**
     * 获取工会活动列表
     *
     * @param orgId
     * @param activityType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionActivityList(String orgId, String activityType, String beginTime, String endTime, String search) {
        return partyUnionActivityMapper.getPartyUnionActivityList(orgId, activityType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取工会活动列表
     *
     * @param pageParam
     * @param activityType
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getPartyUnionActivityList(PageParam pageParam, String activityType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyUnionActivityList(pageParam.getOrgId(), activityType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
