package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionTraining;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyunion.PartyUnionTrainingMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnionTrainingService {

    private PartyUnionTrainingMapper partyUnionTrainingMapper;

    @Autowired
    public void setPartyUnionTrainingMapper(PartyUnionTrainingMapper partyUnionTrainingMapper) {
        this.partyUnionTrainingMapper = partyUnionTrainingMapper;
    }

    public int insertPartyUnionTraining(PartyUnionTraining partyUnionTraining) {
        return partyUnionTrainingMapper.insert(partyUnionTraining);
    }

    public int deletePartyUnionTraining(PartyUnionTraining partyUnionTraining) {
        return partyUnionTrainingMapper.delete(partyUnionTraining);
    }

    public int updatePartyUnionTraining(Example example, PartyUnionTraining partyUnionTraining) {
        return partyUnionTrainingMapper.updateByExampleSelective(partyUnionTraining, example);
    }

    public PartyUnionTraining selectOnePartyUnionTraining(PartyUnionTraining partyUnionTraining) {
        return partyUnionTrainingMapper.selectOne(partyUnionTraining);
    }

    /**
     * 获取就业培训记录列表
     *
     * @param orgId
     * @param trainType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionTrainingList(String orgId, String trainType, String beginTime, String endTime, String search) {
        return partyUnionTrainingMapper.getPartyUnionTrainingList(orgId, trainType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取就业培训记录列表
     *
     * @param pageParam
     * @param trainType
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getPartyUnionTrainingList(PageParam pageParam, String trainType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyUnionTrainingList(pageParam.getOrgId(), trainType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
