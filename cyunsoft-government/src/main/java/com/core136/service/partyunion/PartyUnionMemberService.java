package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionMember;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyunion.PartyUnionMemberMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnionMemberService {

    private PartyUnionMemberMapper partyUnionMemberMapper;

    @Autowired
    public void setPartyUnionMemberMapper(PartyUnionMemberMapper partyUnionMemberMapper) {
        this.partyUnionMemberMapper = partyUnionMemberMapper;
    }

    public int insertPartyUnionMember(PartyUnionMember partyUnionMember) {
        return partyUnionMemberMapper.insert(partyUnionMember);
    }

    public int deletePartyUnionMember(PartyUnionMember partyUnionMember) {
        return partyUnionMemberMapper.delete(partyUnionMember);
    }

    public int updatePartyUnionMember(Example example, PartyUnionMember partyUnionMember) {
        return partyUnionMemberMapper.updateByExampleSelective(partyUnionMember, example);
    }

    public PartyUnionMember selectOnePartyUnionMember(PartyUnionMember partyUnionMember) {
        return partyUnionMemberMapper.selectOne(partyUnionMember);
    }

    /**
     * 按基层获取成员列表
     *
     * @param orgId
     * @param deptId
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionMemberListByDeptId(String orgId, String deptId, String search) {
        return partyUnionMemberMapper.getPartyUnionMemberListByDeptId(orgId, deptId, "%" + search + "%");
    }

    /**
     * 按基层获取成员列表
     *
     * @param pageParam
     * @param deptId
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getPartyUnionMemberListByDeptId(PageParam pageParam, String deptId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyUnionMemberListByDeptId(pageParam.getOrgId(), deptId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
