package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionWoman;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyunion.PartyUnionWomanMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnionWomanService {
    private PartyUnionWomanMapper partyUnionWomanMapper;

    @Autowired
    public void setPartyUnionWomanMapper(PartyUnionWomanMapper partyUnionWomanMapper) {
        this.partyUnionWomanMapper = partyUnionWomanMapper;
    }

    public int insertPartyUnionWoman(PartyUnionWoman partyUnionWoman) {
        return partyUnionWomanMapper.insert(partyUnionWoman);
    }

    public int deletePartyUnionWoman(PartyUnionWoman partyUnionWoman) {
        return partyUnionWomanMapper.delete(partyUnionWoman);
    }

    public int updatePartyUnionWoman(Example example, PartyUnionWoman partyUnionWoman) {
        return partyUnionWomanMapper.updateByExampleSelective(partyUnionWoman, example);
    }

    public PartyUnionWoman selectOnePartyUnionWoman(PartyUnionWoman partyUnionWoman) {
        return partyUnionWomanMapper.selectOne(partyUnionWoman);
    }

    /**
     * 获取三八红旗手列表
     *
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionWomanList(String orgId, String beginTime, String endTime, String search) {
        return partyUnionWomanMapper.getPartyUnionWomanList(orgId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取三八红旗手列表
     *
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getPartyUnionWomanList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyUnionWomanList(pageParam.getOrgId(), beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
