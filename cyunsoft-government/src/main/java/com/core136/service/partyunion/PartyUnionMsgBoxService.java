package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionMsgBox;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyunion.PartyUnionMsgBoxMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Service
public class PartyUnionMsgBoxService implements Serializable {
    private PartyUnionMsgBoxMapper partyUnionMsgBoxMapper;

    @Autowired
    public void setPartyunionMsgBoxMapper(PartyUnionMsgBoxMapper partyUnionMsgBoxMapper) {
        this.partyUnionMsgBoxMapper = partyUnionMsgBoxMapper;
    }

    public int insertPartyUnionMsgBox(PartyUnionMsgBox partyUnionMsgBox) {
        return partyUnionMsgBoxMapper.insert(partyUnionMsgBox);
    }

    public int deletePartyUnionMsgBox(PartyUnionMsgBox partyUnionMsgBox) {
        return partyUnionMsgBoxMapper.delete(partyUnionMsgBox);
    }

    public int updatePartyUnionMsgBox(Example example, PartyUnionMsgBox partyUnionMsgBox) {
        return partyUnionMsgBoxMapper.updateByExampleSelective(partyUnionMsgBox, example);
    }

    public PartyUnionMsgBox selectOnePartyUnionMsgBox(PartyUnionMsgBox partyUnionMsgBox) {
        return partyUnionMsgBoxMapper.selectOne(partyUnionMsgBox);
    }


    public List<Map<String, String>> getMyMsgBoxList(String orgId, String accountId, String search) {
        return partyUnionMsgBoxMapper.getMyMsgBoxList(orgId, accountId, "%" + search + "%");
    }

    public PageInfo<Map<String, String>> getMyMsgBoxList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyMsgBoxList(pageParam.getOrgId(), pageParam.getAccountId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    public List<Map<String, String>> getLeaderMsgBoxList(String orgId, String search) {
        return partyUnionMsgBoxMapper.getLeaderMsgBoxList(orgId, "%" + search + "%");
    }

    public PageInfo<Map<String, String>> getLeaderMsgBoxList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getLeaderMsgBoxList(pageParam.getOrgId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
