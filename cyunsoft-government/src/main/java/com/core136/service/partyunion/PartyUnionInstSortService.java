package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionInstSort;
import com.core136.mapper.partyunion.PartyUnionInstSortMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnionInstSortService {
    private PartyUnionInstSortMapper partyUnionInstSortMapper;

    @Autowired
    public void setPartyUnionInstSortMapper(PartyUnionInstSortMapper partyUnionInstSortMapper) {
        this.partyUnionInstSortMapper = partyUnionInstSortMapper;
    }

    public int insertPartyUnionInstSort(PartyUnionInstSort partyUnionInstSort) {
        return partyUnionInstSortMapper.insert(partyUnionInstSort);
    }

    public int deletePartyUnionInstSort(PartyUnionInstSort partyUnionInstSort) {
        return partyUnionInstSortMapper.delete(partyUnionInstSort);
    }

    public int updatePartyUnionInstSort(Example example, PartyUnionInstSort partyUnionInstSort) {
        return partyUnionInstSortMapper.updateByExampleSelective(partyUnionInstSort, example);
    }

    public PartyUnionInstSort selectOnePartyUnionInstSort(PartyUnionInstSort partyUnionInstSort) {
        return partyUnionInstSortMapper.selectOne(partyUnionInstSort);
    }

    /**
     * 获取法律法规分类
     *
     * @param orgId
     * @param levelId
     * @return
     */
    public List<Map<String, String>> getPartyUnionInstSortTree(String orgId, String levelId) {
        return partyUnionInstSortMapper.getPartyUnionInstSortTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     *
     * @param orgId
     * @param sortId
     * @return
     */
    public int isExistChild(String orgId, String sortId) {
        return partyUnionInstSortMapper.isExistChild(orgId, sortId);
    }
}
