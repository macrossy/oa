package com.core136.service.partyunion;

import com.core136.bean.partyunion.PartyUnionHelp;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyunion.PartyUnionHelpMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyUnionHelpService {
    private PartyUnionHelpMapper partyUnionHelpMapper;

    @Autowired
    public void setPartyUnionHelpMapper(PartyUnionHelpMapper partyUnionHelpMapper) {
        this.partyUnionHelpMapper = partyUnionHelpMapper;
    }

    public int insertPartyUnionHelp(PartyUnionHelp partyUnionHelp) {
        return partyUnionHelpMapper.insert(partyUnionHelp);
    }

    public int deletePartyUnionHelp(PartyUnionHelp partyUnionHelp) {
        return partyUnionHelpMapper.delete(partyUnionHelp);
    }

    public int updatePartyUnionHelp(Example example, PartyUnionHelp partyUnionHelp) {
        return partyUnionHelpMapper.updateByExampleSelective(partyUnionHelp, example);
    }

    public PartyUnionHelp selectOnePartyUnionHelp(PartyUnionHelp partyUnionHelp) {
        return partyUnionHelpMapper.selectOne(partyUnionHelp);
    }

    /**
     * 获取困难帮扶列表
     *
     * @param orgId
     * @param helpType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionHelpList(String orgId, String helpType, String beginTime, String endTime, String search) {
        return partyUnionHelpMapper.getPartyUnionHelpList(orgId, helpType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取困难帮扶列表
     *
     * @param pageParam
     * @param helpType
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getPartyUnionHelpList(PageParam pageParam, String helpType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyUnionHelpList(pageParam.getOrgId(), helpType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
