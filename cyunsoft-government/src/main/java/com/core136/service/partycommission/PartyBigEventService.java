package com.core136.service.partycommission;

import com.core136.bean.partycommission.PartyBigEvent;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partycommission.PartyBigEventMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyBigEventService {
    private PartyBigEventMapper partyBigEventMapper;

    @Autowired
    public void setPartyBigEventMapper(PartyBigEventMapper partyBigEventMapper) {
        this.partyBigEventMapper = partyBigEventMapper;
    }

    public int insertPartyBigEvent(PartyBigEvent partyBigEvent) {
        return partyBigEventMapper.insert(partyBigEvent);
    }

    public int deletePartyBigEvent(PartyBigEvent partyBigEvent) {
        return partyBigEventMapper.delete(partyBigEvent);
    }

    public int updatePartyBigEvent(Example example, PartyBigEvent partyBigEvent) {
        return partyBigEventMapper.updateByExampleSelective(partyBigEvent, example);
    }

    public PartyBigEvent selectOnePartyBigEvent(PartyBigEvent partyBigEvent) {
        return partyBigEventMapper.selectOne(partyBigEvent);
    }

    /**
     * @param orgId
     * @param eventType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyBigEventList
     * @Description:  获取重大事件记录列表
     */
    public List<Map<String, String>> getPartyBigEventList(String orgId, String eventType, String beginTime, String endTime, String search) {
        return partyBigEventMapper.getPartyBigEventList(orgId, eventType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param eventType
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyBigEventList
     * @Description:  获取重大事件记录列表
     */
    public PageInfo<Map<String, String>> getPartyBigEventList(PageParam pageParam, String eventType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyBigEventList(pageParam.getOrgId(), eventType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
