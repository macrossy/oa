package com.core136.service.partycommission;

import com.core136.bean.partycommission.PartyCommission;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partycommission.PartyCommissionMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class PartyCommissionService {
    private PartyCommissionMapper partyCommissionMapper;

    @Autowired
    public void setPartyCommissionMapper(PartyCommissionMapper partyCommissionMapper) {
        this.partyCommissionMapper = partyCommissionMapper;
    }

    public int insertPartyCommission(PartyCommission partyCommission) {
        return partyCommissionMapper.insert(partyCommission);
    }

    public int deletePartyCommission(PartyCommission partyCommission) {
        return partyCommissionMapper.delete(partyCommission);
    }

    public int updatePartyCommission(Example example, PartyCommission partyCommission) {
        return partyCommissionMapper.updateByExampleSelective(partyCommission, example);
    }

    public PartyCommission selectOnePartyCommission(PartyCommission partyCommission) {
        return partyCommissionMapper.selectOne(partyCommission);
    }

    /**
     * @param orgId
     * @return List<PartyCommission>
     * @Title: getAllCommissionRecordList
     * @Description:  获取历史届次信息
     */
    public List<PartyCommission> getAllCommissionRecordList(String orgId) {
        PartyCommission partyCommission = new PartyCommission();
        partyCommission.setOrgId(orgId);
        return partyCommissionMapper.select(partyCommission);
    }

    /**
     * @param pageParam
     * @return PageInfo<PartyCommission>
     * @throws Exception
     * @Title: getAllCommissionRecordList
     * @Description:  获取历史届次信息
     */
    public PageInfo<PartyCommission> getAllCommissionRecordList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<PartyCommission> datalist = getAllCommissionRecordList(pageParam.getOrgId());
        PageInfo<PartyCommission> pageInfo = new PageInfo<PartyCommission>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @return PartyCommission
     * @Title: getPartyCommission
     * @Description:  获取当前届次信息
     */
    public PartyCommission getPartyCommission(String orgId) {
        String nowTime = SysTools.getTime("yyyy-MM-dd");
        return partyCommissionMapper.getPartyCommission(orgId, nowTime);
    }

}
