package com.core136.service.partyrelation;

import com.core136.bean.partymember.PartyMember;
import com.core136.bean.partyrelation.PartyRelOut;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyrelation.PartyRelOutMapper;
import com.core136.service.partymember.PartyMemberService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyRelOutService {
    private PartyRelOutMapper partyRelOutMapper;

    @Autowired
    public void setPartyRelOutMapper(PartyRelOutMapper partyRelOutMapper) {
        this.partyRelOutMapper = partyRelOutMapper;
    }

    private PartyMemberService partyMemberService;

    @Autowired
    public void setPartyMemberService(PartyMemberService partyMemberService) {
        this.partyMemberService = partyMemberService;
    }

    public int insertPartyRelOut(PartyRelOut partyRelOut) {
        return partyRelOutMapper.insert(partyRelOut);
    }

    public int deletePartyRelOut(PartyRelOut partyRelOut) {
        return partyRelOutMapper.delete(partyRelOut);
    }

    public int updatePartyRelOut(Example example, PartyRelOut partyRelOut) {
        return partyRelOutMapper.updateByExampleSelective(partyRelOut, example);
    }

    public PartyRelOut selectOnePartyRelOut(PartyRelOut partyRelOut) {
        return partyRelOutMapper.selectOne(partyRelOut);
    }

    /**
     * @param partyRelOut
     * @return RetDataBean
     * @Title: setRelMemberToOutOtherOrg
     * @Description:  党员党组织关系转出
     */
    @Transactional(value = "generalTM")
    public RetDataBean setRelMemberToOutOtherOrg(PartyRelOut partyRelOut) {
        try {
            insertPartyRelOut(partyRelOut);
            if (StringUtils.isNotBlank(partyRelOut.getOutPartyOrgId())) {
                PartyMember partyMember = new PartyMember();
                partyMember.setPartyOrgId(partyRelOut.getOutPartyOrgId());
                Example example = new Example(PartyMember.class);
                example.createCriteria().andEqualTo("orgId", partyRelOut.getOrgId()).andEqualTo("memberId", partyRelOut.getMemberId()).andEqualTo("partyOrgId", partyRelOut.getOldPartyOrgId());
                partyMemberService.updatePartyMember(example, partyMember);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param orgId
     * @param isInSys
     * @param outType
     * @param beginTime
     * @param endTime
     * @param oldPartyOrgId
     * @param outPartyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getRelOutList
     * @Description:  获取党员党组织关系转出记录
     */
    public List<Map<String, String>> getRelOutList(String orgId, String isInSys, String outType, String beginTime, String endTime, String oldPartyOrgId, String outPartyOrgId, String search) {
        return partyRelOutMapper.getRelOutList(orgId, isInSys, outType, beginTime, endTime, oldPartyOrgId, outPartyOrgId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param isInSys
     * @param outType
     * @param beginTime
     * @param endTime
     * @param oldPartyOrgId
     * @param outPartyOrgId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getRelOutList
     * @Description:  获取党员党组织关系转出记录
     */
    public PageInfo<Map<String, String>> getRelOutList(PageParam pageParam, String isInSys, String outType, String beginTime, String endTime, String oldPartyOrgId, String outPartyOrgId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getRelOutList(pageParam.getOrgId(), isInSys, outType, beginTime, endTime, oldPartyOrgId, outPartyOrgId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
