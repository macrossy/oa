package com.core136.service.partyrelation;

import com.core136.bean.partymember.PartyMember;
import com.core136.bean.partyrelation.PartyRelBetween;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyrelation.PartyRelBetweenMapper;
import com.core136.service.partymember.PartyMemberService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyRelBetweenService {
    private PartyRelBetweenMapper partyRelBetweenMapper;

    @Autowired
    public void setPartyRelBetweenMapper(PartyRelBetweenMapper partyRelBetweenMapper) {
        this.partyRelBetweenMapper = partyRelBetweenMapper;
    }

    private PartyMemberService partyMemberService;

    @Autowired
    public void setPartyMemberService(PartyMemberService partyMemberService) {
        this.partyMemberService = partyMemberService;
    }

    public int insertPartyRelBetween(PartyRelBetween partyRelBetween) {
        return partyRelBetweenMapper.insert(partyRelBetween);
    }

    public int deletePartyRelBetween(PartyRelBetween partyRelBetween) {
        return partyRelBetweenMapper.delete(partyRelBetween);
    }

    public int updatePartyRelBetween(Example example, PartyRelBetween partyRelBetween) {
        return partyRelBetweenMapper.updateByExampleSelective(partyRelBetween, example);
    }

    public PartyRelBetween selectOnePartyRelBetween(PartyRelBetween partyRelBetween) {
        return partyRelBetweenMapper.selectOne(partyRelBetween);
    }

    /**
     * @param partyRelBetween
     * @return RetDataBean
     * @Title: setRelMemberToOtherOrg
     * @Description:  党支部间党员调转
     */
    @Transactional(value = "generalTM")
    public RetDataBean setRelMemberToOtherOrg(PartyRelBetween partyRelBetween) {
        try {
            insertPartyRelBetween(partyRelBetween);
            PartyMember partyMember = new PartyMember();
            partyMember.setPartyOrgId(partyRelBetween.getRelPartyOrgId());
            Example example = new Example(PartyMember.class);
            example.createCriteria().andEqualTo("orgId", partyRelBetween.getOrgId()).andEqualTo("memberId", partyRelBetween.getMemberId()).andEqualTo("partyOrgId", partyRelBetween.getOldPartyOrgId());
            partyMemberService.updatePartyMember(example, partyMember);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param orgId
     * @param relType
     * @param beginTime
     * @param endTime
     * @param oldPartyOrgId
     * @param relPartyOrgId
     * @return List<Map < String, String>>
     * @Title: getRelBetweenList
     * @Description:  获取支部间调转记录列表
     */
    public List<Map<String, String>> getRelBetweenList(String orgId, String relType, String beginTime, String endTime, String oldPartyOrgId, String relPartyOrgId) {
        return partyRelBetweenMapper.getRelBetweenList(orgId, relType, beginTime, endTime, oldPartyOrgId, relPartyOrgId);
    }

    /**
     * @param pageParam
     * @param relType
     * @param beginTime
     * @param endTime
     * @param oldPartyOrgId
     * @param relPartyOrgId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getRelBetweenList
     * @Description:  获取支部间调转记录列表
     */
    public PageInfo<Map<String, String>> getRelBetweenList(PageParam pageParam, String relType, String beginTime, String endTime, String oldPartyOrgId, String relPartyOrgId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getRelBetweenList(pageParam.getOrgId(), relType, beginTime, endTime, oldPartyOrgId, relPartyOrgId);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
