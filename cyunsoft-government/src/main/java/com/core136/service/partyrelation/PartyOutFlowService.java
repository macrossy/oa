package com.core136.service.partyrelation;

import com.core136.bean.partymember.PartyMember;
import com.core136.bean.partyrelation.PartyOutFlow;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyrelation.PartyOutFlowMapper;
import com.core136.service.partymember.PartyMemberService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyOutFlowService {
    private PartyOutFlowMapper partyOutFlowMapper;

    @Autowired
    public void setPartyOutFlowMapper(PartyOutFlowMapper partyOutFlowMapper) {
        this.partyOutFlowMapper = partyOutFlowMapper;
    }

    private PartyMemberService partyMemberService;

    @Autowired
    public void setPartyMemberService(PartyMemberService partyMemberService) {
        this.partyMemberService = partyMemberService;
    }

    public int insertPartyOutFlow(PartyOutFlow partyOutFlow) {
        return partyOutFlowMapper.insert(partyOutFlow);
    }

    public int deletePartyOutFlow(PartyOutFlow partyOutFlow) {
        return partyOutFlowMapper.delete(partyOutFlow);
    }

    public int updatePartyOutFlow(Example example, PartyOutFlow partyOutFlow) {
        return partyOutFlowMapper.updateByExampleSelective(partyOutFlow, example);
    }

    public PartyOutFlow selectOnePartyOutFlow(PartyOutFlow partyOutFlow) {
        return partyOutFlowMapper.selectOne(partyOutFlow);
    }

    /**
     * @param partyOutFlow
     * @return RetDataBean
     * @Title: setPartyOutFlow
     * @Description:  添加党员流出记录
     */
    @Transactional(value = "generalTM")
    public RetDataBean setPartyOutFlow(PartyOutFlow partyOutFlow) {
        try {
            if (StringUtils.isNotBlank(partyOutFlow.getMemberId())) {
                PartyMember partyMember = new PartyMember();
                partyMember.setDelFlag("1");
                Example example = new Example(PartyMember.class);
                example.createCriteria().andEqualTo("orgId", partyOutFlow.getOrgId()).andEqualTo("memberId", partyOutFlow.getMemberId());
                partyMemberService.updatePartyMember(example, partyMember);
                return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertPartyOutFlow(partyOutFlow));
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyOutFlowList
     * @Description:  党员流出管理
     */
    public List<Map<String, String>> getPartyOutFlowList(String orgId, String partyOrgId, String beginTime, String endTime, String search) {
        return partyOutFlowMapper.getPartyOutFlowList(orgId, partyOrgId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getPartyOutFlowList
     * @Description:  党员流出管理
     */
    public PageInfo<Map<String, String>> getPartyOutFlowList(PageParam pageParam, String partyOrgId, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getPartyOutFlowList(pageParam.getOrgId(), partyOrgId, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
