package com.core136.service.partyrelation;

import com.core136.bean.partymember.PartyMember;
import com.core136.bean.partyrelation.PartyRelIn;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.partyrelation.PartyRelInMapper;
import com.core136.service.partymember.PartyMemberService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class PartyRelInService {
    private PartyRelInMapper partyRelInMapper;

    @Autowired
    public void setPartyRelInMapper(PartyRelInMapper partyRelInMapper) {
        this.partyRelInMapper = partyRelInMapper;
    }

    private PartyMemberService partyMemberService;

    @Autowired
    public void setPartyMemberService(PartyMemberService partyMemberService) {
        this.partyMemberService = partyMemberService;
    }

    public int insertPartyRelIn(PartyRelIn partyRelIn) {
        return partyRelInMapper.insert(partyRelIn);
    }

    public int deletePartyRelIn(PartyRelIn partyRelIn) {
        return partyRelInMapper.delete(partyRelIn);
    }

    public int updatePartyRelIn(Example example, PartyRelIn partyRelIn) {
        return partyRelInMapper.updateByExampleSelective(partyRelIn, example);
    }

    public PartyRelIn selectOnePartyRelIn(PartyRelIn partyRelIn) {
        return partyRelInMapper.selectOne(partyRelIn);
    }

    /**
     * @param orgId
     * @param inType
     * @param beginTime
     * @param endTime
     * @param partyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getRelInList
     * @Description:  获取关系转入列表
     */
    public List<Map<String, String>> getRelInList(String orgId, String inType, String beginTime, String endTime, String partyOrgId, String search) {
        return partyRelInMapper.getRelInList(orgId, inType, beginTime, endTime, partyOrgId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param inType
     * @param beginTime
     * @param endTime
     * @param partyOrgId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getRelInList
     * @Description:  获取关系转入列表
     */
    public PageInfo<Map<String, String>> getRelInList(PageParam pageParam, String inType, String beginTime, String endTime, String partyOrgId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getRelInList(pageParam.getOrgId(), inType, beginTime, endTime, partyOrgId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param partyRelIn
     * @return RetDataBean
     * @Title: setRelIn
     * @Description:  党员关系转入
     */
    @Transactional(value = "generalTM")
    public RetDataBean setRelIn(PartyRelIn partyRelIn) {
        try {
            PartyMember partyMember = new PartyMember();
            partyMember.setMemberId(SysTools.getGUID());
            partyMember.setUserName(partyRelIn.getUserName());
            partyMember.setUserSex(partyRelIn.getUserSex());
            partyMember.setBirthday(partyRelIn.getBirthday());
            partyMember.setCardId(partyRelIn.getCardId());
            partyMember.setUserPost(partyRelIn.getUserPost());
            partyMember.setUserType(partyRelIn.getUserType());
            partyMember.setEducation(partyRelIn.getEducation());
            partyMember.setDegree(partyRelIn.getDegree());
            partyMember.setNation(partyMember.getNation());
            partyMember.setNativePlace(partyRelIn.getNativePlace());
            partyMember.setPhoto(partyRelIn.getPhoto());
            partyMember.setDues(partyRelIn.getDues());
            partyMember.setJoinTime(partyRelIn.getJoinTime());
            partyMember.setDuesBase(partyRelIn.getDuesBase());
            partyMember.setCreateTime(partyRelIn.getCreateTime());
            partyMember.setCreateUser(partyRelIn.getCreateUser());
            partyMember.setOrgId(partyRelIn.getOrgId());
            partyMemberService.insertPartyMember(partyMember);
            insertPartyRelIn(partyRelIn);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


}
