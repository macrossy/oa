package com.core136.mapper.party;

import com.core136.bean.party.PartyAlbumType;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyAlbumTypeMapper extends MyMapper<PartyAlbumType> {

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getAlbumTypeForSelect
     * @Description:  获取专辑分类
     */
    public List<Map<String, String>> getAlbumTypeForSelect(@Param(value = "orgId") String orgId);

    public Map<String,String>getAlbumTypeName(@Param(value="orgId")String orgId,@Param(value="albumTypeId")String albumTypeId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getAlbumTypeForHome
     * @Description:  获取首页分类
     */
    public List<Map<String, String>> getAlbumTypeForHome(@Param(value = "orgId") String orgId);

}
