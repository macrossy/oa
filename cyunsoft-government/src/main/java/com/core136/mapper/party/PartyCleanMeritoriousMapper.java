package com.core136.mapper.party;

import com.core136.bean.party.PartyCleanMeritorious;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyCleanMeritoriousMapper extends MyMapper<PartyCleanMeritorious> {

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param meritoriousType
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getCleanMeritoriousList
     * @Description:  获取先进事迹列表
     */
    public List<Map<String, String>> getCleanMeritoriousList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag, @Param(value = "accountId") String accountId,
                                                             @Param(value = "meritoriousType") String meritoriousType, @Param(value = "status") String status, @Param("beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyCleanMeritoriousListForPortal
     * @Description:  获门户先进事迹列表
     */
    public List<Map<String, String>> getMyCleanMeritoriousListForPortal(@Param(value = "orgId") String orgId, @Param(value = "memberId") String memberId);

    /**
     * @param orgId
     * @param accountId
     * @param deptId
     * @param levelId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyCleanMeritoriousList
     * @Description:  获取更多先进事迹
     */
    public List<Map<String, String>> getMyCleanMeritoriousList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "deptId") String deptId, @Param(value = "levelId") String levelId, @Param(value = "search") String search);


}
