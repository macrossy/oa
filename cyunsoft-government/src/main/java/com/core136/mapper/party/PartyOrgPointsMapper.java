package com.core136.mapper.party;

import com.core136.bean.party.PartyOrgPoints;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyOrgPointsMapper extends MyMapper<PartyOrgPoints> {

    /**
     * 获取党组织汇总积分列表
     *
     * @param orgId
     * @param pointsYear
     * @param partyOrgId
     * @return
     */
    public List<Map<String, String>> getOrgPointsTotalList(@Param(value = "orgId") String orgId, @Param(value = "pointsYear") String pointsYear, @Param(value = "partyOrgId") String partyOrgId);

    /**
     * 获取党组织积分列表
     *
     * @param orgId
     * @param pointsYear
     * @param partyOrgId
     * @return
     */
    public List<Map<String, String>> getOrgPointsList(@Param(value = "orgId") String orgId, @Param(value = "pointsYear") String pointsYear, @Param(value = "partyOrgId") String partyOrgId);
}
