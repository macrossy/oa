package com.core136.mapper.party;

import com.core136.bean.party.PartyIntegral;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyIntegralMapper extends MyMapper<PartyIntegral> {

    /**
     * @param orgId
     * @param partyOrgId
     * @param year
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyVerifyAllList
     * @Description:  获取所有考核记录
     */
    public List<Map<String, String>> getPartyVerifyAllList(@Param(value = "orgId") String orgId, @Param(value = "partyOrgId") String partyOrgId,
                                                           @Param(value = "year") String year, @Param(value = "search") String search);
}
