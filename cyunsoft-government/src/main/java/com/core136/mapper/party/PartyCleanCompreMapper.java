package com.core136.mapper.party;

import com.core136.bean.party.PartyCleanCompre;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyCleanCompreMapper extends MyMapper<PartyCleanCompre> {
    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param comprehensionType
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getCleanComprehensionList
     * @Description:  获取感悟列表
     */
    public List<Map<String, String>> getCleanComprehensionList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag, @Param(value = "accountId") String accountId, @Param(value = "comprehensionType") String comprehensionType,
                                                               @Param(value = "status") String status, @Param("beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getMyCleanComprehensionListForPortal
     * @Description:  获取门户学习感悟列表
     */
    public List<Map<String, String>> getMyCleanComprehensionListForPortal(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMyCleanComprehensionList
     * @Description:  获取更多感悟记录
     */
    public List<Map<String, String>> getMyCleanComprehensionList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);

}
