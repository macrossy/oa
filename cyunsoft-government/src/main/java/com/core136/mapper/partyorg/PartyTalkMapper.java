package com.core136.mapper.partyorg;

import com.core136.bean.partyorg.PartyTalk;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyTalkMapper extends MyMapper<PartyTalk> {

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyTalkList
     * @Description:  获取书记谈话记录
     */
    public List<Map<String, String>> getPartyTalkList(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime,
                                                      @Param(value = "endTime") String endTime, @Param(value = "search") String search);
}
