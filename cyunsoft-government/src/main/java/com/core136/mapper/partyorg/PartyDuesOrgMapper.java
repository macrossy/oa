package com.core136.mapper.partyorg;

import com.core136.bean.partyorg.PartyDuesOrg;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyDuesOrgMapper extends MyMapper<PartyDuesOrg> {

    /**
     * @param orgId
     * @param onTime
     * @param partyOrgId
     * @param year
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyDeusOrgList
     * @Description:  组织党费记录
     */
    public List<Map<String, String>> getPartyDeusOrgList(@Param(value = "orgId") String orgId, @Param(value = "onTime") String onTime,
                                                         @Param(value = "partyOrgId") String partyOrgId, @Param(value = "year") String year, @Param(value = "search") String search);


}
