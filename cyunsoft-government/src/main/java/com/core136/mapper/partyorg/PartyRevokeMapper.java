package com.core136.mapper.partyorg;

import com.core136.bean.partyorg.PartyRevoke;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyRevokeMapper extends MyMapper<PartyRevoke> {

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyRevokeList
     * @Description:  获取党组织撤销记录
     */
    public List<Map<String, String>> getPartyRevokeList(
            @Param(value = "orgId") String orgId,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "search") String search);

}
