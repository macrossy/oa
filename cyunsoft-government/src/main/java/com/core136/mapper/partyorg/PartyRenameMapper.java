package com.core136.mapper.partyorg;

import com.core136.bean.partyorg.PartyRename;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyRenameMapper extends MyMapper<PartyRename> {
    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getPartyRenameList
     * @Description:  获取党组织更名历史记录
     */
    public List<Map<String, String>> getPartyRenameList(
            @Param(value = "orgId") String orgId,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "search") String search);
}
