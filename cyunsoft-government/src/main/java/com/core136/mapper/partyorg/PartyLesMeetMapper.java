package com.core136.mapper.partyorg;

import com.core136.bean.partyorg.PartyLesMeet;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyLesMeetMapper extends MyMapper<PartyLesMeet> {

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param meetType
     * @param meetModel
     * @param search
     * @return List<Map < String, String>>
     * @Title: getLesMeetList
     * @Description:  获取三会一课记录列表
     */
    public List<Map<String, String>> getLesMeetList(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                    @Param(value = "meetType") String meetType, @Param(value = "meetModel") String meetModel, @Param(value = "search") String search);

}
