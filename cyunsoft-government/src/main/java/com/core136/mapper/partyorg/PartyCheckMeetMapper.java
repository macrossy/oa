package com.core136.mapper.partyorg;

import com.core136.bean.partyorg.PartyCheckMeet;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyCheckMeetMapper extends MyMapper<PartyCheckMeet> {

    /**
     * @param orgId
     * @param checkYear
     * @param checkStatus
     * @param checkLevel
     * @param partyOrgId
     * @return List<Map < String, String>>
     * @Title: getCheckMeetList
     * @Description:  获取述评考记录列表
     */
    public List<Map<String, String>> getCheckMeetList(@Param(value = "orgId") String orgId, @Param(value = "checkYear") String checkYear,
                                                      @Param(value = "checkStatus") String checkStatus, @Param(value = "checkLevel") String checkLevel, @Param(value = "partyOrgId") String partyOrgId);

}
