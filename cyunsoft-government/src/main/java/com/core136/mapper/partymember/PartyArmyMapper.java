package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyArmy;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyArmyMapper extends MyMapper<PartyArmy> {

    /**
     * @param orgId
     * @param userType
     * @param education
     * @param partyMember
     * @param search
     * @return List<Map < String, String>>
     * @Title: getArmyUserList
     * @Description:  获取统战人员列表
     */
    public List<Map<String, String>> getArmyUserList(@Param(value = "orgId") String orgId, @Param(value = "userType") String userType,
                                                     @Param(value = "education") String education, @Param(value = "partyMember") String partyMember,
                                                     @Param(value = "armyRank") String armyRank, @Param(value = "search") String search);

}
