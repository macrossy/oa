package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyFamilyRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyFamilyRecordMapper extends MyMapper<PartyFamilyRecord> {

    public List<Map<String, String>> getMemberFamilyList(@Param(value = "orgId") String orgId, @Param(value = "userName") String userName, @Param(value = "memberId") String memberId);

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyMemberFamilyList
     * @Description:  获取个人家庭成员记录
     */
    public List<Map<String, String>> getMyMemberFamilyList(@Param(value = "orgId") String orgId, @Param(value = "memberId") String memberId);


}
