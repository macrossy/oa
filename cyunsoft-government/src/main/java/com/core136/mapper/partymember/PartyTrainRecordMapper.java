package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyTrainRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyTrainRecordMapper extends MyMapper<PartyTrainRecord> {

    /**
     * @param orgId
     * @param classType
     * @param trainType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getMemberTrainList
     * @Description:  获取党员培训记录列表
     */
    public List<Map<String, String>> getMemberTrainList(@Param(value = "orgId") String orgId, @Param(value = "classType") String classType, @Param(value = "trainType") String trainType,
                                                        @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param memberId
     * @return List<Map < String, String>>
     * @Title: getMyMemberTrainList
     * @Description:  获取党员自己的培训记录
     */
    public List<Map<String, String>> getMyMemberTrainList(@Param(value = "orgId") String orgId, @Param(value = "memberId") String memberId);

}
