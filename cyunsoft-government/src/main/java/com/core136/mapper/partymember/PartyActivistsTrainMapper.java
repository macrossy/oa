package com.core136.mapper.partymember;

import com.core136.bean.partymember.PartyActivistsTrain;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyActivistsTrainMapper extends MyMapper<PartyActivistsTrain> {

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param partyOrgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getActivistsTrainList
     * @Description:  获取积极份子培训记录
     */
    public List<Map<String, String>> getActivistsTrainList(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime,
                                                           @Param(value = "endTime") String endTime, @Param(value = "partyOrgId") String partyOrgId, @Param(value = "jobStatus") String jobStatus,
                                                           @Param(value = "search") String search);
}
