package com.core136.mapper.partyparam;

import com.core136.bean.partyparam.PartyNativePlace;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyNativePlaceMapper extends MyMapper<PartyNativePlace> {
    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getNativePlaceTree
     * @Description:  获取籍贯分类树
     */
    public List<Map<String, String>> getNativePlaceTree(@Param(value = "orgId") String orgId, @Param(value = "levelId") String levelId);

    /**
     * @param @param  orgId
     * @param @param  sortId
     * @param @return 设定文件
     * @return int 返回类型
     * @Title: isExistChild
     * @Description:  判断是否还有子集
     */
    public int isExistChild(@Param(value = "orgId") String orgId, @Param(value = "sortId") String sortId);
}
