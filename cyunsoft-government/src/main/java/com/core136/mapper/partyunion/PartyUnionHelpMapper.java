package com.core136.mapper.partyunion;

import com.core136.bean.partyunion.PartyUnionHelp;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface PartyUnionHelpMapper extends MyMapper<PartyUnionHelp> {
    /**
     * 获取困难帮扶列表
     *
     * @param orgId
     * @param helpType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getPartyUnionHelpList(@Param(value = "orgId") String orgId, @Param(value = "helpType") String helpType, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);
}
