package com.core136.mapper.partyunion;

import com.core136.bean.partyunion.PartyUnionParty;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartyUnionPartyMapper extends MyMapper<PartyUnionParty> {
}
