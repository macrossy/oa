package com.core136.mapper.partyrelation;

import com.core136.bean.partyrelation.PartyIntroduce;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface PartyIntroduceMapper extends MyMapper<PartyIntroduce> {

    /**
     * @param orgId
     * @param beginTime
     * @param endTime
     * @param memberId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getIntroduceList
     * @Description:  获取介绍信列表
     */
    public List<Map<String, String>> getIntroduceList(@Param(value = "orgId") String orgId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                      @Param(value = "memberId") String memberId, @Param(value = "search") String search);


}
