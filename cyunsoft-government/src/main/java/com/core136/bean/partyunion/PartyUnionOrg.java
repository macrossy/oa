package com.core136.bean.partyunion;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * 工会机构
 */
@Table(name = "party_union_org")
public class PartyUnionOrg implements Serializable {
    private static final long serialVersionUID = 1L;
    private String unionOrgId;
    private String createType;
    private String chairmanFir;
    private String chairmanSec;
    private String memberIds;
    private String linkMan;
    private String linkTel;
    private String remark;
    private String beginTime;
    private String endTime;
    private String deadLine;
    private String attach;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getUnionOrgId() {
        return unionOrgId;
    }

    public void setUnionOrgId(String unionOrgId) {
        this.unionOrgId = unionOrgId;
    }

    public String getCreateType() {
        return createType;
    }

    public void setCreateType(String createType) {
        this.createType = createType;
    }

    public String getChairmanFir() {
        return chairmanFir;
    }

    public void setChairmanFir(String chairmanFir) {
        this.chairmanFir = chairmanFir;
    }

    public String getChairmanSec() {
        return chairmanSec;
    }

    public void setChairmanSec(String chairmanSec) {
        this.chairmanSec = chairmanSec;
    }

    public String getMemberIds() {
        return memberIds;
    }

    public void setMemberIds(String memberIds) {
        this.memberIds = memberIds;
    }

    public String getLinkMan() {
        return linkMan;
    }

    public void setLinkMan(String linkMan) {
        this.linkMan = linkMan;
    }

    public String getLinkTel() {
        return linkTel;
    }

    public void setLinkTel(String linkTel) {
        this.linkTel = linkTel;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(String deadLine) {
        this.deadLine = deadLine;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
