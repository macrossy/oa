package com.core136.bean.partyorg;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PartyUnitBase
 * @Description: 党组织单位基本信息
 * @author: 稠云技术
 * @date: 2021年2月10日 下午6:16:28
 * @Copyright: 2021 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "party_unit_base")
public class PartyUnitBase implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String unitId;
    private String unitName;
    private String shortName;
    private String unitType;
    private String partyStatus;
    private String creditCode;
    private String unitAffiliation;
    private String industry;
    private String serviceIndustry;
    private String address;
    private String legalPerson;
    private String linkTel;
    private String unitType1;
    private String unitType2;
    private String unitType3;
    private String subordinateUnit;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getPartyStatus() {
        return partyStatus;
    }

    public void setPartyStatus(String partyStatus) {
        this.partyStatus = partyStatus;
    }

    public String getCreditCode() {
        return creditCode;
    }

    public void setCreditCode(String creditCode) {
        this.creditCode = creditCode;
    }

    public String getUnitAffiliation() {
        return unitAffiliation;
    }

    public void setUnitAffiliation(String unitAffiliation) {
        this.unitAffiliation = unitAffiliation;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getServiceIndustry() {
        return serviceIndustry;
    }

    public void setServiceIndustry(String serviceIndustry) {
        this.serviceIndustry = serviceIndustry;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLegalPerson() {
        return legalPerson;
    }

    public void setLegalPerson(String legalPerson) {
        this.legalPerson = legalPerson;
    }

    public String getLinkTel() {
        return linkTel;
    }

    public void setLinkTel(String linkTel) {
        this.linkTel = linkTel;
    }

    public String getUnitType1() {
        return unitType1;
    }

    public void setUnitType1(String unitType1) {
        this.unitType1 = unitType1;
    }

    public String getUnitType2() {
        return unitType2;
    }

    public void setUnitType2(String unitType2) {
        this.unitType2 = unitType2;
    }

    public String getUnitType3() {
        return unitType3;
    }

    public void setUnitType3(String unitType3) {
        this.unitType3 = unitType3;
    }

    public String getSubordinateUnit() {
        return subordinateUnit;
    }

    public void setSubordinateUnit(String subordinateUnit) {
        this.subordinateUnit = subordinateUnit;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }


}
