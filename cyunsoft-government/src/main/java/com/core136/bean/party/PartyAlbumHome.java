package com.core136.bean.party;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PartyAlbumHome
 * @Description: 视屏专辑首页设置
 * @author: 稠云技术
 * @date: 2020年11月10日 下午4:12:43
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "party_album_home")
public class PartyAlbumHome implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String homePageId;
    private String logoPic;
    private String title;
    private String bannerPic;
    private String createUser;
    private String createTime;
    private String orgId;

    public String getHomePageId() {
        return homePageId;
    }

    public void setHomePageId(String homePageId) {
        this.homePageId = homePageId;
    }

    public String getLogoPic() {
        return logoPic;
    }

    public void setLogoPic(String logoPic) {
        this.logoPic = logoPic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBannerPic() {
        return bannerPic;
    }

    public void setBannerPic(String bannerPic) {
        this.bannerPic = bannerPic;
    }

    public String getCreateUser() {
        return createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
