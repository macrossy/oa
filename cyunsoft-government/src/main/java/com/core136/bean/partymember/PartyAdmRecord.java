package com.core136.bean.partymember;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PartyAdmRecord
 * @Description: 行政任职记录
 * @author: 稠云技术
 * @date: 2021年3月8日 下午7:42:12
 * @Copyright: 2021 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "party_adm_record")
public class PartyAdmRecord implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String memberId;
    private Integer sortNo;
    private String partyOrgId;
    private String otherPartyOrgName;
    private String admPost;
    private String otherAdmPost;
    private String manageWork;
    private String postTime;
    private String postType;
    private String lostTime;
    private String postLevel;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getPartyOrgId() {
        return partyOrgId;
    }

    public void setPartyOrgId(String partyOrgId) {
        this.partyOrgId = partyOrgId;
    }

    public String getOtherPartyOrgName() {
        return otherPartyOrgName;
    }

    public void setOtherPartyOrgName(String otherPartyOrgName) {
        this.otherPartyOrgName = otherPartyOrgName;
    }

    public String getAdmPost() {
        return admPost;
    }

    public void setAdmPost(String admPost) {
        this.admPost = admPost;
    }

    public String getOtherAdmPost() {
        return otherAdmPost;
    }

    public void setOtherAdmPost(String otherAdmPost) {
        this.otherAdmPost = otherAdmPost;
    }

    public String getManageWork() {
        return manageWork;
    }

    public void setManageWork(String manageWork) {
        this.manageWork = manageWork;
    }

    public String getPostTime() {
        return postTime;
    }

    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getLostTime() {
        return lostTime;
    }

    public void setLostTime(String lostTime) {
        this.lostTime = lostTime;
    }

    public String getPostLevel() {
        return postLevel;
    }

    public void setPostLevel(String postLevel) {
        this.postLevel = postLevel;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


}
