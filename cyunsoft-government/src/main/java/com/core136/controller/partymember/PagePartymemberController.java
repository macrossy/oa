package com.core136.controller.partymember;

import com.core136.common.retdataunit.RetDataBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/app/core/partymember")
public class PagePartymemberController {

    /**
     * @return ModelAndView
     * @Title: goPartyMemberLoseDetails
     * @Description:  失联党员详情
     */
    @RequestMapping("/partymemberlosedetails")
    @RequiresPermissions("/app/core/partymember/partymemberlosedetails")
    public ModelAndView goPartyMemberLoseDetails() {
        try {
            return new ModelAndView("app/core/partymember/partymemberlosedetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goDevTotal
     * @Description:  党员发展统计
     */
    @RequestMapping("/devtotal")
    @RequiresPermissions("/app/core/partymember/devtotal")
    public ModelAndView goDevTotal() {
        try {
            return new ModelAndView("app/core/partymember/devtotal");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goMySeizuredues
     * @Description:  我的党费
     */
    @RequestMapping("/myseizuredues")
    @RequiresPermissions("/app/core/partymember/myseizuredues")
    public ModelAndView goMySeizuredues() {
        try {
            return new ModelAndView("app/core/partymember/myseizuredues");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goMyPartyMemberInfo
     * @Description:  我的党籍
     */

    @RequestMapping("/mypartymemberinfo")
    @RequiresPermissions("/app/core/partymember/mypartymemberinfo")
    public ModelAndView goMyPartyMemberInfo() {
        try {
            return new ModelAndView("app/core/partymember/mypartymemberinfo");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goPartyMemberLose
     * @Description:  失联党员管理
     */
    @RequestMapping("/lose")
    @RequiresPermissions("/app/core/partymember/lose")
    public ModelAndView goPartyMemberLose(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/partymemberlosemanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partymember/partymemberlose");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goPartyMemberTotal
     * @Description:  党员统计
     */
    @RequestMapping("/total")
    @RequiresPermissions("/app/core/partymember/total")
    public ModelAndView goPartyMemberTotal() {
        try {
            return new ModelAndView("app/core/partymember/partymembertotal");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goDifficultyDetails
     * @Description:  困难党员详情
     */
    @RequestMapping("/difficultydetails")
    @RequiresPermissions("/app/core/partymember/difficultydetails")
    public ModelAndView goDifficultyDetails() {
        try {
            return new ModelAndView("app/core/partymember/difficultydetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goDifficulty
     * @Description:  困难党员管理
     */
    @RequestMapping("/difficulty")
    @RequiresPermissions("/app/core/partymember/difficulty")
    public ModelAndView goDifficulty(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/difficultymanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partymember/difficulty");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goActivistsTrainDetails
     * @Description:  积极份子培训记录详情
     */
    @RequestMapping("/activiststraindetails")
    @RequiresPermissions("/app/core/partymember/activiststraindetails")
    public ModelAndView goActivistsTrainDetails() {
        try {
            return new ModelAndView("app/core/partymember/activiststraindetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goVolunteerDetails
     * @Description:  志愿者活动详情
     */
    @RequestMapping("/volunteerdetails")
    @RequiresPermissions("/app/core/partymember/volunteerdetails")
    public ModelAndView goVolunteerDetails() {
        try {
            return new ModelAndView("app/core/partymember/volunteerdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 志愿者活动列表
     *
     * @return ModelAndView
     * @Title: goVolunteerRead
     * @Description:
     */
    @RequestMapping("/volunteerread")
    @RequiresPermissions("/app/core/partymember/volunteerread")
    public ModelAndView goVolunteerRead() {
        try {
            return new ModelAndView("app/core/partymember/volunteerread");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goArmyDetails
     * @Description:  统战记录详情
     */
    @RequestMapping("/armydetails")
    @RequiresPermissions("/app/core/partymember/armydetails")
    public ModelAndView goArmyDetails() {
        try {
            return new ModelAndView("app/core/partymember/armydetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goVolunteer
     * @Description:  志愿者服务
     */
    @RequestMapping("/volunteer")
    @RequiresPermissions("/app/core/partymember/volunteer")
    public ModelAndView goVolunteer(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/volunteermanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partymember/volunteer");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goTargetTrain
     * @Description:  发展对象培训
     */
    @RequestMapping("/targettrain")
    @RequiresPermissions("/app/core/partymember/targettrain")
    public ModelAndView goTargetTrain(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/targettrainmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partymember/targettrain");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goTargetTrainDetails
     * @Description: 发展党员培训详情
     */
    @RequestMapping("/targettraindetails")
    @RequiresPermissions("/app/core/partymember/targettraindetails")
    public ModelAndView goTargetTrainDetails() {
        try {
            return new ModelAndView("app/core/partymember/targettraindetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goArmy
     * @Description:  统战管理
     */
    @RequestMapping("/army")
    @RequiresPermissions("/app/core/partymember/army")
    public ModelAndView goArmy(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/armymanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/partymember/army");
                } else if (view.equals("import")) {
                    mv = new ModelAndView("app/core/partymember/armyimport");
                    RetDataBean retDataBean = new RetDataBean();
                    mv.addObject("retDataBean", retDataBean);
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goActivistsTrain
     * @Description:  积极份子培训管理
     */
    @RequestMapping("/activiststrain")
    @RequiresPermissions("/app/core/partymember/activiststrain")
    public ModelAndView goActivistsTrain(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/activiststrainmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partymember/activiststrain");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goPartyJoinApplyDetails
     * @Description:  入党申请详情
     */
    @RequestMapping("/partyjoinapplydetails")
    @RequiresPermissions("/app/core/partymember/partyjoinapplydetails")
    public ModelAndView goPartyJoinApplyDetails() {
        try {
            return new ModelAndView("app/core/partymember/partyjoinapplydetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goTarget
     * @Description:  确定为发展对象
     */
    @RequestMapping("/target")
    @RequiresPermissions("/app/core/partymember/target")
    public ModelAndView goTarget(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/target");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/partymember/targetmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goFormal
     * @Description:  预备党员转正
     */
    @RequestMapping("/formal")
    @RequiresPermissions("/app/core/partymember/formal")
    public ModelAndView goFormal(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/formal");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/partymember/formalmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @param view
     * @return ModelAndView
     * @Title: goPrepare
     * @Description:  设置为预备党员
     */
    @RequestMapping("/prepare")
    @RequiresPermissions("/app/core/partymember/prepare")
    public ModelAndView goPrepare(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/prepare");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/partymember/preparemanage");
                }
            }
            return mv;
        } catch (Exception e) {
            mv = new ModelAndView("titps");
            return mv;
        }
    }


    /**
     * @param view
     * @return ModelAndView
     * @Title: goActivists
     * @Description:  确定为积极份子
     */
    @RequestMapping("/activists")
    @RequiresPermissions("/app/core/partymember/activists")
    public ModelAndView goActivists(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/activists");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/partymember/activistsmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            mv = new ModelAndView("titps");
            return mv;
        }
    }

    /**
     * @return ModelAndView
     * @Title: goJoinApplay
     * @Description:  入党申请
     */
    @RequestMapping("/joinapplay")
    @RequiresPermissions("/app/core/partymember/joinapplay")
    public ModelAndView goJoinApplay(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/partyjoinapplymanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partymember/partyjoinapply");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goFamily
     * @Description:  家庭成员
     */
    @RequestMapping("/family")
    @RequiresPermissions("/app/core/partymember/family")
    public ModelAndView goFamily(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/familymanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partymember/family");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goFamilyDetails
     * @Description:  家庭成员详情
     */
    @RequestMapping("/familydetails")
    @RequiresPermissions("/app/core/partymember/familydetails")
    public ModelAndView goFamilyDetails() {
        try {
            return new ModelAndView("app/core/partymember/familydetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goPunish
     * @Description:  惩戒记录
     */
    @RequestMapping("/punish")
    @RequiresPermissions("/app/core/partymember/punish")
    public ModelAndView goPunish(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/punishmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partymember/punish");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goPunishDetails
     * @Description:  惩戒记录详情
     */
    @RequestMapping("/punishdetails")
    @RequiresPermissions("/app/core/partymember/punishdetails")
    public ModelAndView goPunishDetails() {
        try {
            return new ModelAndView("app/core/partymember/punishdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goReward
     * @Description:  奖励记录
     */
    @RequestMapping("/reward")
    @RequiresPermissions("/app/core/partymember/reward")
    public ModelAndView goReward(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/rewardmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partymember/reward");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goRewardDetails
     * @Description:  党员奖励详情
     */
    @RequestMapping("/rewarddetails")
    @RequiresPermissions("/app/core/partymember/rewarddetails")
    public ModelAndView goRewardDetails() {
        try {
            return new ModelAndView("app/core/partymember/rewarddetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goAdmPost
     * @Description:
     */
    @RequestMapping("/admpost")
    @RequiresPermissions("/app/core/partymember/admpost")
    public ModelAndView goAdmPost(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/admpostmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partymember/admpost");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goAdmpostDetails
     * @Description:  行政任职记录详情
     */
    @RequestMapping("/admpostdetails")
    @RequiresPermissions("/app/core/partymember/admpostdetails")
    public ModelAndView goAdmpostDetails() {
        try {
            return new ModelAndView("app/core/partymember/admpostdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goPartyLife
     * @Description:  组织生活管理
     */
    @RequestMapping("/partylife")
    @RequiresPermissions("/app/core/partymember/partylife")
    public ModelAndView goPartyLife(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/partylifemanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partymember/partylife");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goAppraisal
     * @Description:  民主评议
     */
    @RequestMapping("/appraisal")
    @RequiresPermissions("/app/core/partymember/appraisal")
    public ModelAndView goAppraisal(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/appraisalmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partymember/appraisal");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goAppraisalDetails
     * @Description:  民主评议详情
     */
    @RequestMapping("/appraisaldetails")
    @RequiresPermissions("/app/core/partymember/appraisaldetails")
    public ModelAndView goAppraisalDetails() {
        try {
            return new ModelAndView("app/core/partymember/appraisaldetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goTrainDetails
     * @Description:  党员培训详情
     */
    @RequestMapping("/traindetails")
    @RequiresPermissions("/app/core/partymember/traindetails")
    public ModelAndView goTrainDetails() {
        try {
            return new ModelAndView("app/core/partymember/traindetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goPartyPostDetails
     * @Description:  党内任职记录详情
     */
    @RequestMapping("/partypostdetails")
    @RequiresPermissions("/app/core/partymember/partypostdetails")
    public ModelAndView goPartyPostDetails() {
        try {
            return new ModelAndView("app/core/partymember/partypostdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goPartyPost
     * @Description:  党内任职
     */
    @RequestMapping("/partypost")
    @RequiresPermissions("/app/core/partymember/partypost")
    public ModelAndView goPartyPost(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/partypostmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partymember/partypost");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goTrain
     * @Description:  党内培训基本信息
     */
    @RequestMapping("/train")
    @RequiresPermissions("/app/core/partymember/train")
    public ModelAndView goTrain(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/trainmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partymember/train");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goOutPartyMember
     * @Description:  党员出党
     */
    @RequestMapping("/outpartymember")
    @RequiresPermissions("/app/core/partymember/outpartymember")
    public ModelAndView goOutPartyMember(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/outpartymembermanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partymember/outpartymember");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goDeath
     * @Description:  党员去世
     */
    @RequestMapping("/death")
    @RequiresPermissions("/app/core/partymember/death")
    public ModelAndView goDeath(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/partymember/deathmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/partymember/death");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goMemberDeathDetails
     * @Description:  党员去世详情
     */
    @RequestMapping("/partydeathdetails")
    @RequiresPermissions("/app/core/partymember/partydeathdetails")
    public ModelAndView goMemberDeathDetails() {
        try {
            return new ModelAndView("app/core/partymember/deathdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goMemberOutDetails
     * @Description:  党员出党详情
     */
    @RequestMapping("/memberoutdetails")
    @RequiresPermissions("/app/core/partymember/memberoutdetails")
    public ModelAndView goMemberOutDetails() {
        try {
            return new ModelAndView("app/core/partymember/outpartymemberdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goPartyMember
     * @Description:  党员信息管理
     */
    @RequestMapping("/partymember")
    @RequiresPermissions("/app/core/partymember/partymember")
    public ModelAndView goPartyMember() {
        try {
            return new ModelAndView("app/core/partymember/partymember");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goPartyMemberDetails
     * @Description:  党员信息详情
     */
    @RequestMapping("/partymemberdetails")
    @RequiresPermissions("/app/core/partymember/partymemberdetails")
    public ModelAndView goPartyMemberDetails() {
        try {
            return new ModelAndView("app/core/partymember/partymemberdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
