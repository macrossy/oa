package com.core136.controller.partyorg;

import com.core136.bean.account.Account;
import com.core136.bean.partyorg.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.partyorg.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

@RestController
@RequestMapping("/set/partyorgset")
public class RouteSetPartyOrgController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private PartyOrgService partyOrgService;

    @Autowired
    public void setPartyOrgService(PartyOrgService partyOrgService) {
        this.partyOrgService = partyOrgService;
    }

    private PartyUnitBaseService partyUnitBaseService;

    @Autowired
    public void setPartyUnitBaseService(PartyUnitBaseService partyUnitBaseService) {
        this.partyUnitBaseService = partyUnitBaseService;
    }

    private PartyCommitteeService partyCommitteeService;

    @Autowired
    public void setPartyCommitteeService(PartyCommitteeService partyCommitteeService) {
        this.partyCommitteeService = partyCommitteeService;
    }

    private PartyElectionService partyElectionService;

    @Autowired
    public void setPartyElectionService(PartyElectionService partyElectionService) {
        this.partyElectionService = partyElectionService;
    }

    private PartyRevokeService partyRevokeService;

    @Autowired
    public void setPartyRevokeService(PartyRevokeService partyRevokeService) {
        this.partyRevokeService = partyRevokeService;
    }

    private PartyRebuildService partyRebuildService;

    @Autowired
    public void setPartyRebuildService(PartyRebuildService partyRebuildService) {
        this.partyRebuildService = partyRebuildService;
    }

    private PartyRenameService partyRenameService;

    @Autowired
    public void setPartyRenameService(PartyRenameService partyRenameService) {
        this.partyRenameService = partyRenameService;
    }

    private PartyTemporgService partyTemporgService;

    @Autowired
    public void setPartyTemporgService(PartyTemporgService partyTemporgService) {
        this.partyTemporgService = partyTemporgService;
    }

    private PartyTalkService partyTalkService;

    @Autowired
    public void setPartyTalkService(PartyTalkService partyTalkService) {
        this.partyTalkService = partyTalkService;
    }

    private PartyLesMeetService partyLesMeetService;

    @Autowired
    public void setPartyLesMeetService(PartyLesMeetService partyLesMeetService) {
        this.partyLesMeetService = partyLesMeetService;
    }

    private PartyCheckMeetService partyCheckMeetService;

    @Autowired
    public void setPartyCheckMeetService(PartyCheckMeetService partyCheckMeetService) {
        this.partyCheckMeetService = partyCheckMeetService;
    }

    private PartyMzMeetService partyMzMeetService;

    @Autowired
    public void setPartyMzMeetService(PartyMzMeetService partyMzMeetService) {
        this.partyMzMeetService = partyMzMeetService;
    }

    private PartyLifeMeetService partyLifeMeetService;

    @Autowired
    public void setPartyLifeMeetService(PartyLifeMeetService partyLifeMeetService) {
        this.partyLifeMeetService = partyLifeMeetService;
    }

    private PartyPrimaryMeetService partyPrimaryMeetService;

    @Autowired
    public void setPartyPrimaryMeetService(PartyPrimaryMeetService partyPrimaryMeetService) {
        this.partyPrimaryMeetService = partyPrimaryMeetService;
    }

    private PartyDuesOrgService partyDuesOrgService;

    @Autowired
    public void setPartyDuesOrgService(PartyDuesOrgService partyDuesOrgService) {
        this.partyDuesOrgService = partyDuesOrgService;
    }

    private PartyHistoryRecordService partyHistoryRecordService;

    @Autowired
    public void setPartyHistoryRecordService(PartyHistoryRecordService partyHistoryRecordService) {
        this.partyHistoryRecordService = partyHistoryRecordService;
    }

    /**
     * 添加历史组织机构
     *
     * @param partyHistoryRecord
     * @return
     */
    @RequestMapping(value = "/insertPartyHistoryRecord", method = RequestMethod.POST)
    public RetDataBean insertPartyHistoryRecord(PartyHistoryRecord partyHistoryRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyHistoryRecord.setRecordId(SysTools.getGUID());
            partyHistoryRecord.setCreateUser(account.getAccountId());
            partyHistoryRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyHistoryRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyHistoryRecordService.insertPartyHistoryRecord(partyHistoryRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除历史党组织记录
     *
     * @param partyHistoryRecord
     * @return
     */
    @RequestMapping(value = "/deletePartyHistoryRecord", method = RequestMethod.POST)
    public RetDataBean deletePartyHistoryRecord(PartyHistoryRecord partyHistoryRecord) {
        try {
            if (StringUtils.isBlank(partyHistoryRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyHistoryRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyHistoryRecordService.deletePartyHistoryRecord(partyHistoryRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 更新历史党组机构记录
     *
     * @param partyHistoryRecord
     * @return
     */
    @RequestMapping(value = "/updatePartyHistoryRecord", method = RequestMethod.POST)
    public RetDataBean updatePartyHistoryRecord(PartyHistoryRecord partyHistoryRecord) {
        try {
            if (StringUtils.isBlank(partyHistoryRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyHistoryRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyHistoryRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyHistoryRecordService.updatePartyHistoryRecord(example, partyHistoryRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyDuesOrg
     * @return RetDataBean
     * @Title: insertPartyDuesOrg
     * @Description:  添加组织党费记录
     */
    @RequestMapping(value = "/insertPartyDuesOrg", method = RequestMethod.POST)
    public RetDataBean insertPartyDuesOrg(PartyDuesOrg partyDuesOrg) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyDuesOrg.setRecordId(SysTools.getGUID());
            partyDuesOrg.setCreateUser(account.getAccountId());
            partyDuesOrg.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyDuesOrg.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyDuesOrgService.insertPartyDuesOrg(partyDuesOrg));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyDuesOrg
     * @return RetDataBean
     * @Title: deletePartyDuesOrg
     * @Description:  删除组织党费缴纳记录
     */
    @RequestMapping(value = "/deletePartyDuesOrg", method = RequestMethod.POST)
    public RetDataBean deletePartyDuesOrg(PartyDuesOrg partyDuesOrg) {
        try {
            if (StringUtils.isBlank(partyDuesOrg.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyDuesOrg.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyDuesOrgService.deletePartyDuesOrg(partyDuesOrg));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyDuesOrg
     * @return RetDataBean
     * @Title: updatePartyDuesOrg
     * @Description:  修改组织党费缴纳记录
     */
    @RequestMapping(value = "/updatePartyDuesOrg", method = RequestMethod.POST)
    public RetDataBean updatePartyDuesOrg(PartyDuesOrg partyDuesOrg) {
        try {
            if (StringUtils.isBlank(partyDuesOrg.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyDuesOrg.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyDuesOrg.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyDuesOrgService.updatePartyDuesOrg(example, partyDuesOrg));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyPrimaryMeet
     * @return RetDataBean
     * @Title: insertPartyPrimaryMeet
     * @Description:  添加主题党日记录
     */
    @RequestMapping(value = "/insertPartyPrimaryMeet", method = RequestMethod.POST)
    public RetDataBean insertPartyPrimaryMeet(PartyPrimaryMeet partyPrimaryMeet) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPrimaryMeet.setRecordId(SysTools.getGUID());
            partyPrimaryMeet.setCreateUser(account.getAccountId());
            partyPrimaryMeet.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyPrimaryMeet.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyPrimaryMeetService.insertPartyPrimaryMeet(partyPrimaryMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPrimaryMeet
     * @return RetDataBean
     * @Title: deletePartyPrimaryMeet
     * @Description:  删除主题党日
     */
    @RequestMapping(value = "/deletePartyPrimaryMeet", method = RequestMethod.POST)
    public RetDataBean deletePartyPrimaryMeet(PartyPrimaryMeet partyPrimaryMeet) {
        try {
            if (StringUtils.isBlank(partyPrimaryMeet.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPrimaryMeet.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyPrimaryMeetService.deletePartyPrimaryMeet(partyPrimaryMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPrimaryMeet
     * @return RetDataBean
     * @Title: updatePartyPrimaryMeet
     * @Description:  更新主题党日
     */
    @RequestMapping(value = "/updatePartyPrimaryMeet", method = RequestMethod.POST)
    public RetDataBean updatePartyPrimaryMeet(PartyPrimaryMeet partyPrimaryMeet) {
        try {
            if (StringUtils.isBlank(partyPrimaryMeet.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyPrimaryMeet.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyPrimaryMeet.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyPrimaryMeetService.updatePartyPrimaryMeet(example, partyPrimaryMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMzMeet
     * @return RetDataBean
     * @Title: insertPartyMzMeet
     * @Description:  添加民主生活会记录
     */
    @RequestMapping(value = "/insertPartyMzMeet", method = RequestMethod.POST)
    public RetDataBean insertPartyMzMeet(PartyMzMeet partyMzMeet) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMzMeet.setRecordId(SysTools.getGUID());
            partyMzMeet.setCreateUser(account.getAccountId());
            partyMzMeet.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyMzMeet.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyMzMeetService.insertPartyMzMeet(partyMzMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMzMeet
     * @return RetDataBean
     * @Title: deletePartyMzMeet
     * @Description:  删除民主生活会记录
     */
    @RequestMapping(value = "/deletePartyMzMeet", method = RequestMethod.POST)
    public RetDataBean deletePartyMzMeet(PartyMzMeet partyMzMeet) {
        try {
            if (StringUtils.isBlank(partyMzMeet.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyMzMeet.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyMzMeetService.deletePartyMzMeet(partyMzMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyMzMeet
     * @return RetDataBean
     * @Title: updatePartyMzMeet
     * @Description:  更新民主生活会记录
     */
    @RequestMapping(value = "/updatePartyMzMeet", method = RequestMethod.POST)
    public RetDataBean updatePartyMzMeet(PartyMzMeet partyMzMeet) {
        try {
            if (StringUtils.isBlank(partyMzMeet.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyMzMeet.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyMzMeet.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyMzMeetService.updatePartyMzMeet(example, partyMzMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyLifeMeet
     * @return RetDataBean
     * @Title: insertPartyLifeMeet
     * @Description:  添加党组织生活会记录
     */
    @RequestMapping(value = "/insertPartyLifeMeet", method = RequestMethod.POST)
    public RetDataBean insertPartyLifeMeet(PartyLifeMeet partyLifeMeet) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyLifeMeet.setRecordId(SysTools.getGUID());
            partyLifeMeet.setCreateUser(account.getAccountId());
            partyLifeMeet.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyLifeMeet.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyLifeMeetService.insertPartyLifeMeet(partyLifeMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyLifeMeet
     * @return RetDataBean
     * @Title: deletePartyLifeMeet
     * @Description:  删除党组织生活会记录
     */
    @RequestMapping(value = "/deletePartyLifeMeet", method = RequestMethod.POST)
    public RetDataBean deletePartyLifeMeet(PartyLifeMeet partyLifeMeet) {
        try {
            if (StringUtils.isBlank(partyLifeMeet.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyLifeMeet.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyLifeMeetService.deletePartyLifeMeet(partyLifeMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyLifeMeet
     * @return RetDataBean
     * @Title: updatePartyLifeMeet
     * @Description:  更新党组织生活会记录
     */
    @RequestMapping(value = "/updatePartyLifeMeet", method = RequestMethod.POST)
    public RetDataBean updatePartyLifeMeet(PartyLifeMeet partyLifeMeet) {
        try {
            if (StringUtils.isBlank(partyLifeMeet.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyLifeMeet.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyLifeMeet.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyLifeMeetService.updatePartyLifeMeet(example, partyLifeMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyLesMeet
     * @return RetDataBean
     * @Title: insertPartyLesMeet
     * @Description:  添加加三会一课
     */
    @RequestMapping(value = "/insertPartyLesMeet", method = RequestMethod.POST)
    public RetDataBean insertPartyLesMeet(PartyLesMeet partyLesMeet) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyLesMeet.setRecordId(SysTools.getGUID());
            partyLesMeet.setCreateUser(account.getAccountId());
            partyLesMeet.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyLesMeet.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyLesMeetService.insertPartyLesMeet(partyLesMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyLesMeet
     * @return RetDataBean
     * @Title: deletePartyLesMeet
     * @Description:  删除三会一课
     */
    @RequestMapping(value = "/deletePartyLesMeet", method = RequestMethod.POST)
    public RetDataBean deletePartyLesMeet(PartyLesMeet partyLesMeet) {
        try {
            if (StringUtils.isBlank(partyLesMeet.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyLesMeet.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyLesMeetService.deletePartyLesMeet(partyLesMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyLesMeet
     * @return RetDataBean
     * @Title: updatePartyLesMeet
     * @Description:  更新三会一课
     */
    @RequestMapping(value = "/updatePartyLesMeet", method = RequestMethod.POST)
    public RetDataBean updatePartyLesMeet(PartyLesMeet partyLesMeet) {
        try {
            if (StringUtils.isBlank(partyLesMeet.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyLesMeet.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyLesMeet.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyLesMeetService.updatePartyLesMeet(example, partyLesMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyCheckMeet
     * @return RetDataBean
     * @Title: insertPartyCheckMeet
     * @Description:  添加述评考记录
     */
    @RequestMapping(value = "/insertPartyCheckMeet", method = RequestMethod.POST)
    public RetDataBean insertPartyCheckMeet(PartyCheckMeet partyCheckMeet) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCheckMeet.setRecordId(SysTools.getGUID());
            partyCheckMeet.setCreateUser(account.getAccountId());
            partyCheckMeet.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyCheckMeet.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyCheckMeetService.insertPartyCheckMeet(partyCheckMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCheckMeet
     * @return RetDataBean
     * @Title: deletePartyCheckMeet
     * @Description:  删除述评考记录
     */
    @RequestMapping(value = "/deletePartyCheckMeet", method = RequestMethod.POST)
    public RetDataBean deletePartyCheckMeet(PartyCheckMeet partyCheckMeet) {
        try {
            if (StringUtils.isBlank(partyCheckMeet.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCheckMeet.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyCheckMeetService.deletePartyCheckMeet(partyCheckMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCheckMeet
     * @return RetDataBean
     * @Title: updatePartyCheckMeet
     * @Description:  更新述评考记录
     */
    @RequestMapping(value = "/updatePartyCheckMeet", method = RequestMethod.POST)
    public RetDataBean updatePartyCheckMeet(PartyCheckMeet partyCheckMeet) {
        try {
            if (StringUtils.isBlank(partyCheckMeet.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyCheckMeet.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyCheckMeet.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyCheckMeetService.updatePartyCheckMeet(example, partyCheckMeet));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTalk
     * @return RetDataBean
     * @Title: insertPartyTalk
     * @Description:  添加领导谈话
     */
    @RequestMapping(value = "/insertPartyTalk", method = RequestMethod.POST)
    public RetDataBean insertPartyTalk(PartyTalk partyTalk) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyTalk.setRecordId(SysTools.getGUID());
            partyTalk.setCreateUser(account.getAccountId());
            partyTalk.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyTalk.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyTalkService.insertPartyTalk(partyTalk));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTalk
     * @return RetDataBean
     * @Title: deletePartyTalk
     * @Description:  删除领导谈话
     */
    @RequestMapping(value = "/deletePartyTalk", method = RequestMethod.POST)
    public RetDataBean deletePartyTalk(PartyTalk partyTalk) {
        try {
            if (StringUtils.isBlank(partyTalk.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyTalk.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyTalkService.deletePartyTalk(partyTalk));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTalk
     * @return RetDataBean
     * @Title: updatePartyTalk
     * @Description:  删除领导谈话
     */
    @RequestMapping(value = "/updatePartyTalk", method = RequestMethod.POST)
    public RetDataBean updatePartyTalk(PartyTalk partyTalk) {
        try {
            if (StringUtils.isBlank(partyTalk.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyTalk.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyTalk.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyTalkService.updatePartyTalk(example, partyTalk));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTemporg
     * @return RetDataBean
     * @Title: insertPartyTemporg
     * @Description:  添加临时党组织
     */
    @RequestMapping(value = "/insertPartyTemporg", method = RequestMethod.POST)
    public RetDataBean insertPartyTemporg(PartyTemporg partyTemporg) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyTemporg.setRecordId(SysTools.getGUID());
            partyTemporg.setCreateUser(account.getAccountId());
            partyTemporg.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyTemporg.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyTemporgService.insertPartyTemporg(partyTemporg));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTemporg
     * @return RetDataBean
     * @Title: deletePartyTemporg
     * @Description:  修改临时党组织
     */
    @RequestMapping(value = "/deletePartyTemporg", method = RequestMethod.POST)
    public RetDataBean deletePartyTemporg(PartyTemporg partyTemporg) {
        try {
            if (StringUtils.isBlank(partyTemporg.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyTemporg.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyTemporgService.deletePartyTemporg(partyTemporg));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRename
     * @return RetDataBean
     * @Title: insertPartyRename
     * @Description:  添加党组织更名记录
     */
    @RequestMapping(value = "/insertPartyRename", method = RequestMethod.POST)
    public RetDataBean insertPartyRename(PartyRename partyRename) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRename.setRecordId(SysTools.getGUID());
            partyRename.setCreateUser(account.getAccountId());
            partyRename.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyRename.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyRenameService.insertPartyRename(partyRename));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRename
     * @return RetDataBean
     * @Title: deletePartyRename
     * @Description:  删除党组织更名记录
     */
    @RequestMapping(value = "/deletePartyRename", method = RequestMethod.POST)
    public RetDataBean deletePartyRename(PartyRename partyRename) {
        try {
            if (StringUtils.isBlank(partyRename.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRename.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyRenameService.deletePartyRename(partyRename));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRename
     * @return RetDataBean
     * @Title: updatePartyRename
     * @Description:  更新党组织更名记录
     */
    @RequestMapping(value = "/updatePartyRename", method = RequestMethod.POST)
    public RetDataBean updatePartyRename(PartyRename partyRename) {
        try {
            if (StringUtils.isBlank(partyRename.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyRename.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyRename.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyRenameService.updatePartyRename(example, partyRename));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRebuild
     * @return RetDataBean
     * @Title: insertPartyRebuild
     * @Description:  新建党组织改建记录
     */
    @RequestMapping(value = "/insertPartyRebuild", method = RequestMethod.POST)
    public RetDataBean insertPartyRebuild(PartyRebuild partyRebuild) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRebuild.setRecordId(SysTools.getGUID());
            partyRebuild.setCreateUser(account.getAccountId());
            partyRebuild.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyRebuild.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyRebuildService.insertPartyRebuild(partyRebuild));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRebuild
     * @return RetDataBean
     * @Title: deletePartyRebuild
     * @Description:  删除党组织改建记录
     */
    @RequestMapping(value = "/deletePartyRebuild", method = RequestMethod.POST)
    public RetDataBean deletePartyRebuild(PartyRebuild partyRebuild) {
        try {
            if (StringUtils.isBlank(partyRebuild.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRebuild.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyRebuildService.deletePartyRebuild(partyRebuild));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRebuild
     * @return RetDataBean
     * @Title: updatePartyRebuild
     * @Description:  更新党组织改建记录
     */
    @RequestMapping(value = "/updatePartyRebuild", method = RequestMethod.POST)
    public RetDataBean updatePartyRebuild(PartyRebuild partyRebuild) {
        try {
            if (StringUtils.isBlank(partyRebuild.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyRebuild.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyRebuild.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyRebuildService.updatePartyRebuild(example, partyRebuild));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRevoke
     * @return RetDataBean
     * @Title: insertPartyRevoke
     * @Description:  添加撤销的党组记录
     */
    @RequestMapping(value = "/insertPartyRevoke", method = RequestMethod.POST)
    public RetDataBean insertPartyRevoke(PartyRevoke partyRevoke) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRevoke.setRecordId(SysTools.getGUID());
            partyRevoke.setCreateUser(account.getAccountId());
            partyRevoke.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyRevoke.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyRevokeService.insertPartyRevoke(partyRevoke));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRevoke
     * @return RetDataBean
     * @Title: deletePartyRevoke
     * @Description:  删除撤销的党组记录
     */
    @RequestMapping(value = "/deletePartyRevoke", method = RequestMethod.POST)
    public RetDataBean deletePartyRevoke(PartyRevoke partyRevoke) {
        try {
            if (StringUtils.isBlank(partyRevoke.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRevoke.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyRevokeService.deletePartyRevoke(partyRevoke));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRevoke
     * @return RetDataBean
     * @Title: updatePartyRevoke
     * @Description:  更新撤销的党组信息
     */
    @RequestMapping(value = "/updatePartyRevoke", method = RequestMethod.POST)
    public RetDataBean updatePartyRevoke(PartyRevoke partyRevoke) {
        try {
            if (StringUtils.isBlank(partyRevoke.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyRevoke.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyRevoke.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyRevokeService.updatePartyRevoke(example, partyRevoke));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyTemporg
     * @return RetDataBean
     * @Title: updatePartyTemporg
     * @Description:  更新临时党组织
     */
    @RequestMapping(value = "/updatePartyTemporg", method = RequestMethod.POST)
    public RetDataBean updatePartyTemporg(PartyTemporg partyTemporg) {
        try {
            if (StringUtils.isBlank(partyTemporg.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyTemporg.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyTemporg.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyTemporgService.updatePartyTemporg(example, partyTemporg));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyElection
     * @return RetDataBean
     * @Title: insertPartyElection
     * @Description:  创建选举届次信息
     */
    @RequestMapping(value = "/insertPartyElection", method = RequestMethod.POST)
    public RetDataBean insertPartyElection(PartyElection partyElection) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyElection.setRecordId(SysTools.getGUID());
            partyElection.setCreateUser(account.getAccountId());
            partyElection.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyElection.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyElectionService.insertPartyElection(partyElection));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyElection
     * @return RetDataBean
     * @Title: deletePartyElection
     * @Description:  删除届次信息
     */
    @RequestMapping(value = "/deletePartyElection", method = RequestMethod.POST)
    public RetDataBean deletePartyElection(PartyElection partyElection) {
        try {
            if (StringUtils.isBlank(partyElection.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyElection.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyElectionService.deletePartyElection(partyElection));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyElection
     * @return RetDataBean
     * @Title: updatePartyElection
     * @Description:  更新届次信息
     */
    @RequestMapping(value = "/updatePartyElection", method = RequestMethod.POST)
    public RetDataBean updatePartyElection(PartyElection partyElection) {
        try {
            if (StringUtils.isBlank(partyElection.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyElection.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyElection.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyElectionService.updatePartyElection(example, partyElection));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCommittee
     * @return RetDataBean
     * @Title: insertPartyCommittee
     * @Description:  添加委员记录
     */
    @RequestMapping(value = "/insertPartyCommittee", method = RequestMethod.POST)
    public RetDataBean insertPartyCommittee(PartyCommittee partyCommittee) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCommittee.setRecordId(SysTools.getGUID());
            partyCommittee.setCreateUser(account.getAccountId());
            partyCommittee.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyCommittee.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyCommitteeService.insertPartyCommittee(partyCommittee));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCommittee
     * @return RetDataBean
     * @Title: deletePartyCommittee
     * @Description:  删除委员信息
     */
    @RequestMapping(value = "/deletePartyCommittee", method = RequestMethod.POST)
    public RetDataBean deletePartyCommittee(PartyCommittee partyCommittee) {
        try {
            if (StringUtils.isBlank(partyCommittee.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCommittee.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyCommitteeService.deletePartyCommittee(partyCommittee));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCommittee
     * @return RetDataBean
     * @Title: updatePartyCommittee
     * @Description:  更新委员信息
     */
    @RequestMapping(value = "/updatePartyCommittee", method = RequestMethod.POST)
    public RetDataBean updatePartyCommittee(PartyCommittee partyCommittee) {
        try {
            if (StringUtils.isBlank(partyCommittee.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyCommittee.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", partyCommittee.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyCommitteeService.updatePartyCommittee(example, partyCommittee));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyUnitBase
     * @return RetDataBean
     * @Title: insertPartyUnitBase
     * @Description:  添加单位基本信息
     */
    @RequestMapping(value = "/insertPartyUnitBase", method = RequestMethod.POST)
    public RetDataBean insertPartyUnitBase(PartyUnitBase partyUnitBase) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyUnitBase.getSubordinateUnit())) {
                partyUnitBase.setSubordinateUnit("0");
            }
            partyUnitBase.setUnitId(SysTools.getGUID());
            partyUnitBase.setCreateUser(account.getAccountId());
            partyUnitBase.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyUnitBase.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyUnitBaseService.insertPartyUnitBase(partyUnitBase));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyUnitBase
     * @return RetDataBean
     * @Title: deletePartyUnitBase
     * @Description:  删除单位基本信息
     */
    @RequestMapping(value = "/deletePartyUnitBase", method = RequestMethod.POST)
    public RetDataBean deletePartyUnitBase(PartyUnitBase partyUnitBase) {
        try {
            if (StringUtils.isBlank(partyUnitBase.getUnitId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyUnitBase.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyUnitBaseService.deletePartyUnitBase(partyUnitBase));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyUnitBase
     * @return RetDataBean
     * @Title: updatePartyUnitBase
     * @Description:  更新单位基本信息
     */
    @RequestMapping(value = "/updatePartyUnitBase", method = RequestMethod.POST)
    public RetDataBean updatePartyUnitBase(PartyUnitBase partyUnitBase) {
        try {
            if (StringUtils.isBlank(partyUnitBase.getUnitId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyUnitBase.class);
            if (StringUtils.isBlank(partyUnitBase.getSubordinateUnit())) {
                partyUnitBase.setSubordinateUnit("0");
            }
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("unitId", partyUnitBase.getUnitId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyUnitBaseService.updatePartyUnitBase(example, partyUnitBase));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyOrg
     * @return RetDataBean
     * @Title: insertPartyOrg
     * @Description:  添加党组织
     */
    @RequestMapping(value = "/insertPartyOrg", method = RequestMethod.POST)
    public RetDataBean insertPartyOrg(PartyOrg partyOrg) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(partyOrg.getLevelId())) {
                partyOrg.setLevelId("0");
            }
            partyOrg.setPartyOrgId(SysTools.getGUID());
            partyOrg.setCreateUser(account.getAccountId());
            partyOrg.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            partyOrg.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, partyOrgService.insertPartyOrg(partyOrg));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyOrg
     * @return RetDataBean
     * @Title: deletePartyOrg
     * @Description:  删除党组织
     */
    @RequestMapping(value = "/deletePartyOrg", method = RequestMethod.POST)
    public RetDataBean deletePartyOrg(PartyOrg partyOrg) {
        try {
            if (StringUtils.isBlank(partyOrg.getPartyOrgId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            partyOrg.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, partyOrgService.deletePartyOrg(partyOrg));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyOrg
     * @return RetDataBean
     * @Title: updatePartyOrg
     * @Description:  更新党组织
     */
    @RequestMapping(value = "/updatePartyOrg", method = RequestMethod.POST)
    public RetDataBean updatePartyOrg(PartyOrg partyOrg) {
        try {
            if (partyOrg.getPartyOrgId().equals(partyOrg.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(partyOrg.getPartyOrgId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyOrg.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("partyOrgId", partyOrg.getPartyOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, partyOrgService.updatePartyOrg(example, partyOrg));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
