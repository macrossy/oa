package com.core136.controller.party;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.party.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.party.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ret/partyget")
public class RouteGetPartyController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private PartyRulesLearnService partyRulesLearnService;

    @Autowired
    public void setPartyRulesLearnService(PartyRulesLearnService partyRulesLearnService) {
        this.partyRulesLearnService = partyRulesLearnService;
    }

    private PartyRulesRecordService partyRulesRecordService;

    @Autowired
    public void setPartyRulesRecordService(PartyRulesRecordService partyRulesRecordService) {
        this.partyRulesRecordService = partyRulesRecordService;
    }

    private PartyRulesSortService partyRulesSortService;

    @Autowired
    public void setPartyRulesSortService(PartyRulesSortService partyRulesSortService) {
        this.partyRulesSortService = partyRulesSortService;
    }

    private PartyAlbumTypeService partyAlbumTypeService;

    @Autowired
    public void setPartyAlbumTypeService(PartyAlbumTypeService partyAlbumTypeService) {
        this.partyAlbumTypeService = partyAlbumTypeService;
    }

    private PartyAlbumVideoService partyAlbumVideoService;

    @Autowired
    public void setPartyAlbumVideoService(PartyAlbumVideoService partyAlbumVideoService) {
        this.partyAlbumVideoService = partyAlbumVideoService;
    }

    private PartyAlbumHomeService partyAlbumHomeService;

    @Autowired
    public void setPartyAlbumHomeService(PartyAlbumHomeService partyAlbumHomeService) {
        this.partyAlbumHomeService = partyAlbumHomeService;
    }

    private PartyAlbumConfigService partyAlbumConfigService;

    @Autowired
    public void setPartyAlbumConfigService(PartyAlbumConfigService partyAlbumConfigService) {
        this.partyAlbumConfigService = partyAlbumConfigService;
    }

    private PartyAlbumSubscribeService partyAlbumSubscribeService;

    @Autowired
    public void setPartyAlbumSubscribeService(PartyAlbumSubscribeService partyAlbumSubscribeService) {
        this.partyAlbumSubscribeService = partyAlbumSubscribeService;
    }

    private PartyAlbumCommentsService partyAlbumCommentsService;

    @Autowired
    public void setPartyAlbumCommentsService(PartyAlbumCommentsService partyAlbumCommentsService) {
        this.partyAlbumCommentsService = partyAlbumCommentsService;
    }

    private PartyAlbumHistoryService partyAlbumHistoryService;

    @Autowired
    public void setPartyAlbumHistoryService(PartyAlbumHistoryService partyAlbumHistoryService) {
        this.partyAlbumHistoryService = partyAlbumHistoryService;
    }

    private PartyDuesService partyDuesService;

    @Autowired
    public void setPartyDuesService(PartyDuesService partyDuesService) {
        this.partyDuesService = partyDuesService;
    }

    private PartyCleanExposeService partyCleanExposeService;

    @Autowired
    public void setPartyCleanExposeService(PartyCleanExposeService partyCleanExposeService) {
        this.partyCleanExposeService = partyCleanExposeService;
    }

    private PartyIntegralService partyIntegralService;

    @Autowired
    public void setPartyIntegralService(PartyIntegralService partyIntegralService) {
        this.partyIntegralService = partyIntegralService;
    }

    private PartyCleanMeritoriousService partyCleanMeritoriousService;

    @Autowired
    public void setPartyCleanMeritoriousService(PartyCleanMeritoriousService partyCleanMeritoriousService) {
        this.partyCleanMeritoriousService = partyCleanMeritoriousService;
    }

    private PartyCleanSuggestionsService partyCleanSuggestionsService;

    @Autowired
    public void setPartyCleanSuggestionsService(PartyCleanSuggestionsService partyCleanSuggestionsService) {
        this.partyCleanSuggestionsService = partyCleanSuggestionsService;
    }

    private PartyCleanTipoffsService partyCleanTipoffsService;

    @Autowired
    public void setPartyCleanTipoffsService(PartyCleanTipoffsService partyCleanTipoffsService) {
        this.partyCleanTipoffsService = partyCleanTipoffsService;
    }

    private PartyCleanReportService partyCleanReportService;

    @Autowired
    public void setPartyCleanReportService(PartyCleanReportService partyCleanReportService) {
        this.partyCleanReportService = partyCleanReportService;
    }

    private PartyCleanCompreService partyCleanCompreService;

    @Autowired
    public void setPartyCleanCompreService(PartyCleanCompreService partyCleanCompreService) {
        this.partyCleanCompreService = partyCleanCompreService;
    }

    private PartyPayStandardService partyPayStandardService;

    @Autowired
    public void setPartyPayStandardService(PartyPayStandardService partyPayStandardService) {
        this.partyPayStandardService = partyPayStandardService;
    }

    private PartyFundsService partyFundsService;

    @Autowired
    public void setPartyFundsService(PartyFundsService partyFundsService) {
        this.partyFundsService = partyFundsService;
    }

    private PartyBusinessGuideService partyBusinessGuideService;

    @Autowired
    public void setPartyBusinessGuideService(PartyBusinessGuideService partyBusinessGuideService) {
        this.partyBusinessGuideService = partyBusinessGuideService;
    }

    private PartyNewsService partyNewsService;

    @Autowired
    public void setPartyNewsService(PartyNewsService partyNewsService) {
        this.partyNewsService = partyNewsService;
    }

    private PartyOrgPointsService partyOrgPointsService;

    @Autowired
    public void setPartyOrgPointsService(PartyOrgPointsService partyOrgPointsService) {
        this.partyOrgPointsService = partyOrgPointsService;
    }

    /**
     * 获取组织积分详情
     *
     * @param partyOrgPoints
     * @return
     */
    @RequestMapping(value = "/getPartyOrgPointsById", method = RequestMethod.POST)
    public RetDataBean getPartyOrgPointsById(PartyOrgPoints partyOrgPoints) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyOrgPoints.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyOrgPointsService.selectOnePartyOrgPoints(partyOrgPoints));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取党组织积分列表
     *
     * @param pageParam
     * @param pointsYear
     * @param partyOrgId
     * @return
     */
    @RequestMapping(value = "/getOrgPointsList", method = RequestMethod.POST)
    public RetDataBean getOrgPointsList(PageParam pageParam, String pointsYear, String partyOrgId) {
        if (StringUtils.isBlank(pointsYear)) {
            pointsYear = SysTools.getTime("yyyy");
        }
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("O.POINTS_YEAR");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("DESC");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyOrgPointsService.getOrgPointsList(pageParam, pointsYear, partyOrgId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取党组织汇总积分列表
     *
     * @param pageParam
     * @param pointsYear
     * @param partyOrgId
     * @return
     */
    @RequestMapping(value = "/getOrgPointsTotalList", method = RequestMethod.POST)
    public RetDataBean getOrgPointsTotalList(PageParam pageParam, String pointsYear, String partyOrgId) {
        if (StringUtils.isBlank(pointsYear)) {
            pointsYear = SysTools.getTime("yyyy");
        }
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("O.POINTS_YEAR");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("DESC");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyOrgPointsService.getOrgPointsTotalList(pageParam, pointsYear, partyOrgId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMyPartyNewsList
     * @Description:  获取个人新闻列表
     * @param: request
     * @param: pageParam
     * @param: newsType
     * @param: status
     * @param: beginTime
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMyPartyNewsList", method = RequestMethod.POST)
    public RetDataBean getMyPartyNewsList(
            PageParam pageParam,
            String newsType,
            String status,
            String beginTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            String endTime = SysTools.getTime("yyyy-MM-dd");
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setOrderBy(orderBy);
            PageInfo<Map<String, Object>> pageInfo = partyNewsService.getMyPartyNewsList(pageParam, userInfo.getDeptId(), userInfo.getLeadLevel(), newsType, status, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param year
     * @return RetDataBean
     * @Title: getMyPartyDeusList
     * @Description:  获取党员缴费列表
     */
    @RequestMapping(value = "/getMyPartyDeusList", method = RequestMethod.POST)
    public RetDataBean getMyPartyDeusList(String year) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyDuesService.getMyPartyDeusList(account.getOrgId(), account.getMemberId(), year));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param newsType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getPartyNewsList
     * @Description:  获取党建新闻列表
     */
    @RequestMapping(value = "/getPartyNewsList", method = RequestMethod.POST)
    public RetDataBean getPartyNewsList(PageParam pageParam, String newsType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.send_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyNewsService.getPartyNewsList(pageParam, newsType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyNews
     * @return RetDataBean
     * @Title: getPartyNewsById
     * @Description:  党建新闻详情
     */
    @RequestMapping(value = "/getPartyNewsById", method = RequestMethod.POST)
    public RetDataBean getPartyNewsById(PartyNews partyNews) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyNews.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyNewsService.selectOnePartyNews(partyNews));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyBusinessGuide
     * @return RetDataBean
     * @Title: getPartyBusinessGuideById
     * @Description:  获取业务指南详情
     */
    @RequestMapping(value = "/getPartyBusinessGuideById", method = RequestMethod.POST)
    public RetDataBean getPartyBusinessGuideById(PartyBusinessGuide partyBusinessGuide) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyBusinessGuide.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyBusinessGuideService.selectOnePartyBusinessGuide(partyBusinessGuide));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param guideType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getPartyBusinessGuideList
     * @Description:  获取业务指南列表
     */
    @RequestMapping(value = "/getPartyBusinessGuideList", method = RequestMethod.POST)
    public RetDataBean getPartyBusinessGuideList(PageParam pageParam, String guideType, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("o.sort_no");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyBusinessGuideService.getPartyBusinessGuideList(pageParam, guideType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyFunds
     * @return RetDataBean
     * @Title: getPartyFundsById
     * @Description:  获取经费申请详情
     */
    @RequestMapping(value = "/getPartyFundsById", method = RequestMethod.POST)
    public RetDataBean getPartyFundsById(PartyFunds partyFunds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyFunds.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyFundsService.selectOnePartyFunds(partyFunds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param fundsType
     * @param applyUser
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getApprovalFundsOldList
     * @Description:  获取经费历史审批记录
     */
    @RequestMapping(value = "/getApprovalFundsOldList", method = RequestMethod.POST)
    public RetDataBean getApprovalFundsOldList(PageParam pageParam, String status, String fundsType, String applyUser, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyFundsService.getApprovalFundsOldList(pageParam, status, fundsType, applyUser, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param fundsType
     * @param applyUser
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getApprovalFundsList
     * @Description:  获取审批列表
     */
    @RequestMapping(value = "/getApprovalFundsList", method = RequestMethod.POST)
    public RetDataBean getApprovalFundsList(PageParam pageParam, String fundsType, String applyUser, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyFundsService.getApprovalFundsList(pageParam, fundsType, applyUser, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param fundsType
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getGovFundsList
     * @Description:  获取经费申请列表
     */
    @RequestMapping(value = "/getGovFundsList", method = RequestMethod.POST)
    public RetDataBean getGovFundsList(PageParam pageParam, String status, String fundsType, String approvalUser, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyFundsService.getGovFundsList(pageParam, status, fundsType, approvalUser, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyPayStandard
     * @return RetDataBean
     * @Title: getPartyPayStandardById
     * @Description: 党费缴纳标准详情
     */
    @RequestMapping(value = "/getPartyPayStandardById", method = RequestMethod.POST)
    public RetDataBean getPartyPayStandardById(PartyPayStandard partyPayStandard) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyPayStandard.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyPayStandardService.selectOnePartyPayStandard(partyPayStandard));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getPayStandardList
     * @Description:  获取党费缴纳标准列表
     */
    @RequestMapping(value = "/getPayStandardList", method = RequestMethod.POST)
    public RetDataBean getGovPayStandardList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyPayStandardService.getPayStandardList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyCleanTipoffsList
     * @Description:  获取更多举报记录
     */
    @RequestMapping(value = "/getMyCleanTipoffsList", method = RequestMethod.POST)
    public RetDataBean getMyCleanTipoffsList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyCleanTipoffsService.getMyCleanTipoffsList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyCleanComprehensionList
     * @Description:  获取更多学习感悟
     */
    @RequestMapping(value = "/getMyCleanComprehensionList", method = RequestMethod.POST)
    public RetDataBean getMyCleanComprehensionList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyCleanCompreService.getMyCleanComprehensionList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyCleanSuggestionsList
     * @Description:  获取更多计策记录
     */
    @RequestMapping(value = "/getMyCleanSuggestionsList", method = RequestMethod.POST)
    public RetDataBean getMyCleanSuggestionsList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("s.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyCleanSuggestionsService.getMyCleanSuggestionsList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyCleanReportList
     * @Description:  获取更多领导报告列表
     */
    @RequestMapping(value = "/getMyCleanReportList", method = RequestMethod.POST)
    public RetDataBean getMyCleanReportList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyCleanReportService.getMyCleanReportList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyCleanMeritoriousList
     * @Description:  获取更多先进事迹
     */
    @RequestMapping(value = "/getMyCleanMeritoriousList", method = RequestMethod.POST)
    public RetDataBean getMyCleanMeritoriousList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setDeptId(userInfo.getDeptId());
            pageParam.setLevelId(userInfo.getLeadLevel());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyCleanMeritoriousService.getMyCleanMeritoriousList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getMyCleanExposeList
     * @Description:  获取更多曝光记录
     */
    @RequestMapping(value = "/getMyCleanExposeList", method = RequestMethod.POST)
    public RetDataBean getMyCleanExposeList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyCleanExposeService.getMyCleanExposeList(pageParam, account.getMemberId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @return RetDataBean
     * @Title: getMyCleanTipoffsListForPortal
     * @Description:  获取门户举报列表
     */
    @RequestMapping(value = "/getMyCleanTipoffsListForPortal", method = RequestMethod.POST)
    public RetDataBean getMyCleanTipoffsListForPortal() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyCleanTipoffsService.getMyCleanTipoffsListForPortal(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyCleanComprehensionListForPortal
     * @Description:  获取门户学习感悟列表
     */
    @RequestMapping(value = "/getMyCleanComprehensionListForPortal", method = RequestMethod.POST)
    public RetDataBean getMyCleanComprehensionListForPortal() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyCleanCompreService.getMyCleanComprehensionListForPortal(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyCleanSuggestionsListForPortal
     * @Description:  获取门户计策列表
     */
    @RequestMapping(value = "/getMyCleanSuggestionsListForPortal", method = RequestMethod.POST)
    public RetDataBean getMyCleanSuggestionsListForPortal() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyCleanSuggestionsService.getMyCleanSuggestionsListForPortal(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyCleanReportListForPortal
     * @Description:  获取门户领导报告列表
     */
    @RequestMapping(value = "/getMyCleanReportListForPortal", method = RequestMethod.POST)
    public RetDataBean getMyCleanReportListForPortal() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyCleanReportService.getMyCleanReportListForPortal(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyCleanMeritoriousListForPortal
     * @Description:  获门户先进事迹列表
     */
    @RequestMapping(value = "/getMyCleanMeritoriousListForPortal", method = RequestMethod.POST)
    public RetDataBean getMyCleanMeritoriousListForPortal() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyCleanMeritoriousService.getMyCleanMeritoriousListForPortal(account.getOrgId(), account.getMemberId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getMyCleanExposeListForPortal
     * @Description:  获取门户曝光记录列表
     */
    @RequestMapping(value = "/getMyCleanExposeListForPortal", method = RequestMethod.POST)
    public RetDataBean getMyCleanExposeListForPortal() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyCleanExposeService.getMyCleanExposeListForPortal(account.getOrgId(), account.getMemberId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanCompre
     * @return RetDataBean
     * @Title: getPartyCleanCompreById
     * @Description:  学习感悟详情
     */
    @RequestMapping(value = "/getPartyCleanCompreById", method = RequestMethod.POST)
    public RetDataBean getPartyCleanCompreById(PartyCleanCompre partyCleanCompre) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanCompre.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyCleanCompreService.selectOnePartyCleanCompre(partyCleanCompre));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param comprehensionType
     * @param status
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getCleanComprehensionList
     * @Description:  获取感悟详情
     */
    @RequestMapping(value = "/getCleanComprehensionList", method = RequestMethod.POST)
    public RetDataBean getCleanComprehensionList(PageParam pageParam, String comprehensionType, String status, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyCleanCompreService.getCleanComprehensionList(pageParam, comprehensionType, status, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param reportType
     * @param status
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getCleanReportList
     * @Description:  获取报告列表
     */
    @RequestMapping(value = "/getCleanReportList", method = RequestMethod.POST)
    public RetDataBean getCleanReportList(PageParam pageParam, String reportType, String status, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyCleanReportService.getCleanReportList(pageParam, reportType, status, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanReport
     * @return RetDataBean
     * @Title: getPartyCleanReportById
     * @Description:  报告详情
     */
    @RequestMapping(value = "/getPartyCleanReportById", method = RequestMethod.POST)
    public RetDataBean getPartyCleanReportById(PartyCleanReport partyCleanReport) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanReport.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyCleanReportService.selectOnePartyCleanReport(partyCleanReport));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param tipoffsType
     * @param status
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getCleanTipoffsList
     * @Description:  获取举报列表
     */
    @RequestMapping(value = "/getCleanTipoffsList", method = RequestMethod.POST)
    public RetDataBean getCleanTipoffsList(PageParam pageParam, String tipoffsType, String status, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyCleanTipoffsService.getCleanTipoffsList(pageParam, tipoffsType, status, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanTipoffs
     * @return RetDataBean
     * @Title: getPartyCleanTipoffsById
     * @Description:  获取举报记录详情
     */
    @RequestMapping(value = "/getPartyCleanTipoffsById", method = RequestMethod.POST)
    public RetDataBean getPartyCleanTipoffsById(PartyCleanTipoffs partyCleanTipoffs) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanTipoffs.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyCleanTipoffsService.selectOnePartyCleanTipoffs(partyCleanTipoffs));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param suggestionsType
     * @param status
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getCleanSuggestionsList
     * @Description:  获取计策列表
     */
    @RequestMapping(value = "/getCleanSuggestionsList", method = RequestMethod.POST)
    public RetDataBean getCleanSuggestionsList(PageParam pageParam, String suggestionsType, String status, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("s.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyCleanSuggestionsService.getCleanSuggestionsList(pageParam, suggestionsType, status, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanSuggestions
     * @return RetDataBean
     * @Title: getPartyCleanSuggestionsById
     * @Description:  获取献计献策详情
     */
    @RequestMapping(value = "/getPartyCleanSuggestionsById", method = RequestMethod.POST)
    public RetDataBean getPartyCleanSuggestionsById(PartyCleanSuggestions partyCleanSuggestions) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanSuggestions.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyCleanSuggestionsService.selectOnePartyCleanSuggestions(partyCleanSuggestions));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param meritoriousType
     * @param status
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getCleanMeritoriousList
     * @Description:  获取先进事迹列表
     */
    @RequestMapping(value = "/getCleanMeritoriousList", method = RequestMethod.POST)
    public RetDataBean getCleanMeritoriousList(PageParam pageParam, String meritoriousType, String status, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyCleanMeritoriousService.getCleanMeritoriousList(pageParam, meritoriousType, status, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyCleanMeritorious
     * @return RetDataBean
     * @Title: getPartyCleanMeritoriousById
     * @Description:  获取先进事迹详情
     */
    @RequestMapping(value = "/getPartyCleanMeritoriousById", method = RequestMethod.POST)
    public RetDataBean getPartyCleanMeritoriousById(PartyCleanMeritorious partyCleanMeritorious) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanMeritorious.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyCleanMeritoriousService.selectOnePartyCleanMeritorious(partyCleanMeritorious));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getCleanExposeList
     * @Description:  获取曝光台列表
     */
    @RequestMapping(value = "/getCleanExposeList", method = RequestMethod.POST)
    public RetDataBean getCleanExposeList(PageParam pageParam, String exposeType, String status, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyCleanExposeService.getCleanExposeList(pageParam, exposeType, status, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param year
     * @param partyOrgId
     * @return RetDataBean
     * @Title: getPartyVerifyAllList
     * @Description:  获取所有考核记录
     */
    @RequestMapping(value = "/getPartyVerifyAllList", method = RequestMethod.POST)
    public RetDataBean getPartyVerifyAllList(PageParam pageParam, String year, String partyOrgId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("i.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyIntegralService.getPartyVerifyAllList(pageParam, partyOrgId, year);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyIntegral
     * @return RetDataBean
     * @Title: getPartyIntegralById
     * @Description:  获取党员考核记录详情
     */
    @RequestMapping(value = "/getPartyIntegralById", method = RequestMethod.POST)
    public RetDataBean getPartyIntegralById(PartyIntegral partyIntegral) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyIntegral.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyIntegralService.selectOnePartyIntegral(partyIntegral));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyCleanExpose
     * @return RetDataBean
     * @Title: getPartyCleanExposeById
     * @Description:  获取曝光记录详情
     */
    @RequestMapping(value = "/getPartyCleanExposeById", method = RequestMethod.POST)
    public RetDataBean getPartyCleanExposeById(PartyCleanExpose partyCleanExpose) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyCleanExpose.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyCleanExposeService.selectOnePartyCleanExpose(partyCleanExpose));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param duesType
     * @param year
     * @return RetDataBean
     * @Title: getPartyDeusList
     * @Description:  获取党员缴费列表
     */
    @RequestMapping(value = "/getPartyDeusList", method = RequestMethod.POST)
    public RetDataBean getPartyDeusList(PageParam pageParam, String onTime, String duesType, String year) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("j.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyDuesService.getPartyDeusList(pageParam, onTime, duesType, year);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyDues
     * @return RetDataBean
     * @Title: getPartyDuesById
     * @Description:  获取党费详情
     */
    @RequestMapping(value = "/getPartyDuesById", method = RequestMethod.POST)
    public RetDataBean getPartyDuesById(PartyDues partyDues) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyDues.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyDuesService.selectOnePartyDues(partyDues));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getAlbumVideoByHistory
     * @Description:  获取当前用户历史记录
     */
    @RequestMapping(value = "/getAlbumVideoByHistory", method = RequestMethod.POST)
    public RetDataBean getAlbumVideoByHistory() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAlbumVideoService.getAlbumVideoByHistory(account.getOrgId(), account.getAccountId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param albumType
     * @return RetDataBean
     * @Title: getAlbumVideoByAlbumType
     * @Description:  按分类获取视频
     */
    @RequestMapping(value = "/getAlbumVideoByAlbumType", method = RequestMethod.POST)
    public RetDataBean getAlbumVideoByAlbumType(

            PageParam pageParam,
            String albumType
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("v.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyAlbumVideoService.getAlbumVideoByAlbumType(pageParam, albumType, account.getMemberId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param videoId
     * @return RetDataBean
     * @Title: getAlbumCommentsList
     * @Description:  获取专辑评论列表
     */
    @RequestMapping(value = "/getAlbumCommentsList", method = RequestMethod.POST)
    public RetDataBean getAlbumCommentsList(String videoId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAlbumCommentsService.getAlbumCommentsList(account.getOrgId(), videoId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getAlbumVideoByCreateUser
     * @Description:  获取当前用户最近10条视频
     */
    @RequestMapping(value = "/getAlbumVideoByCreateUser", method = RequestMethod.POST)
    public RetDataBean getAlbumVideoByCreateUser(PartyAlbumVideo partyAlbumVideo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumVideo.setOrgId(account.getOrgId());
            partyAlbumVideo = partyAlbumVideoService.selectOnePartyAlbumVideo(partyAlbumVideo);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAlbumVideoService.getAlbumVideoByCreateUser(account.getOrgId(), partyAlbumVideo.getCreateUser()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getApprovelUserList
     * @Description:  获取审批人员列表
     */
    @RequestMapping(value = "/getApprovelUserList", method = RequestMethod.POST)
    public RetDataBean getApprovelUserList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAlbumConfigService.getApprovelUserList(account));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param status
     * @param albumType
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getAlbumVideoForApprovelOld
     * @Description:  获取历史审批记录
     */
    @RequestMapping(value = "/getAlbumVideoForApprovelOld", method = RequestMethod.POST)
    public RetDataBean getAlbumVideoForApprovelOld(PageParam pageParam, String status, String albumType, String createUser, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("v.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyAlbumVideoService.getAlbumVideoForApprovelOld(pageParam, status, beginTime, endTime, albumType, createUser);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param albumType
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getAlbumVideoForApprovel
     * @Description:  获取待审批列表
     */
    @RequestMapping(value = "/getAlbumVideoForApprovel", method = RequestMethod.POST)
    public RetDataBean getAlbumVideoForApprovel(PageParam pageParam, String albumType, String createUser, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("v.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyAlbumVideoService.getAlbumVideoForApprovel(pageParam, beginTime, endTime, albumType, createUser);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    @RequestMapping(value = "/getAlbumVideoForManage", method = RequestMethod.POST)
    public RetDataBean getAlbumVideoForManage(PageParam pageParam, String albumType, String createUser, String beginTime, String endTime,String status) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("v.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyAlbumVideoService.getAlbumVideoForManage(pageParam, beginTime, endTime, albumType, createUser,status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getAlbumVideoForHome
     * @Description:  获取专辑首页视频列表
     */
    @RequestMapping(value = "/getAlbumVideoForHome", method = RequestMethod.POST)
    public RetDataBean getAlbumVideoForHome(String albumType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAlbumVideoService.getAlbumVideoForHome(account.getOrgId(), albumType, account.getMemberId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getSendVideoUserList
     * @Description:  获取所有发布视频的人员列表前10
     */
    @RequestMapping(value = "/getSendVideoUserList", method = RequestMethod.POST)
    public RetDataBean getSendVideoUserList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAlbumVideoService.getSendVideoUserList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getSubscribeMyUserList
     * @Description:  获取关注我的人员列表
     */
    @RequestMapping(value = "/getSubscribeMyUserList", method = RequestMethod.POST)
    public RetDataBean getSubscribleMyUserList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAlbumSubscribeService.getSubscribeMyUserList(account.getOrgId(), account.getAccountId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getSubscribeUserListAll
     * @Description:  获取所有关注人员
     */
    @RequestMapping(value = "/getSubscribeUserListAll", method = RequestMethod.POST)
    public RetDataBean getSubscribeUserListAll() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAlbumSubscribeService.getSubscribeUserListAll(account));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getSubscribeUserList
     * @Description:  获取个人订阅人员列表
     */
    @RequestMapping(value = "/getSubscribeUserList", method = RequestMethod.POST)
    public RetDataBean getSubscribeUserList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAlbumSubscribeService.getSubscribeUserList(account));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getAlbumVideoForHomeTop
     * @Description:  获取热榜TOP10
     */
    @RequestMapping(value = "/getAlbumVideoForHomeTop", method = RequestMethod.POST)
    public RetDataBean getAlbumVedioForHomeTop() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAlbumVideoService.getAlbumVideoForHomeTop(account.getOrgId(), account.getMemberId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getAlbumTypeFroSelect
     * @Description:  获取专辑分类
     */
    @RequestMapping(value = "/getAlbumTypeForSelect", method = RequestMethod.POST)
    public RetDataBean getAlbumTypeForSelect() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAlbumTypeService.getAlbumTypeForSelect(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取分类名称
     * @param albumTypeId
     * @return
     */
    @RequestMapping(value = "/getAlbumTypeName", method = RequestMethod.POST)
    public RetDataBean getAlbumTypeName(String albumTypeId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAlbumTypeService.getAlbumTypeName(account.getOrgId(),albumTypeId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyAlbumConfig
     * @return RetDataBean
     * @Title: getPartyAlbumConfigById
     * @Description:  获取专辑参数详情
     */
    @RequestMapping(value = "/getPartyAlbumConfigById", method = RequestMethod.POST)
    public RetDataBean getPartyAlbumConfigById(PartyAlbumConfig partyAlbumConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAlbumConfigService.selectOnePartyAlbumConfig(partyAlbumConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param partyAlbumHome
     * @return RetDataBean
     * @Title: getPartyAlbumHomeById
     * @Description:  获取首页设置详情
     */
    @RequestMapping(value = "/getPartyAlbumHomeById", method = RequestMethod.POST)
    public RetDataBean getPartyAlbumHomeById(PartyAlbumHome partyAlbumHome) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumHome.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAlbumHomeService.selectOnePartyAlbumHome(partyAlbumHome));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @return RetDataBean
     * @Title: getPartyAlbumTypeList
     * @Description:  获取专辑分类列表
     */
    @RequestMapping(value = "/getPartyAlbumTypeList", method = RequestMethod.POST)
    public RetDataBean getPartyAlbumTypeList(

            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "sort_no";
            } else {
                sort = StrTools.upperCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(PartyAlbumType.class);
            example.setOrderByClause(orderBy);
            Criteria criteria = example.createCriteria();
            criteria.andEqualTo("orgId", account.getOrgId());
            if (StringUtils.isNotEmpty(search)) {
                Criteria criteria2 = example.createCriteria();
                criteria2.orLike("typeName", "%" + search + "%").orLike("remark", "%" + search + "%");
                example.and(criteria2);
            }
            PageInfo<PartyAlbumType> pageInfo = partyAlbumTypeService.getPartyAlbumTypeList(example, pageNumber, pageSize);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAlbumVideo
     * @return RetDataBean
     * @Title: getPartyAlbumVideoById
     * @Description:  获取视频专辑详情
     */
    @RequestMapping(value = "/getPartyAlbumVideoById", method = RequestMethod.POST)
    public RetDataBean getPartyAlbumVideoById(PartyAlbumVideo partyAlbumVideo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumVideo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAlbumVideoService.selectOnePartyAlbumVideo(partyAlbumVideo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyAlbumType
     * @return RetDataBean
     * @Title: getPartyAlbumTypeById
     * @Description:  获取视频类型详情
     */
    @RequestMapping(value = "/getPartyAlbumTypeById", method = RequestMethod.POST)
    public RetDataBean getPartyAlbumTypeById(PartyAlbumType partyAlbumType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyAlbumType.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyAlbumTypeService.selectOnePartyAlbumType(partyAlbumType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getLearnRecordList
     * @Description:  查询学习记录
     */
    @RequestMapping(value = "/getLearnRecordList", method = RequestMethod.POST)
    public RetDataBean getLearnRecordList(PageParam pageParam, String accountId, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("l.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyRulesLearnService.getLearnRecordList(pageParam, accountId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param rulesRecordId
     * @return RetDataBean
     * @Title: getMyLearnRecordList
     * @Description:  获取当前用户的历史学习记录
     */
    @RequestMapping(value = "/getMyLearnRecordList", method = RequestMethod.POST)
    public RetDataBean getMyLearnRecordList(String rulesRecordId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyRulesLearnService.getMyLearnRecordList(account.getOrgId(), account.getAccountId(), rulesRecordId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param partyRulesLearn
     * @return RetDataBean
     * @Title: getPartyRulesLearnById
     * @Description:  获取学习记录详情
     */
    @RequestMapping(value = "/getPartyRulesLearnById", method = RequestMethod.POST)
    public RetDataBean getPartyRulesLearnById(PartyRulesLearn partyRulesLearn) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRulesLearn.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyRulesLearnService.selectOnePartyRulesLearn(partyRulesLearn));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param sortId
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getPartyRulesRecordList
     * @Description:  获取规章制度列表
     */
    @RequestMapping(value = "/getPartyRulesRecordList", method = RequestMethod.POST)
    public RetDataBean getPartyRulesRecordList(PageParam pageParam, String sortId, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.sort_no");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = partyRulesRecordService.getPartyRulesRecordList(pageParam, sortId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @param  request
     * @param @param  contractSort
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getPartyRulesRecordById
     * @Description:  获取规章制度详情
     */
    @RequestMapping(value = "/getPartyRulesRecordById", method = RequestMethod.POST)
    public RetDataBean getPartyRulesRecordById(PartyRulesRecord partyRulesRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRulesRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyRulesRecordService.selectOnePartyRulesRecord(partyRulesRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, Object>>
     * @Title getErpBomSortTree
     * @Description  获取BOM 分类树结构
     */
    @RequestMapping(value = "/getRulesSortTree", method = RequestMethod.POST)
    public List<Map<String, String>> getRulesSortTree(String sortId) {
        try {
            String sortLevel = "0";
            if (StringUtils.isNotBlank(sortId)) {
                sortLevel = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return partyRulesSortService.getRulesSortTree(account.getOrgId(), sortLevel);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param @param  request
     * @param @param  contractSort
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getPartyRulesSortById
     * @Description:  获取规章制度分类的详情信息
     */
    @RequestMapping(value = "/getPartyRulesSortById", method = RequestMethod.POST)
    public RetDataBean getPartyRulesSortById(PartyRulesSort partyRulesSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            partyRulesSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, partyRulesSortService.selectOnePartyRulesSort(partyRulesSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
