package com.core136.controller.party;

import com.core136.bean.account.Account;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.service.account.AccountService;
import com.core136.service.party.EchartsPartyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/ret/partychartsget")
public class EchartsController {
    private EchartsPartyService echartsPartyService;

    @Autowired
    public void setEchartsPartyService(EchartsPartyService echartsPartyService) {
        this.echartsPartyService = echartsPartyService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * 按类型统计三会一课
     *
     * @return
     */
    @RequestMapping(value = "/getPartyLesMeetForBar", method = RequestMethod.POST)
    public RetDataBean getPartyLesMeetForBar() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getPartyLesMeetForBar(account, "三会一课统计"));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @return RetDataBean
     * @Title: getMyPointsByMonthLine
     * @Description:  近一年内党员发展情况
     */
    @RequestMapping(value = "/getMyPointsByMonthLine", method = RequestMethod.POST)
    public RetDataBean getMyPointsByMonthLine() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getMyPointsByMonthLine(account));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 预备党员转正
     *
     * @return
     */
    @RequestMapping(value = "/getPartyLesMeetByMonthLine", method = RequestMethod.POST)
    public RetDataBean getPartyLesMeetByMonthLine() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getPartyLesMeetByMonthLine(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getPartyJoinByMonthLine
     * @Description:  近一年内党员发展情况
     */
    @RequestMapping(value = "/getPartyJoinByMonthLine", method = RequestMethod.POST)
    public RetDataBean getPartyJoinByMonthLine() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getPartyJoinByMonthLine(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getPartyMemberJoinToPartyOrgPie
     * @Description:  获取各党组织党员发展情况
     */
    @RequestMapping(value = "/getPartyMemberJoinToPartyOrgPie", method = RequestMethod.POST)
    public RetDataBean getPartyMemberJoinToPartyOrgPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getPartyMemberJoinToPartyOrgPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBarPartyMemberJoinByStatus
     * @Description:  党员发展各阶段对比
     */
    @RequestMapping(value = "/getBarPartyMemberJoinByStatus", method = RequestMethod.POST)
    public RetDataBean getBarPartyMemberJoinByStatus() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getBarPartyMemberJoinByStatus(account, "党员发展各阶段对比"));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @return RetDataBean
     * @Title: getPartyJoinByYearLine
     * @Description:  获取过去10年内的党员入党情况
     */
    @RequestMapping(value = "/getPartyJoinByYearLine", method = RequestMethod.POST)
    public RetDataBean getPartyJoinByYearLine() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getPartyJoinByYearLine(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiPartyFundsAnalysis
     * @Description:  按党组织对比经费使用情况
     */
    @RequestMapping(value = "/getBiPartyFundsAnalysis", method = RequestMethod.POST)
    public RetDataBean getBiPartyFundsAnalysis() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getBiPartyFundsAnalysis(account, "各党组织经费使用情况"));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getPartyFundsPie
     * @Description:  按经费类型饼状图
     */
    @RequestMapping(value = "/getPartyFundsPie", method = RequestMethod.POST)
    public RetDataBean getPartyFundsPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getPartyFundsPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiPartyFundsByMonthLine
     * @Description:  按月份汇总经费使用情况
     */
    @RequestMapping(value = "/getBiPartyFundsByMonthLine", method = RequestMethod.POST)
    public RetDataBean getBiPartyFundsByMonthLine() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getBiPartyFundsByMonthLine(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getPartyMemberByAgeCountList
     * @Description:  按年龄段获取党员数量
     */
    @RequestMapping(value = "/getPartyMemberByAgeCountList", method = RequestMethod.POST)
    public RetDataBean getPartyMemberByAgeCountList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getPartyMemberByAgeCountList(account.getOrgId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getPartyBigEventList
     * @Description:  重大事件前10条记录
     */
    @RequestMapping(value = "/getPartyBigEventList", method = RequestMethod.POST)
    public RetDataBean getPartyBigEventList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getPartyBigEventList(account.getOrgId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getPartyOrgCountList
     * @Description:  获取各类党组织数量
     */
    @RequestMapping(value = "/getPartyOrgCountList", method = RequestMethod.POST)
    public RetDataBean getPartyOrgCountList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getPartyOrgCountList(account.getOrgId()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return ModelAndView
     * @Title: gopartyorgbigdata
     * @Description:  党组织分析数据大屏
     */
    @RequestMapping("/partyorgbigdata")
    public ModelAndView gopartyorgbigdata() {
        try {
            return new ModelAndView("app/core/party/bigdata/partyorgbigdata");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiPartyDeusByYearLine
     * @Description:  按年份汇总收缴的党费
     */
    @RequestMapping(value = "/getBiPartyDeusByYearLine", method = RequestMethod.POST)
    public RetDataBean getBiPartyDeusByYearLine() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getBiPartyDeusByYearLine(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiJoinPartyByYearLine
     * @Description:  按年份分析入党趋势
     */
    @RequestMapping(value = "/getBiJoinPartyByYearLine", method = RequestMethod.POST)
    public RetDataBean getBiJoinPartyByYearLine() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getBiJoinPartyByYearLine(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiMemberBySexPie
     * @Description:  党员性别统计
     */
    @RequestMapping(value = "/getBiMemberBySexPie", method = RequestMethod.POST)
    public RetDataBean getBiMemberBySexPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getBiMemberBySexPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @return RetDataBean
     * @Title: getBiMemberByPartyOrgIdPie
     * @Description:  所属党组织占比
     */
    @RequestMapping(value = "/getBiMemberByPartyOrgIdPie", method = RequestMethod.POST)
    public RetDataBean getBiMemberByPartyOrgIdPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getBiMemberByPartyOrgIdPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBiMemberByEducationPie
     * @Description:  党员学历分析
     */
    @RequestMapping(value = "/getBiMemberByEducationPie", method = RequestMethod.POST)
    public RetDataBean getBiMemberByEducationPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getBiMemberByEducationPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @return RetDataBean
     * @Title: getPartyRulesLearnByMonthLine
     * @Description:  按月份学习趋势分析
     */
    @RequestMapping(value = "/getPartyRulesLearnByMonthLine", method = RequestMethod.POST)
    public RetDataBean getPartyRulesLearnByMonthLine() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getPartyRulesLearnByMonthLine(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getPartyRulesLearnByAccountPie
     * @Description:  前10位人员学习次数占比
     */
    @RequestMapping(value = "/getPartyRulesLearnByAccountPie", method = RequestMethod.POST)
    public RetDataBean getPartyRulesLearnByAccountPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getPartyRulesLearnByAccountPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getPartyRulesLearnPie
     * @Description:  获取规章制度分类前10的占比
     */
    @RequestMapping(value = "/getPartyRulesLearnPie", method = RequestMethod.POST)
    public RetDataBean getPartyRulesLearnPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getPartyRulesLearnPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getPartyRulesLearnByDeptPie
     * @Description:  按部门学习对比前10的占比
     */
    @RequestMapping(value = "/getPartyRulesLearnByDeptPie", method = RequestMethod.POST)
    public RetDataBean getPartyRulesLearnByDeptPie() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, echartsPartyService.getPartyRulesLearnByDeptPie(account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }
}
