package com.core136.controller.party;

import com.core136.bean.account.Account;
import com.core136.bean.party.PartyAlbumConfig;
import com.core136.bean.party.PartyAlbumHome;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserInfoService;
import com.core136.service.party.PartyAlbumConfigService;
import com.core136.service.party.PartyAlbumHomeService;
import com.core136.service.party.PartyAlbumTypeService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/app/core/party")
public class PagePartyController {

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private PartyAlbumConfigService partyAlbumConfigService;

    @Autowired
    public void setPartyAlbumConfigService(PartyAlbumConfigService partyAlbumConfigService) {
        this.partyAlbumConfigService = partyAlbumConfigService;
    }

    private PartyAlbumHomeService partyAlbumHomeService;

    @Autowired
    public void setPartyAlbumHomeService(PartyAlbumHomeService partyAlbumHomeService) {
        this.partyAlbumHomeService = partyAlbumHomeService;
    }

    private PartyAlbumTypeService partyAlbumTypeService;

    @Autowired
    public void setPartyAlbumTypeService(PartyAlbumTypeService partyAlbumTypeService) {
        this.partyAlbumTypeService = partyAlbumTypeService;
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goMyPartyNews
     * @Description: 我的党建新闻
     */
    @RequestMapping("/mypartynews")
    @RequiresPermissions("/app/core/party/mypartynews")
    public ModelAndView goMyPartyNews(String view) {
        try {
            return new ModelAndView("app/core/party/propaganda/mypartynews");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goGovernmentInfo
     * @Description:  党建资讯
     */
    @RequestMapping("/governmentinfo")
    @RequiresPermissions("/app/core/party/governmentinfo")
    public ModelAndView goGovernmentInfo(String view) {
        try {
            return new ModelAndView("app/core/party/propaganda/governmentinfo");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goFundsTotal
     * @Description:  经费统计
     */
    @RequestMapping("/fundstotal")
    @RequiresPermissions("/app/core/party/fundstotal")
    public ModelAndView goFundsTotal(String view) {
        try {
            return new ModelAndView("app/core/party/funds/fundstotal");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goPartyNews
     * @Description:  党建新闻
     */
    @RequestMapping("/partynews")
    @RequiresPermissions("/app/core/party/partynews")
    public ModelAndView goPartyNews(String view) {
        try {
            return new ModelAndView("app/core/party/propaganda/partynews");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goBusinessGuide
     * @Description:  业务指南
     */
    @RequestMapping("/businessguide")
    @RequiresPermissions("/app/core/party/businessguide")
    public ModelAndView goBusinessGuide(String view) {
        try {
            return new ModelAndView("app/core/party/propaganda/businessguide");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goRuleSet
     * @Description:  积分规则设置
     */
    @RequestMapping("/points/ruleset")
    @RequiresPermissions("/app/core/party/points/ruleset")
    public ModelAndView goRuleSet(String view) {
        try {
            return new ModelAndView("app/core/party/points/ruleset");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goMyPoints
     * @Description:  党员个人积分
     */
    @RequestMapping("/points/mypoints")
    @RequiresPermissions("app/core/party/points/mypoints")
    public ModelAndView goMyPoints(String view) {
        try {
            return new ModelAndView("app/core/party/points/mypoints");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goOrgTotal
     * @Description:  党组织积分统计
     */
    @RequestMapping("/points/orgtotal")
    @RequiresPermissions("/app/core/party/points/orgtotal")
    public ModelAndView goOrgTotal(String view) {
        try {
            return new ModelAndView("app/core/party/points/orgtotal");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goMemberTotal
     * @Description:  党员积分统计
     */
    @RequestMapping("/points/membertotal")
    @RequiresPermissions("/app/core/party/points/membertotal")
    public ModelAndView goMemberTotal(String view) {
        try {
            return new ModelAndView("app/core/party/points/membertotal");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @param view
     * @return ModelAndView
     * @Title: goMemberPoints
     * @Description:  党员积分维护
     */
    @RequestMapping("/points/memberpoints")
    @RequiresPermissions("/app/core/party/points/memberpoints")
    public ModelAndView goMemberPoints(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/points/memberpointsmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/party/points/memberpoints");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goOrgPoints
     * @Description:  组织积分维护
     */
    @RequestMapping("/points/orgpoints")
    @RequiresPermissions("/app/core/party/points/orgpoints")
    public ModelAndView goOrgPoints(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/points/orgpointsmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/party/points/orgpoints");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @param view
     * @return ModelAndView
     * @Title: goFundsApproval
     * @Description:  经费审批
     */
    @RequestMapping("/fundsApproval")
    @RequiresPermissions("/app/core/party/fundsApproval")
    public ModelAndView goFundsApproval(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/funds/approval");
            } else {
                if (view.equals("old")) {
                    mv = new ModelAndView("app/core/party/funds/approvalmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goFunds
     * @Description:  经费管理
     */
    @RequestMapping("/funds")
    @RequiresPermissions("/app/core/party/funds")
    public ModelAndView goFunds(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/funds/fundsmanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/party/funds/funds");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goPayStandard
     * @Description:  党费缴纳标准
     */
    @RequestMapping("/paystandard")
    @RequiresPermissions("/app/core/party/paystandard")
    public ModelAndView goPayStandard(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/paystandard/paymanage");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("app/core/party/paystandard/pay");
                } else if (view.equals("import")) {
                    mv = new ModelAndView("app/core/party/paystandard/payimport");
                    RetDataBean retDataBean = new RetDataBean();
                    mv.addObject("retDataBean", retDataBean);
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @return ModelAndView
     * @Title: goDetailsList
     * @Description:  党风廉政更多
     */
    @RequestMapping("/detailslist")
    @RequiresPermissions("/app/core/party/detailslist")
    public ModelAndView goDetailsList() {
        try {
            return new ModelAndView("app/core/party/clean/detailslist");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goDetails
     * @Description:  详情页
     */
    @RequestMapping("/details")
    @RequiresPermissions("/app/core/party/details")
    public ModelAndView goDetails() {
        try {
            return new ModelAndView("app/core/party/clean/details");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goComprehensionDetails
     * @Description:  感悟详情
     */
    @RequestMapping("/comprehensiondetails")
    @RequiresPermissions("/app/core/party/comprehensiondetails")
    public ModelAndView goComprehensionDetails() {
        try {
            return new ModelAndView("app/core/party/clean/comprehensiondetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goReportDetails
     * @Description:  报告详情
     */
    @RequestMapping("/reportdetails")
    @RequiresPermissions("/app/core/party/reportdetails")
    public ModelAndView goReportDetails() {
        try {
            return new ModelAndView("app/core/party/clean/reportdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goApproval
     * @Description:  信息审核
     */
    @RequestMapping("/clean/approval")
    @RequiresPermissions("/app/core/party/clean/approval")
    public ModelAndView goApproval(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/clean/approval");
            } else {
                if (view.equals("meritorious")) {
                    mv = new ModelAndView("app/core/party/clean/approval1");
                } else if (view.equals("suggestions")) {
                    mv = new ModelAndView("app/core/party/clean/approval2");
                } else if (view.equals("tipoffs")) {
                    mv = new ModelAndView("app/core/party/clean/approval3");
                } else if (view.equals("comprehension")) {
                    mv = new ModelAndView("app/core/party/clean/approval4");
                } else if (view.equals("report")) {
                    mv = new ModelAndView("app/core/party/clean/approval5");
                }

            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goTipoffsDetails
     * @Description:  举报详情
     */
    @RequestMapping("/tipoffsdetails")
    @RequiresPermissions("/app/core/party/tipoffsdetails")
    public ModelAndView goTipoffsDetails() {
        try {
            return new ModelAndView("app/core/party/clean/tipoffsdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goSuggestionsDetails
     * @Description:  计策详情
     */
    @RequestMapping("/suggestionsdetails")
    @RequiresPermissions("/app/core/party/suggestionsdetails")
    public ModelAndView goSuggestionsDetails() {
        try {
            return new ModelAndView("app/core/party/clean/suggestionsdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goMeritoriousDetails
     * @Description:  先进事迹详情
     */
    @RequestMapping("/meritoriousdetails")
    @RequiresPermissions("/app/core/party/meritoriousdetails")
    public ModelAndView goMeritoriousDetails() {
        try {
            return new ModelAndView("app/core/party/clean/meritoriousdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goExposeDetails
     * @Description:  曝光记录详情
     */
    @RequestMapping("/exposedetails")
    @RequiresPermissions("/app/core/party/exposedetails")
    public ModelAndView goExposeDetails() {
        try {
            return new ModelAndView("app/core/party/clean/exposedetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goReport
     * @Description:  领导报告
     */
    @RequestMapping("/clean/report")
    @RequiresPermissions("/app/core/party/clean/report")
    public ModelAndView goReport(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/clean/report");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/party/clean/reportmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goComprehension
     * @Description:  学习感悟
     */
    @RequestMapping("/clean/comprehension")
    @RequiresPermissions("/app/core/party/clean/comprehension")
    public ModelAndView goComprehension(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/clean/comprehension");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/party/clean/comprehensionmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goTipOffs
     * @Description:  群众举报
     */
    @RequestMapping("/clean/tipoffs")
    @RequiresPermissions("/app/core/party/clean/tipoffs")
    public ModelAndView goTipOffs(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/clean/tipoffs");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/party/clean/tipoffsmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goSuggestions
     * @Description:  献计献策
     */
    @RequestMapping("/clean/suggestions")
    @RequiresPermissions("/app/core/party/clean/suggestions")
    public ModelAndView goSuggestions(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/clean/suggestions");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/party/clean/suggestionsmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goMeritorious
     * @Description:  先进事迹
     */
    @RequestMapping("/clean/meritorious")
    @RequiresPermissions("/app/core/party/clean/meritorious")
    public ModelAndView goMeritorious(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/clean/meritorious");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/party/clean/meritoriousmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @return ModelAndView
     * @Title: goVerifyDetails
     * @Description:  考核详情
     */
    @RequestMapping("/verifydetails")
    @RequiresPermissions("/app/core/party/verifydetails")
    public ModelAndView goVerifyDetails() {
        try {
            return new ModelAndView("app/core/party/integral/verifydetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goVerifyTotal
     * @Description:  考核汇总
     */
    @RequestMapping("/integral/verifytotal")
    @RequiresPermissions("/app/core/party/integral/verifytotal")
    public ModelAndView goVerifyTotal() {
        try {
            return new ModelAndView("app/core/party/integral/verifytotal");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goIntegralVerify
     * @Description:  党员考核
     */
    @RequestMapping("/integral/verify")
    @RequiresPermissions("/app/core/party/integral/verify")
    public ModelAndView goIntegralVerify(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/integral/verifyplan");
            } else {
                if (view.equals("verify")) {
                    mv = new ModelAndView("app/core/party/integral/verify");
                } else if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/party/integral/verifymanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goExpose
     * @Description:  曝光台
     */
    @RequestMapping("/clean/expose")
    @RequiresPermissions("/app/core/party/clean/expose")
    public ModelAndView goExpose(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/clean/expose");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/party/clean/exposemanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goDeusTotal
     * @Description:  党费汇总统计
     */
    @RequestMapping("/dues/duestotal")
    @RequiresPermissions("/app/core/party/dues/duestotal")
    public ModelAndView goDeusTotal() {
        try {
            return new ModelAndView("app/core/party/dues/duestotal");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goOrgDues
     * @Description:  组织党费收缴
     */
    @RequestMapping("/dues/orgdues")
    @RequiresPermissions("/app/core/party/dues/orgdues")
    public ModelAndView goOrgDues(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/dues/orgdues");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/party/dues/orgduesmanage");
                } else if (view.equals("import")) {
                    mv = new ModelAndView("app/core/party/dues/orgduesimport");
                    RetDataBean retDataBean = new RetDataBean();
                    mv.addObject("retDataBean", retDataBean);
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goSeizureDuesDetails
     * @Description:  党员党费收缴详情
     */
    @RequestMapping("/seizureduesdetails")
    @RequiresPermissions("/app/core/party/seizureduesdetails")
    public ModelAndView goSeizureDuesDetails() {
        try {
            return new ModelAndView("app/core/party/dues/seizureduesdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goSeizureDues
     * @Description:  党员收缴党费管理
     */
    @RequestMapping("/dues/seizuredues")
    @RequiresPermissions("/app/core/party/dues/seizuredues")
    public ModelAndView goSeizureDues(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/dues/seizuredues");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/party/dues/seizureduesmanage");
                } else if (view.equals("import")) {
                    mv = new ModelAndView("app/core/party/dues/seizureduesimport");
                    RetDataBean retDataBean = new RetDataBean();
                    mv.addObject("retDataBean", retDataBean);
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goPartyMemberWorkToDo
     * @Description:  待党员参与活动列表
     */
    @RequestMapping("/party/partymemberworktodo")
    @RequiresPermissions("/app/core/party/party/partymemberworktodo")
    public ModelAndView goPartyMemberWorkToDo(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/party/partymemberworktodo");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/party/party/partymemberworkhistory");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goPartyMemberSendWork
     * @Description:  党员活动发布
     */
    @RequestMapping("/party/partymemsendwork")
    @RequiresPermissions("/app/core/party/party/partymemsendwork")
    public ModelAndView goPartyMemberSendWork(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/party/party/partymembersendwork");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/party/party/partymemberworkmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goPartyWorkDetails
     * @Description:  党员活动详情
     */
    @RequestMapping("/partyworkdetails")
    @RequiresPermissions("/app/core/party/partyworkdetails")
    public ModelAndView goPartyWorkDetails() {
        try {
            return new ModelAndView("app/core/party/party/partyworkdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goSetParty
     * @Description:  设置党组织机构
     */
    @RequestMapping("/party/setparty")
    @RequiresPermissions("/app/core/party/party/setparty")
    public ModelAndView goSetParty() {
        try {
            return new ModelAndView("/app/core/party/party/partyorg");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @return ModelAndView
     * @Title: goSingleVideo
     * @Description:  其他人员视频
     */
    @RequestMapping("/album/singleother")
    @RequiresPermissions("/app/core/party/album/singleother")
    public ModelAndView goSingleOther(String view) {
        ModelAndView mv = null;
        try {
            if (view.equals("channals")) {
                mv = new ModelAndView("/app/core/party/album/browse-channals");
            } else if (view.equals("catagroies")) {
                Account account = accountService.getRedisAUserInfoToAccount();
                mv = new ModelAndView("/app/core/party/album/browse-catagroies");
                List<Map<String, String>> typeList = partyAlbumTypeService.getAlbumTypeForHome(account.getOrgId());
                mv.addObject("typeList", typeList);
            } else if (view.equals("history")) {
                mv = new ModelAndView("/app/core/party/album/your-history");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            PartyAlbumHome partyAlbumHome = new PartyAlbumHome();
            partyAlbumHome.setOrgId(account.getOrgId());
            partyAlbumHome = partyAlbumHomeService.selectOnePartyAlbumHome(partyAlbumHome);
            mv.addObject("logoPic", partyAlbumHome.getLogoPic());
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goSingleVideo
     * @Description:  播放视屏详情页
     */
    @RequestMapping("/album/single-video")
    @RequiresPermissions("/app/core/party/album/single-video")
    public ModelAndView goSingleVideo() {
        ModelAndView mv = null;
        try {
            mv = new ModelAndView("/app/core/party/album/single-video");
            Account account = accountService.getRedisAUserInfoToAccount();
            PartyAlbumHome partyAlbumHome = new PartyAlbumHome();
            partyAlbumHome.setOrgId(account.getOrgId());
            partyAlbumHome = partyAlbumHomeService.selectOnePartyAlbumHome(partyAlbumHome);
            mv.addObject("logoPic", partyAlbumHome.getLogoPic());
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 专辑详情
     * @param attachId
     * @return
     */
    @RequestMapping("/album/videodetails")
    public ModelAndView goVideoDetails(String attachId) {
        ModelAndView mv = null;
        try {
            mv = new ModelAndView("/app/core/party/album/uploaddetails");
            mv.addObject("attachId", attachId);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goAlbumAppoval
     * @Description:  专题视频审核
     */
    @RequestMapping("/album/appoval")
    @RequiresPermissions("/app/core/party/album/appoval")
    public ModelAndView goAlbumAppoval(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/party/album/albumapprovel");
            } else {
                if (view.equals("old")) {
                    mv = new ModelAndView("/app/core/party/album/albumapprovelold");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goSetParam
     * @Description:  首页面设置
     */
    @RequestMapping("/album/sethomeinfo")
    @RequiresPermissions("/app/core/party/album/sethomeinfo")
    public ModelAndView goSetHomeInfo() {
        ModelAndView mv = null;
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            mv = new ModelAndView("/app/core/party/album/sethomeinfo");
            PartyAlbumHome partyAlbumHome = new PartyAlbumHome();
            partyAlbumHome.setOrgId(account.getOrgId());
            partyAlbumHome = partyAlbumHomeService.selectOnePartyAlbumHome(partyAlbumHome);
            mv.addObject("partyAlbumHome", partyAlbumHome);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goSetParam
     * @Description:  系统参数设置
     */
    @RequestMapping("/album/setparam")
    @RequiresPermissions("/app/core/party/album/setparam")
    public ModelAndView goSetParam() {
        ModelAndView mv = null;
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            mv = new ModelAndView("/app/core/party/album/param");
            PartyAlbumConfig partyAlbumConfig = new PartyAlbumConfig();
            partyAlbumConfig.setOrgId(account.getOrgId());
            partyAlbumConfig = partyAlbumConfigService.selectOnePartyAlbumConfig(partyAlbumConfig);
            if (partyAlbumConfig != null) {
                mv.addObject("partyAlbumConfig", partyAlbumConfig);
                mv.addObject("approvelUser", userInfoService.getUserInfoByAccountId(partyAlbumConfig.getApprovelUser(), partyAlbumConfig.getOrgId()).getUserName());
            } else {
                partyAlbumConfig = new PartyAlbumConfig();
                partyAlbumConfig.setApprovelUser("");
                partyAlbumConfig.setAnonymousFlag("0");
                mv.addObject("partyAlbumConfig", partyAlbumConfig);
                mv.addObject("approvelUser", "");
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goAlbumUploadav
     * @Description:  专辑上传
     */
    @RequestMapping("/album/uploadav")
    @RequiresPermissions("/app/core/party/album/uploadav")
    public ModelAndView goAlbumUploadav(String view) {
        ModelAndView mv = null;
        try {
            if(StringUtils.isBlank(view))
            {
                    mv = new ModelAndView("/app/core/party/album/uploadavmanage");
            }else
            {
                if(view.equals("input"))
                {
                    mv = new ModelAndView("/app/core/party/album/uploadav");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goAlbumSetType
     * @Description:  设置视频分类
     */
    @RequestMapping("/album/settype")
    @RequiresPermissions("/app/core/party/album/settype")
    public ModelAndView goAlbumSetType() {
        try {
            return new ModelAndView("/app/core/party/album/albumtype");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goAlbumIndex
     * @Description: 
     */
    @RequestMapping("/album/index")
    @RequiresPermissions("/app/core/party/album/index")
    public ModelAndView goAlbumIndex() {
        ModelAndView mv = null;
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            mv = new ModelAndView("/app/core/party/album/home");
            PartyAlbumHome partyAlbumHome = new PartyAlbumHome();
            partyAlbumHome.setOrgId(account.getOrgId());
            partyAlbumHome = partyAlbumHomeService.selectOnePartyAlbumHome(partyAlbumHome);
            List<String> list = new ArrayList<String>();
            if (partyAlbumHome == null) {
                mv.addObject("logoPic", "");
            } else {
                mv.addObject("logoPic", partyAlbumHome.getLogoPic());
                if (StringUtils.isNotBlank(partyAlbumHome.getBannerPic())) {
                    String string = partyAlbumHome.getBannerPic();
                    String[] tArr = string.split("\\*");
                    list = Arrays.asList(tArr);
                }
            }
            mv.addObject("bannerPic", list);
            List<Map<String, String>> typeList = partyAlbumTypeService.getAlbumTypeForHome(account.getOrgId());
            mv.addObject("typeList", typeList);
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @return ModelAndView
     * @Title: goRulesLearnQuery
     * @Description:  学习记录查询
     */
    @RequestMapping("/ruleslearnquery")
    @RequiresPermissions("/app/core/party/ruleslearnquery")
    public ModelAndView goRulesLearnQuery() {
        try {
            return new ModelAndView("/app/core/party/rules/ruleslearnquery");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goRulesLearnTotal
     * @Description:  学习记录统计
     */
    @RequestMapping("/ruleslearntotal")
    @RequiresPermissions("/app/core/party/ruleslearntotal")
    public ModelAndView goRulesLearnTotal() {
        try {
            return new ModelAndView("/app/core/party/rules/ruleslearntotal");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @return ModelAndView
     * @Title: goRecordRulesDetails
     * @Description:  规章制度详情
     */
    @RequestMapping("/recordrulesdetails")
    @RequiresPermissions("/app/core/party/recordrulesdetails")
    public ModelAndView goRecordRulesDetails(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/party/rules/recordrulesdetails");
            } else {
                if (view.equals("learn")) {
                    mv = new ModelAndView("/app/core/party/rules/recordrulestolearn");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goRulesLearn
     * @Description:  规章制度学习
     */
    @RequestMapping("/ruleslearn")
    @RequiresPermissions("/app/core/party/ruleslearn")
    public ModelAndView goRulesLearn() {
        try {
            return new ModelAndView("/app/core/party/rules/recordlearn");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param view
     * @return ModelAndView
     * @Title: goCreateRules
     * @Description:  创建规章制度
     */
    @RequestMapping("/createrules")
    @RequiresPermissions("/app/core/party/createrules")
    public ModelAndView goCreateRules(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/party/rules/recordmange");
            } else {
                if (view.equals("input")) {
                    mv = new ModelAndView("/app/core/party/rules/recordcreate");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @return ModelAndView
     * @Title: goRulesSort
     * @Description:  规章制度分类
     */
    @RequestMapping("/rulessort")
    @RequiresPermissions("/app/core/party/rulessort")
    public ModelAndView goRulesSort() {
        try {
            return new ModelAndView("/app/core/party/rules/rulessort");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

}
