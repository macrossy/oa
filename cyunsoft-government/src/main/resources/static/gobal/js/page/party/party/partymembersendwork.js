$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD hh:mm:ss"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD hh:mm:ss"
    });
    $('#remark').summernote({height: 300});
    $("#createbut").unbind("click").click(function () {
        sendWork();
    })
})

function sendWork() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/insertPartyWorkRecord",
        type: "post",
        dataType: "json",
        data: {
            title: $("#title").val(),
            joinUser: $("#joinUser").attr("data-value"),
            sponsor: $("#sponsor").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            workType: $("#workType").val(),
            address: $("#address").val(),
            attach: $("#attach").attr("data_value"),
            remark: $("#remark").code(),
            msgType: getCheckBoxValue("msgType")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
