$(function () {
    $.ajax({
        url: "/ret/partycommissionget/getPartyCommission",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "memberId") {
                        $("#memberId").html(getPartyMemberName(recordInfo[id]));
                    } else if (id == "secretary") {
                        $("#secretary").html(getPartyMemberName(recordInfo[id]));
                    } else if (id == "secretaryOther") {
                        $("#secretaryOther").html(getPartyMemberName(recordInfo[id]));
                    } else if (id == "createType") {
                        if (recordInfo[id] == "1") {
                            $("#createType").html("差额")
                        } else if (recordInfo[id] == "2") {
                            $("#createType").html("等额")
                        } else if (recordInfo[id] == "3") {
                            $("#createType").html("其他")
                        } else {
                            $("#createType").html("未知")
                        }
                    } else if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})

function getPartyMemberName(memberIds) {
    var userNameStr = "";
    $.ajax({
        url: "/ret/partymemberget/getPartyMemeberByIds",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            memberIds: memberIds
        },
        success: function (data) {
            if (data.status == "200") {
                var userNameArr = [];
                if (data.list) {
                    for (var i = 0; i < data.list.length; i++) {
                        userNameArr.push(data.list[i].userName);
                    }
                }
                userNameStr = userNameArr.join(",");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
    return userNameStr;
}
