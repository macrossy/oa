$(function () {
    getCodeClass("reportType", "party_report");
    $("#createbut").unbind("click").click(function () {
        addReportRecord();
    })
    $('#content').summernote({height: 300});
    jeDate("#reportTime", {
        format: "YYYY-MM-DD"
    });
})

function addReportRecord() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyset/insertPartyCleanReport",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            mainPic: $("#file").attr("data-value"),
            reportType: $("#reportType").val(),
            memberId: $("#memberId").attr("data-value"),
            reportTime: $("#reportTime").val(),
            content: $("#content").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
