$(function () {
    query();
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#sendTime", {
        format: "YYYY-MM-DD"
    });
    $('#remark').summernote({height: 300});
    getAlbumTypeForSelect();
    getApprovelUserList();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partyget/getAlbumVideoForManage',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'videoId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            title: '专辑标题',
            sortable: true,
            width: '150px',
            formatter: function (value, row, index) {
                return "<a href=\"javascript:void(0);playAlbum('" + row.videoId + "','"+row.attach+"')\" style='cursor: pointer' >" + value + "</a>"
            }
        }, {
            field: 'subheading',
            title: '副标题',
            sortable: true,
            width: '150px'
        }, {
            field: 'sendTime',
            width: '100px',
            title: '发布时间'
        }, {
            field: 'typeName',
            width: '50px',
            title: '专辑分类'
        }, {
            field: 'status',
            width: '50px',
            title: '当前状态',
            formatter: function (value, row, index) {
                if (value == "0") {
                    return "审批中"
                } else if (value == "1") {
                    return "审批通过"
                } else if (value == "2") {
                    return "审批未通过"
                }
            }
        }, {
            field: 'createUser',
            width: '100px',
            title: '申请人',
            formatter: function (value, row, index) {
                return getUserNameByStr(value);
            }

        }, {
            field: 'createTime',
            width: '100px',
            title: '创建时间'
        },

            {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '100px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.videoId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        albumType: $("#albumTypeQuery").val(),
        beginTime: $("#beginTime").val(),
        endTime: $("#endTime").val(),
        createUser: $("#createUser").attr("data-value"),
        status: $("#statusQuery").val()
    };
    return temp;
};

function createOptBtn(videoId) {
    var html = "<a href=\"javascript:void(0);edit('" + videoId + "')\" class=\"btn btn-sky btn-xs\" >编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deleterecord('" + videoId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}
function playAlbum(videoId,attachId) {
    window.open("/app/core/party/album/videodetails?videoId=" + videoId+"&attachId="+attachId);
}
function edit(videoId) {
    $("#listdiv").hide();
    $("#avdiv").show();
    $("#approvelUser").attr("data_value", "");
    $("#memberId").attr("data_value", "");
    $("#attach").attr("data_value","");
    $("#show_attach").empty();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    document.getElementById("form1").reset()
    $.ajax({
        url: "/ret/partyget/getPartyAlbumVideoById",
        type: "post",
        dataType: "json",
        data: {videoId: videoId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                let info = data.list;
                console.log(info);
                for (let id in info) {
                    if (id == "attach") {
                        $("#show_attach").html("");
                        $("#attach").attr("data_value", info.attach);
                    } else if (id == "memberId") {
                        $("#" + id).val(getUserNameByStr(info[id]));
                        $("#" + id).attr("data-value", info[id]);
                    } else if (id == "remark") {
                        $("#" + id).code(info[id]);
                    } else {
                        $("#" + id).val(info[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updatePartyAlbumVideo(videoId);
                })
            }
        }
    })

}

function updatePartyAlbumVideo(videoId) {
    if ($("#approvelUser").val() == '') {
        layer.msg("请先选择审批人员！");
        return;
    }
    $.ajax({
        url: "/set/partyset/updatePartyAlbumVideo",
        type: "post",
        dataType: "json",
        data: {
            videoId: videoId,
            title: $("#title").val(),
            sortNo: $("#sortNo").val(),
            memberId: $("#memberId").attr("data-value"),
            remark: $("#remark").code(),
            sendTime: $("#sendTime").val(),
            albumType: $("#albumType").val(),
            approvelUser: $("#approvelUser").val(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                $("#myTable").bootstrapTable("refresh");
                layer.msg(sysmsg[data.msg]);
                goback();
            }
        }
    })
}

function deleterecord(videoId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/partyset/deletePartyAlbumVideo",
            type: "post",
            dataType: "json",
            data: {
                videoId: videoId
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    $("#myTable").bootstrapTable("refresh");
                    layer.msg(sysmsg[data.msg]);
                }
            }
        })
    }
}


function getAlbumTypeForSelect() {
    $.ajax({
        url: "/ret/partyget/getAlbumTypeForSelect",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                $("#albumType").append("<option value=''>请选择</option>")
                $("#albumTypeQuery").append("<option value=''>全部</option>")
                for (var i = 0; i < data.list.length; i++) {
                    $("#albumType").append("<option value='" + data.list[i].albumTypeId + "'>" + data.list[i].typeName + "</option>")
                    $("#albumTypeQuery").append("<option value='" + data.list[i].albumTypeId + "'>" + data.list[i].typeName + "</option>")

                }
            }
        }
    })
}

function getApprovelUserList() {
    $.ajax({
        url: "/ret/partyget/getApprovelUserList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                $("#approvelUser").append("<option value=''>请选择</option>")
                for (var i = 0; i < data.list.length; i++) {
                    $("#approvelUser").append("<option value='" + data.list[i].accountId + "'>" + data.list[i].userName + "</option>")
                }
            }
        }
    })
}

function goback() {
    $("#avdiv").hide();
    $("#listdiv").show();
}

