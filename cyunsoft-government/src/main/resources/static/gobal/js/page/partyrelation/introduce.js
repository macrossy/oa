$(function () {
    getCodeClass("relType", "gov_rel_type");
    var topNode = [{
        partyOrgName: orgName,
        levelId: '',
        isParent: "true",
        partyOrgId: "0",
        icon: "/gobal/img/org/org.png"
    }];
    zTree = $.fn.zTree.init($("#tree"), setting, topNode);// 初始化树节点时，添加同步获取的数据
    var nodes = zTree.getNodes();
    for (var i = 0; i < nodes.length; i++) {
        zTree.expandNode(nodes[i], true, false, false);//默认展开第一级节点
    }
    query("");
    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });
    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
});


function query(partyOrgId) {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getPartyMemberListByPartyOrgId?partyOrgId=' + partyOrgId,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'memberId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            width: '100px',
            title: '姓名'
        }, {
            field: 'cardId',
            width: '100px',
            title: '身份证号'
        }, {
            field: 'userSex',
            width: '50px',
            title: '性别'
        }, {
            field: 'partyOrgName',
            width: '100px',
            title: '所属党组织'
        }, {
            field: 'userType',
            title: '党员类型',
            width: '100px',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "正式党员";
                } else if (value == "2") {
                    return "预备党员";
                }
            }
        }, {
            field: 'postStatus',
            title: '党籍状态',
            width: '100px',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "正式党员";
                } else if (value == "2") {
                    return "预备党员";
                } else if (value == "3") {
                    return "已出党";
                } else if (value == "4") {
                    return "已停止党籍";
                } else if (value == "5") {
                    return "已死亡";
                }
            }
        }, {
            field: 'joinTime',
            title: '入党时间',
            width: '100px'
        }, {
            field: 'mobileNo',
            title: '联系电话',
            width: '100px',
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '120px',
            formatter: function (value, row, index) {
                return createOptBtn(row.partyOrgId, row.memberId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function createOptBtn(partyOrgId, memberId) {
    var html = "<a href=\"javascript:void(0);insertPartyIntroduce('" + partyOrgId + "','" + memberId + "')\" class=\"btn btn-primary btn-xs\">开介绍信</a>";
    return html;
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function insertPartyIntroduce(partyOrgId, memberId) {
    $("#intromodal").modal("show");
    $(".js-save").unbind("click").click(function () {
        $.ajax({
            url: "/set/partyrelationset/insertPartyIntroduce",
            type: "post",
            dataType: "json",
            data: {
                partyOrgId: partyOrgId,
                memberId: memberId,
                recUnitName: $("#recUnitName").val(),
                docNum: $("#docNum").val(),
                mobile: $("#mobile").val(),
                beginTime: $("#beginTime").val(),
                effective: $("#effective").val(),
                tel: $("#tel").val(),
                fax: $("#fax").val(),
                postCode: $("#postCode").val()
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    $("#myTable").bootstrapTable("refresh");
                    $("#intromodal").modal("hide");
                    layer.msg(sysmsg[data.msg]);
                }
            }
        })
    })
}

var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    }
};

function zTreeOnClick(event, treeId, treeNode) {
    $("#myTable").bootstrapTable('destroy');
    if (treeNode.partyOrgId == "0") {
        query("");
    } else {
        query(treeNode.partyOrgId);
    }
}
