$(function () {
    $.ajax({
        url: "/ret/partymemberget/getPartyFamilyRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "relation") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("丈夫");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("妻子");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("子女");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("父母");
                        } else if (recordInfo[id] == "5") {
                            $("#" + id).html("兄弟");
                        } else if (recordInfo[id] == "6") {
                            $("#" + id).html("姐妹");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "positionStatus") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("中共党员");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("中共预备党员");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("共青团员");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("民革党员");
                        } else if (recordInfo[id] == "5") {
                            $("#" + id).html("民盟盟员");
                        } else if (recordInfo[id] == "6") {
                            $("#" + id).html("民建会员");
                        } else if (recordInfo[id] == "7") {
                            $("#" + id).html("民进会员");
                        } else if (recordInfo[id] == "8") {
                            $("#" + id).html("农工党党员");
                        } else if (recordInfo[id] == "9") {
                            $("#" + id).html("致公党党员");
                        } else if (recordInfo[id] == "10") {
                            $("#" + id).html("九三学社社员");
                        } else if (recordInfo[id] == "11") {
                            $("#" + id).html("台盟盟员");
                        } else if (recordInfo[id] == "12") {
                            $("#" + id).html("无党派人士");
                        } else if (recordInfo[id] == "13") {
                            $("#" + id).html("群众");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "memberId") {
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.userName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "partyType") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyTypeById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {sortId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.sortName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
