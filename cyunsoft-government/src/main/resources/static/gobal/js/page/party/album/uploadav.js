$(function () {
    $('#remark').summernote({height: 300});
    jeDate("#sendTime", {
        format: "YYYY-MM-DD",
        minDate: getSysDate(),
        isinitVal: true
    });
    $(".js-add-save").unbind("click").click(function () {
        insertPartyAlbumVideo();
    })
    getAlbumTypeForSelect();
    getApprovelUserList();
});

function insertPartyAlbumVideo() {
    if ($("#approvelUser").val() == '') {
        layer.msg("请先选择审批人员！");
        return;
    }
    $.ajax({
        url: "/set/partyset/insertPartyAlbumVideo",
        type: "post",
        dataType: "json",
        data: {
            title: $("#title").val(),
            sortNo: $("#sortNo").val(),
            memberId: $("#memberId").attr("data-value"),
            remark: $("#remark").code(),
            sendTime: $("#sendTime").val(),
            albumType: $("#albumType").val(),
            approvelUser: $("#approvelUser").val(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}

function getAlbumTypeForSelect() {
    $.ajax({
        url: "/ret/partyget/getAlbumTypeForSelect",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                $("#albumType").append("<option value=''>请选择</option>")
                for (var i = 0; i < data.list.length; i++) {
                    $("#albumType").append("<option value='" + data.list[i].albumTypeId + "'>" + data.list[i].typeName + "</option>")
                }
            }
        }
    })
}


function getApprovelUserList() {
    $.ajax({
        url: "/ret/partyget/getApprovelUserList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                $("#approvelUser").append("<option value=''>请选择</option>")
                if (data.list != undefined) {
                    for (var i = 0; i < data.list.length; i++) {
                        $("#approvelUser").append("<option value='" + data.list[i].accountId + "'>" + data.list[i].userName + "</option>")
                    }
                }
            }
        }
    })
}
