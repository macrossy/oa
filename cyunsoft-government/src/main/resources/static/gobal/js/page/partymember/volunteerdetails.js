$(function () {
    $.ajax({
        url: "/ret/partymemberget/getPartyVolunteerById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    {
                        if (id == "mainPic") {
                            $("#file_img").attr("src", "/sys/file/getStaticImg?r=" + Math.random() + "&module=party&fileName=" + recordInfo[id] + "")
                        } else {
                            $("#" + id).html(recordInfo[id]);
                        }
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
