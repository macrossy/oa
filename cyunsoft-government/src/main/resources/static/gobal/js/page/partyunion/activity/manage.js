$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    getCodeClass("activityTypeQuery", "partyunion_activity");
    getCodeClass("activityType", "partyunion_activity");
    $('#content').summernote({height: 300});
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partyunionget/getPartyUnionActivityList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        sortOrder: "asc",
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            width: '100px',
            title: '活动标题',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        }, {
            field: 'activityType',
            width: '100px',
            title: '活动类型',
            formatter: function (value, row, index) {
                return getCodeClassName(value, 'partyunion_activity');
            }
        }, {
            field: 'subheading',
            title: '活动简介',
            width: '150px'
        }, {
            field: 'beginTime',
            width: '100px',
            title: '开始时间'
        }, {
            field: 'endTime',
            width: '100px',
            title: '结束时间'
        }, {
            field: 'userName',
            width: '100px',
            title: '创建人'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '120px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        activityType: $("#activityType").val(),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;"
        + "<a href=\"javascript:void(0);deleteRecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function deleteRecord(recordId) {
    if (confirm("确定删除当前记录吗？")) {
        $.ajax({
            url: "/set/partyunitonset/deletePartyUnionActivity",
            type: "post",
            dataType: "json",
            data: {recordId: recordId},
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function edit(recordId) {
    $("#baseinfolistdiv").hide();
    $("#baseinfodiv").show();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    $("#content").code("");
    $("#file").attr("data-value", "");
    $("#file_img").attr("src", "")
    $("#joinUser").attr("data-value", "");
    $("#attach").attr("data_value", "");
    document.getElementById("form1").reset();
    $.ajax({
        url: "/ret/partyunionget/getPartyUnionActivityById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var info = data.list;
                for (var id in info) {
                    if (id == "attach") {
                        $("#show_attach").html("");
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 4);
                    } else if (id == "content") {
                        $("#content").code(info[id]);
                    } else if (id == "mainImg") {
                        $("#file").attr("data-value", info[id]);
                        $("#file_img").attr("src", "/sys/file/getStaticImg?r=" + Math.random() + "&module=party&fileName=" + info[id] + "")
                    } else if (id == "joinUser") {
                        $("#" + id).attr("data-value", info[id]);
                        $("#" + id).val(getUserNameByStr(info[id]));
                    } else {
                        $("#" + id).val(info[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updatePartyUnionActivity(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function details(recordId) {
    window.open("/app/core/partyunion/activitydetails?recordId=" + recordId);
}

function updatePartyUnionActivity(recordId) {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyunionset/updatePartyUnionActivity",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            sortNo: $("#sortNo").val(),
            activityType: $("#activityType").val(),
            title: $("#title").val(),
            mainImg: $("#file").attr("data-value"),
            organizer: $("#organizer").val(),
            linkMan: $("#linkMan").val(),
            linkTel: $("#linkTel").val(),
            address: $("#address").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            joinUser: $("#joinUser").attr("data-value"),
            content: $("#content").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $("#baseinfodiv").hide();
                $("#baseinfolistdiv").show();
                $("#myTable").bootstrapTable("refresh");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function goback() {
    $("#baseinfodiv").hide();
    $("#baseinfolistdiv").show();
}
