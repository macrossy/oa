$(function () {
    $('#content').summernote({height: 200});
    $(".js-add-save").unbind("click").click(function () {
        insertPartyUnionEssence();
    })
    getCodeClass("storyType", "partyunion_story");
})

function insertPartyUnionEssence() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyunionset/insertPartyUnionStory",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            storyType: $("#storyType").val(),
            title: $("#title").val(),
            mainImg: $("#file").attr("data-value"),
            content: $("#content").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
