$(function () {
    $.ajax({
        url: "/ret/partymemberget/getPartyMemberById",
        type: "post",
        dataType: "json",
        data: {memberId: memberId},
        success: function (data) {
            if (data.status == "200") {
                var info = data.list;
                for (var id in info) {
                    if (id == "partyOrgId") {
                        $.ajax({
                            url: "/ret/partyorgget/getPartyOrgById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {
                                partyOrgId: info[id]
                            },
                            success: function (res) {
                                if (res.status == 200) {
                                    if (res.list) {
                                        $("#partyOrgId").html(res.list.partyOrgName);
                                    }
                                } else if (res.status == "100") {
                                    console.log(res.msg);
                                } else {
                                    console.log(res.msg);
                                }
                            }
                        });
                    } else if (id == "isLost") {
                        if (info[id] == "0") {
                            $("#isLost").html("否");
                        } else if (info[id] == "1") {
                            $("#isLost").html("是");
                        } else {
                            $("#isLost").html("未知");
                        }
                    } else if (id == "isFlow") {
                        if (info[id] == "0") {
                            $("#isFlow").html("否");
                        } else if (info[id] == "1") {
                            $("#isFlow").html("是");
                        } else {
                            $("#isFlow").html("未知");
                        }
                    } else if (id == "isMg") {
                        if (info[id] == "0") {
                            $("#isMg").html("否");
                        } else if (info[id] == "1") {
                            $("#isMg").html("是");
                        } else {
                            $("#isMg").html("未知");
                        }
                    } else if (id == "partyStatus") {
                        if (info[id] == "1") {
                            $("#partyStatus").html("正式党员");
                        } else if (info[id] == "2") {
                            $("#partyStatus").html("预备党员");
                        } else if (info[id] == "3") {
                            $("#partyStatus").html("已出党");
                        } else if (info[id] == "4") {
                            $("#partyStatus").html("已停止党籍");
                        } else if (info[id] == "5") {
                            $("#partyStatus").html("已死亡");
                        } else {
                            $("#partyStatus").html("未知");
                        }
                    } else if (id == "workingConditions") {
                        $("#workingConditions").html(getCodeClassName(info[id], "gov_working_conditions"));
                    } else if (id == "nation") {
                        $("#nation").html(getCodeClassName(info[id], "nation"));
                    } else if (id == "userType") {
                        if (info[id] == "1") {
                            $("#userType").html("正式党员");
                        } else if (info[id] == "2") {
                            $("#userType").html("预备党员");
                        } else {
                            $("#userType").html("未知");
                        }
                    } else if (id == "nativePlace") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyNativePlaceById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#nativePlace").html(res.list.sortName);
                                    } else {
                                        $("#nativePlace").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "education") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyEducationById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#education").html(res.list.sortName);
                                    } else {
                                        $("#education").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "degree") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyDegreeById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#degree").html(res.list.sortName);
                                    } else {
                                        $("#degree").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "userPost") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyPostById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#userPost").html(res.list.sortName);
                                    } else {
                                        $("#userPost").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "admPosition") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyAdmPositionById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#admPosition").html(res.list.sortName);
                                    } else {
                                        $("#admPosition").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "positionalTitles") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyTitleById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#positionalTitles").html(res.list.sortName);
                                    } else {
                                        $("#positionalTitles").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "classSituation") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyStratumById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#classSituation").html(res.list.sortName);
                                    } else {
                                        $("#classSituation").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "photo") {
                        $("#file_img").attr("src", "/sys/file/getStaticImg?module=govphotos&fileName=" + info[id]);
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
