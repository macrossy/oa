$(function () {
    getMySubcribeUserList();
    getMySubscribeUserList();
    getSubscribeMyUserList();
})

function getMySubscribeUserList() {
    $.ajax({
        url: "/ret/partyget/getSubscribeUserList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                if(data.list)
                {
                    var userList = data.list;
                    for (var i = 0; i < userList.length; i++) {
                        var tempLate = ['<li> <a href="#"> <img src="/sys/file/getOtherHeadImg?headImg=' + userList[i].headImg + '" alt="">',
                            '                                    <span>' + userList[i].userName + ' </span> <span class="dot-notiv"></span></a></li>'].join("");
                        $("#mySubscribeUserList").append(tempLate);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getMySubcribeUserList() {
    $.ajax({
        url: "/ret/partyget/getSubscribeUserListAll",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var userList = data.list;
                for (var i = 0; i < userList.length; i++) {
                    var tempLate = ['<li tabindex="-1" class="uk-active">',
                        '                                <a href="#">',
                        '                                    </a><div class="single-channal"><a href="single-channal.html">',
                        '                                        <div class="single-channal-creator">',
                        '                                            <img src="/sys/file/getOtherHeadImg?headImg=' + userList[i].headImg + '" alt="">',
                        '                                        </div>',
                        '                                        </a><div class="single-channal-body"><a href="single-channal.html">',
                        '                                            <h4>' + userList[i].userName + '</h4>',
                        '                                            <p> 32.8M subscribers </p>',
                        '                                            </a><a href="#" onclick="setCancelSubscribeUser(\'' + userList[i].accountId + '\');" class="button soft-primary small circle"> <i class="uil-bell"></i> 取消关注 </a>',
                        '                                        </div>',
                        '                                    </div>',
                        '                                ',
                        '                            </li>'].join("");
                    $("#mySubcribleUserList").append(tempLate);
                }

            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function setCancelSubscribeUser(subscribeUser) {
    $.ajax({
        url: "/set/partyset/setCancelSubscribeUser",
        type: "post",
        dataType: "json",
        data: {subscribeUser: subscribeUser},
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getSubscribeMyUserList() {
    $.ajax({
        url: "/ret/partyget/getSubscribeMyUserList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var userList = data.list;
                for (var i = 0; i < userList.length; i++) {
                    var tempLate = ['<li tabindex="-1" class="uk-active">',
                        '                                <a href="single-channal.html">',
                        '                                    </a><div class="single-channal"><a href="single-channal.html">',
                        '                                        <div class="single-channal-creator">',
                        '                                            <img src="/sys/file/getOtherHeadImg?headImg=' + userList[i].headImg + '" alt="">',
                        '                                        </div>',
                        '                                        </a><div class="single-channal-body"><a href="single-channal.html">',
                        '                                            <h4>' + userList[i].userName + '</h4>',
                        '                                            <p> 32.8M subscribers </p>',
                        '                                            </a><a href="#" class="button soft-primary small circle"> <i class="uil-bell"></i> 现在关注 </a>',
                        '                                        </div>',
                        '                                    </div>',
                        '                                ',
                        '                            </li>'].join("");
                    $("#subscribleMyUserList").append(tempLate);
                }

            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
