$(function () {
    query();
    $(".js-addablumType").unbind("click").click(function () {
        doadd();
    });
    $(".js-delAll").unbind("click").click(function () {
        dodelAll();
    });
});

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partyget/getPartyAlbumTypeList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'albumTypeId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'typeName',
                title: '分类名称',
                sortable: true,
                width: '100px'
            },
            {
                field: 'remark',
                width: '300px',
                title: '分类编码名称'
            },
            {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '150px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.albumTypeId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
                layer.msg(sysmsg[res.msg]);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function createOptBtn(albumTypeId) {
    var html = "<a href=\"javascript:void(0);edit('" + albumTypeId + "')\" class=\"btn btn-sky btn-xs\" >修改</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);del('" + albumTypeId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function edit(albumTypeId) {
    document.getElementById("form1").reset();
    $.ajax({
        url: "/ret/partyget/getPartyAlbumTypeById",
        type: "post",
        dataType: "json",
        data: {
            albumTypeId: albumTypeId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                for (var name in data.list) {
                    $("#" + name).val(data.list[name]);
                }
                $("#setTypeModal").modal("show");
                $(".js-save").unbind("click").click(function () {
                    $.ajax({
                        url: "/set/partyset/updatePartyAlbumType",
                        type: "post",
                        dataType: "json",
                        data: {
                            albumTypeId: albumTypeId,
                            sortNo: $("#sortNo").val(),
                            typeName: $("#typeName").val(),
                            remark: $("#remark").val()
                        },
                        success: function (data) {
                            if (data.status == "500") {
                                console.log(data.msg);
                            } else if (status == "100") {
                                layer.msg(sysmsg[data.msg]);
                            } else {
                                layer.msg(sysmsg[data.msg]);
                                $("#setTypeModal").modal("hide");
                                $("#myTable").bootstrapTable("refresh");
                            }
                        }
                    });
                })
            }
        }
    });
}

function del(albumTypeId) {
    if (confirm("确定删除当前分类吗？")) {
        $.ajax({
            url: "/set/partyset/deletePartyAlbumType",
            type: "post",
            dataType: "json",
            data: {
                albumTypeId: albumTypeId
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else {
                    $("#myTable").bootstrapTable("refresh");
                    layer.msg(sysmsg[data.msg]);
                }
            }
        });
    } else {
        return;
    }
}

function doadd() {
    document.getElementById("form1").reset();
    $("#setTypeModal").modal("show");
    $(".js-save").unbind("click").click(function () {
        $.ajax({
            url: "/set/partyset/insertPartyAlbumType",
            type: "post",
            dataType: "json",
            data: {
                sortNo: $("#sortNo").val(),
                typeName: $("#typeName").val(),
                remark: $("#remark").val()
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                }
            }
        });
        $("#setTypeModal").modal("hide");
    });
}

function dodelAll() {
    var a = $("#myTable").bootstrapTable('getSelections');
    var typeIdArr = [];
    for (var i = 0; i < a.length; i++) {
        typeIdArr.push(a[i].albumTypeId);
    }
    if (typeIdArr.length <= 0) {
        layer.msg("至少选择一个分类码!")
        return;
    } else {
        if (confirm("确定删除当前分类吗？")) {
            $.ajax({
                url: "/set/partyset/deletePartyAlbumTypeBatch",
                type: "post",
                dataType: "json",
                data: {
                    typeIdArr: typeIdArr
                },
                success: function (data) {
                    if (data.status == 200) {
                        layer.msg(sysmsg[data.msg]);
                        $("#myTable").bootstrapTable("refresh");
                    } else if (data.status = "100") {
                        layer.msg(sysmsg[data.msg]);
                    } else {
                        console.log(data.msg);
                    }
                }
            });
        } else {
            return;
        }
    }
}
