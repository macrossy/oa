$(function () {
    $(".js-setbtn").unbind("click").click(function () {
        setAlbumHomeInfo();
    })
    $("#bannerPic").zyUpload({
        width: "650px",                 // 宽度
        height: "100%",                 // 宽度
        itemWidth: "120px",                 // 文件项的宽度
        itemHeight: "100px",                 // 文件项的高度
        url: "/sys/file/uploadBannerImg",  // 上传文件的路径
        multiple: true,                    // 是否可以多个文件上传
        dragDrop: true,                    // 是否可以拖动上传文件
        del: true,                    // 是否可以删除文件
        finishDel: false,  				  // 是否在上传文件完成后删除预览
        /* 外部获得的回调接口 */
        onSelect: function (files, allFiles) {                    // 选择文件的回调方法
        },
        onDelete: function (file, surplusFiles) {                     // 删除一个文件的回调方法
        },
        onSuccess: function (file) {                    // 文件上传成功的回调方法
        },
        onFailure: function (file) {                    // 文件上传失败的回调方法
        },
        onComplete: function (responseInfo) {           // 上传完成的回调方法
        }
    });
    if (bannerPic != '') {
        var arr = bannerPic.split("*");
        for (var i = 0; i < arr.length; i++) {
            $("#uploadInf").append("<div style='width:100px;display:inline-block;text-align: center;margin-left:5px;margin-top:5px;'><img data-value='" + arr[i] + "' width='100px' height='100px' src='/sys/file/getBannerImg?fileName=" + arr[i] + "'/><a style='cursor: pointer;' onclick='delimg(this)'>删除</a></div>");
        }
    }

})


function getBannerImg() {
    var arr = [];
    $("#uploadInf").find("img").each(function () {
        arr.push($(this).attr("data-value"))
    });
    return arr.join("*");
}


function setAlbumHomeInfo() {
    $.ajax({
        url: "/set/partyset/setAlbumHomeInfo",
        type: "post",
        dataType: "json",
        data: {
            title: $("#title").val(),
            logoPic: $("#logoPic").attr("data-value"),
            bannerPic: getBannerImg()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}


function uploadAblumlogo(fileId) {
    $.ajaxFileUpload({
        url: '/sys/file/uploadAblumlogo', //上传文件的服务端
        secureuri: false,  //是否启用安全提交
        async: false,
        dataType: 'json',   //数据类型
        fileElementId: fileId, //表示文件域ID
        success: function (data, status) {
            if (data.status == "200") {
                $("#" + fileId).attr("data-value", data.redirect);
                $("#" + fileId + "_img").attr("src", "/sys/file/getBannerImg?fileName=" + data.redirect);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        },
        error: function (data, status, e) {
            console.log(data.msg);
        }
    });
}

function delimg(Obj) {
    $(Obj).parent("div").remove();
}
