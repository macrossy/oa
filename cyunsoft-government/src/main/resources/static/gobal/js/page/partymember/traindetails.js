$(function () {
    $.ajax({
        url: "/ret/partymemberget/getPartyTrainRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "trainType") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("初任培训（新录用人员培训）");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("任职培训（晋升培训）");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("专门业务培训");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("专门技术培训");
                        } else if (recordInfo[id] == "5") {
                            $("#" + id).html("其他培训");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "jobStatus") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("全脱产（离岗）");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("半脱产（半离岗）");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("不脱产（不离岗）");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "organizerLevel") {
                        $("#" + id).html(getCodeClassName(recordInfo[id], "gov_unit_level"));
                    } else if (id == "stunitType") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyStunitTypeById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: recordInfo[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#stunitType").html(res.list.sortName);
                                    } else {
                                        $("#stunitType").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "classType") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("进修班");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("培训班");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("理论班");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("专题研讨（研究）班");
                        } else if (recordInfo[id] == "5") {
                            $("#" + id).html("其他");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "isAbroad") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("是");
                        } else {
                            $("#" + id).html("否");
                        }

                    } else if (id == "trainStatus") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("毕业");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("肄业");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("结业");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("在训");
                        } else if (recordInfo[id] == "5") {
                            $("#" + id).html("未完成");
                        } else if (recordInfo[id] == "6") {
                            $("#" + id).html("转学校");
                        } else if (recordInfo[id] == "7") {
                            $("#" + id).html("转系");
                        } else if (recordInfo[id] == "8") {
                            $("#" + id).html("转专业");
                        } else if (recordInfo[id] == "9") {
                            $("#" + id).html("其他");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "partyMember") {
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.userName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
