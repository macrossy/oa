$(function () {
    if (type == "1") {
        getPartyCleanExposeById();
    } else if (type == "2") {
        getPartyCleanMeritoriousById()
    } else if (type == "3") {
        getPartyCleanReportById()
    } else if (type == "4") {
        getPartyCleanSuggestionsById();
    } else if (type == "5") {
        getPartyCleanComprehensionById();
    } else if (type == "6") {
        getPartyCleanTipoffsById();
    }
});

function getPartyCleanTipoffsById() {
    $("#file_img").remove();
    $.ajax({
        url: "/ret/partyget/getPartyCleanTipoffsById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "content") {
                        $("#content").html(recordInfo[id]);
                    } else if (id == "tipoffsType") {
                        $("#type").html(getCodeClassName(recordInfo[id], "party_tipoffs"));
                    } else if (id == "title") {
                        $("#" + id).html(recordInfo[id]);
                    } else if (id == "createTime") {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}


function getPartyCleanComprehensionById() {
    $.ajax({
        url: "/ret/partyget/getPartyCleanComprehensionById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "content") {
                        $("#content").html(recordInfo[id]);
                    } else if (id == "comprehensionType") {
                        $("#type").html(getCodeClassName(recordInfo[id], "party_comprehension"));
                    } else if (id == "mainPic") {
                        $("#file_img").attr("src", "/sys/file/getStaticImg?r=" + Math.random() + "&module=party&fileName=" + recordInfo[id] + "")
                    } else if (id == "title") {
                        $("#" + id).html(recordInfo[id]);
                    } else if (id == "createTime") {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function getPartyCleanExposeById() {
    $.ajax({
        url: "/ret/partyget/getPartyCleanExposeById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "content") {
                        $("#content").html(recordInfo[id]);
                    } else if (id == "exposeType") {
                        $("#type").html(getCodeClassName(recordInfo[id], "party_expose"));
                    } else if (id == "mainPic") {
                        $("#file_img").attr("src", "/sys/file/getStaticImg?r=" + Math.random() + "&module=party&fileName=" + recordInfo[id] + "")
                    } else if (id == "title") {
                        $("#" + id).html(recordInfo[id]);
                    } else if (id == "createTime") {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function getPartyCleanMeritoriousById() {
    $.ajax({
        url: "/ret/partyget/getPartyCleanMeritoriousById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "content") {
                        $("#content").html(recordInfo[id]);
                    } else if (id == "meritoriousType") {
                        $("#type").html(getCodeClassName(recordInfo[id], "party_meritorious"));
                    } else if (id == "mainPic") {
                        $("#file_img").attr("src", "/sys/file/getStaticImg?r=" + Math.random() + "&module=party&fileName=" + recordInfo[id] + "")
                    } else if (id == "title") {
                        $("#" + id).html(recordInfo[id]);
                    } else if (id == "createTime") {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function getPartyCleanReportById() {
    $.ajax({
        url: "/ret/partyget/getPartyCleanReportById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "content") {
                        $("#content").html(recordInfo[id]);
                    } else if (id == "reportType") {
                        $("#type").html(getCodeClassName(recordInfo[id], "party_report"));
                    } else if (id == "mainPic") {
                        $("#file_img").attr("src", "/sys/file/getStaticImg?r=" + Math.random() + "&module=party&fileName=" + recordInfo[id] + "")
                    } else if (id == "title") {
                        $("#" + id).html(recordInfo[id]);
                    } else if (id == "createTime") {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function getPartyCleanSuggestionsById() {
    $("#file_img").remove();
    $.ajax({
        url: "/ret/partyget/getPartyCleanSuggestionsById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "content") {
                        $("#content").html(recordInfo[id]);
                    } else if (id == "suggestionsType") {
                        $("#type").html(getCodeClassName(recordInfo[id], "party_suggestions"));
                    } else if (id == "title") {
                        $("#" + id).html(recordInfo[id]);
                    } else if (id == "createTime") {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
