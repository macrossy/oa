$(function () {
    $.ajax({
        url: "/ret/partyorgget/getPartyLesMeetById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "joinMember") {
                        $("#" + id).html(getPartyMemberName(recordInfo.joinMember));
                    } else if (id == "meetType") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("支部党员大会");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("支委会");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("党小组会");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("党课");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "meetModel") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("会议");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("活动");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "meetYear") {
                        $("#" + id).html(recordInfo[id] + "年度");
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})


function getPartyMemberName(memberIds) {
    var userNameStr = "";
    $.ajax({
        url: "/ret/partymemberget/getPartyMemeberByIds",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            memberIds: memberIds
        },
        success: function (data) {
            if (data.status == "200") {
                var userNameArr = [];
                if (data.list) {
                    for (var i = 0; i < data.list.length; i++) {
                        userNameArr.push(data.list[i].userName);
                    }
                }
                userNameStr = userNameArr.join(",");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
    return userNameStr;
}
