$(function () {
    jeDate("#joinTime", {
        format: "YYYY-MM-DD"
    });
    $("#createbut").unbind("click").click(function () {
        insertPartyLife();
    })
})

function insertPartyLife() {
    if ($("#memberId").attr("data-value") == "") {
        layer.msg("党员不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/insertPartyLifeRecord",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            memberId: $("#memberId").attr("data-value"),
            joinTime: $("#joinTime").val(),
            lifeContent: $("#lifeContent").val(),
            reason: $("#reason").val(),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
