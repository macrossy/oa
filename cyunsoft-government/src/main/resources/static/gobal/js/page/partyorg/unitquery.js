$(function () {
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    $.ajax({
        url: "/ret/partyparamget/getUnitSuboTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "TOP全部",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#unitAffiliationQueryTree"), unitAffiliationQuerysetting, newTreeNodes);
        }
    });
    $("#unitAffiliationQuery").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#unitAffiliationQueryContent").css({
            "width": $(this).outerWidth() + "px",
            "left": $(this).position().left + "px"
        }).slideDown(200);
    });
    $("#serviceIndustryQuery").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#serviceIndustryQueryContent").css({
            "width": $(this).outerWidth() + "px",
            "left": $(this).position().left + "px"
        }).slideDown(200);
    });

    $.ajax({
        url: "/ret/partyparamget/getUnitServiceTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "TOP全部",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#serviceIndustryQueryTree"), serviceIndustryQuerysetting, newTreeNodes);
        }
    });

    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });
    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
})
var unitAffiliationQuerysetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getUnitSuboTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("unitAffiliationQueryTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#unitAffiliationQuery");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
var serviceIndustryQuerysetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getUnitServiceTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("serviceIndustryQueryTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#serviceIndustryQuery");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partyorgget/getUnitBaseInfoList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'unitId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'unitName',
            title: '单位全称',
            width: '150px'
        }, {
            field: 'shortName',
            width: '100px',
            title: '单位简称'
        }, {
            field: 'subordinateUnit',
            width: '150px',
            title: '隶属单位名称',
            formatter: function (value, row, index) {
                if (value != "0") {
                    return getUnitName(value)
                } else {
                    return "无";
                }
            }
        }, {
            field: '',
            width: '100px',
            title: '单位类型',
            formatter: function (value, row, index) {
                var returnStr = [];
                if (row.unitType1 == "1") {
                    returnStr.push("独立法人单位");
                }
                if (row.unitType2 == "1") {
                    returnStr.push("建立党员服务机构");
                }
                if (row.unitType3 == "1") {
                    returnStr.push("建立党员志愿者队伍");
                }
                return returnStr.join(",");
            }
        }, {
            field: 'linkTel',
            width: '100px',
            title: '单位联系电话'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '60px',
            formatter: function (value, row, index) {
                return createOptBtn(row.unitId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        partyStatus: $("#partyStatusQuery").val(),
        unitAffiliation: $("#unitAffiliationQuery").attr("data-value"),
        serviceIndustry: $("#serviceIndustryQuery").attr("data-value")
    };
    return temp;
};

function createOptBtn(unitId) {
    return "<a href=\"javascript:void(0);details('" + unitId + "')\" class=\"btn btn-sky btn-xs\" >详情</a>";
}

function getUnitName(unitId) {
    var retStr = "";
    $.ajax({
        url: "/ret/partyorgget/getPartyUnitBaseById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            unitId: unitId
        },
        success: function (data) {
            if (data.status == "200") {
                retStr = data.list.unitName;
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
    return retStr;
}
