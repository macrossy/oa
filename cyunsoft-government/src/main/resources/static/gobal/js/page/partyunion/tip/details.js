$(function () {
    $.ajax({
        url: "/ret/partyunionget/getPartyUnionTipById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var info = data.list;
                for (var id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 1);
                    } else if (id == "content") {
                        $("#content").html(info[id]);
                    } else if (id == "accountId") {
                        $("#" + id).html(getUserNameByStr(info[id]));
                    } else if (id == "isFlag") {
                        if (info[id] == "0") {
                            $("#" + id).html("匿名");
                        } else if (info[id] == "1") {
                            $("#" + id).html("实名");
                        }
                    } else if (id == "tipType") {
                        $("#" + id).html(getCodeClassName(info[id], "partyunion_tip"));
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
