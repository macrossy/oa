$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    $('#trainContent').summernote({height: 300});
    $('#remark').summernote({height: 300});
    getCodeClass("organizerLevel", "gov_unit_level");

    $.ajax({
        url: "/ret/partyparamget/getStunitTypeTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "空",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#stunitTypeTree"), stunitTypesetting, newTreeNodes);
        }
    });
    $("#stunitType").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#stunitTypeContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });

    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    $("#createbut").unbind("click").click(function () {
        addPartyTrain();
    })
})

function addPartyTrain() {
    if ($("#partyMember").attr("data-value")== "") {
        layer.msg("受训党员不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/insertPartyTrainRecord",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            partyMember: $("#partyMember").attr("data-value"),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            trainType: $("#trainType").val(),
            jobStatus: $("#jobStatus").val(),
            organizer: $("#organizer").val(),
            organizerOther: $("#organizerOther").val(),
            organizerLevel: $("#organizerLevel").val(),
            stunitType: $("#stunitType").attr("data-value"),
            partyOrgName: $("#partyOrgName").val(),
            partyOrgNameOther: $("#partyOrgNameOther").val(),
            classType: $("#classType").val(),
            className: $("#className").val(),
            userCount: $("#userCount").val(),
            major: $("#major").val(),
            isAbroad: getCheckBoxValue("isAbroad"),
            address: $("#address").val(),
            trainStatus: $("#trainStatus").val(),
            period: $("#period").val(),
            trainContent: $("#trainContent").code(),
            attach: $("#attach").attr("data_value"),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

var stunitTypesetting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyparamget/getStunitTypeTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("stunitTypeTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#stunitType");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
