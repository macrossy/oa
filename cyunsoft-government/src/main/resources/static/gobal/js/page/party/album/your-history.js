$(function () {
    getAlbumVideoByHistory();
})

function getAlbumVideoByHistory() {

    $.ajax({
        url: "/ret/partyget/getAlbumVideoByHistory",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var infoList = data.list;
                for (var i = 0; i < infoList.length; i++) {
                    var template = ['<a href="/app/core/party/album/single-video?attachId=' + infoList[i].attach + '&videoId=' + infoList[i].videoId + '" class="video-post video-post-list">',
                        '                                <div class="video-post-thumbnail">',
                        '                                    <span class="play-btn-trigger"></span>',
                        '                                    <span class="video-post-time">' + infoList[i].videoDuration + '</span>',
                        '                                    <img src="/sys/file/getVideoPic?attachId=' + infoList[i].attach + '&fileName=' + infoList[i].pics + '" alt="">',
                        '                                </div>',
                        '                                <!-- Blog Post Content -->',
                        '                                <div class="video-post-content">',
                        '                                    <button class="p-3 circle bg-secondary uk-align-right ml-2"',
                        '                                        uk-tooltip="title: 清除缓存 ; pos: left" type="button" uk-close onclick="clearCach(\'' + infoList[i].videoId + '\')"></button>',
                        '                                    <h3>' + infoList[i].title + '</h3>',
                        '                                    <span class="video-post-user">' + infoList[i].createUserName + '</span>',
                        '                                    <span class="video-post-date"> ' + getshowtime(infoList[i].sendTime) + ' </span>',
                        '                                    <p>' + infoList[i].subheading + '</p>',
                        '                                </div>',
                        '                            </a>'].join("");
                    $("#historyList").append(template);
                }

            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function clearCach(videoId) {

    if (confirm("确定清除历史记录吗？")) {
        $.ajax({
            url: "/set/partyset/setAlbumClearCache",
            type: "post",
            dataType: "json",
            data: {
                videoId: videoId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    location.reload();
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }

}
