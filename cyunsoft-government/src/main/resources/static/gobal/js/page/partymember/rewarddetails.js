$(function () {
    $.ajax({
        url: "/ret/partymemberget/getPartyRewardRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "honoraryLevel") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("国家级荣誉称号");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("省（自治区、直辖市）级荣誉称号");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("部（委）级荣誉称号");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("区（地、厅、司、局）级");
                        } else if (recordInfo[id] == "5") {
                            $("#" + id).html("县、处级");
                        } else if (recordInfo[id] == "6") {
                            $("#" + id).html("系统内部荣誉称号");
                        } else if (recordInfo[id] == "7") {
                            $("#" + id).html("基层企事业单位荣誉称号");
                        } else if (recordInfo[id] == "8") {
                            $("#" + id).html("国外授予的荣誉称号");
                        } else if (recordInfo[id] == "9") {
                            $("#" + id).html("其他");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "isInTime") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("是");
                        } else {
                            $("#" + id).html("否");
                        }
                    } else if (id == "rewardLevel") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("中央、国家级(正)");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("中央、国家级(副)");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("省、部级");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("区（地、厅、司、局）级");
                        } else if (recordInfo[id] == "5") {
                            $("#" + id).html("副区（副地、副厅、副司、副局）级");
                        } else if (recordInfo[id] == "6") {
                            $("#" + id).html("县、处级");
                        } else if (recordInfo[id] == "7") {
                            $("#" + id).html("副县、副处级");
                        } else if (recordInfo[id] == "8") {
                            $("#" + id).html("乡、科级");
                        } else if (recordInfo[id] == "9") {
                            $("#" + id).html("副乡、副科级");
                        } else if (recordInfo[id] == "10") {
                            $("#" + id).html("股级");
                        } else if (recordInfo[id] == "11") {
                            $("#" + id).html("地（局）级");
                        } else if (recordInfo[id] == "12") {
                            $("#" + id).html("省（区、市）人民政府直属工作部门、人民团体");
                        } else if (recordInfo[id] == "13") {
                            $("#" + id).html("大行政区(军政委员会)");
                        } else if (recordInfo[id] == "14") {
                            $("#" + id).html("中央军委级");
                        } else if (recordInfo[id] == "15") {
                            $("#" + id).html("解放军总部级");
                        } else if (recordInfo[id] == "16") {
                            $("#" + id).html("大军区正级");
                        } else if (recordInfo[id] == "17") {
                            $("#" + id).html("大军区副级");
                        } else if (recordInfo[id] == "18") {
                            $("#" + id).html("正兵团级");
                        } else if (recordInfo[id] == "19") {
                            $("#" + id).html("副兵团级");
                        } else if (recordInfo[id] == "20") {
                            $("#" + id).html("正军级");
                        } else if (recordInfo[id] == "21") {
                            $("#" + id).html("副军级");
                        } else if (recordInfo[id] == "22") {
                            $("#" + id).html("正师级");
                        } else if (recordInfo[id] == "23") {
                            $("#" + id).html("副师(正旅)级");
                        } else if (recordInfo[id] == "24") {
                            $("#" + id).html("正团(副旅)级");
                        } else if (recordInfo[id] == "25") {
                            $("#" + id).html("副团级");
                        } else if (recordInfo[id] == "26") {
                            $("#" + id).html("正营级");
                        } else if (recordInfo[id] == "27") {
                            $("#" + id).html("副营级");
                        } else if (recordInfo[id] == "28") {
                            $("#" + id).html("正连级");
                        } else if (recordInfo[id] == "29") {
                            $("#" + id).html("副连级");
                        } else if (recordInfo[id] == "30") {
                            $("#" + id).html("排级");
                        } else if (recordInfo[id] == "31") {
                            $("#" + id).html("班级");
                        } else if (recordInfo[id] == "32") {
                            $("#" + id).html("基层单位");
                        } else if (recordInfo[id] == "33") {
                            $("#" + id).html("国际组织、机构");
                        } else if (recordInfo[id] == "34") {
                            $("#" + id).html("外国组织、机构");
                        } else if (recordInfo[id] == "35") {
                            $("#" + id).html("无级别");
                        } else if (recordInfo[id] == "36") {
                            $("#" + id).html("其他");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "rewardReasons") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("忠于职守，积极工作，成绩显著");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("遵守纪律，廉洁奉公，作风正派，办事公道，模范作用突出");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("在工作中有发明创造或者提出合理化建议，取得显著经济效益或者社会效益");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("为增进民族团结、维护社会稳定做出突出贡献");
                        } else if (recordInfo[id] == "5") {
                            $("#" + id).html("爱护公共财产，节约国家资财有突出成绩");
                        } else if (recordInfo[id] == "6") {
                            $("#" + id).html("防止或者消除事故有功，使国家和人民群众利益免受或者减少损失");
                        } else if (recordInfo[id] == "7") {
                            $("#" + id).html("在抢险、救灾等特定环境中奋不顾身，做出贡献");
                        } else if (recordInfo[id] == "8") {
                            $("#" + id).html("同违法违纪行为作斗争有功绩");
                        } else if (recordInfo[id] == "9") {
                            $("#" + id).html("在对外交往中为国家争得荣誉和利益");
                        } else if (recordInfo[id] == "10") {
                            $("#" + id).html("其他突出功绩");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "loseReasons") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("弄虚作假，骗取奖励");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("申报奖励时隐瞒严重错误或者严重违反规定程序");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("有法律、法规规定应当撤销奖励的其他情形");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("其他突出功绩");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "memberId") {
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.userName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
