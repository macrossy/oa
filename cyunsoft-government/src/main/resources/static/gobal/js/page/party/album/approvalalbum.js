$(function () {
    query();
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    getAlbumTypeForSelect();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partyget/getAlbumVideoForApprovel',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'videoId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            title: '专辑标题',
            sortable: true,
            width: '150px',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);playAlbum('" + row.videoId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        }, {
            field: 'subheading',
            title: '副标题',
            sortable: true,
            width: '150px'
        }, {
            field: 'sendTime',
            width: '100px',
            title: '发布时间'
        }, {
            field: 'typeName',
            width: '50px',
            title: '专辑分类'
        }, {
            field: 'createUser',
            width: '100px',
            title: '申请人',
            formatter: function (value, row, index) {
                return getUserNameByStr(value);
            }

        }, {
            field: 'createTime',
            width: '100px',
            title: '创建时间'
        },

            {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '100px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.videoId,row.attach);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        albumType: $("#albumType").val(),
        beginTime: $("#beginTime").val(),
        endTime: $("#endTime").val(),
        createUser: $("#createUser").attr("data-value")
    };
    return temp;
};

function createOptBtn(videoId,attachId) {
    var html = "<a href=\"javascript:void(0);approval('" + videoId + "','"+attachId+"')\" class=\"btn btn-darkorange btn-xs\" >审批</a>";
    return html;
}

function playAlbum(videoId,attachId) {
    window.open("/app/core/party/album/videodetails?videoId=" + videoId+"&attachId="+attachId);
}
function approval(videoId) {
    $("#approvermodal").modal("show");
    $(".js-app-not-pass").unbind("click").click(function () {
        appAlbum(videoId, "2")
    });
    $(".js-app-pass").unbind("click").click(function () {
        appAlbum(videoId, "1")
    });
}

function appAlbum(videoId, status) {
    $.ajax({
        url: "/set/partyset/setAlbumApproval",
        type: "post",
        dataType: "json",
        data: {
            videoId: videoId,
            status: status
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                $('#myTable').bootstrapTable('refresh');
                $("#approvermodal").modal("hide");
            }
        }
    })
}


function getAlbumTypeForSelect() {
    $.ajax({
        url: "/ret/partyget/getAlbumTypeForSelect",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                $("#albumType").append("<option value=''>请选择</option>")
                for (var i = 0; i < data.list.length; i++) {
                    $("#albumType").append("<option value='" + data.list[i].albumTypeId + "'>" + data.list[i].typeName + "</option>")
                }
            }
        }
    })
}
