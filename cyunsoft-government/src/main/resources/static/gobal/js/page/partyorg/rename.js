$(function () {
    jeDate("#renameTime", {
        format: "YYYY-MM-DD",
        minDate: getSysDate(),
        isinitVal: true
    });
    jeDate("#approvalTime", {
        format: "YYYY-MM-DD",
        minDate: getSysDate(),
        isinitVal: true
    });
    $('#remark').summernote({height: 300});

    $.ajax({
        url: "/ret/partyorgget/getPartyOrgTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            $.fn.zTree.init($("#menuTree"), setting1, data);
        }
    });
    $("#partyOrgId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $(".js-add-save").unbind("click").click(function () {
        addRenameRecord();
    })

    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });

    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
})

function addRenameRecord() {
    if ($("#partyOrgId").attr("data-value")== "") {
        layer.msg("党组织不能为空！");
        return;
    }
    if ($("#newName").val()== "") {
        layer.msg("新名称不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyorgset/insertPartyRename",
        type: "post",
        dataType: "json",
        data: {
            partyOrgId: $("#partyOrgId").attr("data-value"),
            newName: $("#newName").val(),
            pinYin: $("#pinYin").val(),
            newShortName: $("#newShortName").val(),
            renameTime: $("#renameTime").val(),
            approvalTime: $("#approvalTime").val(),
            docNum: $("#docNum").val(),
            remark: $("#remark").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}

var setting1 = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyorgget/getPartyOrgTree",// Ajax 获取数据的 URL 地址
        autoParam: ["partyOrgId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        // 禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "partyOrgId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "partyOrgName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("menuTree");
            var nodes = zTree.getSelectedNodes();
            var v = "";
            var vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.partyOrgId;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].partyOrgName + ",";
                vid += nodes[i].partyOrgId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#partyOrgId");
            idem.val(v);
            idem.attr("data-value", vid);
            var partyType = nodes[0].partyType;
            $("#oldType").attr("data-value", partyType);
            $.ajax({
                url: "/ret/partyparamget/getPartyTypeById",
                type: "post",
                dataType: "json",
                data: {
                    sortId: partyType
                },
                success: function (data) {
                    if (data.status == "200") {
                        $("#oldType").html(data.list.sortName);
                    } else if (data.status == "100") {
                        layer.msg(sysmsg[data.msg]);
                    } else if (data.status == "500") {
                        console.log(data.msg);
                    }
                }
            })
        }
    }
};
