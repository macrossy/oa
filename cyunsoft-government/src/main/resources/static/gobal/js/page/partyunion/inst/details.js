$(function () {
    $.ajax({
        url: "/ret/partyunionget/getPartyUnionInstById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var info = data.list;
                for (var id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 1);
                    } else if (id == "sortId") {
                        $.ajax({
                            url: "/ret/partyunionget/getPartyUnionInstSortById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info.sortId
                            },
                            success: function (data) {
                                if (data.status == "200") {
                                    if (data.list) {
                                        $("#sortId").html(data.list.sortName);
                                    }
                                }
                            }
                        });
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
