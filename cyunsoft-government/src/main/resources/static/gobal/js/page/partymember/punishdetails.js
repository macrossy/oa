$(function () {
    $.ajax({
        url: "/ret/partymemberget/getPartyPunishRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "punishLevel") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("中央、国家级(正)");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("中央、国家级(副)");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("省、部级");
                        } else if (recordInfo[id] == "4") {
                            $("#" + id).html("区（地、厅、司、局）级");
                        } else if (recordInfo[id] == "5") {
                            $("#" + id).html("副区（副地、副厅、副司、副局）级");
                        } else if (recordInfo[id] == "6") {
                            $("#" + id).html("县、处级");
                        } else if (recordInfo[id] == "7") {
                            $("#" + id).html("副县、副处级");
                        } else if (recordInfo[id] == "8") {
                            $("#" + id).html("乡、科级");
                        } else if (recordInfo[id] == "9") {
                            $("#" + id).html("副乡、副科级");
                        } else if (recordInfo[id] == "10") {
                            $("#" + id).html("股级");
                        } else if (recordInfo[id] == "11") {
                            $("#" + id).html("地（局）级");
                        } else if (recordInfo[id] == "12") {
                            $("#" + id).html("省（区、市）人民政府直属工作部门、人民团体");
                        } else if (recordInfo[id] == "13") {
                            $("#" + id).html("大行政区(军政委员会)");
                        } else if (recordInfo[id] == "14") {
                            $("#" + id).html("中央军委级");
                        } else if (recordInfo[id] == "15") {
                            $("#" + id).html("解放军总部级");
                        } else if (recordInfo[id] == "16") {
                            $("#" + id).html("大军区正级");
                        } else if (recordInfo[id] == "17") {
                            $("#" + id).html("大军区副级");
                        } else if (recordInfo[id] == "18") {
                            $("#" + id).html("正兵团级");
                        } else if (recordInfo[id] == "19") {
                            $("#" + id).html("副兵团级");
                        } else if (recordInfo[id] == "20") {
                            $("#" + id).html("正军级");
                        } else if (recordInfo[id] == "21") {
                            $("#" + id).html("副军级");
                        } else if (recordInfo[id] == "22") {
                            $("#" + id).html("正师级");
                        } else if (recordInfo[id] == "23") {
                            $("#" + id).html("副师(正旅)级");
                        } else if (recordInfo[id] == "24") {
                            $("#" + id).html("正团(副旅)级");
                        } else if (recordInfo[id] == "25") {
                            $("#" + id).html("副团级");
                        } else if (recordInfo[id] == "26") {
                            $("#" + id).html("正营级");
                        } else if (recordInfo[id] == "27") {
                            $("#" + id).html("副营级");
                        } else if (recordInfo[id] == "28") {
                            $("#" + id).html("正连级");
                        } else if (recordInfo[id] == "29") {
                            $("#" + id).html("副连级");
                        } else if (recordInfo[id] == "30") {
                            $("#" + id).html("排级");
                        } else if (recordInfo[id] == "31") {
                            $("#" + id).html("班级");
                        } else if (recordInfo[id] == "32") {
                            $("#" + id).html("基层单位");
                        } else if (recordInfo[id] == "33") {
                            $("#" + id).html("国际组织、机构");
                        } else if (recordInfo[id] == "34") {
                            $("#" + id).html("外国组织、机构");
                        } else if (recordInfo[id] == "35") {
                            $("#" + id).html("无级别");
                        } else if (recordInfo[id] == "36") {
                            $("#" + id).html("其他");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "memberId") {
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.userName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "partyType") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyTypeById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {sortId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.sortName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
