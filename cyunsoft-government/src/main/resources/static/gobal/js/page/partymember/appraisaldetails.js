$(function () {
    $.ajax({
        url: "/ret/partymemberget/getPartyAppraisalRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "appResult") {
                        if (recordInfo[id] == "1") {
                            $("#" + id).html("优秀");
                        } else if (recordInfo[id] == "2") {
                            $("#" + id).html("合格");
                        } else if (recordInfo[id] == "3") {
                            $("#" + id).html("不合格");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "reason") {
                        $.ajax({
                            url: "/ret/partyparamget/getParetyAppReaById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: recordInfo[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#reason").html(res.list.sortName);
                                    }
                                }
                            }
                        });
                    } else if (id == "situation") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyAppSituById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: recordInfo[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#situation").html(res.list.sortName);
                                    }
                                }
                            }
                        });
                    } else if (id == "memberId") {
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).html(res.list.userName);
                                    } else {
                                        $("#" + id).html("");
                                    }
                                }
                            }
                        });
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
