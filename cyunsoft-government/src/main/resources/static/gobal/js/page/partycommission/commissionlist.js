$(function () {
    $("#myTable").bootstrapTable({
        url: '/ret/partycommissionget/getAllCommissionRecordList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: false,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: false,// 启用搜索
        sortOrder: "asc",
        showColumns: false,// 是否显示 内容列下拉框
        showRefresh: false,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'deadline',
            title: '届次',
            width: '100px'
        }, {
            field: 'createType',
            title: '产生方式',
            width: '100px',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "差额";
                } else if (value == "2") {
                    return "等额";
                } else if (value == "3") {
                    return "其他";
                } else {
                    return "未知";
                }

            }
        }, {
            field: 'beginTime',
            title: '届始日期',
            width: '100px'
        }, {
            field: 'endTime',
            title: '届满日期',
            width: '100px'
        }, {
            field: 'secretary',
            width: '50px',
            title: '书记',
            formatter: function (value, row, index) {
                return getPartyMemberName(value);
            }
        }, {
            field: 'secretaryOther',
            width: '50px',
            title: '副书记',
            formatter: function (value, row, index) {
                return getPartyMemberName(value);
            }
        }, {
            field: 'memberId',
            width: '200px',
            title: '委员',
            formatter: function (value, row, index) {
                return getPartyMemberName(value);
            }
        },
            {
                field: 'remark',
                width: '100px',
                title: '备注'
            }, {
                field: 'attach',
                width: '100px',
                title: '相关文件',
                formatter: function (value, row, index) {
                    return createTableAttach(value);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
})

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function getPartyMemberName(memberIds) {
    var userNameStr = "";
    $.ajax({
        url: "/ret/partymemberget/getPartyMemeberByIds",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            memberIds: memberIds
        },
        success: function (data) {
            if (data.status == "200") {
                var userNameArr = [];
                if (data.list) {
                    for (var i = 0; i < data.list.length; i++) {
                        userNameArr.push(data.list[i].userName);
                    }
                }
                userNameStr = userNameArr.join(",");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
    return userNameStr;
}
