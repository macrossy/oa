$(function () {
    getMyPartyMemberInfo();
    getMyMemberTrainList();
    getMyPartyPostList();
    getMyAppraisalList();
    getMyPartyLifeList();
    getMyAdmRecordList();
    getMyMemberRewardList();
    getMyMemberPunishList();
    getMyMemberFamilyList();
})

function getMyMemberFamilyList() {
    $.ajax({
        url: "/ret/partymemberget/getMyMemberFamilyList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                if (recordInfo.length == 0) {
                    $("#tablePage8").append("<td colspan='9' align='center' style='font-size:14px;line-height:40px;'>未查到相关数据</td>");
                } else {
                    for (var i = 0; i < recordInfo.length; i++) {
                        var html = "<tr>";
                        html += "<td>" + (i + 1) + "</td>";
                        html += "<td>" + recordInfo[i].memberUserName + "</td>";
                        html += "<td>" + recordInfo[i].partyOrgName + "</td>";
                        html += "<td>" + recordInfo[i].userName + "</td>";
                        html += "<td>" + recordInfo[i].userSex + "</td>";
                        html += "<td>" + recordInfo[i].birthday + "</td>";
                        html += "<td>";
                        if (recordInfo[i].relation == "1") {
                            html += "丈夫";
                        } else if (recordInfo[i].relation == "2") {
                            html += "妻子";
                        } else if (recordInfo[i].relation == "3") {
                            html += "子女";
                        } else if (recordInfo[i].relation == "4") {
                            html += "父母";
                        } else if (recordInfo[i].relation == "5") {
                            html += "兄弟";
                        } else if (recordInfo[i].relation == "6") {
                            html += "姐妹";
                        } else {
                            html += "未知";
                        }
                        html += "</td>";
                        html += "<td>" + recordInfo[i].workUnit + "</td>";
                        html += "<td><a href=\"javascript:void(0);familydetails('" + recordInfo[i].recordId + "')\" class=\"btn btn-sky btn-xs\" >详情</a></td>";
                        html += "</tr>";
                        $("#tablePage8").append(html);
                    }
                }
            } else if (data.status == "100") {
                $("#tablePage8").append("<td colspan='9' align='center' style='font-size:14px;line-height:40px;'>" + data.msg + "</td>");
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function familydetails(recordId) {
    window.open("/app/core/partymember/familydetails?recordId=" + recordId);
}

function getMyMemberPunishList() {
    $.ajax({
        url: "/ret/partymemberget/getMyMemberPunishList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                if (recordInfo.length == 0) {
                    $("#tablePage7").append("<td colspan='8' align='center' style='font-size:14px;line-height:40px;'>未查到相关数据</td>");
                } else {
                    for (var i = 0; i < recordInfo.length; i++) {
                        var html = "<tr>";
                        html += "<td>" + (i + 1) + "</td>";
                        html += "<td>" + recordInfo[i].userName + "</td>";
                        html += "<td>" + recordInfo[i].partyOrgName + "</td>";
                        html += "<td>";
                        if (recordInfo[i].otherTitle != "") {
                            html += recordInfo[i].title + "/" + recordInfo[i].otherTitle;
                        } else {
                            html += recordInfo[i].title;
                        }
                        html += "</td>";
                        html += "<td>" + recordInfo[i].unitName + "</td>";
                        html += "<td>" + recordInfo[i].punishTime + "</td>";
                        html += "<td>" + recordInfo[i].punishReasons + "</td>";
                        html += "<td><a href=\"javascript:void(0);punishdetails('" + recordInfo[i].recordId + "')\" class=\"btn btn-sky btn-xs\" >详情</a></td>";
                        html += "</tr>";
                        $("#tablePage7").append(html);
                    }
                }
            } else if (data.status == "100") {
                $("#tablePage7").append("<td colspan='8' align='center' style='font-size:14px;line-height:40px;'>" + data.msg + "</td>");
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function punishdetails(recordId) {
    window.open("/app/core/partymember/punishdetails?recordId=" + recordId);
}


function getMyMemberRewardList() {
    $.ajax({
        url: "/ret/partymemberget/getMyMemberRewardList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                if (recordInfo.length == 0) {
                    $("#tablePage6").append("<td colspan='9' align='center' style='font-size:14px;line-height:40px;'>未查到相关数据</td>");
                } else {
                    for (var i = 0; i < recordInfo.length; i++) {
                        var html = "<tr>";
                        html += "<td>" + (i + 1) + "</td>";
                        html += "<td>" + recordInfo[i].userName + "</td>";
                        html += "<td>" + recordInfo[i].partyOrgName + "</td>";
                        html += "<td>";
                        if (recordInfo[i].otherTitle != "") {
                            html += recordInfo[i].title + "/" + recordInfo[i].otherTitle;
                        } else {
                            html += recordInfo[i].title;
                        }
                        html += "</td>";
                        html += "<td>";
                        if (recordInfo[i].rewardReasons == "1") {
                            html += "忠于职守，积极工作，成绩显著";
                        } else if (recordInfo[i].rewardReasons == "2") {
                            html += "遵守纪律，廉洁奉公，作风正派，办事公道，模范作用突出";
                        } else if (recordInfo[i].rewardReasons == "3") {
                            html += "在工作中有发明创造或者提出合理化建议，取得显著经济效益或者社会效益";
                        } else if (recordInfo[i].rewardReasons == "4") {
                            html += "为增进民族团结、维护社会稳定做出突出贡献";
                        } else if (recordInfo[i].rewardReasons == "5") {
                            html += "爱护公共财产，节约国家资财有突出成绩";
                        } else if (recordInfo[i].rewardReasons == "6") {
                            html += "防止或者消除事故有功，使国家和人民群众利益免受或者减少损失";
                        } else if (recordInfo[i].rewardReasons == "7") {
                            html += "在抢险、救灾等特定环境中奋不顾身，做出贡献";
                        } else if (recordInfo[i].rewardReasons == "8") {
                            html += "同违法违纪行为作斗争有功绩";
                        } else if (recordInfo[i].rewardReasons == "9") {
                            html += "在对外交往中为国家争得荣誉和利益";
                        } else if (recordInfo[i].rewardReasons == "10") {
                            html += "其他突出功绩";
                        } else {
                            html += "未知";
                        }
                        html += "</td>";
                        html += "<td>";
                        if (recordInfo[i].isInTime == "1") {
                            html += "是";
                        } else {
                            html += "否";
                        }
                        html += "</td>";
                        html += "<td>" + recordInfo[i].rewardTime + "</td>";

                        html += "<td>";
                        if (recordInfo[i].honoraryLevel == "1") {
                            html += "国家级荣誉称号";
                        } else if (recordInfo[i].honoraryLevel == "2") {
                            html += "省（自治区、直辖市）级荣誉称号";
                        } else if (recordInfo[i].honoraryLevel == "3") {
                            html += "部（委）级荣誉称号";
                        } else if (recordInfo[i].honoraryLevel == "4") {
                            html += "区（地、厅、司、局）级";
                        } else if (recordInfo[i].honoraryLevel == "5") {
                            html += "县、处级";
                        } else if (recordInfo[i].honoraryLevel == "6") {
                            html += "系统内部荣誉称号";
                        } else if (recordInfo[i].honoraryLevel == "7") {
                            html += "基层企事业单位荣誉称号";
                        } else if (recordInfo[i].honoraryLevel == "8") {
                            html += "国外授予的荣誉称号";
                        } else if (recordInfo[i].honoraryLevel == "9") {
                            html += "其他";
                        } else {
                            return "未知";
                        }
                        html += "</td>";
                        html += "<td><a href=\"javascript:void(0);rewarddetails('" + recordInfo[i].recordId + "')\" class=\"btn btn-sky btn-xs\" >详情</a></td>";
                        html += "</tr>";
                        $("#tablePage6").append(html);
                    }
                }
            } else if (data.status == "100") {
                $("#tablePage6").append("<td colspan='9' align='center' style='font-size:14px;line-height:40px;'>" + data.msg + "</td>");
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function rewarddetails(recordId) {
    window.open("/app/core/partymember/rewarddetails?recordId=" + recordId);
}

function getMyAdmRecordList() {
    $.ajax({
        url: "/ret/partymemberget/getMyAdmRecordList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                if (recordInfo.length == 0) {
                    $("#tablePage5").append("<td colspan='8' align='center' style='font-size:14px;line-height:40px;'>未查到相关数据</td>");
                } else {
                    for (var i = 0; i < recordInfo.length; i++) {
                        var html = "<tr>";
                        html += "<td>" + (i + 1) + "</td>";
                        html += "<td>" + recordInfo[i].userName + "</td>";
                        html += "<td>" + recordInfo[i].partyOrgName + "</td>";
                        html += "<td>" + recordInfo[i].otherPartyOrgName + "</td>";
                        html += "<td>";
                        if (recordInfo[i].postType == "1") {
                            html += "不在任";
                        } else if (recordInfo[i].postType == "2") {
                            html += "在任";
                        } else if (recordInfo[i].postType == "3") {
                            html += "挂任";
                        } else if (recordInfo[i].postType == "4") {
                            html += "试用";
                        } else {
                            html += "未知"
                        }
                        html += "</td>";
                        html += "<td>" + getAdmPostName(recordInfo[i].admPost) + "</td>";
                        html += "<td>" + recordInfo[i].postTime + " 至 " + recordInfo[i].lostTime + "</td>";
                        html += "<td><a href=\"javascript:void(0);admpostdetails('" + recordInfo[i].recordId + "')\" class=\"btn btn-sky btn-xs\" >详情</a></td>";
                        html += "</tr>";
                        $("#tablePage5").append(html);
                    }
                }
            } else if (data.status == "100") {
                $("#tablePage5").append("<td colspan='8' align='center' style='font-size:14px;line-height:40px;'>" + data.msg + "</td>");
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function admpostdetails(recordId) {
    window.open("/app/core/partymember/admpostdetails?recordId=" + recordId);
}


function getMyPartyLifeList() {
    $.ajax({
        url: "/ret/partymemberget/getMyPartyLifeList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                if (recordInfo.length == 0) {
                    $("#tablePage4").append("<td colspan='6' align='center' style='font-size:14px;line-height:40px;'>未查到相关数据</td>");
                } else {
                    for (var i = 0; i < recordInfo.length; i++) {
                        var html = "<tr>";
                        html += "<td>" + (i + 1) + "</td>";
                        html += "<td>" + recordInfo[i].userName + "</td>";
                        html += "<td>" + recordInfo[i].joinTime + "</td>";
                        html += "<td>";
                        if (recordInfo[i].lifeContent == "1") {
                            html += "支部委员会";
                        } else if (recordInfo[i].lifeContent == "2") {
                            html += "支部党员大会";
                        } else if (recordInfo[i].lifeContent == "3") {
                            html += "党小组会";
                        } else if (recordInfo[i].lifeContent == "4") {
                            html += "党课";
                        } else if (recordInfo[i].lifeContent == "5") {
                            html += "民主评议党员";
                        } else if (recordInfo[i].lifeContent == "6") {
                            html += "民主生活会";
                        } else if (recordInfo[i].lifeContent == "7") {
                            html += "讨论党组织工作";
                        } else if (recordInfo[i].lifeContent == "8") {
                            html += "评选党内先进";
                        } else if (recordInfo[i].lifeContent == "9") {
                            html += "推选党代表";
                        } else if (recordInfo[i].lifeContent == "10") {
                            html += "其他";
                        } else {
                            html += "未知";
                        }
                        html += "</td>";
                        html += "<td>";
                        if (recordInfo[i].reason == "1") {
                            html += "外出";
                        } else if (recordInfo[i].reason == "2") {
                            html += "生病";
                        } else if (recordInfo[i].reason == "3") {
                            html += "不愿意参加";
                        } else if (recordInfo[i].lifeContent == "4") {
                            html += "其他";
                        } else {
                            html += "未知";
                        }
                        html += "</td>";
                        html += "<td>" + recordInfo[i].remark + "</td>";
                        html += "</tr>";
                        $("#tablePage4").append(html);
                    }
                }
            } else if (data.status == "100") {
                $("#tablePage4").append("<td colspan='6' align='center' style='font-size:14px;line-height:40px;'>" + data.msg + "</td>");
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function getMyAppraisalList() {
    $.ajax({
        url: "/ret/partymemberget/getMyAppraisalList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                if (recordInfo.length == 0) {
                    $("#tablePage3").append("<td colspan='7' align='center' style='font-size:14px;line-height:40px;'>未查到相关数据</td>");
                } else {
                    for (var i = 0; i < recordInfo.length; i++) {
                        var html = "<tr>";
                        html += "<td>" + (i + 1) + "</td>";
                        html += "<td>" + recordInfo[i].userName + "</td>";
                        html += "<td>" + recordInfo[i].beginTime + " 至 " + recordInfo[i].endTime + "</td>";
                        html += "<td>" + getPartyAppSituName(recordInfo[i].situation) + "</td>";
                        html += "<td>" + getPartyAppRea(recordInfo[i].reason) + "</td>";
                        html += "<td>";
                        if (recordInfo[i].appResult == "1") {
                            html += "优秀";
                        } else if (recordInfo[i].appResult == "2") {
                            html += "合格";
                        } else if (recordInfo[i].appResult == "3") {
                            html += "不合格";
                        } else {
                            html += "未知";
                        }
                        html += "</td>";
                        html += "<td><a href=\"javascript:void(0);mypartyappraisaldetails('" + recordInfo[i].recordId + "')\" class=\"btn btn-sky btn-xs\" >详情</a></td>";
                        html += "</tr>";
                        $("#tablePage3").append(html);
                    }
                }
            } else if (data.status == "100") {
                $("#tablePage3").append("<td colspan='7' align='center' style='font-size:14px;line-height:40px;'>" + data.msg + "</td>");
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function mypartyappraisaldetails(recordId) {
    window.open("/app/core/partymember/appraisaldetails?recordId=" + recordId);
}

function getMyPartyPostList() {
    $.ajax({
        url: "/ret/partymemberget/getMyPartyPostList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                if (recordInfo.length == 0) {
                    $("#tablePage2").append("<td colspan='9' align='center' style='font-size:14px;line-height:40px;'>未查到相关数据</td>");
                } else {
                    for (var i = 0; i < recordInfo.length; i++) {
                        var html = "<tr>";
                        html += "<td>" + (i + 1) + "</td>";
                        html += "<td>" + recordInfo[i].userName + "</td>";
                        html += "<td>" + recordInfo[i].partyOrgName + "</td>";
                        html += "<td>" + recordInfo[i].otherPartyOrgName + "</td>";
                        html += "<td>" + recordInfo[i].deadline + "</td>";
                        html += "<td>" + getGovPostName(recordInfo[i].govPost) + "</td>";
                        html += "<td>";
                        if (recordInfo[i].takeType == "1") {
                            html += "党委决定任职";
                        } else if (recordInfo[i].takeType == "2") {
                            html += "差额选举";
                        } else if (recordInfo[i].takeType == "3") {
                            html += "等额选举";
                        } else {
                            html += "未知";
                        }
                        html += "</td>";
                        html += "<td>" + recordInfo[i].beginTime + " 至 " + recordInfo[i].endTime + "</td>";
                        html += "<td><a href=\"javascript:void(0);mypartypostdetails('" + recordInfo[i].recordId + "')\" class=\"btn btn-sky btn-xs\" >详情</a></td>";
                        html += "</tr>";
                        $("#tablePage2").append(html);
                    }
                }
            } else if (data.status == "100") {
                $("#tablePage2").append("<td colspan='9' align='center' style='font-size:14px;line-height:40px;'>" + data.msg + "</td>");
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function mypartypostdetails(recordId) {
    window.open("/app/core/partymember/partypostdetails?recordId=" + recordId);
}

function getMyMemberTrainList() {
    $.ajax({
        url: "/ret/partymemberget/getMyMemberTrainList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                if (recordInfo.length == 0) {
                    $("#tablePage1").append("<td colspan='12' align='center' style='font-size:14px;line-height:40px;'>未查到相关数据</td>");
                } else {
                    for (var i = 0; i < recordInfo.length; i++) {
                        var html = "<tr>";
                        html += "<td>" + (i + 1) + "</td>";
                        html += "<td>" + recordInfo[i].partyOrgName + "</td>";
                        html += "<td>" + recordInfo[i].userName + "</td>";
                        html += "<td>" + recordInfo[i].organizer + "</td>";
                        html += "<td>" + recordInfo[i].partyOrgName + "</td>";
                        html += "<td>";
                        if (recordInfo[i].classType == "1") {
                            html += "进修班";
                        } else if (recordInfo[i].classType == "2") {
                            html += "培训班";
                        } else if (recordInfo[i].classType == "3") {
                            html += "理论班";
                        } else if (recordInfo[i].classType == "4") {
                            html += "专题研讨（研究）班";
                        } else if (recordInfo[i].classType == "5") {
                            html += "其他";
                        } else {
                            html += "未知";
                        }
                        html += "</td>";
                        html += "<td>" + recordInfo[i].className + "</td>";
                        html += "<td>" + recordInfo[i].beginTime + " 至 " + recordInfo[i].endTime + "</td>";
                        html += "<td>";
                        if (recordInfo[i].isAbroad == "1") {
                            html += "是";
                        } else {
                            html += "否";
                        }
                        html += "</td>";
                        html += "<td>" + recordInfo[i].period + "</td>";
                        html += "<td>";
                        if (recordInfo[i].trainStatus == "1") {
                            html += "毕业";
                        } else if (recordInfo[i].trainStatus == "2") {
                            html += "肄业";
                        } else if (recordInfo[i].trainStatus == "3") {
                            html += "结业";
                        } else if (recordInfo[i].trainStatus == "4") {
                            html += "在训";
                        } else if (recordInfo[i].trainStatus == "5") {
                            html += "未完成";
                        } else if (recordInfo[i].trainStatus == "6") {
                            html += "转学校";
                        } else if (recordInfo[i].trainStatus == "7") {
                            html += "转系";
                        } else if (recordInfo[i].trainStatus == "8") {
                            html += "转专业";
                        } else if (recordInfo[i].trainStatus == "9") {
                            html += "其他";
                        } else {
                            html += "未知";
                        }
                        html += "</td>";
                        html += "<td><a href=\"javascript:void(0);mytraindetails('" + recordInfo[i].recordId + "')\" class=\"btn btn-sky btn-xs\" >详情</a></td>";
                        html += "</tr>";
                        $("#tablePage1").append(html);
                    }
                }
            } else if (data.status == "100") {
                $("#tablePage1").append("<td colspan='12' align='center' style='font-size:14px;line-height:40px;'>" + data.msg + "</td>");
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function mytraindetails(recordId) {
    window.open("/app/core/partymember/traindetails?recordId=" + recordId);
}

function getMyPartyMemberInfo() {
    $.ajax({
        url: "/ret/partymemberget/getMyPartyMemberById",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var info = data.list;
                for (var id in info) {
                    if (id == "partyOrgId") {
                        $("#partyOrgId").attr("data-value", info[id]);
                        $.ajax({
                            url: "/ret/partyorgget/getPartyOrgById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {
                                partyOrgId: info[id]
                            },
                            success: function (res) {
                                if (res.status == 200) {
                                    if (res.list) {
                                        $("#partyOrgId").html(res.list.partyOrgName);
                                    }
                                } else if (res.status == "100") {
                                    console.log(res.msg);
                                } else {
                                    console.log(res.msg);
                                }
                            }
                        });
                    } else if (id == "userType") {
                        if (info[id] == "1") {
                            $("#" + id).html("正式党员");
                        } else if (info[id] == "2") {
                            $("#" + id).html("预备党员");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "nation") {
                        $("#" + id).html(getCodeClassName(info[id], "nation"));
                    } else if (id == "partyStatus") {
                        if (info[id] == "1") {
                            $("#" + id).html("正式党员");
                        } else if (info[id] == "2") {
                            $("#" + id).html("预备党员");
                        } else if (info[id] == "3") {
                            $("#" + id).html("已出党");
                        } else if (info[id] == "4") {
                            $("#" + id).html("已停止党籍");
                        } else if (info[id] == "5") {
                            $("#" + id).html("已死亡");
                        } else {
                            $("#" + id).html("未知");
                        }

                    } else if (id == "isLost") {
                        if (info[id] == "1") {
                            $("#" + id).html("是");
                        } else if (info[id] == "0") {
                            $("#" + id).html("否");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "isFlow") {
                        if (info[id] == "1") {
                            $("#" + id).html("是");
                        } else if (info[id] == "0") {
                            $("#" + id).html("否");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "isMg") {
                        if (info[id] == "1") {
                            $("#" + id).html("是");
                        } else if (info[id] == "0") {
                            $("#" + id).html("否");
                        } else {
                            $("#" + id).html("未知");
                        }
                    } else if (id == "workingConditions") {
                        $("#" + id).html(getCodeClassName(info[id], "gov_working_conditions"));
                    } else if (id == "nativePlace") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyNativePlaceById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#nativePlace").html(res.list.sortName);
                                    } else {
                                        $("#nativePlace").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "education") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyEducationById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#education").html(res.list.sortName);
                                    } else {
                                        $("#education").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "degree") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyDegreeById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#degree").html(res.list.sortName);
                                    } else {
                                        $("#degree").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "userPost") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyPostById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#userPost").html(res.list.sortName);
                                    } else {
                                        $("#userPost").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "admPosition") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyAdmPositionById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#admPosition").html(res.list.sortName);
                                    } else {
                                        $("#admPosition").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "positionalTitles") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyTitleById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#positionalTitles").html(res.list.sortName);
                                    } else {
                                        $("#positionalTitles").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "classSituation") {
                        $.ajax({
                            url: "/ret/partyparamget/getPartyStratumById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#classSituation").html(res.list.sortName);
                                    } else {
                                        $("#classSituation").html("");
                                    }
                                }
                            }
                        });
                    } else if (id == "photo") {
                        $("#file_img").attr("src", "/sys/file/getStaticImg?module=govphotos&fileName=" + info[id]);
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
                $("#updatabut").unbind("click").click(function () {
                    updatePartyMember(memberId);
                });
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getPartyAppSituName(sortId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/partyparamget/getPartyAppSituById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            sortId: sortId
        },
        success: function (res) {
            if (res.status == "200") {
                if (res.list) {
                    returnStr = res.list.sortName;
                }
            }
        }
    });
    return returnStr;
}

function getPartyAppRea(sortId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/partyparamget/getPartyAppReaById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            sortId: sortId
        },
        success: function (res) {
            if (res.status == "200") {
                if (res.list) {
                    returnStr = res.list.sortName;
                }
            }
        }
    });
    return returnStr;
}

function getGovPostName(sortId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/partyparamget/getPartyGovPostById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            sortId: sortId
        },
        success: function (res) {
            if (res.status == "200") {
                if (res.list) {
                    returnStr = res.list.sortName;
                }
            }
        }
    });
    return returnStr;
}

function getAdmPostName(sortId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/partyparamget/getPartyAdmPositionById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            sortId: sortId
        },
        success: function (res) {
            if (res.status == "200") {
                if (res.list) {
                    returnStr = res.list.sortName;
                }
            }
        }
    });
    return returnStr;
}
