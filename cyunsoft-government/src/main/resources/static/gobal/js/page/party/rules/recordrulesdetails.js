$(function () {
    $.ajax({
        url: "/ret/partyget/getPartyRulesRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var rulesInfo = data.list;
                for (var id in rulesInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", rulesInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "memberId") {
                        $("#" + id).html(getPartyMemberName(rulesInfo[id]));
                    } else if (id == "sortId") {
                        $.ajax({
                            url: "/ret/partyget/getPartyRulesSortById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: rulesInfo[id]
                            },
                            success: function (data) {
                                if (data.status == "200") {
                                    if (data.list) {
                                        $("#sortId").html(data.list.sortName);
                                    } else {
                                        $("#sortId").val("");
                                    }
                                }
                            }
                        });
                    } else if (id == "remark") {
                        $("#remark").html(rulesInfo[id]);
                    } else {
                        $("#" + id).html(rulesInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})

function getPartyMemberName(memberIds) {
    var userNameStr = "";
    $.ajax({
        url: "/ret/partymemberget/getPartyMemeberByIds",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            memberIds: memberIds
        },
        success: function (data) {
            if (data.status == "200") {
                var userNameArr = [];
                if (data.list) {
                    for (var i = 0; i < data.list.length; i++) {
                        userNameArr.push(data.list[i].userName);
                    }
                }
                userNameStr = userNameArr.join(",");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
    return userNameStr;
}
