$(function () {
    $.ajax({
        url: "/ret/partyunionget/getPartyUnionMemberById",
        type: "post",
        dataType: "json",
        data: {
            memberId: memberId
        },
        success: function (data) {
            if (data.status == 200) {
                let info = data.list;
                for (let id in info) {
                    if (id == "photos") {
                        $("#file_img").attr("src", "/sys/file/getStaticImg?module=govphotos&fileName=" + info[id]);
                    } else if (id == "deptId") {
                        $.ajax({
                            url: "/ret/partyunionget/getPartyUnionDeptById",
                            type: "post",
                            dataType: "json",
                            data: info[id],
                            success: function (data) {
                                if (data.status == "500") {
                                    console.log(data.msg);
                                } else if (data.status == "100") {
                                    layer.msg(sysmsg[data.msg]);
                                } else {
                                    $("#deptId").html(data.list.deptName);
                                }
                            }
                        })
                    } else if (id == "levelId") {
                        $.ajax({
                            url: "/ret/partyunionget/getPartyUnionLevelById",
                            type: "post",
                            dataType: "json",
                            data: info[id],
                            success: function (data) {
                                if (data.status == "500") {
                                    console.log(data.msg);
                                } else if (data.status == "100") {
                                    layer.msg(sysmsg[data.msg]);
                                } else {
                                    $("#levelId").html(data.list.levelName);
                                }
                            }
                        })
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
})
