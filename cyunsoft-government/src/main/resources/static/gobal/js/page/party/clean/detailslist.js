$(function () {
    if (type == "1") {
        var url = '/ret/partyget/getMyCleanExposeList';
        query(url, "曝光记录列表");
    } else if (type == "2") {
        var url = '/ret/partyget/getMyCleanMeritoriousList';
        query(url, "先进事迹列表");
    } else if (type == "3") {
        var url = '/ret/partyget/getMyCleanReportList';
        query(url, "领导报告列表");
    } else if (type == "4") {
        var url = '/ret/partyget/getMyCleanSuggestionsList';
        query(url, "献计献策列表");
    } else if (type == "5") {
        var url = '/ret/partyget/getMyCleanComprehensionList';
        query(url, "学习感悟列表");
    } else if (type == "6") {
        var url = '/ret/partyget/getMyCleanTipoffsList';
        query(url, "群众举报列表");
    }
})

function query(url, title) {
    $("#myTable").bootstrapTable({
        url: url,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            field: '',
            title: '<div class="widget-header bg-themeprimary"><span class="widget-caption" id="titles">' + title + '</span></div>',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return getRecordHtml(index, row);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function getRecordHtml(i, recordInfo) {
    html = "<ol><li>\n" +
        "<span class=\"listnum1\">NO." + (i + 1) + "</span> <a href=\"#\" onclick=\"readDetails('" + recordInfo.recordId + "')\" title=\"" + recordInfo.title + "\"><b>" + recordInfo.title + "</b>\n" +
        "<p class=\"c9\">" + recordInfo.subheading + "</p>\n" +
        "</a>\n" +
        "</li></ol>";
    return html;
}

function readDetails(recordId) {
    window.open("/app/core/party/details?type=" + type + "&recordId=" + recordId)
}
