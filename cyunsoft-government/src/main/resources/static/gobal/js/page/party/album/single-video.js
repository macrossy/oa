$(function () {
    let wv = $(".embed-video").width()+"px";
    //let hv = $(".embed-video").height()+"px";
    let hv = ($(".embed-video").width()*0.57)+"px";
    console.log(wv)
    console.log(hv);
    $("#videoEl").css({"width":wv,"height":hv});
    getVideoInfo();
    getAlbumVideoByCreateUser();
    $(".js-sendComm").unbind("click").click(function () {
        setComments();
    });
    getAlbumCommentsList();
    setAlbumVideoClickCount();
})

function getVideoInfo() {
    $.ajax({
        url: "/ret/partyget/getPartyAlbumVideoById",
        type: "post",
        dataType: "json",
        data: {videoId: videoId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                $("#videoTitle").html(data.list.title);
                $("#clickCount").html("播放次数：" + data.list.clickCount + "次");
                $("#subheading").html(data.list.subheading);
                $("#lastSendTime").html("发布时间：" + data.list.sendTime);
                setSendVideoUserInfo(data.list.createUser);
                $(".js-video-create-user").unbind("click").click(function () {
                    setSubscribeUser(data.list.createUser)
                })
            }
        }
    })
}

function setSendVideoUserInfo(accountIds) {
    $.ajax({
        url: "/ret/unitget/getAllUserInfoByAccountList",
        type: "post",
        dataType: "json",
        data: {accountStrs: accountIds},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                $("#createUserHeadImg").attr("src", "/sys/file/getOtherHeadImg?headImg=" + data.list[0].headImg);
                $("#createUserName").html(data.list[0].userName);
            }
        }
    })
}

function getAlbumCommentsList() {
    $.ajax({
        url: "/ret/partyget/getAlbumCommentsList",
        type: "post",
        dataType: "json",
        data: {videoId: videoId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var infoList = data.list;
                for (var i = 0; i < infoList.length; i++) {
                    var headImg = "/sys/file/getOtherHeadImg?headImg=" + infoList[i].headImg;
                    var createUser = infoList[i].createUserName;
                    if (infoList[i].anonymousFlag == "0") {
                        headImg = "/assets/img/nm.jpg";
                        createUser = "匿名";
                    }
                    var template = ['<li>',
                        '                                    <div class="avatar"><img src="' + headImg + '" alt="">',
                        '                                    </div>',
                        '                                    <div class="comment-content">',
                        '                                        <div class="comment-by">' + createUser + '</span>',
                        '                                        </div>',
                        '                                        <p>' + infoList[i].remark + '</p>',
                        '                                    </div>',
                        '                                </li>'].join("");
                    $("#commentsList").append(template);
                }
            }
        }
    })
}


function setSubscribeUser(accountId) {
    $.ajax({
        url: "/set/partyset/setSubscribeUser",
        type: "post",
        dataType: "json",
        data: {videoId: videoId, subscribeUser: accountId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}

function setAlbumVideoClickCount() {
    $.ajax({
        url: "/set/partyset/setAlbumVideoClickCount",
        type: "post",
        dataType: "json",
        data: {videoId: videoId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
            }
        }
    })
}


function getAlbumVideoByCreateUser() {
    $.ajax({
        url: "/ret/partyget/getAlbumVideoByCreateUser",
        type: "post",
        dataType: "json",
        data: {videoId: videoId},
        success: function (data) {
            if (data.status == "200") {
                var topVideo = data.list;
                for (var i = 0; i < topVideo.length; i++) {
                    var tempLate = ['<div>',
                        '                                <a href="/app/core/party/album/single-video?attachId=' + topVideo[i].attach + '&videoId=' + topVideo[i].videoId + '" class="video-post video-post-list">',
                        '                                    <!-- Blog Post Thumbnail -->',
                        '                                    <div class="video-post-thumbnail">',
                        '                                        <span class="video-post-count">' + topVideo[i].fileSize + 'M</span>',
                        '                                        <span class="video-post-time">' + topVideo[i].videoDuration + '</span>',
                        '                                        <span class="play-btn-trigger"></span>',
                        '                                        <img src="/sys/file/getVideoPic?attachId=' + topVideo[i].attach + '&fileName=' + topVideo[i].pics + '" alt="">',
                        '                                    </div>',
                        '                                    <!-- Blog Post Content -->',
                        '                                    <div class="video-post-content">',
                        '                                        <h3>' + topVideo[i].title + ' </h3>',
                        '                                        <img src="/sys/file/getOtherHeadImg?headImg=' + topVideo[i].headImg + '" alt="">',
                        '                                        <span class="video-post-user">' + topVideo[i].createUserName + '</span>',
                        '                                        <span class="video-post-views">播放:' + topVideo[i].clickCount + '次</span>',
                        '                                        <span class="video-post-date"> ' + getshowtime(topVideo[i].sendTime) + '</span>',
                        '                                    </div>',
                        '                                </a>',
                        '                            </div>'].join("");
                    $("#lastvideolist").append(tempLate);
                }

            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function setComments() {
    $.ajax({
        url: "/set/partyset/insertPartyAlbumComments",
        type: "post",
        dataType: "json",
        data: {videoId: videoId, remark: $("#remark").val()},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
