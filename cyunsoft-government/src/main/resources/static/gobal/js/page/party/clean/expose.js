$(function () {
    getCodeClass("exposeType", "party_expose");
    $("#createbut").unbind("click").click(function () {
        addExposeRecord();
    })
    $('#content').summernote({height: 300});
})

function addExposeRecord() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyset/insertPartyCleanExpose",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            mainPic: $("#file").attr("data-value"),
            exposeType: $("#exposeType").val(),
            memberId: $("#memberId").attr("data-value"),
            content: $("#content").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                window.location.reload();
                layer.msg(sysmsg[data.msg]);
            }
        }
    })
}
