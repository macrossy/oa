$(function () {
    getSmsConfig("msgType", "lesMeet");
    jeDate("#beginTime", {
        format: "YYYY-MM-DD hh:mm",
        isinitVal: true
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD hh:mm",
        isinitVal: true
    });
    jeDate("#meetYear", {
        format: "YYYY",
        isinitVal: true
    });
    $('#institution').summernote({height: 300});
    $("#createbut").unbind("click").click(function () {
        insertLesMeet();
    })
})

function insertLesMeet() {
    if ($("#title").val()== "") {
        layer.msg("标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partyorgset/insertPartyLesMeet",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            meetType: $("#meetType").val(),
            title: $("#title").val(),
            meetModel: $("#meetModel").val(),
            userCount: $("#userCount").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            address: $("#address").val(),
            meetYear: $("#meetYear").val(),
            joinMember: $("#joinMember").attr("data-value"),
            otherPriv: $("#otherPriv").val(),
            attach: $("#attach").attr("data_value"),
            remark: $("#remark").val(),
            msgType: getCheckBoxValue("msgType"),
            institution: $("#institution").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
