var zTree;
var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/partyget/getRulesSortTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "sortLevel",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    }
};
$(function () {
    $.ajax({
        url: "/ret/partyget/getRulesSortTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            zTree = $.fn.zTree.init($("#tree"), setting, data);// 初始化树节点时，添加同步获取的数据
        }
    });
    query("");
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("destroy");
        query("");
    })
});

function zTreeOnClick(event, treeId, treeNode) {
    $("#myTable").bootstrapTable("destroy");
    query(treeNode.sortId);
}

function query(sortId) {
    $("#myTable").bootstrapTable({
        url: '/ret/partyget/getPartyRulesRecordList?sortId=' + sortId,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            title: '规章制度标题',
            width: '100px'
        }, {
            field: 'subheading',
            title: '副标题',
            width: '200px'
        }, {
            field: 'sortName',
            width: '100px',
            title: '所属分类'
        }, {
            field: 'version',
            width: '100px',
            title: '版本'
        }, {
            field: 'createTime',
            width: '100px',
            title: '创建时间'
        }, {
            field: 'createUser',
            title: '创建人',
            width: '100px',
            formatter: function (value, row, index) {
                return getUserNameByStr(value);
            }
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '120px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);tolearn('" + recordId + "')\" class=\"btn btn-sky btn-xs\" >学习</a>";
    return html;
}

function tolearn(recordId) {
    window.open("/app/core/party/recordrulesdetails?view=learn&recordId=" + recordId);
}
