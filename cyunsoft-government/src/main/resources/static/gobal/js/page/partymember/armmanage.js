$(function () {
    jeDate("#armyTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate()
    });
    jeDate("#birthday", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate()
    });
    jeDate("#partyTime", {
        format: "YYYY-MM-DD",
        maxDate: getSysDate()
    });
    $('#rewards').summernote({height: 300});
    $('#remark').summernote({height: 300});
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })

})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/partymemberget/getArmyUserList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'cardId',
            width: '100px',
            title: '身份证号'
        }, {
            field: 'userName',
            title: '党员姓名',
            width: '100px',
            formatter: function (value, row, index) {
                if (row.mUserName == "" || row.mUserName == null) {
                    return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+row.userName+"</a>";
                } else {
                    return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer'>"+row.mUserName+"</a>";
                }
            }
        }, {
            field: 'partyOrg',
            title: '所在党支部',
            width: '100px'
        }, {
            field: 'userType',
            width: '100px',
            title: '人员类型',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "民主党派人士";
                } else if (value == "2") {
                    return "无党派人士";
                } else if (value == "3") {
                    return "党外知识分子";
                } else if (value == "4") {
                    return "少数民族人士";
                } else if (value == "5") {
                    return "宗教界人士";
                } else if (value == "6") {
                    return "非公有制经济人士";
                } else if (value == "7") {
                    return "新的社会阶层人士";
                } else if (value == "8") {
                    return "出国和归国留学人员";
                } else if (value == "9") {
                    return "香港同胞、澳门同胞";
                } else if (value == "10") {
                    return "台湾同胞及其在大陆的亲属";
                } else if (value == "11") {
                    return "华侨、归侨及侨眷";
                } else if (value == "12") {
                    return "其它需要联系和团结的人员";
                } else if (value == "13") {
                    return "其他";
                } else {
                    return "未知";
                }
            }
        }, {
            field: 'userPost',
            title: '职务',
            width: '100px'
        }, {
            field: 'tel',
            width: '100px',
            title: '联系电话'
        }, {
            field: 'address',
            title: '通讯地址',
            width: '150px'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '100px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        userType: $("#userTypeQuery").val(),
        education: $("#educationQuery").val(),
        partyMember: $("#partyMemberQuery").val(),
        armyRank: $("#armyRankQuery").val()
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);deleteArmy('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function details(recordId) {
    window.open("/app/core/partymember/armydetails?recordId=" + recordId);
}

function deleteArmy(recordId) {
    if (confirm("确定删除当前记录吗？")) {
        $.ajax({
            url: "/set/partymemberset/deletePartyArmy",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function edit(recordId) {
    $("#armylistdiv").hide();
    $("#armydiv").show();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    $.ajax({
        url: "/ret/partymemberget/getPartyArmyById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "rewards") {
                        $("#rewards").code(recordInfo[id]);
                    } else if (id == "remark") {
                        $("#remark").code(recordInfo[id]);
                    } else if (id == "memberId") {
                        $("#memberId").attr("data-value", recordInfo[id]);
                        $.ajax({
                            url: "/ret/partymemberget/getPartyMemberById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {memberId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (res.list) {
                                        $("#" + id).val(res.list.userName);
                                    } else {
                                        $("#" + id).val("");
                                    }
                                }
                            }
                        });
                    } else {
                        $("#" + id).val(recordInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updateArmy(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function updateArmy(recordId) {
    var userName = "";
    if ($("#accountId").val() != "") {
        userName = $("#accountId").val();
    } else {
        userName = $("#userName").val();
    }
    if(userName=="")
    {
        layer.msg("党员不能为空！");
        return;
    }
    $.ajax({
        url: "/set/partymemberset/updatePartyArmy",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            sortNo: $("#sortNo").val(),
            memberId: $("#memberId").attr("data-value"),
            userName: userName,
            userSex: $("#userSex").val(),
            cardId: $("#cardId").val(),
            birthday: $("#birthday").val(),
            userType: $("#userType").val(),
            partyMember: $("#partyMember").val(),
            partyTime: $("#partyTime").val(),
            partyOrg: $("#partyOrg").val(),
            party: $("#party").val(),
            userLevel: $("#userLevel").val(),
            tel: $("#tel").val(),
            address: $("#address").val(),
            armyTime: $("#armyTime").val(),
            armyRank: $("#armyRank").val(),
            education: $("#education").val(),
            userPost: $("#userPost").val(),
            attach: $("#attach").attr("data_value"),
            rewards: $("#rewards").code(),
            remark: $("#remark").code()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function goback() {
    $("#armydiv").hide();
    $("#armylistdiv").show();
}
