let vm = new Vue({
    el: '#app',  //实例化对象
    data: {
        classArr: []
    },
    mounted: function () {
        this.getAllClassList();
        getAllEduGradeList();
        getAllEduMajorList();
    },
    methods: {
        //存放实例方法
        getAllClassList: function () {
            $.ajax({
                url: "/ret/eduget/getEduClassListForSet",
                type: "post",
                dataType: "json",
                data: {
                    gradeId: $("#gradeId").val(),
                    departmentsId: $("#departmentsId").attr("data-value"),
                    majorId: $("#majorId").val(),
                },
                success: function (data) {
                    if (data.status == "200") {
                        vm.classArr = data.list;
                    } else if (data.status == "100") {
                        layer.msg(sysmsg[data.msg]);
                    } else {
                        console.log(data.msg);
                    }
                }
            });
        }
    }
})


$(function () {
    $.ajax({
        url: "/ret/eduget/getEduDepartmentsTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "全部",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#menuTree"), setting1, newTreeNodes);
        }
    });
    $("#departmentsId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent").css({
            "width": $(this).outerWidth() + "px",
            "left": (document.getElementById("departmentsId").offsetLeft) + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $("#menuContent").hide();
    });
    $("#menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
});
var setting1 = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/eduget/getEduDepartmentsTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "sortLevel",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            let zTree = $.fn.zTree.getZTreeObj("menuTree");
            let nodes = zTree.getSelectedNodes();
            let v = [];
            let vid = [];
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v.push(nodes[i].sortName);
                vid.push(nodes[i].sortId);
            }
            $("#departmentsId").val(v.join(","));
            $("#departmentsId").attr("data-value", vid.join(","));
        }
    }
};

function getAllEduGradeList() {
    $.ajax({
        url: "/ret/eduget/getAllEduGradeList",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                $("#gradeId").append("<option value=''>全部</option>");
                for (var i = 0; i < data.list.length; i++) {
                    $("#gradeId").append("<option value='" + data.list[i].gradeId + "'>" + data.list[i].title + "</option>");
                }
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getAllEduMajorList() {
    $.ajax({
        url: "/ret/eduget/getEduMajorListForTags",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                $("#majorId").append("<option value=''>全部</option>");
                for (var i = 0; i < data.list.length; i++) {
                    $("#majorId").append("<option value='" + data.list[i].majorId + "'>" + data.list[i].title + "</option>");
                }
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
