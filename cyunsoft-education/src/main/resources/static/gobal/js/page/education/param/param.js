function createAutoSelect(module) {
    $.ajax({
        url: "/ret/eduget/getCodeListByModule",
        type: "post",
        dataType: "json",
        async: false,
        data: {module: module},
        success: function (data) {
            if (data.status == "200") {
                var html = "<option value=''>请选择</option>";
                for (var i = 0; i < data.list.length; i++) {
                    html += "<option value=\"" + data.list[i].codeValue + "\">" + data.list[i].codeName + "</option>";
                }
                $("#" + module).html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}

function createAutoSelectQuery(module) {
    $.ajax({
        url: "/ret/eduget/getCodeListByModule",
        type: "post",
        dataType: "json",
        async: false,
        data: {module: module},
        success: function (data) {
            if (data.status == "200") {
                var html = "<option value=''>请选择</option>";
                for (var i = 0; i < data.list.length; i++) {
                    html += "<option value=\"" + data.list[i].codeValue + "\">" + data.list[i].codeName + "</option>";
                }
                $("#" + module + "Query").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
}


function getEduClassCodeName(module, value) {
    var returnStr = [];
    $.ajax({
        url: "/ret/eduget/getEduClassCodeName",
        type: "post",
        dataType: "json",
        async: false,
        data: {module: module, codeValue: value},
        success: function (data) {
            if (data.status == "200") {
                for (var i = 0; i < data.list.length; i++) {
                    returnStr.push(data.list[i].codeName);
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
    return returnStr.join(",");
}

function selectMajor(obj, opt) {
    if ($("#selectdiv").length == 0) {
        $("body").append("<div id='selectdiv'></div>");
    }
    var u = $(obj).attr("opt-id");
    var majorIds = $("#" + u).attr("data-value");
    $("#selectdiv").html(majselect);
    $("#majordiv").modal("show");
    initMajorSelect(majorIds, opt);
    $('.js-selectmajorall').unbind("click").click(function () {
        if (opt == 'true') {
            selectmajorAll();
        } else {
            layer.msg(sysmsg['CAN_NOT_MULTIPLE']);
        }
    });
    $('.js-unselectmajorall').unbind("click").click(function () {
        unselectmajorAll();
    });
    $(".selectokbtn").unbind("click").click(function () {
        var nameStr = [];
        var marjorStr = [];
        $(".selectedmajor div").each(function () {
            nameStr.push($(this).attr("data-name"));
            marjorStr.push($(this).attr("data-value"));
        });
        $("#" + u).val(nameStr.join(","));
        $("#" + u).attr("data-value", marjorStr.join(","));
        $('#majordiv').modal('hide');
    });
}


function initMajorSelect(majorIds, opt) {
    $.ajax({
        url: "/ret/eduget/getEduMajorListForTags",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var majorList = data.list;
                if (majorList) {
                    for (var i = 0; i < majorList.length; i++) {
                        if (("," + majorIds + ",").indexOf((","
                            + majorList[i].majorId + ",")) < 0) {
                            $(".selectmajor").append(
                                "<div class=\"selectdiv\" onclick=\"doSelectMajor(this,'"
                                + opt + "');\"  data-name=\""
                                + majorList[i].title
                                + "\" data-value=\""
                                + majorList[i].majorId
                                + "\"><span>"
                                + majorList[i].title
                                + "</span></div>");
                        } else {
                            $(".selectedmajor").append(
                                "<div class=\"selecteddiv\" onclick=\"doUnSelectMajor(this,'"
                                + opt + "');\"  data-name=\""
                                + majorList[i].title
                                + "\" data-value=\""
                                + majorList[i].majorId
                                + "\"><span>"
                                + majorList[i].title
                                + "</span></div>");
                        }
                    }
                }
            } else if (data.status == 100) {
                console.log(data.msg);
            } else {
                console.log(data.msg);
            }
        }
    });
}


let majselect = [
    ' <div id="majordiv" class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">',
    '        <div class="modal-dialog" style="overflow: auto;border: 1px solid silver;">',
    '            <div class="modal-content" style="width: 450px;">',
    '                <div class="modal-header">',
    '                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>',
    '                    <h4 class="modal-title" id="myLargeModalLabel">专业选择</h4>',
    '                </div>',
    '                <div class="modal-body" style="padding: 0px;">',
    '                    <div class="row" style="margin: 0px">',
    '    			                 <div class="col-xs-6 col-md-6" style="padding-left:4px;padding-right:4px;">',
    '				                 <div class="flat radius-bordered">',
    '		                                <div class="widget-header bg-blueberry" style="text-align: center;padding-left:0px;">',
    '		                                    <span class="widget-caption" style="float:none;">备选专业</span>',
    '		                                    <a class="selectmajorall js-selectmajorall" style="cursor: pointer;color: white">全选</a>',
    '		                                </div>',
    '										<div class="widget-body-1 selectmajor">',
    '											',
    '		                                </div>',
    '		                           </div>',
    '				                 </div>',
    '				                 <div class="col-xs-6 col-md-6" style="padding-right: 4px;padding-left:10px;">',
    '				                 <div class="flat radius-bordered">',
    '		                                <div class="widget-header bg-palegreen" style="text-align: center;padding-left:0px;">',
    '		                                    <span class="widget-caption" style="float:none;">已选专业</span>',
    '		                                    <a class="selectmajorall js-unselectmajorall" style="cursor: pointer;color: white">反选</a>',
    '		                                </div>',
    '										<div class="widget-body-1 selectedmajor">',
    '										',
    '		                                </div>',
    '		                            </div>',
    '				                 </div>',
    '                    </div>',
    '                </div>',
    '                <div class="modal-footer">',
    '                     <button type="button" class="btn btn-warning" data-dismiss="modal">' + sysmsg['OPT_CANCEL'] + '</button>',
    '                     <button type="button" class="btn btn-primary selectokbtn">' + sysmsg['OPT_OK'] + '</button>',
    '                </div>', '            </div><!-- /.modal-content -->',
    '        </div><!-- /.modal-dialog -->', '    </div>'].join("");

function doSelectMajor(Obj, opt) {
    if (opt == 'false') {
        if ($(".selectedmajor div").length == 0) {
            if (isExist(".selectedmajor", $(Obj).attr("data-value"))) {
                $(".selectedmajor").append(
                    "<div class=\"selecteddiv\" onclick=\"doUnSelectMajor(this,'"
                    + opt + "');\"  data-name=\""
                    + $(Obj).attr("data-name") + "\" data-value=\""
                    + $(Obj).attr("data-value") + "\"><span>"
                    + $(Obj).attr("data-name") + "</span></div>");
            }
            $(Obj).remove();
        } else {
            layer.msg(sysmsg['CAN_NOT_MULTIPLE']);
        }
    } else {
        if (isExist(".selectedmajor", $(Obj).attr("data-value"))) {
            $(".selectedmajor").append(
                "<div class=\"selecteddiv\" onclick=\"doUnSelectMajor(this,'"
                + opt + "');\"  data-name=\""
                + $(Obj).attr("data-name") + "\" data-value=\""
                + $(Obj).attr("data-value") + "\"><span>"
                + $(Obj).attr("data-name") + "</span></div>");
        }
        $(Obj).remove();
    }
}

function unselectmajorAll() {
    $(".selectedmajor div").each(function () {
        doUnSelectMajor(this)
    })
}

function selectmajorAll() {
    $(".selectmajor div").each(function () {
        doSelectMajor(this)
    })
}

function doUnSelectMajor(Obj, opt) {
    if (isExist(".selectmajor", $(Obj).attr("data-value"))) {
        $(".selectmajor").append(
            "<div class=\"selectdiv\" onclick=\"doSelectMajor(this,'" + opt
            + "');\" data-name=\"" + $(Obj).attr("data-name")
            + "\" data-value=\"" + $(Obj).attr("data-value")
            + "\"><span>" + $(Obj).attr("data-name")
            + "</span></div>");
    }
    $(Obj).remove();
}

function isExist(c, a) {
    var strs = [];
    $(c + " div").each(function () {
        strs.push($(this).attr("data-value"));
    })
    if ($.inArray(a, strs) >= 0) {
        return false;
    } else {
        return true;
    }
}

function getMajorsName(majorIds) {
    if (majorIds == "") {
        return "";
    } else {
        var majorStrs = [];
        if (majorIds) {
            $.ajax({
                url: "/ret/eduget/getEduMajorNameByIds",
                type: "post",
                dataType: "json",
                async: false,
                data: {
                    majorIds: majorIds
                },
                success: function (data) {
                    if (data.status == 200) {
                        for (var i = 0; i < data.list.length; i++) {
                            majorStrs.push(data.list[i].title);
                        }
                    } else if (data.status == 100) {
                        console.log(data.msg);
                    } else {
                        console.log(data.msg);
                    }
                }
            });
        }
        return majorStrs.join(",");
    }
}

function getCourseName(courseIds) {
    if (courseIds == "") {
        return "";
    } else {
        var courseStrs = [];
        if (courseIds) {
            $.ajax({
                url: "/ret/eduget/getEduCourseNameByIds",
                type: "post",
                dataType: "json",
                async: false,
                data: {
                    courseIds: courseIds
                },
                success: function (data) {
                    if (data.status == 200) {
                        for (var i = 0; i < data.list.length; i++) {
                            courseStrs.push(data.list[i].courseName);
                        }
                    } else if (data.status == 100) {
                        console.log(data.msg);
                    } else {
                        console.log(data.msg);
                    }
                }
            });
        }
        return courseStrs.join(",");
    }
}


function selectCourse(obj, opt) {
    if ($("#selectdiv").length == 0) {
        $("body").append("<div id='selectdiv'></div>");
    }
    var u = $(obj).attr("opt-id");
    var courseIds = $("#" + u).attr("data-value");
    $("#selectdiv").html(cselect);
    $("#coursediv").modal("show");
    initCourseSelect(courseIds, opt);
    $('.js-selectcourseall').unbind("click").click(function () {
        if (opt == 'true') {
            selectcourseAll();
        } else {
            layer.msg(sysmsg['CAN_NOT_MULTIPLE']);
        }
    });
    $('.js-unselectcourseall').unbind("click").click(function () {
        unselectcourseAll();
    });
    $(".selectokbtn").unbind("click").click(function () {
        var nameStr = [];
        var marjorStr = [];
        $(".selectedcourse div").each(function () {
            nameStr.push($(this).attr("data-name"));
            marjorStr.push($(this).attr("data-value"));
        });
        $("#" + u).val(nameStr.join(","));
        $("#" + u).attr("data-value", marjorStr.join(","));
        $('#coursediv').modal('hide');
    });
}
function initCourseSelect(courseIds, opt) {
    $.ajax({
        url: "/ret/eduget/getEduCourseListForTags",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var courseList = data.list;
                if (courseList) {
                    for (var i = 0; i < courseList.length; i++) {
                        if (("," + courseIds + ",").indexOf(("," + courseList[i].courseId + ",")) < 0) {
                            $(".selectcourse").append(
                                "<div class=\"selectdiv\" onclick=\"doSelectCourse(this,'"
                                + opt + "');\"  data-name=\""
                                + courseList[i].courseName
                                + "\" data-value=\""
                                + courseList[i].courseId
                                + "\"><span>"
                                + courseList[i].courseName
                                + "</span></div>");
                        } else {
                            $(".selectedcourse").append(
                                "<div class=\"selecteddiv\" onclick=\"doUnSelectCourse(this,'"
                                + opt + "');\"  data-name=\""
                                + courseList[i].courseName
                                + "\" data-value=\""
                                + courseList[i].courseId
                                + "\"><span>"
                                + courseList[i].courseName
                                + "</span></div>");
                        }
                    }
                }
            } else if (data.status == 100) {
                console.log(data.msg);
            } else {
                console.log(data.msg);
            }
        }
    });
}


let cselect = [
    ' <div id="coursediv" class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">',
    '        <div class="modal-dialog" style="overflow: auto;border: 1px solid silver;">',
    '            <div class="modal-content" style="width: 450px;">',
    '                <div class="modal-header">',
    '                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>',
    '                    <h4 class="modal-title" id="myLargeModalLabel">课程选择</h4>',
    '                </div>',
    '                <div class="modal-body" style="padding: 0px;">',
    '                    <div class="row" style="margin: 0px">',
    '                                <div class="col-xs-6 col-md-6" style="padding-left:4px;padding-right:4px;">',
    '                                <div class="flat radius-bordered">',
    '                                       <div class="widget-header bg-blueberry" style="text-align: center;padding-left:0px;">',
    '                                           <span class="widget-caption" style="float:none;">备选课程</span>',
    '                                           <a class="selectcourseall js-selectcourseall" style="cursor: pointer;color: white">全选</a>',
    '                                       </div>',
    '                                       <div class="widget-body-1 selectcourse">',
    '                                           ',
    '                                       </div>',
    '                                  </div>',
    '                                </div>',
    '                                <div class="col-xs-6 col-md-6" style="padding-right: 4px;padding-left:10px;">',
    '                                <div class="flat radius-bordered">',
    '                                       <div class="widget-header bg-palegreen" style="text-align: center;padding-left:0px;">',
    '                                           <span class="widget-caption" style="float:none;">已选课程</span>',
    '                                           <a class="selectcourseall js-unselectcourseall" style="cursor: pointer;color: white">反选</a>',
    '                                       </div>',
    '                                       <div class="widget-body-1 selectedcourse">',
    '                                       ',
    '                                       </div>',
    '                                   </div>',
    '                                </div>',
    '                    </div>',
    '                </div>',
    '                <div class="modal-footer">',
    '                     <button type="button" class="btn btn-warning" data-dismiss="modal">' + sysmsg['OPT_CANCEL'] + '</button>',
    '                     <button type="button" class="btn btn-primary selectokbtn">' + sysmsg['OPT_OK'] + '</button>',
    '                </div>', '            </div><!-- /.modal-content -->',
    '        </div><!-- /.modal-dialog -->', '    </div>'].join("");

function unselectcourseAll() {
    $(".selectedcourse div").each(function () {
        doUnSelectCourse(this)
    })
}


function selectcourseAll() {
    $(".selectcourse div").each(function () {
        doSelectCourse(this)
    })
}

function doSelectCourse(Obj, opt) {
    if (opt == 'false') {
        if ($(".selectedcourse div").length == 0) {
            if (isExist(".selectedcourse", $(Obj).attr("data-value"))) {
                $(".selectedcourse").append(
                    "<div class=\"selecteddiv\" onclick=\"doUnSelectCourse(this,'"
                    + opt + "');\"  data-name=\""
                    + $(Obj).attr("data-name") + "\" data-value=\""
                    + $(Obj).attr("data-value") + "\"><span>"
                    + $(Obj).attr("data-name") + "</span></div>");
            }
            $(Obj).remove();
        } else {
            layer.msg(sysmsg['CAN_NOT_MULTIPLE']);
        }
    } else {
        if (isExist(".selectedcourse", $(Obj).attr("data-value"))) {
            $(".selectedcourse").append(
                "<div class=\"selecteddiv\" onclick=\"doUnSelectCourse(this,'"
                + opt + "');\"  data-name=\""
                + $(Obj).attr("data-name") + "\" data-value=\""
                + $(Obj).attr("data-value") + "\"><span>"
                + $(Obj).attr("data-name") + "</span></div>");
        }
        $(Obj).remove();
    }
}

function doUnSelectCourse(Obj, opt) {
    if (isExist(".selectcourse", $(Obj).attr("data-value"))) {
        $(".selectcourse").append(
            "<div class=\"selectdiv\" onclick=\"doSelectCourse(this,'" + opt
            + "');\" data-name=\"" + $(Obj).attr("data-name")
            + "\" data-value=\"" + $(Obj).attr("data-value")
            + "\"><span>" + $(Obj).attr("data-name")
            + "</span></div>");
    }
    $(Obj).remove();
}
