package com.core136.service.education;

import com.core136.bean.education.EduGrade;
import com.core136.mapper.education.EduGradeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EduGradeService {
    private EduGradeMapper eduGradeMapper;

    @Autowired
    public void setEduGradeMapper(EduGradeMapper eduGradeMapper) {
        this.eduGradeMapper = eduGradeMapper;
    }

    public int insertEduGrade(EduGrade eduGrade) {
        return eduGradeMapper.insert(eduGrade);
    }

    public int deleteEduGrade(EduGrade eduGrade) {
        return eduGradeMapper.delete(eduGrade);
    }

    public EduGrade selectOneEduGrade(EduGrade eduGrade) {
        return eduGradeMapper.selectOne(eduGrade);
    }

    public int updateEduGrade(Example example, EduGrade eduGrade) {
        return eduGradeMapper.updateByExampleSelective(eduGrade, example);
    }

    public List<Map<String, Object>> getEduGradeChart(String orgId, String nextGrade) {
        return eduGradeMapper.getEduGradeChart(orgId, nextGrade);
    }

    /**
     * 获取年级CHART数据
     */
    public List<Map<String, Object>> getAllEduGradeChart(String orgId, String nextGrade) {
        List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> eduGradeList = getEduGradeChart(orgId, nextGrade);
        for (int i = 0; i < eduGradeList.size(); i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            map = eduGradeList.get(i);
            map.put("children", getAllEduGradeChart(orgId, eduGradeList.get(i).get("id").toString()));
            listMap.add(map);
        }
        return listMap;
    }

    public List<Map<String, String>> getAllEduGradeList(String orgId) {
        return eduGradeMapper.getAllEduGradeList(orgId);
    }

}
