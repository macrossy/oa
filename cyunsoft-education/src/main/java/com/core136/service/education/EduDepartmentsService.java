package com.core136.service.education;

import com.core136.bean.education.EduDepartments;
import com.core136.mapper.education.EduDepartmentsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class EduDepartmentsService {
    private EduDepartmentsMapper eduDepartmentsMapper;

    @Autowired
    public void setEduDepartmentsMapper(EduDepartmentsMapper eduDepartmentsMapper) {
        this.eduDepartmentsMapper = eduDepartmentsMapper;
    }

    public int insertEduDepartments(EduDepartments eduDepartments) {
        return eduDepartmentsMapper.insert(eduDepartments);
    }

    public int deleteEduDepartments(EduDepartments eduDepartments) {
        return eduDepartmentsMapper.delete(eduDepartments);
    }

    public int updateEduDepartments(Example example, EduDepartments eduDepartments) {
        return eduDepartmentsMapper.updateByExampleSelective(eduDepartments, example);
    }

    public EduDepartments selectOneEduDepartments(EduDepartments eduDepartments) {
        return eduDepartmentsMapper.selectOne(eduDepartments);
    }

    /**
     * @param orgId
     * @param sortLevel
     * @return List<Map < String, String>>
     * @Title: getEduDepartmentsTree
     * @Description:  获取院系树形结构
     */
    public List<Map<String, String>> getEduDepartmentsTree(String orgId, String sortLevel) {
        return eduDepartmentsMapper.getEduDepartmentsTree(orgId, sortLevel);
    }

    public List<Map<String, String>> getEduClassTreeForSelect(String orgId, String sortLevel) {
        return eduDepartmentsMapper.getEduClassTreeForSelect(orgId, sortLevel);
    }

    /**
     * 获取班级树结构
     *
     * @param orgId
     * @param sortLevel
     * @return
     */
    public List<Map<String, String>> getEduClassTree(String orgId, String sortLevel) {
        return eduDepartmentsMapper.getEduClassTree(orgId, sortLevel);
    }


}
