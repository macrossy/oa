package com.core136.service.education;

import com.core136.bean.education.EduTeacherTrain;
import com.core136.mapper.education.EduTeacherTrainMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class EduTeacherTrainService {
    private EduTeacherTrainMapper eduTeacherTrainMapper;

    @Autowired
    public void setEduTeacherTrainMapper(EduTeacherTrainMapper eduTeacherTrainMapper) {
        this.eduTeacherTrainMapper = eduTeacherTrainMapper;
    }

    public int insertEduTeacherTrain(EduTeacherTrain eduTeacherTrain) {
        return eduTeacherTrainMapper.insert(eduTeacherTrain);
    }

    public int deleteEduTeacherTrain(EduTeacherTrain eduTeacherTrain) {
        return eduTeacherTrainMapper.delete(eduTeacherTrain);
    }

    public EduTeacherTrain selectOneEduTeacherTrain(EduTeacherTrain eduTeacherTrain) {
        return eduTeacherTrainMapper.selectOne(eduTeacherTrain);
    }

    public int updateEduTeacherTrain(Example example, EduTeacherTrain eduTeacherTrain) {
        return eduTeacherTrainMapper.updateByExampleSelective(eduTeacherTrain, example);
    }

}
