package com.core136.service.education;

import com.core136.bean.education.EduClassTeacher;
import com.core136.mapper.education.EduClassTeacherMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class EduClassTeacherService {
    private EduClassTeacherMapper eduClassTeacherMapper;

    @Autowired
    public void setEduClassTeacherMapper(EduClassTeacherMapper eduClassTeacherMapper) {
        this.eduClassTeacherMapper = eduClassTeacherMapper;
    }

    public int insertEduClassTeacher(EduClassTeacher eduClassTeacher) {
        return eduClassTeacherMapper.insert(eduClassTeacher);
    }

    public int deleteEduClassTeacher(EduClassTeacher eduClassTeacher) {
        return eduClassTeacherMapper.delete(eduClassTeacher);
    }

    public EduClassTeacher selectOneEduClassTeacher(EduClassTeacher eduClassTeacher) {
        return eduClassTeacherMapper.selectOne(eduClassTeacher);
    }

    public int updateEduClassTeacher(Example example, EduClassTeacher eduClassTeacher) {
        return eduClassTeacherMapper.updateByExampleSelective(eduClassTeacher, example);
    }
}
