package com.core136.service.education;

import com.core136.bean.education.EduSemester;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.education.EduSemesterMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class EduSemesterService {
    private EduSemesterMapper eduSemesterMapper;

    @Autowired
    public void setEduSemesterMapper(EduSemesterMapper eduSemesterMapper) {
        this.eduSemesterMapper = eduSemesterMapper;
    }

    public int insertEduSemester(EduSemester eduSemester) {
        return eduSemesterMapper.insert(eduSemester);
    }

    public int deleteEduSemester(EduSemester eduSemester) {
        return eduSemesterMapper.delete(eduSemester);
    }

    public int updateEduSemester(Example example, EduSemester eduSemester) {
        return eduSemesterMapper.updateByExampleSelective(eduSemester, example);
    }

    public EduSemester selectOneEduSemester(EduSemester eduSemester) {
        return eduSemesterMapper.selectOne(eduSemester);
    }

    /**
     * @param orgId
     * @return String
     * @Title: getCurrentSemesterId
     * @Description:  获取当前学期
     */
    public String getCurrentSemesterId(String orgId) {
        Example example = new Example(EduSemester.class);
        example.createCriteria().andEqualTo("orgId").andEqualTo("currentStatus", "1");
        EduSemester eduSemester = eduSemesterMapper.selectOneByExample(example);
        return eduSemester.getSemesterId();
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getEduSemesterListForSelect
     * @Description:  获取学期列表
     */
    public List<Map<String, String>> getEduSemesterListForSelect(String orgId) {
        return eduSemesterMapper.getEduSemesterListForSelect(orgId);
    }

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getEduSemesterList
     * @Description:  获取学期列表
     */
    public List<Map<String, String>> getEduSemesterList(String orgId, String search) {
        return eduSemesterMapper.getEduSemesterList(orgId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getEduSemesterList
     * @Description:  获取学期列表
     */
    public PageInfo<Map<String, String>> getEduSemesterList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getEduSemesterList(pageParam.getOrgId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
