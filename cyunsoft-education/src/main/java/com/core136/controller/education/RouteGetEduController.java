package com.core136.controller.education;

import com.core136.bean.account.Account;
import com.core136.bean.education.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.education.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ret/eduget")
public class RouteGetEduController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private EduGradeService eduGradeService;

    @Autowired
    public void setEduGradeService(EduGradeService eduGradeService) {
        this.eduGradeService = eduGradeService;
    }

    private EduCampusService eduCampusService;

    @Autowired
    public void setEduCampusService(EduCampusService eduCampusService) {
        this.eduCampusService = eduCampusService;
    }

    private EduSemesterService eduSemesterService;

    @Autowired
    public void setEduSemesterService(EduSemesterService eduSemesterService) {
        this.eduSemesterService = eduSemesterService;
    }

    private EduClassService eduClassService;

    @Autowired
    public void setEduClassService(EduClassService eduClassService) {
        this.eduClassService = eduClassService;
    }

    private EduDepartmentsService eduDepartmentsService;

    @Autowired
    public void setEduDepartmentsService(EduDepartmentsService eduDepartmentsService) {
        this.eduDepartmentsService = eduDepartmentsService;
    }

    private EduCourseService eduCourseService;

    @Autowired
    public void setEduCourseService(EduCourseService eduCourseService) {
        this.eduCourseService = eduCourseService;
    }

    private EduStudentService eduStudentService;

    @Autowired
    public void setEduStudentService(EduStudentService eduStudentService) {
        this.eduStudentService = eduStudentService;
    }

    private EduTeacherService eduTeacherService;

    @Autowired
    public void setEduTeacherService(EduTeacherService eduTeacherService) {
        this.eduTeacherService = eduTeacherService;
    }

    public EduTeacherTrainService eduTeacherTrainService;

    @Autowired
    public void setEduTeacherTrainService(EduTeacherTrainService eduTeacherTrainService) {
        this.eduTeacherTrainService = eduTeacherTrainService;
    }

    private EduTeacherRePuService eduTeacherRePuService;

    @Autowired
    private void setEduTeacherRePuService(EduTeacherRePuService eduTeacherRePuService) {
        this.eduTeacherRePuService = eduTeacherRePuService;
    }

    private EduTeacherExperienceService eduTeacherExperienceService;

    @Autowired
    private void setEduTeacherExperienceService(EduTeacherExperienceService eduTeacherExperienceService) {
        this.eduTeacherExperienceService = eduTeacherExperienceService;
    }

    private EduClassCodeService eduClassCodeService;

    @Autowired
    public void setEduClassCodeService(EduClassCodeService eduClassCodeService) {
        this.eduClassCodeService = eduClassCodeService;
    }

    private EduMajorService eduMajorService;

    @Autowired
    public void setEduMajorService(EduMajorService eduMajorService) {
        this.eduMajorService = eduMajorService;
    }


    /**
     * 获取当前学期的班级列表
     *
     * @return
     */
    @RequestMapping(value = "/getEduClassListForSet", method = RequestMethod.POST)
    public RetDataBean getEduClassListForSet(String departmentsId, String gradeId, String majorId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS,
                    eduClassService.getEduClassListForSet(account.getOrgId(), account.getOpFlag(), account.getAccountId(), departmentsId, gradeId, majorId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取全部年级
     *
     * @param eduGrade
     * @return
     */
    @RequestMapping(value = "/getAllEduGradeList", method = RequestMethod.POST)
    public RetDataBean getAllEduGradeList(EduGrade eduGrade) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduGradeService.getAllEduGradeList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取年级升级线路
     *
     * @param nextGrade
     * @return
     */
    @RequestMapping(value = "/getEduGradeChart", method = RequestMethod.POST)
    public Object getEduGradeChart(String nextGrade) {
        try {
            if (StringUtils.isBlank(nextGrade)) {
                nextGrade = "0";
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            List<Map<String, Object>> listMap = eduGradeService.getAllEduGradeChart(account.getOrgId(), nextGrade);
            if (listMap.size() > 0) {
                return listMap.get(0);
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取年级详情
     *
     * @param eduGrade
     * @return
     */
    @RequestMapping(value = "/getEduGradeById", method = RequestMethod.POST)
    public RetDataBean getEduGradeById(EduGrade eduGrade) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduGrade.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduGradeService.selectOneEduGrade(eduGrade));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取课程名称
     *
     * @param courseIds
     * @return
     */
    @RequestMapping(value = "/getEduCourseNameByIds", method = RequestMethod.POST)
    public RetDataBean getEduCourseNameByIds(String courseIds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(courseIds)) {
                String[] paramsList;
                if (courseIds.indexOf(",") > -1) {
                    paramsList = courseIds.split(",");
                } else {
                    paramsList = new String[]{courseIds};
                }
                List<String> list = Arrays.asList(paramsList);
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduCourseService.getEduCourseNameByIds(account.getOrgId(), list));
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 获取专业名称
     *
     * @param majorIds
     * @return
     */
    @RequestMapping(value = "/getEduMajorNameByIds", method = RequestMethod.POST)
    public RetDataBean getEduMajorNameByIds(String majorIds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(majorIds)) {
                String[] paramsList;
                if (majorIds.indexOf(",") > -1) {
                    paramsList = majorIds.split(",");
                } else {
                    paramsList = new String[]{majorIds};
                }
                List<String> list = Arrays.asList(paramsList);
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduMajorService.getEduMajorNameByIds(account.getOrgId(), list));
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取所有备选课程
     *
     * @return
     */
    @RequestMapping(value = "/getEduCourseListForTags", method = RequestMethod.POST)
    public RetDataBean getEduCourseListForTags() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduCourseService.getEduCourseListForTags(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取可选专业列表
     *
     * @return
     */
    @RequestMapping(value = "/getEduMajorListForTags", method = RequestMethod.POST)
    public RetDataBean getEduMajorListForTags() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduMajorService.getEduMajorListForTags(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取专业详情
     *
     * @param eduMajor
     * @return
     */
    @RequestMapping(value = "/getEduMajorById", method = RequestMethod.POST)
    public RetDataBean getEduMajorById(EduMajor eduMajor) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduMajor.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduMajorService.selectOneEduMajor(eduMajor));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取专业列表
     *
     * @param pageNumber
     * @param pageSize
     * @param search
     * @param sort
     * @param sortOrder
     * @return
     */
    @RequestMapping(value = "/getEduMajorList", method = RequestMethod.POST)
    public RetDataBean getEduMajorList(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "sort_no";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(EduMajor.class);
            example.setOrderByClause(orderBy);
            Example.Criteria criteria = example.createCriteria();
            criteria.andEqualTo("orgId", account.getOrgId());
            if (StringUtils.isNotEmpty(search)) {
                Example.Criteria criteria2 = example.createCriteria();
                criteria2.orLike("title", "%" + search + "%");
                example.and(criteria2);
            }
            PageInfo<EduMajor> pageInfo = eduMajorService.getEduMajorList(example, pageNumber, pageSize);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取任教经历
     *
     * @param pageParam
     * @param courseType
     * @param grade
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getEduTeacherExperienceList", method = RequestMethod.POST)
    public RetDataBean getEduTeacherExperienceList(PageParam pageParam, String courseType, String grade, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("e.begin_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = eduTeacherExperienceService.getEduTeacherExperienceList(pageParam, courseType, grade, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取教师列表for select2
     *
     * @param search
     * @return
     */
    @RequestMapping(value = "/getTeacherListForSelect2", method = RequestMethod.POST)
    public RetDataBean getTeacherListForSelect2(String search) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduTeacherService.getTeacherListForSelect2(account.getOrgId(), search));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 任教经历详情
     *
     * @param eduTeacherExperience
     * @return
     */
    @RequestMapping(value = "/getEduTeacherExperienceById", method = RequestMethod.POST)
    public RetDataBean getEduTeacherExperienceById(EduTeacherExperience eduTeacherExperience) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduTeacherExperience.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduTeacherExperienceService.selectOneEduTeacherExperience(eduTeacherExperience));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 教师奖惩记录详情
     *
     * @param eduTeacherRePu
     * @return
     */
    @RequestMapping(value = "/getEduTeacherRePuById", method = RequestMethod.POST)
    public RetDataBean getEduTeacherRePuById(EduTeacherRePu eduTeacherRePu) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduTeacherRePu.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduTeacherRePuService.selectOneEduTeacherRePu(eduTeacherRePu));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取培训详情
     *
     * @param eduTeacherTrain
     * @return
     */
    @RequestMapping(value = "/getEduTeacherTrainById", method = RequestMethod.POST)
    public RetDataBean getEduTeacherTrainById(EduTeacherTrain eduTeacherTrain) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduTeacherTrain.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduTeacherTrainService.selectOneEduTeacherTrain(eduTeacherTrain));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取教师档案列表
     *
     * @param pageParam
     * @param accountId
     * @param major
     * @param status
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getTeacherList", method = RequestMethod.POST)
    public RetDataBean getTeacherList(PageParam pageParam, String accountId, String major, String status, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("t.sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = eduTeacherService.getTeacherList(pageParam, accountId, major, status, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param semesterId
     * @param major
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getDutyStudentList
     * @Description:  获取入学学生管理列表
     */
    @RequestMapping(value = "/getDutyStudentList", method = RequestMethod.POST)
    public RetDataBean getDutyStudentList(PageParam pageParam, String semesterId, String major, String status, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = eduStudentService.getDutyStudentList(pageParam, semesterId, major, status, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param eduTeacher
     * @return RetDataBean
     * @Title: getEduTeacherById
     * @Description:  获取教师详情
     */
    @RequestMapping(value = "/getEduTeacherById", method = RequestMethod.POST)
    public RetDataBean getEduTeacherById(EduTeacher eduTeacher) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduTeacher.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduTeacherService.selectOneEduTeacher(eduTeacher));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduStudent
     * @return RetDataBean
     * @Title: getEduStudentById
     * @Description:  获取学生信息详情
     */
    @RequestMapping(value = "/getEduStudentById", method = RequestMethod.POST)
    public RetDataBean getEduStudentById(EduStudent eduStudent) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduStudent.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduStudentService.selectOneEduStudent(eduStudent));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getEduSemesterListForSelect
     * @Description:   获取学期列表
     */
    @RequestMapping(value = "/getEduSemesterListForSelect", method = RequestMethod.POST)
    public RetDataBean getEduSemesterListForSelect() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduSemesterService.getEduSemesterListForSelect(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduCourse
     * @return RetDataBean
     * @Title: getEduCourseById
     * @Description:  获取课程详情
     */
    @RequestMapping(value = "/getEduCourseById", method = RequestMethod.POST)
    public RetDataBean getEduCourseById(EduCourse eduCourse) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduCourse.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduCourseService.selectOneEduCourse(eduCourse));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    @RequestMapping(value = "/getEduClassTreeForSelect", method = RequestMethod.POST)
    public List<Map<String, String>> getEduClassTreeForSelect(String sortId) {
        try {
            String sortLevel = "0";
            if (StringUtils.isNotBlank(sortId)) {
                sortLevel = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return eduDepartmentsService.getEduClassTreeForSelect(account.getOrgId(), sortLevel);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 获取班级树结构
     *
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getEduClassTree", method = RequestMethod.POST)
    public List<Map<String, String>> getEduClassTree(String sortId) {
        try {
            String sortLevel = "0";
            if (StringUtils.isNotBlank(sortId)) {
                sortLevel = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return eduDepartmentsService.getEduClassTree(account.getOrgId(), sortLevel);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, String>>
     * @Title: getEduDepartmentsTree
     * @Description:  获取院系树形结构
     */
    @RequestMapping(value = "/getEduDepartmentsTree", method = RequestMethod.POST)
    public List<Map<String, String>> getEduDepartmentsTree(String sortId) {
        try {
            String sortLevel = "0";
            if (StringUtils.isNotBlank(sortId)) {
                sortLevel = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return eduDepartmentsService.getEduDepartmentsTree(account.getOrgId(), sortLevel);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param eduClass
     * @return RetDataBean
     * @Title: getEduClassById
     * @Description:  获取班级详情
     */
    @RequestMapping(value = "/getEduClassById", method = RequestMethod.POST)
    public RetDataBean getEduClassById(EduClass eduClass) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduClass.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduClassService.selectOneEduClass(eduClass));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduDepartments
     * @return RetDataBean
     * @Title: getEduDepartmentsById
     * @Description:  获取院系详情
     */
    @RequestMapping(value = "/getEduDepartmentsById", method = RequestMethod.POST)
    public RetDataBean getEduDepartmentsById(EduDepartments eduDepartments) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduDepartments.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduDepartmentsService.selectOneEduDepartments(eduDepartments));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduSemester
     * @return RetDataBean
     * @Title: getEduSemesterById
     * @Description:  获取学期详情
     */
    @RequestMapping(value = "/getEduSemesterById", method = RequestMethod.POST)
    public RetDataBean getEduSemesterById(EduSemester eduSemester) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduSemester.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduSemesterService.selectOneEduSemester(eduSemester));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduCampus
     * @return RetDataBean
     * @Title: getEduCampusById
     * @Description:  获取校区信息详情
     */
    @RequestMapping(value = "/getEduCampusById", method = RequestMethod.POST)
    public RetDataBean getEduCampusById(EduCampus eduCampus) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduCampus.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduCampusService.selectOneEduCampus(eduCampus));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取所有校区
     *
     * @return
     */
    @RequestMapping(value = "/getAllEduCampusList", method = RequestMethod.POST)
    public RetDataBean getAllEduCampusList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            EduCampus eduCampus = new EduCampus();
            eduCampus.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduCampusService.getAllEduCampus(eduCampus));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getEduSemesterList
     * @Description:  获取学期列表
     */
    @RequestMapping(value = "/getEduSemesterList", method = RequestMethod.POST)
    public RetDataBean getEduSemesterList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = eduSemesterService.getEduSemesterList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getEduCampusList
     * @Description:  获取校区列表
     */
    @RequestMapping(value = "/getEduCampusList", method = RequestMethod.POST)
    public RetDataBean getEduCampusList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = eduCampusService.getEduCampusList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param departmentsId
     * @return RetDataBean
     * @Title: getEduClassList
     * @Description:  获取班级列表
     */
    @RequestMapping(value = "/getEduClassList", method = RequestMethod.POST)
    public RetDataBean getEduClassList(PageParam pageParam, String departmentsId, String gradeId, String majorId, String semesterId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = eduClassService.getEduClassList(pageParam, departmentsId, gradeId, majorId, semesterId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getEduCourseLsit
     * @Description:  获取课程列表
     */
    @RequestMapping(value = "/getEduCourseList", method = RequestMethod.POST)
    public RetDataBean getEduCourseList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = eduCourseService.getEduCourseList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @return RetDataBean
     * @Title: getAllParentCodeList
     * @Description: 获取所有的主分类
     */
    @RequestMapping(value = "/getAllParentCodeList", method = RequestMethod.POST)
    public RetDataBean getAllParentCodeList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduClassCodeService.getAllParentCodeList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param module
     * @return RetDataBean
     * @Title: getCodeListByModule
     * @Description:  获取子
     */
    @RequestMapping(value = "/getCodeListByModule", method = RequestMethod.POST)
    public RetDataBean getCodeListByModule(String module) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduClassCodeService.getCodeListByModule(account.getOrgId(), module));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param eduClassCode
     * @return RetDataBean
     * @Title: getHrClassCodeById
     * @Description:  获取分类详情
     */
    @RequestMapping(value = "/getEduClassCodeById", method = RequestMethod.POST)
    public RetDataBean getEduClassCodeById(EduClassCode eduClassCode) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            eduClassCode.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduClassCodeService.selectOneEduClassCode(eduClassCode));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param module
     * @param codeValue
     * @return RetDataBean
     * @Title: getHrClassCodeName
     * @Description:  获取分类码名称
     */
    @RequestMapping(value = "/getEduClassCodeName", method = RequestMethod.POST)
    public RetDataBean getEduClassCodeName(String module, String codeValue) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, eduClassCodeService.getEduClassCodeName(account.getOrgId(), module, codeValue));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
