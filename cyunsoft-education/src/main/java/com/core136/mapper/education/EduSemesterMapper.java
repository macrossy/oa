package com.core136.mapper.education;

import com.core136.bean.education.EduSemester;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EduSemesterMapper extends MyMapper<EduSemester> {

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getEduSemesterList
     * @Description:  获取学期列表
     */
    public List<Map<String, String>> getEduSemesterList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getEduSemesterListForSelect
     * @Description:  获取学期列表
     */
    public List<Map<String, String>> getEduSemesterListForSelect(@Param(value = "orgId") String orgId);
}
