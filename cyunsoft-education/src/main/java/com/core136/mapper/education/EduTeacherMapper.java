package com.core136.mapper.education;

import com.core136.bean.education.EduTeacher;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EduTeacherMapper extends MyMapper<EduTeacher> {
    /**
     * 获取教师档案列表
     *
     * @param orgId
     * @param accountId
     * @param major
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getTeacherList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "major") String major, @Param(value = "status") String status, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * 获取教师列表for select2
     *
     * @param orgId
     * @param search
     * @return
     */
    public List<Map<String, String>> getTeacherListForSelect2(@Param(value = "orgId") String orgId, @Param(value = "search") String search);

}
