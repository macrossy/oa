package com.core136.mapper.education;

import com.core136.bean.education.EduCampus;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EduCampusMapper extends MyMapper<EduCampus> {

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getEduCampusList
     * @Description:  获取校区列表
     */
    public List<Map<String, String>> getEduCampusList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);
}
