package com.core136.mapper.education;

import com.core136.bean.education.EduGrade;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EduGradeMapper extends MyMapper<EduGrade> {

    public List<Map<String, Object>> getEduGradeChart(@Param(value = "orgId") String orgId, @Param(value = "nextGrade") String nextGrade);

    public List<Map<String, String>> getAllEduGradeList(@Param(value = "orgId") String orgId);
}
