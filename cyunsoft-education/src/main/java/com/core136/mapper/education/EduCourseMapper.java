package com.core136.mapper.education;

import com.core136.bean.education.EduCourse;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface EduCourseMapper extends MyMapper<EduCourse> {

    /**
     * @param orgId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getEduCourseLsit
     * @Description:  获取课程列表
     */
    public List<Map<String, String>> getEduCourseList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);

    /**
     * 获取课程名称
     *
     * @param orgId
     * @param list
     * @return
     */
    public List<Map<String, String>> getEduCourseNameByIds(@Param(value = "orgId") String orgId, @Param(value = "list") List<String> list);

    /**
     * 获取所有备选课程
     *
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getEduCourseListForTags(@Param(value = "orgId") String orgId);

}
