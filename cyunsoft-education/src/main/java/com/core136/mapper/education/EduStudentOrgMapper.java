package com.core136.mapper.education;

import com.core136.bean.education.EduStudentOrg;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EduStudentOrgMapper extends MyMapper<EduStudentOrg> {
}
