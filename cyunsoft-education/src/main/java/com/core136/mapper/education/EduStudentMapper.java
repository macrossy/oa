package com.core136.mapper.education;

import com.core136.bean.education.EduStudent;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EduStudentMapper extends MyMapper<EduStudent> {

    /**
     * @param orgId
     * @param semesterId
     * @param major
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getDutyStudentList
     * @Description:  获取入学学生管理列表
     */
    public List<Map<String, String>> getDutyStudentList(@Param(value = "orgId") String orgId, @Param(value = "semesterId") String semesterId, @Param(value = "major") String major, @Param(value = "status") String status, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

}
