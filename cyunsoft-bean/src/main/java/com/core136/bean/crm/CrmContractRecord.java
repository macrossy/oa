package com.core136.bean.crm;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: CrmContactRecord
 * @Description: 客户联系记录
 * @author: 稠云信息
 * @date: 2019年2月26日 下午4:30:22
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "crm_contract_record")
public class CrmContractRecord implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer sortNo;
    private String recordId;
    private String customerId;
    private String linkManId;
    private String content;
    private String attach;
    private String recordType;
    private String nextVisit;
    private String linkType;
    private String createTime;
    private String createUser;
    private String msgType;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getLinkManId() {
        return linkManId;
    }

    public void setLinkManId(String linkManId) {
        this.linkManId = linkManId;
    }

    public String getNextVisit() {
        return nextVisit;
    }

    public void setNextVisit(String nextVisit) {
        this.nextVisit = nextVisit;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getLinkType() {
        return linkType;
    }

    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
