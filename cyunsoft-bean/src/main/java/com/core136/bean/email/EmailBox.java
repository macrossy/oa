/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: EMAIL_BOX.java
 * @Package com.core136.bean.oa
 * @Description: (用一句话描述该文件做什么)
 * @author: 刘绍全
 * @date: 2019年1月14日 下午7:39:50
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.email;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: EMAIL_BOX
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 刘绍全
 * @date: 2019年1月14日 下午7:39:50
 *
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云科信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "email_box")
public class EmailBox implements Serializable {
    private static final long serialVersionUID = 1L;
    private String boxId;
    private Integer sortNo;
    private String boxLevel;
    private String boxName;
    private String accountId;
    private String orgId;

    public String getBoxId() {
        return boxId;
    }

    public void setBoxId(String boxId) {
        this.boxId = boxId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getBoxLevel() {
        return boxLevel;
    }

    public void setBoxLevel(String boxLevel) {
        this.boxLevel = boxLevel;
    }

    public String getBoxName() {
        return boxName;
    }

    public void setBoxName(String boxName) {
        this.boxName = boxName;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
