/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: FixedAssetsConfig.java
 * @Package com.core136.bean.fixedassets
 * @Description: 描述
 * @author: lsq
 * @date: 2019年12月10日 上午8:58:42
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.fixedassets;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 *
 */
@Table(name = "fixed_assets_config")
public class FixedAssetsConfig implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String configId;
    /**
     * 管理人员
     */
    private String management;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    /**
     * @return the configId
     */
    public String getConfigId() {
        return configId;
    }

    /**
     * @param configId the configId to set
     */
    public void setConfigId(String configId) {
        this.configId = configId;
    }

    /**
     * @return the management
     */
    public String getManagement() {
        return management;
    }

    /**
     * @param management the management to set
     */
    public void setManagement(String management) {
        this.management = management;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
