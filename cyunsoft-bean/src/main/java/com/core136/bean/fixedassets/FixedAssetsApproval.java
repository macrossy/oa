/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: FixedAssetsApproval.java
 * @Package com.core136.bean.fixedassets
 * @Description: 描述
 * @author: lsq
 * @date: 2019年12月26日 下午3:00:25
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.fixedassets;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 *
 */
@Table(name = "fixed_assets_approval")
public class FixedAssetsApproval implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String approvalId;
    private String applyId;
    private String remark;
    private String status;
    private String createTime;
    private String createUser;
    private String orgId;

    /**
     * @return the approvalId
     */
    public String getApprovalId() {
        return approvalId;
    }

    /**
     * @param approvalId the approvalId to set
     */
    public void setApprovalId(String approvalId) {
        this.approvalId = approvalId;
    }

    /**
     * @return the applyId
     */
    public String getApplyId() {
        return applyId;
    }

    /**
     * @param applyId the applyId to set
     */
    public void setApplyId(String applyId) {
        this.applyId = applyId;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
