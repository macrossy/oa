package com.core136.bean.budget;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: BudgetConfig
 * @Description: 费用预算的配置
 * @author: 稠云技术
 * @date: 2020年9月25日 下午12:20:55
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "budget_config")
public class BudgetConfig implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String configId;
    private String costUpdateType;
    private String flowIdUpdate;
    private String costApplayType;
    private String flowIdCost;
    private String costApprovalUser;
    private String adjustmentType;
    private String adjustmentApprovalUser;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getAdjustmentApprovalUser() {
        return adjustmentApprovalUser;
    }

    public void setAdjustmentApprovalUser(String adjustmentApprovalUser) {
        this.adjustmentApprovalUser = adjustmentApprovalUser;
    }

    public String getCostUpdateType() {
        return costUpdateType;
    }

    public void setCostUpdateType(String costUpdateType) {
        this.costUpdateType = costUpdateType;
    }

    public String getFlowIdUpdate() {
        return flowIdUpdate;
    }

    public void setFlowIdUpdate(String flowIdUpdate) {
        this.flowIdUpdate = flowIdUpdate;
    }

    public String getCostApplayType() {
        return costApplayType;
    }

    public void setCostApplayType(String costApplayType) {
        this.costApplayType = costApplayType;
    }

    public String getFlowIdCost() {
        return flowIdCost;
    }

    public void setFlowIdCost(String flowIdCost) {
        this.flowIdCost = flowIdCost;
    }

    public String getCostApprovalUser() {
        return costApprovalUser;
    }

    public void setCostApprovalUser(String costApprovalUser) {
        this.costApprovalUser = costApprovalUser;
    }

    public String getAdjustmentType() {
        return adjustmentType;
    }

    public void setAdjustmentType(String adjustmentType) {
        this.adjustmentType = adjustmentType;
    }

}
