package com.core136.bean.baseinfo;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * 学历
 */
@Table(name = "base_education")
public class BaseEducation implements Serializable {
    private String educationId;
    private Integer sortNo;
    private String educationName;

    public String getEducationId() {
        return educationId;
    }

    public void setEducationId(String educationId) {
        this.educationId = educationId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getEducationName() {
        return educationName;
    }

    public void setEducationName(String educationName) {
        this.educationName = educationName;
    }
}
