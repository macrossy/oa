/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: BpmQueryAdvTemplate.java
 * @Package com.core136.bean.bpm
 * @Description: 描述
 * @author: lsq
 * @date: 2019年12月12日 下午1:26:15
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.bpm;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 *
 */
@Table(name = "bpm_query_adv_template")
public class BpmQueryAdvTemplate implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String templateId;
    private Integer sortNo;
    private String templateTitle;
    private String flowId;
    private String fieldsExample;
    private String createUser;
    private String orgId;

    /**
     * @return the templateId
     */
    public String getTemplateId() {
        return templateId;
    }

    /**
     * @param templateId the templateId to set
     */
    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    /**
     * @return the sortNo
     */
    public Integer getSortNo() {
        return sortNo;
    }

    /**
     * @param sortNo the sortNo to set
     */
    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    /**
     * @return the templateTitle
     */
    public String getTemplateTitle() {
        return templateTitle;
    }

    /**
     * @param templateTitle the templateTitle to set
     */
    public void setTemplateTitle(String templateTitle) {
        this.templateTitle = templateTitle;
    }

    /**
     * @return the flowId
     */
    public String getFlowId() {
        return flowId;
    }

    /**
     * @param flowId the flowId to set
     */
    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    /**
     * @return the fieldsExample
     */
    public String getFieldsExample() {
        return fieldsExample;
    }

    /**
     * @param fieldsExample the fieldsExample to set
     */
    public void setFieldsExample(String fieldsExample) {
        this.fieldsExample = fieldsExample;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
