/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: BpmSealSign.java
 * @Package com.core136.bean.bpm
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月22日 上午10:17:50
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.bpm;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 *
 */
@Table(name = "bpm_seal_sign")
public class BpmSealSign implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String sealSignId;
    private Integer sortNo;
    private String title;
    private String accountId;
    private String password;
    private String sealSignByte;
    private String createUser;
    private String createTime;
    private String orgId;

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    /**
     * @return the sealSignId
     */
    public String getSealSignId() {
        return sealSignId;
    }

    /**
     * @param sealSignId the sealSignId to set
     */
    public void setSealSignId(String sealSignId) {
        this.sealSignId = sealSignId;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }


    /**
     * @return the sealSignByte
     */
    public String getSealSignByte() {
        return sealSignByte;
    }

    /**
     * @param sealSignByte the sealSignByte to set
     */
    public void setSealSignByte(String sealSignByte) {
        this.sealSignByte = sealSignByte;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

}
