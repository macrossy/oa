/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: BpmAgent.java
 * @Package com.core136.bean.bpm
 * @Description: (用一句话描述该文件做什么)
 * @author: 刘绍全
 * @date: 2019年1月27日 上午11:50:47
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.bpm;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: BpmAgent
 * @Description: BPM代理
 * @author: 刘绍全
 * @date: 2019年1月27日 上午11:50:47
 *
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云科信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "bpm_agent")
public class BpmAgent implements Serializable {

    private static final long serialVersionUID = 1L;
    private String flowId;
    private String processId;
    private String fromId;
    private String toId;
    private String beginTime;
    private String endTime;
    private String orgId;
}
