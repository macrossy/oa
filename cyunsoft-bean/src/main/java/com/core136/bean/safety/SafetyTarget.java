package com.core136.bean.safety;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * 监管管理目标
 */
@Table(name="safety_target")
public class SafetyTarget implements Serializable {
    private String targetId;
    private Integer sortNo;
    private String entId;
    private String targetSort;
    private String title;
    private String remark;
    private String attach;
    private String targetYear;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getEntId() {
        return entId;
    }

    public void setEntId(String entId) {
        this.entId = entId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getTargetYear() {
        return targetYear;
    }

    public void setTargetYear(String targetYear) {
        this.targetYear = targetYear;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getTargetSort() {
        return targetSort;
    }

    public void setTargetSort(String targetSort) {
        this.targetSort = targetSort;
    }
}
