package com.core136.bean.archives;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: ArchivesVolume
 * @Description: 档案卷
 * @author: 稠云技术
 * @date: 2020年7月3日 下午4:25:29
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "archives_volume")
public class ArchivesVolume implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Integer sortNo;
    private String volumeId;
    private String volumeCode;
    private String volumeTitle;
    private String repositoryId;
    private String beginDate;
    private String endDate;
    private String createOrg;
    private String deptId;
    private String storagePeriod;
    private String secretLevel;
    private String microNo;
    private String voucherType;
    private String voucherBeginNo;
    private String voucherEndNo;
    private Integer pageTotal;
    private String remark;
    private String manageUser;
    private String destoryFlag;
    private String createUser;
    private String createTime;
    private String orgId;

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getVolumeId() {
        return volumeId;
    }

    public void setVolumeId(String volumeId) {
        this.volumeId = volumeId;
    }

    public String getDestoryFlag() {
        return destoryFlag;
    }

    public void setDestoryFlag(String destoryFlag) {
        this.destoryFlag = destoryFlag;
    }

    public String getVolumeCode() {
        return volumeCode;
    }

    public void setVolumeCode(String volumeCode) {
        this.volumeCode = volumeCode;
    }

    public String getVolumeTitle() {
        return volumeTitle;
    }

    public void setVolumeTitle(String volumeTitle) {
        this.volumeTitle = volumeTitle;
    }

    public String getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(String repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCreateOrg() {
        return createOrg;
    }

    public void setCreateOrg(String createOrg) {
        this.createOrg = createOrg;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getStoragePeriod() {
        return storagePeriod;
    }

    public void setStoragePeriod(String storagePeriod) {
        this.storagePeriod = storagePeriod;
    }

    public String getSecretLevel() {
        return secretLevel;
    }

    public void setSecretLevel(String secretLevel) {
        this.secretLevel = secretLevel;
    }

    public String getMicroNo() {
        return microNo;
    }

    public void setMicroNo(String microNo) {
        this.microNo = microNo;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public String getVoucherBeginNo() {
        return voucherBeginNo;
    }

    public void setVoucherBeginNo(String voucherBeginNo) {
        this.voucherBeginNo = voucherBeginNo;
    }

    public String getVoucherEndNo() {
        return voucherEndNo;
    }

    public void setVoucherEndNo(String voucherEndNo) {
        this.voucherEndNo = voucherEndNo;
    }

    public Integer getPageTotal() {
        return pageTotal;
    }

    public void setPageTotal(Integer pageTotal) {
        this.pageTotal = pageTotal;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getManageUser() {
        return manageUser;
    }

    public void setManageUser(String manageUser) {
        this.manageUser = manageUser;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

}
