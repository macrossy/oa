/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: MeetingNotes.java
 * @Package com.core136.bean.meeting
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月31日 上午10:52:21
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.meeting;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 * 会议记要
 */
@Table(name = "meeting_notes")
public class MeetingNotes implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String notesId;
    private String meetingId;
    private String msgType;
    private String notesTitle;
    private String userPriv;
    private String deptPriv;
    private String levelPriv;
    private String remark;
    private String attach;
    private String attachPriv;
    private String createTime;
    private String createUser;
    private String orgId;

    /**
     * @return the notesId
     */
    public String getNotesId() {
        return notesId;
    }

    /**
     * @param notesId the notesId to set
     */
    public void setNotesId(String notesId) {
        this.notesId = notesId;
    }

    /**
     * @return the meetingId
     */
    public String getMeetingId() {
        return meetingId;
    }

    /**
     * @param meetingId the meetingId to set
     */
    public void setMeetingId(String meetingId) {
        this.meetingId = meetingId;
    }

    /**
     * @return the attach
     */
    public String getAttach() {
        return attach;
    }

    /**
     * @param attach the attach to set
     */
    public void setAttach(String attach) {
        this.attach = attach;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * @return the notesTitle
     */
    public String getNotesTitle() {
        return notesTitle;
    }

    /**
     * @param notesTitle the notesTitle to set
     */
    public void setNotesTitle(String notesTitle) {
        this.notesTitle = notesTitle;
    }

    /**
     * @return the userPriv
     */
    public String getUserPriv() {
        return userPriv;
    }

    /**
     * @param userPriv the userPriv to set
     */
    public void setUserPriv(String userPriv) {
        this.userPriv = userPriv;
    }

    /**
     * @return the deptPriv
     */
    public String getDeptPriv() {
        return deptPriv;
    }

    /**
     * @param deptPriv the deptPriv to set
     */
    public void setDeptPriv(String deptPriv) {
        this.deptPriv = deptPriv;
    }

    /**
     * @return the levelPriv
     */
    public String getLevelPriv() {
        return levelPriv;
    }

    /**
     * @param levelPriv the levelPriv to set
     */
    public void setLevelPriv(String levelPriv) {
        this.levelPriv = levelPriv;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getAttachPriv() {
        return attachPriv;
    }

    public void setAttachPriv(String attachPriv) {
        this.attachPriv = attachPriv;
    }

}
