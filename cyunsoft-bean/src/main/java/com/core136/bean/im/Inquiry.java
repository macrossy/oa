/**
 * All rights Reserved, Designed By www.cyunsoft.com
 * @Title:  Inquiry.java
 * @Package com.core136.bean.im
 * @Description:    (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date:   2019年7月1日 下午1:46:55
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.im;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName:  Inquiry
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 稠云信息
 * @date:   2019年7月1日 下午1:46:55
 * @author lsq
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name="inquiry")
public class Inquiry implements Serializable{
	private static final long serialVersionUID = 1L;
    private String inquiryId;
    private String title;
    private String content;
    private String attach;
    private String endTime;
    private String model;
    private String createTime;
    private String toAccountId;
    private String accountId;
    private String orgName;
    private String orgId;
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getInquiryId() {
        return inquiryId;
    }

    public void setInquiryId(String inquiryId) {
        this.inquiryId = inquiryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

	public String getToAccountId() {
		return toAccountId;
	}

	public void setToAccountId(String toAccountId) {
		this.toAccountId = toAccountId;
	}
}
