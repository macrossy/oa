/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: BiType.java
 * @Package com.core136.bean.bi
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年4月22日 上午9:56:18
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.bi;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: BiType
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 稠云信息
 * @date: 2019年4月22日 上午9:56:18
 * @author lsq
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "bi_type")
public class BiType implements Serializable {
    private static final long serialVersionUID = 1L;
    private String biTypeId;
    private String biFlag;
    private String biTypeEnName;
    private String biTypeCnName;
    private String demoImg;
    private String orgId;

    public String getBiTypeId() {
        return biTypeId;
    }

    public void setBiTypeId(String biTypeId) {
        this.biTypeId = biTypeId;
    }

    public String getBiFlag() {
        return biFlag;
    }

    public void setBiFlag(String biFlag) {
        this.biFlag = biFlag;
    }

    public String getBiTypeEnName() {
        return biTypeEnName;
    }

    public void setBiTypeEnName(String biTypeEnName) {
        this.biTypeEnName = biTypeEnName;
    }

    public String getBiTypeCnName() {
        return biTypeCnName;
    }

    public void setBiTypeCnName(String biTypeCnName) {
        this.biTypeCnName = biTypeCnName;
    }

    public String getDemoImg() {
        return demoImg;
    }

    public void setDemoImg(String demoImg) {
        this.demoImg = demoImg;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


}
