package com.core136.bean.officesupplies;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: OfficeSuppliesGrant
 * @Description: 办公用品发放记录
 * @author: 稠云技术
 * @date: 2019年12月1日 下午9:14:22
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "office_supplies_grant")
public class OfficeSuppliesGrant implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String grantId;
    private String applyId;
    private String suppliesId;
    /**
     * 领用人
     */
    private String getUser;
    private Integer quantity;
    private String remark;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getGrantId() {
        return grantId;
    }

    public void setGrantId(String grantId) {
        this.grantId = grantId;
    }

    public String getApplyId() {
        return applyId;
    }

    public void setApplyId(String applyId) {
        this.applyId = applyId;
    }

    public String getGetUser() {
        return getUser;
    }

    public void setGetUser(String getUser) {
        this.getUser = getUser;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getSuppliesId() {
        return suppliesId;
    }

    public void setSuppliesId(String suppliesId) {
        this.suppliesId = suppliesId;
    }


}
