package com.core136.bean.erp;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name = "erp_unit")
public class ErpUnit implements Serializable {

    /**
     * 计量单位
     */
    private static final long serialVersionUID = 1L;
    private String unitId;
    private String type;
    private String cnName;
    private String enName;
    private String orgId;

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


}
