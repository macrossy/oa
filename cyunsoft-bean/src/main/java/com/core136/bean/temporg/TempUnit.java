package com.core136.bean.temporg;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: TempUnit
 * @Description: 临时组织机构
 * @author: 稠云技术
 * @date: 2021年4月22日 上午10:03:52
 * @Copyright: 2021 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "temp_unit")
public class TempUnit implements Serializable {
    private static final long serialVersionUID = 1L;
    private String tempOrgId;
    private String orgName;
    private String orgAdd;
    private String orgTel;
    private String orgFax;
    private String orgPost;
    private String orgEmail;
    private String orgLegalPerson;
    private String orgBank;
    private String orgBankAccount;
    private String orgTaxNo;
    private String status;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getTempOrgId() {
        return tempOrgId;
    }

    public void setTempOrgId(String tempOrgId) {
        this.tempOrgId = tempOrgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public String getOrgLegalPerson() {
        return orgLegalPerson;
    }

    public void setOrgLegalPerson(String orgLegalPerson) {
        this.orgLegalPerson = orgLegalPerson;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgAdd() {
        return orgAdd;
    }

    public void setOrgAdd(String orgAdd) {
        this.orgAdd = orgAdd;
    }

    public String getOrgTel() {
        return orgTel;
    }

    public void setOrgTel(String orgTel) {
        this.orgTel = orgTel;
    }

    public String getOrgFax() {
        return orgFax;
    }

    public void setOrgFax(String orgFax) {
        this.orgFax = orgFax;
    }

    public String getOrgPost() {
        return orgPost;
    }

    public void setOrgPost(String orgPost) {
        this.orgPost = orgPost;
    }

    public String getOrgEmail() {
        return orgEmail;
    }

    public void setOrgEmail(String orgEmail) {
        this.orgEmail = orgEmail;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgBank() {
        return orgBank;
    }

    public void setOrgBank(String orgBank) {
        this.orgBank = orgBank;
    }

    public String getOrgBankAccount() {
        return orgBankAccount;
    }

    public void setOrgBankAccount(String orgBankAccount) {
        this.orgBankAccount = orgBankAccount;
    }

    public String getOrgTaxNo() {
        return orgTaxNo;
    }

    public void setOrgTaxNo(String orgTaxNo) {
        this.orgTaxNo = orgTaxNo;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
