/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: Dept.java
 * @Package com.core136.bean.sys
 * @Description: (用一句话描述该文件做什么)
 * @author: 刘绍全
 * @date: 2018年12月23日 下午8:31:04
 * @version V1.0
 * @Copyright: 2018 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.temporg;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: TempUnitDept
 * @Description: 虚拟部门
 * @author: 刘绍全
 * @date: 2018年12月23日 下午8:31:04
 *
 * @Copyright: 2018 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云科信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "temp_unit_dept")
public class TempUnitDept implements Serializable {
    private static final long serialVersionUID = 1L;
    private String deptId;
    private Integer sortNo;
    private String deptName;
    private String orgLevelId;
    private String deptTel;
    private String deptEmail;
    private String deptFax;
    private String deptLead;
    private String chargeLead;
    private String deptFunction;
    private String tempOrgId;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getOrgLevelId() {
        return orgLevelId;
    }

    public void setOrgLevelId(String orgLevelId) {
        this.orgLevelId = orgLevelId;
    }

    public String getDeptTel() {
        return deptTel;
    }

    public void setDeptTel(String deptTel) {
        this.deptTel = deptTel;
    }

    public String getDeptEmail() {
        return deptEmail;
    }

    public void setDeptEmail(String deptEmail) {
        this.deptEmail = deptEmail;
    }

    public String getDeptFax() {
        return deptFax;
    }

    public void setDeptFax(String deptFax) {
        this.deptFax = deptFax;
    }

    public String getDeptLead() {
        return deptLead;
    }

    public void setDeptLead(String deptLead) {
        this.deptLead = deptLead;
    }

    public String getDeptFunction() {
        return deptFunction;
    }

    public void setDeptFunction(String deptFunction) {
        this.deptFunction = deptFunction;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getTempOrgId() {
        return tempOrgId;
    }

    public void setTempOrgId(String tempOrgId) {
        this.tempOrgId = tempOrgId;
    }

    public String getChargeLead() {
        return chargeLead;
    }

    public void setChargeLead(String chargeLead) {
        this.chargeLead = chargeLead;
    }
}
