/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: LuceneResult.java
 * @Package com.core136.bean.file
 * @Description: 描述
 * @author: lsq
 * @date: 2019年12月10日 下午2:36:20
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.file;

/**
 * @author lsq
 *	文件全文检索的结果
 */
public class LuceneResult {
    private String keyWords;
    private String fileName;
    private String filePath;
    private String extName;
    private String createTime;
    private String accountId;
    private String attachId;
    private String content;
    private Long fileSize;

    /**
     * @return the keyWords
     */
    public String getKeyWords() {
        return keyWords;
    }

    /**
     * @param keyWords the keyWords to set
     */
    public void setKeyWords(String keyWords) {
        this.keyWords = keyWords;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * @return the extName
     */
    public String getExtName() {
        return extName;
    }

    /**
     * @param extName the extName to set
     */
    public void setExtName(String extName) {
        this.extName = extName;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the accountId
     */
    public String getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the attachId
     */
    public String getAttachId() {
        return attachId;
    }

    /**
     * @param attachId the attachId to set
     */
    public void setAttachId(String attachId) {
        this.attachId = attachId;
    }

    /**
     * @return the fileSize
     */
    public Long getFileSize() {
        return fileSize;
    }

    /**
     * @param fileSize the fileSize to set
     */
    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }


}
