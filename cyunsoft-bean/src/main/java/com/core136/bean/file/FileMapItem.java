package com.core136.bean.file;

import javax.persistence.Table;

@Table(name = "file_map_item")
public class FileMapItem {
    private Integer sortNo;
    private String itemId;
    private String titleName;
    private String sortId;
    private String outUserPriv;
    private String outDeptPriv;
    private String outLevelPriv;
    private String excOutUser;//外部排除人员
    private String inUserPriv;
    private String inDeptPriv;
    private String inLevelPriv;
    private String excInUser;//内部排除人员
    private String fileCreateUser;
    private String fileManageUser;
    private String createUser;
    private String createTime;
    private String orgId;

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    public String getSortId() {
        return sortId;
    }

    public void setSortId(String sortId) {
        this.sortId = sortId;
    }

    public String getOutUserPriv() {
        return outUserPriv;
    }

    public void setOutUserPriv(String outUserPriv) {
        this.outUserPriv = outUserPriv;
    }

    public String getOutDeptPriv() {
        return outDeptPriv;
    }

    public void setOutDeptPriv(String outDeptPriv) {
        this.outDeptPriv = outDeptPriv;
    }

    public String getOutLevelPriv() {
        return outLevelPriv;
    }

    public void setOutLevelPriv(String outLevelPriv) {
        this.outLevelPriv = outLevelPriv;
    }

    public String getExcOutUser() {
        return excOutUser;
    }

    public void setExcOutUser(String excOutUser) {
        this.excOutUser = excOutUser;
    }

    public String getInUserPriv() {
        return inUserPriv;
    }

    public void setInUserPriv(String inUserPriv) {
        this.inUserPriv = inUserPriv;
    }

    public String getInDeptPriv() {
        return inDeptPriv;
    }

    public void setInDeptPriv(String inDeptPriv) {
        this.inDeptPriv = inDeptPriv;
    }

    public String getInLevelPriv() {
        return inLevelPriv;
    }

    public void setInLevelPriv(String inLevelPriv) {
        this.inLevelPriv = inLevelPriv;
    }

    public String getExcInUser() {
        return excInUser;
    }

    public void setExcInUser(String excInUser) {
        this.excInUser = excInUser;
    }

    public String getFileCreateUser() {
        return fileCreateUser;
    }

    public void setFileCreateUser(String fileCreateUser) {
        this.fileCreateUser = fileCreateUser;
    }

    public String getFileManageUser() {
        return fileManageUser;
    }

    public void setFileManageUser(String fileManageUser) {
        this.fileManageUser = fileManageUser;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
