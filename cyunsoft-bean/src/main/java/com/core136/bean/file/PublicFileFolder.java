/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: PublicFilFolder.java
 * @Package com.core136.bean.file
 * @Description: (用一句话描述该文件做什么)
 * @author: 刘绍全
 * @date: 2019年1月14日 下午8:47:19
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.file;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PublicFilFolder
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 刘绍全
 * @date: 2019年1月14日 下午8:47:19
 *
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云科信息技术股份有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "public_file_folder")
public class PublicFileFolder implements Serializable {
    private static final long serialVersionUID = 1L;
    private String folderId;
    private Integer sortNo;
    private String folderName;
    private String folderLevel;
    private String accessUserPriv;
    private String accessDeptPriv;
    private String accessLevelPriv;
    private String downUserPriv;
    private String downDeptPriv;
    private String downLevelPriv;
    private String createUserPriv;
    private String createDeptPriv;
    private String createLevelPriv;
    private String manageUserPriv;
    private String manageDeptPriv;
    private String manageLevelPriv;
    private String owner;
    private String spaceLimit;
    private String status;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getFolderLevel() {
        return folderLevel;
    }

    public void setFolderLevel(String folderLevel) {
        this.folderLevel = folderLevel;
    }

    public String getAccessUserPriv() {
        return accessUserPriv;
    }

    public void setAccessUserPriv(String accessUserPriv) {
        this.accessUserPriv = accessUserPriv;
    }

    public String getAccessDeptPriv() {
        return accessDeptPriv;
    }

    public void setAccessDeptPriv(String accessDeptPriv) {
        this.accessDeptPriv = accessDeptPriv;
    }

    public String getAccessLevelPriv() {
        return accessLevelPriv;
    }

    public void setAccessLevelPriv(String accessLevelPriv) {
        this.accessLevelPriv = accessLevelPriv;
    }

    public String getDownUserPriv() {
        return downUserPriv;
    }

    public void setDownUserPriv(String downUserPriv) {
        this.downUserPriv = downUserPriv;
    }

    public String getDownDeptPriv() {
        return downDeptPriv;
    }

    public void setDownDeptPriv(String downDeptPriv) {
        this.downDeptPriv = downDeptPriv;
    }

    public String getDownLevelPriv() {
        return downLevelPriv;
    }

    public void setDownLevelPriv(String downLevelPriv) {
        this.downLevelPriv = downLevelPriv;
    }

    public String getCreateUserPriv() {
        return createUserPriv;
    }

    public void setCreateUserPriv(String createUserPriv) {
        this.createUserPriv = createUserPriv;
    }

    public String getCreateDeptPriv() {
        return createDeptPriv;
    }

    public void setCreateDeptPriv(String createDeptPriv) {
        this.createDeptPriv = createDeptPriv;
    }

    public String getCreateLevelPriv() {
        return createLevelPriv;
    }

    public void setCreateLevelPriv(String createLevelPriv) {
        this.createLevelPriv = createLevelPriv;
    }

    public String getManageUserPriv() {
        return manageUserPriv;
    }

    public void setManageUserPriv(String manageUserPriv) {
        this.manageUserPriv = manageUserPriv;
    }

    public String getManageDeptPriv() {
        return manageDeptPriv;
    }

    public void setManageDeptPriv(String manageDeptPriv) {
        this.manageDeptPriv = manageDeptPriv;
    }

    public String getManageLevelPriv() {
        return manageLevelPriv;
    }

    public void setManageLevelPriv(String manageLevelPriv) {
        this.manageLevelPriv = manageLevelPriv;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getSpaceLimit() {
        return spaceLimit;
    }

    public void setSpaceLimit(String spaceLimit) {
        this.spaceLimit = spaceLimit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
