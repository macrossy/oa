package com.core136.bean.file;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: KnowledgeComment
 * @Description: 知识点评
 * @author: 稠云技术
 * @date: 2020年9月7日 上午10:10:17
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "knowledge_comment")
public class KnowledgeComment implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String commentId;
    private String knowledgeId;
    private Double levelStar;
    private String remark;
    private String commType;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getKnowledgeId() {
        return knowledgeId;
    }

    public void setKnowledgeId(String knowledgeId) {
        this.knowledgeId = knowledgeId;
    }


    public Double getLevelStar() {
        return levelStar;
    }

    public void setLevelStar(Double levelStar) {
        this.levelStar = levelStar;
    }

    public String getCommType() {
        return commType;
    }

    public void setCommType(String commType) {
        this.commType = commType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


}
