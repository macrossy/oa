package com.core136.bean.platform;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: PlatformMenu
 * @Description: 智能业务平台菜单
 * @author: 稠云技术
 * @date: 2020年12月30日 下午2:07:08
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "platform_menu")
public class PlatformMenu implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String sysMenuId;
    private String sortNo;
    private String sysMenuName;
    private String sysMenuCode;
    private String sysMenuParm;
    private String sysMenuOpen;
    private String sysMenuLevel;
    private String sysMenuPic;
    private String formId;
    private String optType;
    private String remark;
    private String isHide;
    private String i18nFlag;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getSysMenuId() {
        return sysMenuId;
    }

    public void setSysMenuId(String sysMenuId) {
        this.sysMenuId = sysMenuId;
    }

    public String getSortNo() {
        return sortNo;
    }

    public void setSortNo(String sortNo) {
        this.sortNo = sortNo;
    }

    public String getSysMenuName() {
        return sysMenuName;
    }

    public void setSysMenuName(String sysMenuName) {
        this.sysMenuName = sysMenuName;
    }

    public String getSysMenuCode() {
        return sysMenuCode;
    }

    public void setSysMenuCode(String sysMenuCode) {
        this.sysMenuCode = sysMenuCode;
    }

    public String getSysMenuParm() {
        return sysMenuParm;
    }

    public void setSysMenuParm(String sysMenuParm) {
        this.sysMenuParm = sysMenuParm;
    }

    public String getSysMenuOpen() {
        return sysMenuOpen;
    }

    public void setSysMenuOpen(String sysMenuOpen) {
        this.sysMenuOpen = sysMenuOpen;
    }

    public String getSysMenuLevel() {
        return sysMenuLevel;
    }

    public void setSysMenuLevel(String sysMenuLevel) {
        this.sysMenuLevel = sysMenuLevel;
    }

    public String getSysMenuPic() {
        return sysMenuPic;
    }

    public void setSysMenuPic(String sysMenuPic) {
        this.sysMenuPic = sysMenuPic;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getOptType() {
        return optType;
    }

    public void setOptType(String optType) {
        this.optType = optType;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getIsHide() {
        return isHide;
    }

    public void setIsHide(String isHide) {
        this.isHide = isHide;
    }

    public String getI18nFlag() {
        return i18nFlag;
    }

    public void setI18nFlag(String i18nFlag) {
        this.i18nFlag = i18nFlag;
    }
}
