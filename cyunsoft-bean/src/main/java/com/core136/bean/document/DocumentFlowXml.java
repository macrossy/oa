/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: DocumentFlowXml.java
 * @Package com.core136.bean.document
 * @Description: 描述
 * @author: lsq
 * @date: 2020年1月20日 下午4:56:44
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.document;

import java.io.Serializable;
import java.util.List;

/**
 * @author lsq
 *
 */
public class DocumentFlowXml implements Serializable {

    private static final long serialVersionUID = 1L;
    private DocumentFlow documentFlow;
    private List<DocumentProcess> documentProcessList;

    /**
     * @return the documentFlow
     */
    public DocumentFlow getDocumentFlow() {
        return documentFlow;
    }

    /**
     * @param documentFlow the documentFlow to set
     */
    public void setDocumentFlow(DocumentFlow documentFlow) {
        this.documentFlow = documentFlow;
    }

    /**
     * @return the documentProcessList
     */
    public List<DocumentProcess> getDocumentProcessList() {
        return documentProcessList;
    }

    /**
     * @param documentProcessList the documentProcessList to set
     */
    public void setDocumentProcessList(List<DocumentProcess> documentProcessList) {
        this.documentProcessList = documentProcessList;
    }

}
