$(function () {
    getCodeClass("kpiType", "kpiType");
    jeDate("#beginTime", {
        format: "YYYY-MM-DD",
        minDate: getSysDate(),
        isinitVal: true
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD",
        minDate: getSysDate(),
        isinitVal: true
    });
    getSmsConfig("msgType", "hr");
    $(".js-add-save").unbind("click").click(function () {
        insertKpiPlan();
    });
    $("#kpiRule").unbind("change").change(function () {
        if ($(this).val() == "1") {
            $("#chargetr").show();
        } else {
            $("#chargetr").hide();
        }
    });
    $(".js-child-item").unbind("click").click(function () {
        addChildItem();
    });
    $("#kpiType").unbind("change").change(function () {
        getItemlist();
    });
});

function deleteChildItem(Obj) {
    $(Obj).parent("td").parent("tr").remove();
}

function insertKpiPlan() {
    if($("#title").val()=="")
    {
        layer.msg("标题不能为空!");
        return;
    }
    $.ajax({
        url: "/set/hrset/insertHrKpiPlan",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            chargeUser: $("#chargeUser").attr("data-value"),
            kpiRule: $("#kpiRule").val(),
            userPriv: $("#userPriv").attr("data-value"),
            deptPriv: $("#deptPriv").attr("data-value"),
            levelPriv: $("#levelPriv").attr("data-value"),
            msgType: getCheckBoxValue("msgType"),
            attach: $("#hrattach").attr("data_value"),
            remark: $("#remark").val(),
            itemList: getParam()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function addChildItem() {
    $("#setkpiitemmodal").modal("show");
    $(".js-save").unbind("click").click(function () {
        var arr = getItemCheckBoxValue();
        for (var i = 0; i < arr.length; i++) {
            var index = $("#child-item-table").find("tr").length;
            $("#child-item-table").append("<tr><td><input type='number' name='sortNo' value='" + index + "' class='form-control'></td>" +
                "<td data-value=\"" + arr[i].itemId + "\">" + arr[i].title + "</td><td><a onclick=\"deleteChildItem(this)\" class='btn btn-darkorange btn-xs'>删除</a></td></tr>");
        }
        $("#setkpiitemmodal").modal("hide");
    });
}

function getItemlist() {
    $.ajax({
        url: "/ret/hrget/getHrKpiItemListForSelect",
        type: "post",
        dataType: "json",
        data: {kpiType: $("#kpiType").val()},
        success: function (data) {
            if (data.status == "200") {
                var itemInfo = data.list;
                var html = "";
                for (var i = 0; i < itemInfo.length; i++) {
                    html += "<div class=\"checkbox\"><label><input type=\"checkbox\" name=\"kpiitem\" data-title=\"" + itemInfo[i].title + "\" value=\"" + itemInfo[i].itemId + "\"><span class=\"text\">" + itemInfo[i].title + "</span></label></div>";
                }
                $(".itemlist").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function getItemCheckBoxValue() {
    var returnArr = [];
    $('input[name="kpiitem"]:checked').each(function () {
        var json = {};
        json.itemId = $(this).val();
        json.title = $(this).attr("data-title");
        returnArr.push(json);
    });
    return returnArr;
}


function getParam() {
    var paramArr = [];
    $("#child-item-table").find("tr").next().each(function () {
        var json = {};
        var sortNo = $(this).children("td").eq(0).find("input").val();
        var itemId = $(this).children("td").eq(1).attr("data-value");
        json.sortNo = sortNo;
        json.itemId = itemId;
        paramArr.push(json);
    });
    return JSON.stringify(paramArr);
}
