let ue = UE.getEditor("remark");
$(function () {
    getHrRecruitPlanForSelect();
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    jeDate("#workDate", {
        format: "YYYY-MM-DD",
        minDate: getSysDate()
    });
    $(".js-auto-select").each(function () {
        var module = $(this).attr("module");
        createAutoSelect(module);
    })
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/hrget/getHrRecruitTaskList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        sortOrder: "asc",
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'taskId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'userName',
            width: '100px',
            title: '人员姓名',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.taskId + "')\" style='cursor: pointer'>"+value+"</a>";
            }
        },{
            field: 'title',
            title: '招聘计划标题',
            width: '150px'
        }, {
            field: 'createTime',
            width: '100px',
            title: '招聘时间'
        },  {
            field: 'linkTel',
            width: '100px',
            title: '联系电话'
        }, {
            field: 'salary',
            width: '100px',
            title: '拟定薪资'
        }, {
            field: 'workType',
            title: '应聘工种',
            width: '100px',
            formatter: function (value, row, index) {
                return getHrClassCodeName('workType', value);
            }
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '120px',
            formatter: function (value, row, index) {
                return createOptBtn(row.taskId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function createOptBtn(taskId) {
    var html = "";
    html += "<a href=\"javascript:void(0);edit('" + taskId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;";
    html += "<a href=\"javascript:void(0);deleteReocrd('" + taskId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function deleteReocrd(taskId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/hrset/deleteHrRecruitTask",
            type: "post",
            dataType: "json",
            data: {taskId: taskId},
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}

function edit(taskId) {
    $("#planlistdiv").hide();
    $("#plandiv").show();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    $("#hrattach").attr("data_value","");
    $("#hrattach_show").empty();
    ue.setContent("");
    document.getElementById("form1").reset();
    $.ajax({
        url: "/ret/hrget/getHrRecruitTaskById",
        type: "post",
        dataType: "json",
        data: {taskId: taskId},
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#show_hrattach").html("");
                        $("#hrattach").attr("data_value", recordInfo.attach);
                        createAttach("hrattach", 4);
                    } else if (id == "remark") {
                        ue.setContent(recordInfo[id]);
                    } else {
                        $("#" + id).val(recordInfo[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updateHrRecruitTask(taskId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function details(taskId) {
    window.open("/app/core/hr/hrrecruittaskdetails?taskId=" + taskId);
}

function updateHrRecruitTask(taskId) {
    if($("#userName").val()=="")
    {
        layer.msg("人员姓名不能为空！");
        return;
    }
    $.ajax({
        url: "/set/hrset/updateHrRecruitTask",
        type: "post",
        dataType: "json",
        data: {
            taskId: taskId,
            workDate: $("#workDate").val(),
            userName: $("#userName").val(),
            linkTel: $("#linkTel").val(),
            salary: $("#salary").val(),
            workType: $("#workType").val(),
            planId: $("#planId").val(),
            attach: $("#hrattach").attr("data_value"),
            remark: ue.getContent()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function goback() {
    $("#plandiv").hide();
    $("#planlistdiv").show();
}


function getHrRecruitPlanForSelect() {
    $.ajax({
        url: "/ret/hrget/getHrRecruitPlanForSelect",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var html = "<option value=''>请选择</option>";
                for (var i = 0; i < data.list.length; i++) {
                    html += "<option value='" + data.list[i].planId + "'>" + data.list[i].title + "</option>";
                }
                $("#planId").html(html);
            } else if (data.status == "100") {
                layer.msg(data.msg)
            } else if (data.statsu == "500") {
                console.log(data.msg);
            }
        }
    });
}
