/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutSetHrController.java
 * @Package com.core136.controller.hr
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月14日 下午1:21:51
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.controller.hr;


import com.alibaba.fastjson.JSON;
import com.core136.bean.account.Account;
import com.core136.bean.hr.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.hr.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * @author lsq
 */
@RestController
@RequestMapping("/set/hrset")
public class RouteSetHrController {
    private HrDepartmentService hrDepartmentService;

    @Autowired
    public void setHrDepartmentService(HrDepartmentService hrDepartmentService) {
        this.hrDepartmentService = hrDepartmentService;
    }

    private HrUserInfoService hrUserInfoService;

    @Autowired
    public void setHrUserInfoService(HrUserInfoService hrUserInfoService) {
        this.hrUserInfoService = hrUserInfoService;
    }

    private HrUserLevelService hrUserLevelService;

    @Autowired
    public void setHrUserLevelService(HrUserLevelService hrUserLevelService) {
        this.hrUserLevelService = hrUserLevelService;
    }

    private HrWagesLevelService hrWagesLevelService;

    @Autowired
    public void setHrWagesLevelService(HrWagesLevelService hrWagesLevelService) {
        this.hrWagesLevelService = hrWagesLevelService;
    }

    private HrClassCodeService hrClassCodeService;

    @Autowired
    public void setHrClassCodeService(HrClassCodeService hrClassCodeService) {
        this.hrClassCodeService = hrClassCodeService;
    }

    private HrContractService hrContractService;

    @Autowired
    public void setHrContractService(HrContractService hrContractService) {
        this.hrContractService = hrContractService;
    }

    private HrIncentiveService hrIncentiveService;

    @Autowired
    public void setHrIncentiveService(HrIncentiveService hrIncentiveService) {
        this.hrIncentiveService = hrIncentiveService;
    }

    private HrLicenceService hrLicenceService;

    @Autowired
    public void setHrLicenceService(HrLicenceService hrLicenceService) {
        this.hrLicenceService = hrLicenceService;
    }

    private HrLearnRecordService hrLearnRecordService;

    @Autowired
    public void setHrLearnRecordService(HrLearnRecordService hrLearnRecordService) {
        this.hrLearnRecordService = hrLearnRecordService;
    }

    private HrWorkRecordService hrWorkRecordService;

    @Autowired
    public void setHrWorkRecordService(HrWorkRecordService hrWorkRecordService) {
        this.hrWorkRecordService = hrWorkRecordService;
    }

    private HrWorkSkillsService hrWorkSkillsService;

    @Autowired
    public void setHrWorkSkillsService(HrWorkSkillsService hrWorkSkillsService) {
        this.hrWorkSkillsService = hrWorkSkillsService;
    }

    private HrPersonnelTransferService hrPersonnelTransferService;

    @Autowired
    public void setHrPersonnelTransferService(HrPersonnelTransferService hrPersonnelTransferService) {
        this.hrPersonnelTransferService = hrPersonnelTransferService;
    }

    private HrLeaveRecordService hrLeaveRecordService;

    @Autowired
    public void setHrLeaveRecordService(HrLeaveRecordService hrLeaveRecordService) {
        this.hrLeaveRecordService = hrLeaveRecordService;
    }

    private HrReinstatementService hrReinstatementService;

    @Autowired
    public void setHrReinstatementService(HrReinstatementService hrReinstatementService) {
        this.hrReinstatementService = hrReinstatementService;
    }

    private HrTitleEvaluationService hrTitleEvaluationService;

    @Autowired
    public void setHrTitleEvaluationService(HrTitleEvaluationService hrTitleEvaluationService) {
        this.hrTitleEvaluationService = hrTitleEvaluationService;
    }

    private HrCareRecordService hrCareRecordService;

    @Autowired
    public void setHrCareRecordService(HrCareRecordService hrCareRecordService) {
        this.hrCareRecordService = hrCareRecordService;
    }

    private HrTrainRecordService hrTrainRecordService;

    @Autowired
    public void setHrTrainRecordService(HrTrainRecordService hrTrainRecordService) {
        this.hrTrainRecordService = hrTrainRecordService;
    }

    private HrRecruitNeedsService hrRecruitNeedsService;

    @Autowired
    public void setHrRecruitNeedsService(HrRecruitNeedsService hrRecruitNeedsService) {
        this.hrRecruitNeedsService = hrRecruitNeedsService;
    }

    private HrKpiItemService hrKpiItemService;

    @Autowired
    public void setHrKpiItemService(HrKpiItemService hrKpiItemService) {
        this.hrKpiItemService = hrKpiItemService;
    }

    private HrRecruitPlanService hrRecruitPlanService;

    @Autowired
    public void setHrRecruitPlanService(HrRecruitPlanService hrRecruitPlanService) {
        this.hrRecruitPlanService = hrRecruitPlanService;
    }

    private HrSalaryRecordService hrSalaryRecordService;

    @Autowired
    public void setHrSalaryRecordService(HrSalaryRecordService hrSalaryRecordService) {
        this.hrSalaryRecordService = hrSalaryRecordService;
    }

    private HrWelfareRecordService hrWelfareRecordService;

    @Autowired
    public void setHrWelfareRecordService(HrWelfareRecordService hrWelfareRecordService) {
        this.hrWelfareRecordService = hrWelfareRecordService;
    }

    private HrEvaluateService hrEvaluateService;

    @Autowired
    public void setHrEvaluateService(HrEvaluateService hrEvaluateService) {
        this.hrEvaluateService = hrEvaluateService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private HrKpiPlanService hrKpiPlanService;

    @Autowired
    public void setHrKpiPlanService(HrKpiPlanService hrKpiPlanService) {
        this.hrKpiPlanService = hrKpiPlanService;
    }

    private HrKpiPlanItemService hrKpiPlanItemService;

    @Autowired
    public void setHrKpiPlanItemService(HrKpiPlanItemService hrKpiPlanItemService) {
        this.hrKpiPlanItemService = hrKpiPlanItemService;
    }

    private HrKpiPlanRecordService hrKpiPlanRecordService;

    @Autowired
    public void setHrKpiPlanRecordService(HrKpiPlanRecordService hrKpiPlanRecordService) {
        this.hrKpiPlanRecordService = hrKpiPlanRecordService;
    }

    private HrRecruitTaskService hrRecruitTaskService;

    @Autowired
    public void setHrRecruitTaskService(HrRecruitTaskService hrRecruitTaskService) {
        this.hrRecruitTaskService = hrRecruitTaskService;
    }

    /**
     * @param hrRecruitTask
     * @return RetDataBean
     * @Title: insertHrRecruitTask
     * @Description:  创建考聘任务
     */
    @RequestMapping(value = "/insertHrRecruitTask", method = RequestMethod.POST)
    public RetDataBean insertHrRecruitTask(HrRecruitTask hrRecruitTask) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrRecruitTask.setTaskId(SysTools.getGUID());
            hrRecruitTask.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrRecruitTask.setCreateUser(account.getAccountId());
            hrRecruitTask.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrRecruitTaskService.insertHrRecruitTask(hrRecruitTask));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrRecruitTask
     * @return RetDataBean
     * @Title: deleteHrRecruitTask
     * @Description:  删除招聘任务
     */
    @RequestMapping(value = "/deleteHrRecruitTask", method = RequestMethod.POST)
    public RetDataBean deleteHrRecruitTask(HrRecruitTask hrRecruitTask) {
        try {
            if (StringUtils.isBlank(hrRecruitTask.getTaskId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrRecruitTask.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrRecruitTaskService.deleteHrRecruitTask(hrRecruitTask));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrRecruitTask
     * @return RetDataBean
     * @Title: updateHrRecruitTask
     * @Description:  更新招聘任务
     */
    @RequestMapping(value = "/updateHrRecruitTask", method = RequestMethod.POST)
    public RetDataBean updateHrRecruitTask(HrRecruitTask hrRecruitTask) {
        try {
            if (StringUtils.isBlank(hrRecruitTask.getTaskId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrRecruitTask.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("taskId", hrRecruitTask.getTaskId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrRecruitTaskService.updateHrRecruitTask(example, hrRecruitTask));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param planId
     * @param accountId
     * @param scoreList
     * @return RetDataBean
     * @Title: setScoreToUser
     * @Description:  设置考核分
     */
    @RequestMapping(value = "/setScoreToUser", method = RequestMethod.POST)
    public RetDataBean setScoreToUser(String planId, String accountId, String scoreList) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return hrKpiPlanRecordService.setScoreToUser(account, planId, accountId, scoreList);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param planId
     * @return RetDataBean
     * @Title: updatePlanEffect
     * @Description:  考核计划生效
     */
    @RequestMapping(value = "/updatePlanEffect", method = RequestMethod.POST)
    public RetDataBean updatePlanEffect(String planId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            HrKpiPlan hrKpiPlan = new HrKpiPlan();
            hrKpiPlan.setOrgId(account.getOrgId());
            hrKpiPlan.setPlanId(planId);
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrKpiPlanService.updatePlanEffect(account, hrKpiPlan));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrKpiPlanItem
     * @return RetDataBean
     * @Title: insertHrKpiPlanItem
     * @Description:  创建考核指标集
     */
    @RequestMapping(value = "/insertHrKpiPlanItem", method = RequestMethod.POST)
    public RetDataBean insertHrKpiPlanItem(HrKpiPlanItem hrKpiPlanItem) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrKpiPlanItem.setItemId(SysTools.getGUID());
            hrKpiPlanItem.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrKpiPlanItem.setCreateUser(account.getAccountId());
            hrKpiPlanItem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrKpiPlanItemService.insertHrKpiPlanItem(hrKpiPlanItem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrKpiPlanItem
     * @return RetDataBean
     * @Title: deleteHrKpiPlanItem
     * @Description:  删除子指标集
     */
    @RequestMapping(value = "/deleteHrKpiPlanItem", method = RequestMethod.POST)
    public RetDataBean deleteHrKpiPlanItem(HrKpiPlanItem hrKpiPlanItem) {
        try {
            if (StringUtils.isBlank(hrKpiPlanItem.getItemId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrKpiPlanItem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrKpiPlanItemService.deleteHrKpiPlanItem(hrKpiPlanItem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrKpiPlanItem
     * @return RetDataBean
     * @Title: updateHrKpiPlanItem
     * @Description:  更新考核指标集
     */
    @RequestMapping(value = "/updateHrKpiPlanItem", method = RequestMethod.POST)
    public RetDataBean updateHrKpiPlanItem(HrKpiPlanItem hrKpiPlanItem) {
        try {
            if (StringUtils.isBlank(hrKpiPlanItem.getItemId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrKpiPlanItem.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("itemId", hrKpiPlanItem.getItemId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrKpiPlanItemService.updateHrKpiPlanItem(example, hrKpiPlanItem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrKpiPlan
     * @return RetDataBean
     * @Title: insertHrKpiPlan
     * @Description:  创建考核计划与考核指标
     */
    @RequestMapping(value = "/insertHrKpiPlan", method = RequestMethod.POST)
    public RetDataBean insertHrKpiPlan(HrKpiPlan hrKpiPlan, String itemList) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrKpiPlan.setPlanId(SysTools.getGUID());
            hrKpiPlan.setStatus("0");
            hrKpiPlan.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrKpiPlan.setCreateUser(account.getAccountId());
            hrKpiPlan.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrKpiPlanService.addHrKpiPlan(hrKpiPlan, itemList));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrKpiPlan
     * @return RetDataBean
     * @Title: deleteHrKpiPlan
     * @Description:  删除考核计划
     */
    @RequestMapping(value = "/deleteHrKpiPlan", method = RequestMethod.POST)
    public RetDataBean deleteHrKpiPlan(HrKpiPlan hrKpiPlan) {
        try {
            if (StringUtils.isBlank(hrKpiPlan.getPlanId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrKpiPlan.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrKpiPlanService.deleteHrKpiPlan(hrKpiPlan));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrKpiPlan
     * @return RetDataBean
     * @Title: updateHrKpiPlan
     * @Description:  更新考核计划
     */
    @RequestMapping(value = "/updateHrKpiPlan", method = RequestMethod.POST)
    public RetDataBean updateHrKpiPlan(HrKpiPlan hrKpiPlan, String itemList) {
        try {
            if (StringUtils.isBlank(hrKpiPlan.getPlanId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            hrKpiPlan.setStatus("0");
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrKpiPlan.class);
            hrKpiPlan.setOrgId(account.getOrgId());
            hrKpiPlan.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrKpiPlan.setCreateUser(account.getAccountId());
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("planId", hrKpiPlan.getPlanId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrKpiPlanService.updateHrKpiPlanAndItem(example, hrKpiPlan, itemList));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrKpiPlan
     * @return RetDataBean
     * @Title: stopHrKpiPlan
     * @Description:  终止考核计划
     */
    @RequestMapping(value = "/stopHrKpiPlan", method = RequestMethod.POST)
    public RetDataBean stopHrKpiPlan(HrKpiPlan hrKpiPlan) {
        try {
            if (StringUtils.isBlank(hrKpiPlan.getPlanId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrKpiPlan.setStatus("2");
            Example example = new Example(HrKpiPlan.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("planId", hrKpiPlan.getPlanId());
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, hrKpiPlanService.updateHrKpiPlan(example, hrKpiPlan));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrEvaluate
     * @return RetDataBean
     * @Title: insertHrEvaluate
     * @Description:  添加人员评价记录
     */
    @RequestMapping(value = "/insertHrEvaluate", method = RequestMethod.POST)
    public RetDataBean insertHrEvaluate(HrEvaluate hrEvaluate) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrEvaluate.setRecordId(SysTools.getGUID());
            hrEvaluate.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrEvaluate.setCreateUser(account.getAccountId());
            hrEvaluate.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrEvaluateService.insertHrEvaluate(hrEvaluate));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrEvaluate
     * @return RetDataBean
     * @Title: deleteHrEvaluate
     * @Description:  删除人员评价记录
     */
    @RequestMapping(value = "/deleteHrEvaluate", method = RequestMethod.POST)
    public RetDataBean deleteHrEvaluate(HrEvaluate hrEvaluate) {
        try {
            if (StringUtils.isBlank(hrEvaluate.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrEvaluate.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrEvaluateService.deleteHrEvaluate(hrEvaluate));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrEvaluate
     * @return RetDataBean
     * @Title: updateHrEvaluate
     * @Description:  更新人员评价记录
     */
    @RequestMapping(value = "/updateHrEvaluate", method = RequestMethod.POST)
    public RetDataBean updateHrEvaluate(HrEvaluate hrEvaluate) {
        try {
            if (StringUtils.isBlank(hrEvaluate.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrEvaluate.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", hrEvaluate.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrEvaluateService.updateHrEvaluate(example, hrEvaluate));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param file
     * @return ModelAndView
     * @Title: importHrWelfareRecord
     * @Description:  批量导入人员福利
     */
    @RequestMapping(value = "/importHrWelfareRecord", method = RequestMethod.POST)
    public ModelAndView importHrWelfareRecord(MultipartFile file) {
        ModelAndView mv = null;
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            mv = new ModelAndView("app/core/hr/salary/welfareimport");
            RetDataBean retDataBean = hrWelfareRecordService.importHrWelfareRecord(account, file);
            mv.addObject("retDataBean", retDataBean);
            return mv;
        } catch (Exception e) {

            mv.addObject("retDataBean", RetDataTools.NotOk(MessageCode.MESSAGE_FAILED));
            return mv;
        }
    }

    /**
     * @param hrWelfareRecord
     * @return RetDataBean
     * @Title: insertHrWelfareRecord
     * @Description:  添加人员福利记录
     */
    @RequestMapping(value = "/insertHrWelfareRecord", method = RequestMethod.POST)
    public RetDataBean insertHrWelfareRecord(HrWelfareRecord hrWelfareRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrWelfareRecord.setRecordId(SysTools.getGUID());
            hrWelfareRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrWelfareRecord.setCreateUser(account.getAccountId());
            hrWelfareRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrWelfareRecordService.insertHrWelfareRecord(hrWelfareRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrWelfareRecord
     * @return RetDataBean
     * @Title: deleteHrWelfareRecord
     * @Description:  删除人员福利记录
     */
    @RequestMapping(value = "/deleteHrWelfareRecord", method = RequestMethod.POST)
    public RetDataBean deleteHrWelfareRecord(HrWelfareRecord hrWelfareRecord) {
        try {
            if (StringUtils.isBlank(hrWelfareRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrWelfareRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrWelfareRecordService.deleteHrWelfareRecord(hrWelfareRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrWelfareRecord
     * @return RetDataBean
     * @Title: updateHrWelfareRecord
     * @Description:  更新人员福利记录
     */
    @RequestMapping(value = "/updateHrWelfareRecord", method = RequestMethod.POST)
    public RetDataBean updateHrWelfareRecord(HrWelfareRecord hrWelfareRecord) {
        try {
            if (StringUtils.isBlank(hrWelfareRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrWelfareRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", hrWelfareRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrWelfareRecordService.updateHrWelfareRecord(example, hrWelfareRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param file
     * @return ModelAndView
     * @Title: importHrSalaryRecord
     * @Description:  导入工员薪资
     */
    @RequestMapping(value = "/importHrSalaryRecord", method = RequestMethod.POST)
    public ModelAndView importHrSalaryRecord(MultipartFile file) {
        ModelAndView mv = null;
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            mv = new ModelAndView("app/core/hr/salary/salaryimport");
            RetDataBean retDataBean = hrSalaryRecordService.importHrSalaryRecord(account, file);
            mv.addObject("retDataBean", retDataBean);
            return mv;
        } catch (Exception e) {

            mv.addObject("retDataBean", RetDataTools.NotOk(MessageCode.MESSAGE_FAILED));
            return mv;
        }
    }

    /**
     * @param hrSalaryRecord
     * @return RetDataBean
     * @Title: insertHrSalaryRecord
     * @Description:  添加人员薪资记录
     */
    @RequestMapping(value = "/insertHrSalaryRecord", method = RequestMethod.POST)
    public RetDataBean insertHrSalaryRecord(HrSalaryRecord hrSalaryRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrSalaryRecord.setRecordId(SysTools.getGUID());
            hrSalaryRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrSalaryRecord.setCreateUser(account.getAccountId());
            hrSalaryRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrSalaryRecordService.insertHrSalaryRecord(hrSalaryRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrSalaryRecord
     * @return RetDataBean
     * @Title: deleteHrSalaryRecord
     * @Description:  删除人员薪资记录
     */
    @RequestMapping(value = "/deleteHrSalaryRecord", method = RequestMethod.POST)
    public RetDataBean deleteHrSalaryRecord(HrSalaryRecord hrSalaryRecord) {
        try {
            if (StringUtils.isBlank(hrSalaryRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrSalaryRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrSalaryRecordService.deleteHrSalaryRecord(hrSalaryRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrSalaryRecord
     * @return RetDataBean
     * @Title: updateHrSalaryRecord
     * @Description:  更新人员薪资记录
     */
    @RequestMapping(value = "/updateHrSalaryRecord", method = RequestMethod.POST)
    public RetDataBean updateHrSalaryRecord(HrSalaryRecord hrSalaryRecord) {
        try {
            if (StringUtils.isBlank(hrSalaryRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrSalaryRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", hrSalaryRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrSalaryRecordService.updateHrSalaryRecord(example, hrSalaryRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param hrKpiItem
     * @return RetDataBean
     * @Title: insertHrKpiItem
     * @Description:  添加考核指标
     */
    @RequestMapping(value = "/insertHrKpiItem", method = RequestMethod.POST)
    public RetDataBean insertHrKpiItem(HrKpiItem hrKpiItem) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrKpiItem.setItemId(SysTools.getGUID());
            hrKpiItem.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrKpiItem.setCreateUser(account.getAccountId());
            hrKpiItem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrKpiItemService.insertHrKpiItem(hrKpiItem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrKpiItem
     * @return RetDataBean
     * @Title: deleteHrKpiItem
     * @Description:  删除考核指标
     */
    @RequestMapping(value = "/deleteHrKpiItem", method = RequestMethod.POST)
    public RetDataBean deleteHrKpiItem(HrKpiItem hrKpiItem) {
        try {
            if (StringUtils.isBlank(hrKpiItem.getItemId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrKpiItem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrKpiItemService.deleteHrKpiItem(hrKpiItem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrKpiItem
     * @return RetDataBean
     * @Title: updateHrKpiItem
     * @Description:  更新考核指标
     */
    @RequestMapping(value = "/updateHrKpiItem", method = RequestMethod.POST)
    public RetDataBean updateHrKpiItem(HrKpiItem hrKpiItem) {
        try {
            if (StringUtils.isBlank(hrKpiItem.getItemId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrKpiItem.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("itemId", hrKpiItem.getItemId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrKpiItemService.updateHrKpiItem(example, hrKpiItem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrRecruitPlan
     * @return RetDataBean
     * @Title: insertHrRecruitPlan
     * @Description:  创建招聘计划
     */
    @RequestMapping(value = "/insertHrRecruitPlan", method = RequestMethod.POST)
    public RetDataBean insertHrRecruitPlan(HrRecruitPlan hrRecruitPlan) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrRecruitPlan.setPlanId(SysTools.getGUID());
            hrRecruitPlan.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrRecruitPlan.setCreateUser(account.getAccountId());
            hrRecruitPlan.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrRecruitPlanService.insertHrRecruitPlan(hrRecruitPlan));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrRecruitPlan
     * @return RetDataBean
     * @Title: deleteHrRecruitPlan
     * @Description:  删除招聘计划
     */
    @RequestMapping(value = "/deleteHrRecruitPlan", method = RequestMethod.POST)
    public RetDataBean deleteHrRecruitPlan(HrRecruitPlan hrRecruitPlan) {
        try {
            if (StringUtils.isBlank(hrRecruitPlan.getPlanId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrRecruitPlan.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrRecruitPlanService.deleteHrRecruitPlan(hrRecruitPlan));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrRecruitPlan
     * @return RetDataBean
     * @Title: updateHrRecruitPlan
     * @Description:  更新招聘计划
     */
    @RequestMapping(value = "/updateHrRecruitPlan", method = RequestMethod.POST)
    public RetDataBean updateHrRecruitPlan(HrRecruitPlan hrRecruitPlan) {
        try {
            if (StringUtils.isBlank(hrRecruitPlan.getPlanId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrRecruitPlan.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("planId", hrRecruitPlan.getPlanId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrRecruitPlanService.updateHrRecruitPlan(example, hrRecruitPlan));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param file
     * @return ModelAndView
     * @Title: importHrWorkSkills
     * @Description:  劳动技能导入
     */
    @RequestMapping(value = "/importHrWorkSkills", method = RequestMethod.POST)
    public ModelAndView importHrWorkSkills(MultipartFile file) {
        ModelAndView mv = null;
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            mv = new ModelAndView("app/core/hr/work/import");
            RetDataBean retDataBean = hrWorkSkillsService.importHrWorkSkills(account, file);
            mv.addObject("retDataBean", retDataBean);
            return mv;
        } catch (Exception e) {

            mv.addObject("retDataBean", RetDataTools.NotOk(MessageCode.MESSAGE_FAILED));
            return mv;
        }
    }

    /**
     * @param file
     * @return ModelAndView
     * @Title: importHrWorkRecord
     * @Description:  工作经历导入
     */
    @RequestMapping(value = "/importHrWorkRecord", method = RequestMethod.POST)
    public ModelAndView importHrWorkRecord(MultipartFile file) {
        ModelAndView mv = null;
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            mv = new ModelAndView("app/core/hr/work/import");
            RetDataBean retDataBean = hrWorkRecordService.importHrWorkRecord(account, file);
            mv.addObject("retDataBean", retDataBean);
            return mv;
        } catch (Exception e) {

            mv.addObject("retDataBean", RetDataTools.NotOk(MessageCode.MESSAGE_FAILED));
            return mv;
        }
    }

    /**
     * @param file
     * @return ModelAndView
     * @Title: importHrLearnRecord
     * @Description:  学习记录导入
     */
    @RequestMapping(value = "/importHrLearnRecord", method = RequestMethod.POST)
    public ModelAndView importHrLearnRecord(MultipartFile file) {
        ModelAndView mv = null;
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            mv = new ModelAndView("app/core/hr/learn/import");
            RetDataBean retDataBean = hrLearnRecordService.importHrLearnRecord(account, file);
            mv.addObject("retDataBean", retDataBean);
            return mv;
        } catch (Exception e) {

            mv.addObject("retDataBean", RetDataTools.NotOk(MessageCode.MESSAGE_FAILED));
            return mv;
        }
    }

    /**
     * @param file
     * @return ModelAndView
     * @Title: importHrLicence
     * @Description:  证照记录导入
     */
    @RequestMapping(value = "/importHrLicence", method = RequestMethod.POST)
    public ModelAndView importHrLicence(MultipartFile file) {
        ModelAndView mv = null;
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            mv = new ModelAndView("app/core/hr/licence/import");
            RetDataBean retDataBean = hrLicenceService.importHrLicence(account, file);
            mv.addObject("retDataBean", retDataBean);
            return mv;
        } catch (Exception e) {

            mv.addObject("retDataBean", RetDataTools.NotOk(MessageCode.MESSAGE_FAILED));
            return mv;
        }
    }

    /**
     * @param file
     * @return ModelAndView
     * @Title: importHrIncentive
     * @Description:  奖惩记录导入
     */
    @RequestMapping(value = "/importHrIncentive", method = RequestMethod.POST)
    public ModelAndView importHrIncentive(MultipartFile file) {
        ModelAndView mv = null;
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            mv = new ModelAndView("app/core/hr/incentive/import");
            RetDataBean retDataBean = hrIncentiveService.importHrIncentive(account, file);
            mv.addObject("retDataBean", retDataBean);
            return mv;
        } catch (Exception e) {

            mv.addObject("retDataBean", RetDataTools.NotOk(MessageCode.MESSAGE_FAILED));
            return mv;
        }
    }

    /**
     * @param file
     * @return ModelAndView
     * @Title: importHrContract
     * @Description:  人事合同导入
     */
    @RequestMapping(value = "/importHrContract", method = RequestMethod.POST)
    public ModelAndView importHrContract(MultipartFile file) {
        ModelAndView mv = null;
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            mv = new ModelAndView("app/core/hr/contract/import");
            RetDataBean retDataBean = hrContractService.importHrContract(account, file);
            mv.addObject("retDataBean", retDataBean);
            return mv;
        } catch (Exception e) {

            mv.addObject("retDataBean", RetDataTools.NotOk(MessageCode.MESSAGE_FAILED));
            return mv;
        }
    }

    /**
     * @param file
     * @return RetDataBean
     * @Title: importHrUserInfo
     * @Description:  人事档案导入
     */
    @RequestMapping(value = "/importHrUserInfo", method = RequestMethod.POST)
    public ModelAndView importHrUserInfo(MultipartFile file) {
        ModelAndView mv = null;
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            mv = new ModelAndView("app/core/hr/baseinfo/baseinfoimport");
            RetDataBean retDataBean = hrUserInfoService.importHrUserInfo(account, file);
            mv.addObject("retDataBean", retDataBean);
            return mv;
        } catch (Exception e) {

            mv.addObject("retDataBean", RetDataTools.NotOk(MessageCode.MESSAGE_FAILED));
            return mv;
        }
    }

    /**
     * @param hrRecruitNeeds
     * @return RetDataBean
     * @Title: insertHrRecruitNeeds
     * @Description:  添加招聘需求
     */
    @RequestMapping(value = "/insertHrRecruitNeeds", method = RequestMethod.POST)
    public RetDataBean insertHrRecruitNeeds(HrRecruitNeeds hrRecruitNeeds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrRecruitNeeds.setRecordId(SysTools.getGUID());
            hrRecruitNeeds.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrRecruitNeeds.setCreateUser(account.getAccountId());
            hrRecruitNeeds.setStatus("0");
            hrRecruitNeeds.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrRecruitNeedsService.insertHrRecruitNeeds(hrRecruitNeeds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrRecruitNeeds
     * @return RetDataBean
     * @Title: deleteHrRecruitNeeds
     * @Description:  删除招聘需求
     */
    @RequestMapping(value = "/deleteHrRecruitNeeds", method = RequestMethod.POST)
    public RetDataBean deleteHrRecruitNeeds(HrRecruitNeeds hrRecruitNeeds) {
        try {
            if (StringUtils.isBlank(hrRecruitNeeds.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrRecruitNeeds.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrRecruitNeedsService.deleteHrRecruitNeeds(hrRecruitNeeds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrRecruitNeeds
     * @return RetDataBean
     * @Title: approvedHrRecruitNeeds
     * @Description:  招聘需求审批
     */
    @RequestMapping(value = "/approvedHrRecruitNeeds", method = RequestMethod.POST)
    public RetDataBean approvedHrRecruitNeeds(HrRecruitNeeds hrRecruitNeeds) {
        try {
            if (StringUtils.isBlank(hrRecruitNeeds.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            hrRecruitNeeds.setApprovedTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrRecruitNeeds.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", hrRecruitNeeds.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_APPROVAL_SUCCESS, hrRecruitNeedsService.updateHrRecruitNeeds(example, hrRecruitNeeds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrRecruitNeeds
     * @return RetDataBean
     * @Title: updateHrRecruitNeeds
     * @Description:  更新招聘需求
     */
    @RequestMapping(value = "/updateHrRecruitNeeds", method = RequestMethod.POST)
    public RetDataBean updateHrRecruitNeeds(HrRecruitNeeds hrRecruitNeeds) {
        try {
            if (StringUtils.isBlank(hrRecruitNeeds.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrRecruitNeeds.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", hrRecruitNeeds.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrRecruitNeedsService.updateHrRecruitNeeds(example, hrRecruitNeeds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrTrainRecord
     * @return RetDataBean
     * @Title: insertHrTrainRecord
     * @Description:  发起人员培训申请
     */
    @RequestMapping(value = "/insertHrTrainRecord", method = RequestMethod.POST)
    public RetDataBean insertHrTrainRecord(HrTrainRecord hrTrainRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrTrainRecord.setRecordId(SysTools.getGUID());
            hrTrainRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrTrainRecord.setCreateUser(account.getAccountId());
            hrTrainRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrTrainRecordService.insertHrTrainRecord(hrTrainRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrTrainRecord
     * @return RetDataBean
     * @Title: deleteHrTrainRecord
     * @Description:  删除人员培训计划
     */
    @RequestMapping(value = "/deleteHrTrainRecord", method = RequestMethod.POST)
    public RetDataBean deleteHrTrainRecord(HrTrainRecord hrTrainRecord) {
        try {
            if (StringUtils.isBlank(hrTrainRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrTrainRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrTrainRecordService.deleteHrTrainRecord(hrTrainRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrTrainRecord
     * @return RetDataBean
     * @Title: updateHrTrainRecord
     * @Description:  更新培训计划
     */
    @RequestMapping(value = "/updateHrTrainRecord", method = RequestMethod.POST)
    public RetDataBean updateHrTrainRecord(HrTrainRecord hrTrainRecord) {
        try {
            if (StringUtils.isBlank(hrTrainRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrTrainRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", hrTrainRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrTrainRecordService.updateHrTrainRecord(example, hrTrainRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrTrainRecord
     * @return RetDataBean
     * @Title: approvedHrTrainRecord
     * @Description:  审批培训计划
     */
    @RequestMapping(value = "/approvedHrTrainRecord", method = RequestMethod.POST)
    public RetDataBean approvedHrTrainRecord(HrTrainRecord hrTrainRecord) {
        try {
            if (StringUtils.isBlank(hrTrainRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            hrTrainRecord.setApprovedTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrTrainRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", hrTrainRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrTrainRecordService.updateHrTrainRecord(example, hrTrainRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param hrCareRecord
     * @return RetDataBean
     * @Title: insertHrCareRecord
     * @Description:  添加员工关怀记录
     */
    @RequestMapping(value = "/insertHrCareRecord", method = RequestMethod.POST)
    public RetDataBean insertHrCareRecord(HrCareRecord hrCareRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrCareRecord.setRecordId(SysTools.getGUID());
            hrCareRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrCareRecord.setCreateUser(account.getAccountId());
            hrCareRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrCareRecordService.insertHrCareRecord(hrCareRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrCareRecord
     * @return RetDataBean
     * @Title: deleteHrCareRecord
     * @Description:  删除员工关怀记录
     */
    @RequestMapping(value = "/deleteHrCareRecord", method = RequestMethod.POST)
    public RetDataBean deleteHrCareRecord(HrCareRecord hrCareRecord) {
        try {
            if (StringUtils.isBlank(hrCareRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrCareRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrCareRecordService.deleteHrCareRecord(hrCareRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrCareRecord
     * @return RetDataBean
     * @Title: updateHrCareRecord
     * @Description:  更新员工关怀信息
     */
    @RequestMapping(value = "/updateHrCareRecord", method = RequestMethod.POST)
    public RetDataBean updateHrCareRecord(HrCareRecord hrCareRecord) {
        try {
            if (StringUtils.isBlank(hrCareRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrCareRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", hrCareRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrCareRecordService.updateHrCareRecord(example, hrCareRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrTitleEvaluation
     * @return RetDataBean
     * @Title: insertHrTitleEvaluation
     * @Description:  添加人员评定记录
     */
    @RequestMapping(value = "/insertHrTitleEvaluation", method = RequestMethod.POST)
    public RetDataBean insertHrTitleEvaluation(HrTitleEvaluation hrTitleEvaluation) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrTitleEvaluation.setRecordId(SysTools.getGUID());
            hrTitleEvaluation.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrTitleEvaluation.setCreateUser(account.getAccountId());
            hrTitleEvaluation.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrTitleEvaluationService.insertHrTitleEvaluation(hrTitleEvaluation));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrTitleEvaluation
     * @return RetDataBean
     * @Title: deleteHrTitleEvaluation
     * @Description:  删除人员评定记录
     */
    @RequestMapping(value = "/deleteHrTitleEvaluation", method = RequestMethod.POST)
    public RetDataBean deleteHrTitleEvaluation(HrTitleEvaluation hrTitleEvaluation) {
        try {
            if (StringUtils.isBlank(hrTitleEvaluation.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrTitleEvaluation.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrTitleEvaluationService.deleteHrTitleEvaluation(hrTitleEvaluation));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrTitleEvaluation
     * @return RetDataBean
     * @Title: updateHrTitleEvaluation
     * @Description:  更新人员评定信息
     */
    @RequestMapping(value = "/updateHrTitleEvaluation", method = RequestMethod.POST)
    public RetDataBean updateHrTitleEvaluation(HrTitleEvaluation hrTitleEvaluation) {
        try {
            if (StringUtils.isBlank(hrTitleEvaluation.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrTitleEvaluation.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", hrTitleEvaluation.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrTitleEvaluationService.updateHrTitleEvaluation(example, hrTitleEvaluation));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param hrReinstatement
     * @return RetDataBean
     * @Title: insertHrReinstatement
     * @Description:  添加人员复职记录
     */
    @RequestMapping(value = "/insertHrReinstatement", method = RequestMethod.POST)
    public RetDataBean insertHrReinstatement(HrReinstatement hrReinstatement) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrReinstatement.setRecordId(SysTools.getGUID());
            hrReinstatement.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrReinstatement.setCreateUser(account.getAccountId());
            hrReinstatement.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrReinstatementService.insertHrReinstatement(hrReinstatement));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrReinstatement
     * @return RetDataBean
     * @Title: deleteHrReinstatement
     * @Description:  删除人员复职记录
     */
    @RequestMapping(value = "/deleteHrReinstatement", method = RequestMethod.POST)
    public RetDataBean deleteHrReinstatement(HrReinstatement hrReinstatement) {
        try {
            if (StringUtils.isBlank(hrReinstatement.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrReinstatement.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrReinstatementService.deleteHrReinstatement(hrReinstatement));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrReinstatement
     * @return RetDataBean
     * @Title: updateHrReinstatement
     * @Description:  更新人员复职信息
     */
    @RequestMapping(value = "/updateHrReinstatement", method = RequestMethod.POST)
    public RetDataBean updateHrReinstatement(HrReinstatement hrReinstatement) {
        try {
            if (StringUtils.isBlank(hrReinstatement.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrReinstatement.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", hrReinstatement.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrReinstatementService.updateHrReinstatement(example, hrReinstatement));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param hrLeaveRecord
     * @return RetDataBean
     * @Title: insertHrLeaveRecord
     * @Description:  添加人员离职动记录
     */
    @RequestMapping(value = "/insertHrLeaveRecord", method = RequestMethod.POST)
    public RetDataBean insertHrLeaveRecord(HrLeaveRecord hrLeaveRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrLeaveRecord.setRecordId(SysTools.getGUID());
            hrLeaveRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrLeaveRecord.setCreateUser(account.getAccountId());
            hrLeaveRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrLeaveRecordService.insertHrLeaveRecord(hrLeaveRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrLeaveRecord
     * @return RetDataBean
     * @Title: deleteHrLeaveRecord
     * @Description:  删除人员离职记录
     */
    @RequestMapping(value = "/deleteHrLeaveRecord", method = RequestMethod.POST)
    public RetDataBean deleteHrLeaveRecord(HrLeaveRecord hrLeaveRecord) {
        try {
            if (StringUtils.isBlank(hrLeaveRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrLeaveRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrLeaveRecordService.deleteHrLeaveRecord(hrLeaveRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrLeaveRecord
     * @return RetDataBean
     * @Title: updateHrLeaveRecord
     * @Description:  更新人员离职信息
     */
    @RequestMapping(value = "/updateHrLeaveRecord", method = RequestMethod.POST)
    public RetDataBean updateHrLeaveRecord(HrLeaveRecord hrLeaveRecord) {
        try {
            if (StringUtils.isBlank(hrLeaveRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrPersonnelTransfer.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", hrLeaveRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrLeaveRecordService.updateHrLeaveRecord(example, hrLeaveRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param hrPersonnelTrans
     * @return RetDataBean
     * @Title: insertHrPersonnelTransfer
     * @Description:  添加人事调动记录
     */
    @RequestMapping(value = "/insertHrPersonnelTransfer", method = RequestMethod.POST)
    public RetDataBean insertHrPersonnelTransfer(HrPersonnelTransfer hrPersonnelTrans) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrPersonnelTrans.setTransferId(SysTools.getGUID());
            hrPersonnelTrans.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrPersonnelTrans.setCreateUser(account.getAccountId());
            hrPersonnelTrans.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrPersonnelTransferService.insertHrPersonnelTransfer(hrPersonnelTrans));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrPersonnelTrans
     * @return RetDataBean
     * @Title: deleteHrPersonnelTransfer
     * @Description:  删除人事调动记录
     */
    @RequestMapping(value = "/deleteHrPersonnelTransfer", method = RequestMethod.POST)
    public RetDataBean deleteHrPersonnelTransfer(HrPersonnelTransfer hrPersonnelTrans) {
        try {
            if (StringUtils.isBlank(hrPersonnelTrans.getTransferId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrPersonnelTrans.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrPersonnelTransferService.deleteHrPersonnelTransfer(hrPersonnelTrans));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrPersonnelTrans
     * @return RetDataBean
     * @Title: updateHrPersonnelTransfer
     * @Description:  更新人事调动信息
     */
    @RequestMapping(value = "/updateHrPersonnelTransfer", method = RequestMethod.POST)
    public RetDataBean updateHrPersonnelTransfer(HrPersonnelTransfer hrPersonnelTrans) {
        try {
            if (StringUtils.isBlank(hrPersonnelTrans.getTransferId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrPersonnelTransfer.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("transferId", hrPersonnelTrans.getTransferId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrPersonnelTransferService.updateHrPersonnelTransfer(example, hrPersonnelTrans));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrWorkSkills
     * @return RetDataBean
     * @Title: insertHrWorkSkills
     * @Description:  添加劳动持能记录
     */
    @RequestMapping(value = "/insertHrWorkSkills", method = RequestMethod.POST)
    public RetDataBean insertHrWorkSkills(HrWorkSkills hrWorkSkills) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrWorkSkills.setRecordId(SysTools.getGUID());
            hrWorkSkills.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrWorkSkills.setCreateUser(account.getAccountId());
            hrWorkSkills.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrWorkSkillsService.insertHrWorkSkills(hrWorkSkills));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrWorkSkills
     * @return RetDataBean
     * @Title: deleteHrWorkSkills
     * @Description:  删除劳动技能
     */
    @RequestMapping(value = "/deleteHrWorkSkills", method = RequestMethod.POST)
    public RetDataBean deleteHrWorkSkills(HrWorkSkills hrWorkSkills) {
        try {
            if (StringUtils.isBlank(hrWorkSkills.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrWorkSkills.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrWorkSkillsService.deleteHrWorkSkills(hrWorkSkills));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrWorkSkills
     * @return RetDataBean
     * @Title: updateHrWorkSkills
     * @Description:  更新劳动技能
     */
    @RequestMapping(value = "/updateHrWorkSkills", method = RequestMethod.POST)
    public RetDataBean updateHrWorkSkills(HrWorkSkills hrWorkSkills) {
        try {
            if (StringUtils.isBlank(hrWorkSkills.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrWorkSkills.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", hrWorkSkills.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrWorkSkillsService.updateHrWorkSkills(example, hrWorkSkills));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrWorkRecord
     * @return RetDataBean
     * @Title: insertHrWorkRecord
     * @Description:  添加工作记录
     */
    @RequestMapping(value = "/insertHrWorkRecord", method = RequestMethod.POST)
    public RetDataBean insertHrWorkRecord(HrWorkRecord hrWorkRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrWorkRecord.setRecordId(SysTools.getGUID());
            hrWorkRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrWorkRecord.setCreateUser(account.getAccountId());
            hrWorkRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrWorkRecordService.insertHrWorkRecord(hrWorkRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrWorkRecord
     * @return RetDataBean
     * @Title: deleteHrWorkRecord
     * @Description:  删除工作记录
     */
    @RequestMapping(value = "/deleteHrWorkRecord", method = RequestMethod.POST)
    public RetDataBean deleteHrWorkRecord(HrWorkRecord hrWorkRecord) {
        try {
            if (StringUtils.isBlank(hrWorkRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrWorkRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrWorkRecordService.deleteHrWorkRecord(hrWorkRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrWorkRecord
     * @return RetDataBean
     * @Title: updateHrWorkRecord
     * @Description:  更新工作记录
     */
    @RequestMapping(value = "/updateHrWorkRecord", method = RequestMethod.POST)
    public RetDataBean updateHrWorkRecord(HrWorkRecord hrWorkRecord) {
        try {
            if (StringUtils.isBlank(hrWorkRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrWorkRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", hrWorkRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrWorkRecordService.updateHrWorkRecord(example, hrWorkRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param hrLearnRecord
     * @return RetDataBean
     * @Title: insertHrLearnRecord
     * @Description:  添加学习记录
     */
    @RequestMapping(value = "/insertHrLearnRecord", method = RequestMethod.POST)
    public RetDataBean insertHrLearnRecord(HrLearnRecord hrLearnRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrLearnRecord.setRecordId(SysTools.getGUID());
            hrLearnRecord.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrLearnRecord.setCreateUser(account.getAccountId());
            hrLearnRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrLearnRecordService.insertHrLearnRecord(hrLearnRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrLearnRecord
     * @return RetDataBean
     * @Title: deleteHrLearnRecord
     * @Description:  删除学习记录
     */
    @RequestMapping(value = "/deleteHrLearnRecord", method = RequestMethod.POST)
    public RetDataBean deleteHrLearnRecord(HrLearnRecord hrLearnRecord) {
        try {
            if (StringUtils.isBlank(hrLearnRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrLearnRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrLearnRecordService.deleteHrLearnRecord(hrLearnRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrLearnRecord
     * @return RetDataBean
     * @Title: updateHrLearnRecord
     * @Description:  更新学习记录
     */
    @RequestMapping(value = "/updateHrLearnRecord", method = RequestMethod.POST)
    public RetDataBean updateHrLearnRecord(HrLearnRecord hrLearnRecord) {
        try {
            if (StringUtils.isBlank(hrLearnRecord.getRecordId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrLearnRecord.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("recordId", hrLearnRecord.getRecordId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrLearnRecordService.updateHrLearnRecord(example, hrLearnRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrLicence
     * @return RetDataBean
     * @Title: insertHrLicence
     * @Description:  添加证照记录
     */
    @RequestMapping(value = "/insertHrLicence", method = RequestMethod.POST)
    public RetDataBean insertHrLicence(HrLicence hrLicence) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrLicence.setLicenceId(SysTools.getGUID());
            hrLicence.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrLicence.setCreateUser(account.getAccountId());
            hrLicence.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrLicenceService.insertHrLicence(hrLicence));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrLicence
     * @return RetDataBean
     * @Title: deleteHrLicence
     * @Description:  删除证照记录
     */
    @RequestMapping(value = "/deleteHrLicence", method = RequestMethod.POST)
    public RetDataBean deleteHrLicence(HrLicence hrLicence) {
        try {
            if (StringUtils.isBlank(hrLicence.getLicenceId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrLicence.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrLicenceService.deleteHrLicence(hrLicence));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrLicence
     * @return RetDataBean
     * @Title: updateHrLicence
     * @Description:  更新证照记录
     */
    @RequestMapping(value = "/updateHrLicence", method = RequestMethod.POST)
    public RetDataBean updateHrLicence(HrLicence hrLicence) {
        try {
            if (StringUtils.isBlank(hrLicence.getLicenceId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrLicence.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("licenceId", hrLicence.getLicenceId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrLicenceService.updateHrLicence(example, hrLicence));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param hrIncentive
     * @return RetDataBean
     * @Title: insertHrIncentive
     * @Description:  添加奖惩记录
     */
    @RequestMapping(value = "/insertHrIncentive", method = RequestMethod.POST)
    public RetDataBean insertHrIncentive(HrIncentive hrIncentive) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrIncentive.setIncentiveId(SysTools.getGUID());
            hrIncentive.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrIncentive.setCreateUser(account.getAccountId());
            hrIncentive.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrIncentiveService.insertHrIncentive(hrIncentive));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrIncentive
     * @return RetDataBean
     * @Title: deleteHrIncentive
     * @Description:  删除奖惩记录
     */
    @RequestMapping(value = "/deleteHrIncentive", method = RequestMethod.POST)
    public RetDataBean deleteHrIncentive(HrIncentive hrIncentive) {
        try {
            if (StringUtils.isBlank(hrIncentive.getIncentiveId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrIncentive.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrIncentiveService.deleteHrIncentive(hrIncentive));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrIncentive
     * @return RetDataBean
     * @Title: updateHrIncentive
     * @Description:  更新奖惩记录
     */
    @RequestMapping(value = "/updateHrIncentive", method = RequestMethod.POST)
    public RetDataBean updateHrIncentive(HrIncentive hrIncentive) {
        try {
            if (StringUtils.isBlank(hrIncentive.getIncentiveId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrIncentive.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("incentiveId", hrIncentive.getIncentiveId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrIncentiveService.updateHrIncentive(example, hrIncentive));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param hrContract
     * @return RetDataBean
     * @Title: insertHrContract
     * @Description:  添加HR合同
     */
    @RequestMapping(value = "/insertHrContract", method = RequestMethod.POST)
    public RetDataBean insertHrContract(HrContract hrContract) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrContract.setContractId(SysTools.getGUID());
            hrContract.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrContract.setCreateUser(account.getAccountId());
            hrContract.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrContractService.insertHrContract(hrContract));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrContract
     * @return RetDataBean
     * @Title: deleteHrContract
     * @Description:  删除合同
     */
    @RequestMapping(value = "/deleteHrContract", method = RequestMethod.POST)
    public RetDataBean deleteHrContract(HrContract hrContract) {
        try {
            if (StringUtils.isBlank(hrContract.getContractId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrContract.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrContractService.deleteHrContract(hrContract));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrContract
     * @return RetDataBean
     * @Title: updateHrContract
     * @Description:  更新合同
     */
    @RequestMapping(value = "/updateHrContract", method = RequestMethod.POST)
    public RetDataBean updateHrContract(HrContract hrContract) {
        try {
            if (StringUtils.isBlank(hrContract.getContractId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrContract.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("contractId", hrContract.getContractId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrContractService.updateHrContract(example, hrContract));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: insertHrWagesLevel
     * @Description:  添加新工资级别
     * @param: request
     * @param: hrWorkType
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertHrWagesLevel", method = RequestMethod.POST)
    public RetDataBean insertHrWagesLevel(HrWagesLevel hrWagesLevel) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrWagesLevel.setWagesId(SysTools.getGUID());
            hrWagesLevel.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrWagesLevel.setCreateUser(account.getAccountId());
            hrWagesLevel.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrWagesLevelService.insertHrWagesLevel(hrWagesLevel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: deleteHrWagesLevel
     * @Description:  删除工资级别
     * @param: request
     * @param: hrWorkType
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteHrWagesLevel", method = RequestMethod.POST)
    public RetDataBean deleteHrWagesLevel(HrWagesLevel hrWagesLevel) {
        try {
            if (StringUtils.isBlank(hrWagesLevel.getWagesId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrWagesLevel.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrWagesLevelService.deleteHrWagesLevel(hrWagesLevel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateHrWagesLevel
     * @Description:  更新工资级别
     * @param: request
     * @param: hrWorkType
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateHrWagesLevel", method = RequestMethod.POST)
    public RetDataBean updateHrWagesLevel(HrWagesLevel hrWagesLevel) {
        try {
            if (StringUtils.isBlank(hrWagesLevel.getWagesId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrWagesLevel.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("wagesId", hrWagesLevel.getWagesId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrWagesLevelService.updateHrWagesLevel(example, hrWagesLevel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: insertHrUserInfo
     * @Description:  添加人员信息
     * @param: request
     * @param: hrUserInfo
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertHrUserInfo", method = RequestMethod.POST)
    public RetDataBean insertHrUserInfo(HrUserInfo hrUserInfo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrUserInfo.setUserId(SysTools.getGUID());
            hrUserInfo.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrUserInfo.setCreateUser(account.getAccountId());
            hrUserInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrUserInfoService.insertHrUserInfo(hrUserInfo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteHrUserInfo
     * @Description:  删除人员信息
     * @param: request
     * @param: hrUserInfo
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteHrUserInfo", method = RequestMethod.POST)
    public RetDataBean deleteHrUserInfo(HrUserInfo hrUserInfo) {
        try {
            if (StringUtils.isBlank(hrUserInfo.getUserId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrUserInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrUserInfoService.deleteHrUserInfo(hrUserInfo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateHrUserInfo
     * @Description:  更新人员信息
     * @param: request
     * @param: hrUserInfo
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateHrUserInfo", method = RequestMethod.POST)
    public RetDataBean updateHrUserInfo(HrUserInfo hrUserInfo) {
        try {
            if (StringUtils.isBlank(hrUserInfo.getUserId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrUserInfo.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("userId", hrUserInfo.getUserId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrUserInfoService.updateHrUserInfo(example, hrUserInfo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: insertHrDepartment
     * @Description:  添加人员部门
     * @param: request
     * @param: hrDepartment
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertHrDepartment", method = RequestMethod.POST)
    public RetDataBean insertHrDepartment(HrDepartment hrDepartment) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            hrDepartment.setDeptId(SysTools.getGUID());
            if (StringUtils.isBlank(hrDepartment.getOrgLevelId())) {
                hrDepartment.setOrgLevelId("0");
                hrDepartment.setLevelId("/");
            } else {
                if (hrDepartment.getOrgLevelId().equals("0")) {
                    hrDepartment.setLevelId("/");
                } else {
                    HrDepartment parentHrDepartment = new HrDepartment();
                    parentHrDepartment.setDeptId(hrDepartment.getOrgLevelId());
                    parentHrDepartment.setOrgId(account.getOrgId());
                    parentHrDepartment = hrDepartmentService.selectOneHrDepartment(parentHrDepartment);
                    if (parentHrDepartment.getLevelId().equals("/")) {
                        hrDepartment.setLevelId(parentHrDepartment.getLevelId() + hrDepartment.getOrgLevelId());
                    } else {
                        hrDepartment.setLevelId(parentHrDepartment.getLevelId() + "/" + hrDepartment.getOrgLevelId());
                    }
                }
            }
            hrDepartment.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrDepartmentService.insertHrDepartment(hrDepartment));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteHrDepartment
     * @Description:  删除人员部门
     * @param: request
     * @param: hrDepartment
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteHrDepartment", method = RequestMethod.POST)
    public RetDataBean deleteHrDepartment(HrDepartment hrDepartment) {
        try {
            if (StringUtils.isBlank(hrDepartment.getDeptId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrDepartment.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrDepartmentService.deleteHrDepartment(hrDepartment));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateHrDepartment
     * @Description:  更新人员部门信息
     * @param: request
     * @param: hrDepartment
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateHrDepartment", method = RequestMethod.POST)
    public RetDataBean updateHrDepartment(HrDepartment hrDepartment) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(hrDepartment.getDeptId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (hrDepartment.getDeptId().equals(hrDepartment.getOrgLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_PARENT_ID_CANNOT_TO_USE);
            }
            if (StringUtils.isBlank(hrDepartment.getOrgLevelId())) {
                hrDepartment.setOrgLevelId("0");
                hrDepartment.setLevelId("/");
            } else {
                if (hrDepartment.getOrgLevelId().equals("0")) {
                    hrDepartment.setLevelId("/");
                } else {
                    HrDepartment parentHrDepartment = new HrDepartment();
                    parentHrDepartment.setDeptId(hrDepartment.getOrgLevelId());
                    parentHrDepartment.setOrgId(account.getOrgId());
                    parentHrDepartment = hrDepartmentService.selectOneHrDepartment(parentHrDepartment);
                    if (parentHrDepartment.getLevelId().equals("/")) {
                        hrDepartment.setLevelId(parentHrDepartment.getLevelId() + hrDepartment.getOrgLevelId());
                    } else {
                        hrDepartment.setLevelId(parentHrDepartment.getLevelId() + "/" + hrDepartment.getOrgLevelId());
                    }
                }
            }
            Example example = new Example(HrDepartment.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("deptId", hrDepartment.getDeptId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrDepartmentService.updateHrDepartment(example, hrDepartment));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: insertUserLevel
     * @Description:  添加行政级别
     * @param: request
     * @param: hrUserLevel
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertHrUserLevel", method = RequestMethod.POST)
    public RetDataBean insertHrUserLevel(HrUserLevel hrUserLevel) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(hrUserLevel.getSuperior())) {
                hrUserLevel.setSuperior("0");
            }
            hrUserLevel.setLevelId(SysTools.getGUID());
            hrUserLevel.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrUserLevel.setCreateUser(account.getAccountId());
            hrUserLevel.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrUserLevelService.insertHrUserLevel(hrUserLevel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteHrUserLevel
     * @Description:  删除行政级别
     * @param: request
     * @param: hrUserLevel
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteHrUserLevel", method = RequestMethod.POST)
    public RetDataBean deleteHrUserLevel(HrUserLevel hrUserLevel) {
        try {
            if (StringUtils.isBlank(hrUserLevel.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrUserLevel.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrUserLevelService.deleteHrUserLevel(hrUserLevel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateHrUserLevel
     * @Description:  更新
     * @param: request
     * @param: hrUserLevel
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateHrUserLevel", method = RequestMethod.POST)
    public RetDataBean updateHrUserLevel(HrUserLevel hrUserLevel) {
        try {
            if (StringUtils.isBlank(hrUserLevel.getLevelId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrUserLevel.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("levelId", hrUserLevel.getLevelId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrUserLevelService.updateHrUserLevel(example, hrUserLevel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrClassCode
     * @return RetDataBean
     * @Title: insertHrClassCode
     * @Description:  添加HR分类代码
     */
    @RequestMapping(value = "/insertHrClassCode", method = RequestMethod.POST)
    public RetDataBean insertHrClassCode(HrClassCode hrClassCode) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(hrClassCode.getParentId())) {
                hrClassCode.setParentId("");
            }
            hrClassCode.setCodeFlag("1");
            hrClassCode.setCodeId(SysTools.getGUID());
            hrClassCode.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            hrClassCode.setCreateUser(account.getAccountId());
            hrClassCode.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, hrClassCodeService.insertHrClassCode(hrClassCode));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrClassCode
     * @return RetDataBean
     * @Title: updateHrClassCode
     * @Description:  更新HR分类代码
     */
    @RequestMapping(value = "/updateHrClassCode", method = RequestMethod.POST)
    public RetDataBean updateHrClassCode(HrClassCode hrClassCode) {
        try {
            if (StringUtils.isBlank(hrClassCode.getCodeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            Example example = new Example(HrClassCode.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("codeId", hrClassCode.getCodeId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, hrClassCodeService.updateHrClassCode(example, hrClassCode));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param hrClassCode
     * @return RetDataBean
     * @Title: deleteHrClassCode
     * @Description:  删除分类码及以下的子集
     */
    @RequestMapping(value = "/deleteHrClassCode", method = RequestMethod.POST)
    public RetDataBean deleteHrClassCode(HrClassCode hrClassCode) {
        try {
            if (StringUtils.isBlank(hrClassCode.getCodeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            hrClassCode.setOrgId(account.getOrgId());
            hrClassCode = hrClassCodeService.selectOneHrClassCode(hrClassCode);
            HrClassCode hrClassCode1 = new HrClassCode();
            hrClassCode1.setParentId(hrClassCode.getCodeId());
            hrClassCodeService.deleteHrClassCode(hrClassCode1);
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, hrClassCodeService.deleteHrClassCode(hrClassCode));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 导入人事部门
     *
     * @param file
     * @return
     */
    @RequestMapping(value = "/importHrDepartment", method = RequestMethod.POST)
    public void importHrDepartment(HttpServletResponse response, MultipartFile file) {
        PrintWriter out = null;
        try {
            RetDataBean retDataBean = new RetDataBean();
            Account account = accountService.getRedisAUserInfoToAccount();
            retDataBean = hrDepartmentService.importHrDepartment(account, file);
            String s = JSON.toJSONString(retDataBean);
            response.setContentType("text/html; charset=utf-8");
            out = response.getWriter().append(s);
            out.close();
        } catch (Exception e) {
            //e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
}
