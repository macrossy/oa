/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: HrLicenceMapper.java
 * @Package com.core136.mapper.hr
 * @Description: 描述
 * @author: lsq
 * @date: 2019年12月31日 上午10:08:01
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.mapper.hr;

import com.core136.bean.hr.HrLicence;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
@Mapper
public interface HrLicenceMapper extends MyMapper<HrLicence> {
    /**
     *
     * @Title: getHrLicenceList
     * @Description:  获取证照列表
     * @param orgId
     * @param userId
     * @param beginTime
     * @param licenceType
     * @param search
     * @return
     * List<Map < String, String>>

     */
    public List<Map<String, String>> getHrLicenceList(@Param(value = "orgId") String orgId,
                                                      @Param(value = "userId") String userId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                      @Param(value = "licenceType") String licenceType, @Param(value = "search") String search
    );

    /**
     *
     * @Title: getMyHrLicenceList
     * @Description:  查询个人证照信息
     * @param orgId
     * @param accountId
     * @return
     * List<Map < String, String>>

     */
    public List<Map<String, String>> getMyHrLicenceList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

}
