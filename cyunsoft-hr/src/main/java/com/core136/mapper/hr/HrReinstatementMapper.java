package com.core136.mapper.hr;

import com.core136.bean.hr.HrReinstatement;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HrReinstatementMapper extends MyMapper<HrReinstatement> {
    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param reinstatementType
     * @return List<Map < String, String>>
     * @Title: getHrReinstatementList
     * @Description:  获取复值列表
     */
    public List<Map<String, String>> getHrReinstatementList(@Param(value = "orgId") String orgId, @Param(value = "userId") String userId,
                                                            @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "reinstatementType") String reinstatementType);
}
