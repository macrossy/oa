package com.core136.mapper.hr;

import com.core136.bean.hr.HrRecruitTask;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


@Mapper
public interface HrRecruitTaskMapper extends MyMapper<HrRecruitTask> {

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrRecruitTaskList
     * @Description:  获取招聘任务列表
     */
    public List<Map<String, String>> getHrRecruitTaskList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag,
                                                          @Param(value = "accountId") String accountId, @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                          @Param(value = "search") String search);

}
