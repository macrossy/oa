package com.core136.mapper.hr;

import com.core136.bean.hr.HrLeaveRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HrLeaveRecordMapper extends MyMapper<HrLeaveRecord> {
    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param levelType
     * @return List<Map < String, String>>
     * @Title: getHrLeaveRecordList
     * @Description:  获取离职人员列表
     */
    public List<Map<String, String>> getHrLeaveRecordList(@Param(value = "orgId") String orgId, @Param(value = "userId") String userId,
                                                          @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "levelType") String levelType);
}
