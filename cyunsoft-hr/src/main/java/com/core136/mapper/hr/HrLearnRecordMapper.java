package com.core136.mapper.hr;

import com.core136.bean.hr.HrLearnRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface HrLearnRecordMapper extends MyMapper<HrLearnRecord> {
    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrLearnRecordList
     * @Description:  获取教育经历列表
     */
    public List<Map<String, String>> getHrLearnRecordList(@Param(value = "orgId") String orgId, @Param(value = "userId") String userId,
                                                          @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getMyHrLearnRecordList
     * @Description:  个人查询学习记录
     */
    public List<Map<String, String>> getMyHrLearnRecordList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

}
