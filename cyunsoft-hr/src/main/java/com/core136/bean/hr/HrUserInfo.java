/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: HrUserInfo.java
 * @Package com.core136.bean.hr
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年9月10日 下午3:53:57
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.bean.hr;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: HrUserInfo
 * @Description:(这里用一句话描述这个类的作用)
 * @author: 稠云信息
 * @date: 2019年9月10日 下午3:53:57
 * @author lsq
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "hr_user_info")
public class HrUserInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Integer sortNo;
    private String userId;
    private String accountId;
    private String deptId;
    private String levelId;
    private String userName;
    private String userNameEn;
    private String workNo;
    private String staffNo;
    private String beforeUserName;
    private String sex;
    private String staffCardNo;
    private String levelType;
    private String animal;
    private String nativePlace;
    private String address;
    private String bloodType;
    private String nationalty;
    private String maritalStatus;
    private String health;
    private String birthDay;
    private String politicalStatus;
    private String joinPartyTime;
    private String staffType;
    private String staffAddress;
    private String workType;
    private String wagesLevel;
    private String occupation;
    private String employedTime;
    private String workStatus;
    private String workJob;
    private String phone;
    private String attendType;
    private String persentPosition;
    private String workLevel;
    private String mobileNo;
    private String wxNo;
    private String email;
    private String homeAddress;
    private String qq;
    private String otherContact;
    private String bank;
    private String bankAccount;
    private String highsetShool;
    private String highsetDegree;
    private String graduationTime;
    private String graduationShool;
    private String major;
    private String skills;
    private String otherLanguage;
    private String otherLanguageLevel;
    private String cretificate;
    private String surety;
    private String insure;
    private String bodyExamim;
    private String remark;
    private String resume;
    private String photos;
    private String attach;
    private String createTime;
    private String createUser;
    private String orgId;

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNameEn() {
        return userNameEn;
    }

    public void setUserNameEn(String userNameEn) {
        this.userNameEn = userNameEn;
    }

    public String getWorkNo() {
        return workNo;
    }

    public void setWorkNo(String workNo) {
        this.workNo = workNo;
    }

    public String getStaffNo() {
        return staffNo;
    }

    public void setStaffNo(String staffNo) {
        this.staffNo = staffNo;
    }

    public String getBeforeUserName() {
        return beforeUserName;
    }

    public void setBeforeUserName(String beforeUserName) {
        this.beforeUserName = beforeUserName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getStaffCardNo() {
        return staffCardNo;
    }

    public void setStaffCardNo(String staffCardNo) {
        this.staffCardNo = staffCardNo;
    }

    public String getLevelType() {
        return levelType;
    }

    public void setLevelType(String levelType) {
        this.levelType = levelType;
    }

    public String getAnimal() {
        return animal;
    }

    public void setAnimal(String animal) {
        this.animal = animal;
    }

    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getNationalty() {
        return nationalty;
    }

    public void setNationalty(String nationalty) {
        this.nationalty = nationalty;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    public String getPoliticalStatus() {
        return politicalStatus;
    }

    public void setPoliticalStatus(String politicalStatus) {
        this.politicalStatus = politicalStatus;
    }

    public String getJoinPartyTime() {
        return joinPartyTime;
    }

    public void setJoinPartyTime(String joinPartyTime) {
        this.joinPartyTime = joinPartyTime;
    }

    public String getStaffType() {
        return staffType;
    }

    public void setStaffType(String staffType) {
        this.staffType = staffType;
    }

    public String getStaffAddress() {
        return staffAddress;
    }

    public void setStaffAddress(String staffAddress) {
        this.staffAddress = staffAddress;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getWagesLevel() {
        return wagesLevel;
    }

    public void setWagesLevel(String wagesLevel) {
        this.wagesLevel = wagesLevel;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getEmployedTime() {
        return employedTime;
    }

    public void setEmployedTime(String employedTime) {
        this.employedTime = employedTime;
    }

    public String getWorkStatus() {
        return workStatus;
    }

    public void setWorkStatus(String workStatus) {
        this.workStatus = workStatus;
    }

    public String getWorkJob() {
        return workJob;
    }

    public void setWorkJob(String workJob) {
        this.workJob = workJob;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAttendType() {
        return attendType;
    }

    public void setAttendType(String attendType) {
        this.attendType = attendType;
    }

    public String getPersentPosition() {
        return persentPosition;
    }

    public void setPersentPosition(String persentPosition) {
        this.persentPosition = persentPosition;
    }

    public String getWorkLevel() {
        return workLevel;
    }

    public void setWorkLevel(String workLevel) {
        this.workLevel = workLevel;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getWxNo() {
        return wxNo;
    }

    public void setWxNo(String wxNo) {
        this.wxNo = wxNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getOtherContact() {
        return otherContact;
    }

    public void setOtherContact(String otherContact) {
        this.otherContact = otherContact;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getHighsetShool() {
        return highsetShool;
    }

    public void setHighsetShool(String highsetShool) {
        this.highsetShool = highsetShool;
    }

    public String getHighsetDegree() {
        return highsetDegree;
    }

    public void setHighsetDegree(String highsetDegree) {
        this.highsetDegree = highsetDegree;
    }

    public String getGraduationTime() {
        return graduationTime;
    }

    public void setGraduationTime(String graduationTime) {
        this.graduationTime = graduationTime;
    }

    public String getGraduationShool() {
        return graduationShool;
    }

    public void setGraduationShool(String graduationShool) {
        this.graduationShool = graduationShool;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getOtherLanguage() {
        return otherLanguage;
    }

    public void setOtherLanguage(String otherLanguage) {
        this.otherLanguage = otherLanguage;
    }

    public String getOtherLanguageLevel() {
        return otherLanguageLevel;
    }

    public void setOtherLanguageLevel(String otherLanguageLevel) {
        this.otherLanguageLevel = otherLanguageLevel;
    }

    public String getCretificate() {
        return cretificate;
    }

    public void setCretificate(String cretificate) {
        this.cretificate = cretificate;
    }

    public String getSurety() {
        return surety;
    }

    public void setSurety(String surety) {
        this.surety = surety;
    }

    public String getInsure() {
        return insure;
    }

    public void setInsure(String insure) {
        this.insure = insure;
    }

    public String getBodyExamim() {
        return bodyExamim;
    }

    public void setBodyExamim(String bodyExamim) {
        this.bodyExamim = bodyExamim;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

}
