/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: HrUserLevel.java
 * @Package com.core136.bean.hr
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月30日 上午10:36:40
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.hr;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 *	公司人员行政级别
 */
@Table(name = "hr_user_level")
public class HrUserLevel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Integer sortNo;
    private String levelId;
    private String levelNoId;
    private String levelName;
    /**
     * 上级
     */
    private String superior;
    /**
     *
     */
    private String createTime;

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    private String createUser;
    private String orgId;

    /**
     * @return the createUser
     */
    public String getCreateUser() {
        return createUser;
    }

    /**
     * @param createUser the createUser to set
     */
    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    /**
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    /**
     * @return the superior
     */
    public String getSuperior() {
        return superior;
    }

    /**
     * @param superior the superior to set
     */
    public void setSuperior(String superior) {
        this.superior = superior;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }

    public String getLevelNoId() {
        return levelNoId;
    }

    public void setLevelNoId(String levelNoId) {
        this.levelNoId = levelNoId;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

}
