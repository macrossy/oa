package com.core136.bean.hr;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName: HrIncentive
 * @Description: 奖惩记录
 * @author: 稠云技术
 * @date: 2020年4月26日 下午8:30:13
 * @Copyright: 2020 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Table(name = "hr_incentive")
public class HrIncentive implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String incentiveId;
    private Integer sortNo;
    private String incentiveType;
    private String incentiveItem;
    private String userId;
    private String salaryMonth;
    private Double incentiveAmount;
    private String attach;
    private String incentiveTime;
    private String remark;
    private String msgType;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getIncentiveId() {
        return incentiveId;
    }

    public void setIncentiveId(String incentiveId) {
        this.incentiveId = incentiveId;
    }

    public String getIncentiveType() {
        return incentiveType;
    }

    public void setIncentiveType(String incentiveType) {
        this.incentiveType = incentiveType;
    }

    public String getIncentiveItem() {
        return incentiveItem;
    }

    public void setIncentiveItem(String incentiveItem) {
        this.incentiveItem = incentiveItem;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSalaryMonth() {
        return salaryMonth;
    }

    public void setSalaryMonth(String salaryMonth) {
        this.salaryMonth = salaryMonth;
    }

    public Double getIncentiveAmount() {
        return incentiveAmount;
    }

    public void setIncentiveAmount(Double incentiveAmount) {
        this.incentiveAmount = incentiveAmount;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getIncentiveTime() {
        return incentiveTime;
    }

    public void setIncentiveTime(String incentiveTime) {
        this.incentiveTime = incentiveTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }


}
