package com.core136.service.hr;

import com.core136.bean.hr.HrReinstatement;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrReinstatementMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrReinstatementService {
    private HrReinstatementMapper hrReinstatementMapper;

    @Autowired
    public void setHrReinstatementMapper(HrReinstatementMapper hrReinstatementMapper) {
        this.hrReinstatementMapper = hrReinstatementMapper;
    }

    public int insertHrReinstatement(HrReinstatement hrReinstatement) {
        return hrReinstatementMapper.insert(hrReinstatement);
    }

    public int deleteHrReinstatement(HrReinstatement hrReinstatement) {
        return hrReinstatementMapper.delete(hrReinstatement);
    }

    public int updateHrReinstatement(Example example, HrReinstatement hrReinstatement) {
        return hrReinstatementMapper.updateByExampleSelective(hrReinstatement, example);
    }

    public HrReinstatement selectOneHrReinstatement(HrReinstatement hrReinstatement) {
        return hrReinstatementMapper.selectOne(hrReinstatement);
    }

    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param reinstatementType
     * @return List<Map < String, String>>
     * @Title: getHrReinstatementList
     * @Description:  获取复值列表
     */
    public List<Map<String, String>> getHrReinstatementList(String orgId, String userId, String beginTime, String endTime, String reinstatementType) {
        return hrReinstatementMapper.getHrReinstatementList(orgId, userId, beginTime, endTime, reinstatementType);
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param reinstatementType
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrReinstatementList
     * @Description:  获取复值列表
     */
    public PageInfo<Map<String, String>> getHrReinstatementList(PageParam pageParam, String userId, String beginTime, String endTime, String reinstatementType) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrReinstatementList(pageParam.getOrgId(), userId, beginTime, endTime, reinstatementType);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
