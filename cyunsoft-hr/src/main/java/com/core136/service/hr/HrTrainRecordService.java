/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: HrTrainRecordService.java
 * @Package com.core136.service.hr
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月28日 上午9:05:35
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.service.hr;

import com.core136.bean.hr.HrTrainRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrTrainRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * 人员培训记录
 *
 * @author lsq
 */
@Service
public class HrTrainRecordService {
    private HrTrainRecordMapper hrTrainRecordMapper;

    @Autowired
    public void setHrTrainRecordMapper(HrTrainRecordMapper hrTrainRecordMapper) {
        this.hrTrainRecordMapper = hrTrainRecordMapper;
    }

    public int insertHrTrainRecord(HrTrainRecord hrTrainRecord) {
        return hrTrainRecordMapper.insert(hrTrainRecord);
    }

    public int deleteHrTrainRecord(HrTrainRecord hrTrainRecord) {
        return hrTrainRecordMapper.delete(hrTrainRecord);
    }

    public int updateHrTrainRecord(Example example, HrTrainRecord hrTrainRecord) {
        return hrTrainRecordMapper.updateByExampleSelective(hrTrainRecord, example);
    }

    public HrTrainRecord selectOneHrTrainRecord(HrTrainRecord hrTrainRecord) {
        return hrTrainRecordMapper.selectOne(hrTrainRecord);
    }

    /**
     * @param orgId
     * @param createUser
     * @param channel
     * @param courseType
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrTrainRecordList
     * @Description:  获取培训列表
     */
    public List<Map<String, String>> getHrTrainRecordList(String orgId, String createUser, String channel, String courseType, String status, String beginTime, String endTime, String search) {
        return hrTrainRecordMapper.getHrTrainRecordList(orgId, createUser, channel, courseType, status, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param channel
     * @param courseType
     * @param status
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrTrainRecordList
     * @Description:  获取培训列表
     */
    public PageInfo<Map<String, String>> getHrTrainRecordList(PageParam pageParam, String channel, String courseType, String status, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrTrainRecordList(pageParam.getOrgId(), pageParam.getAccountId(), channel, courseType, status, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @param channel
     * @param courseType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrTrainRecordApprovedList
     * @Description:  获取待审批记录
     */
    public List<Map<String, String>> getHrTrainRecordApprovedList(String orgId, String accountId, String channel, String courseType, String beginTime, String endTime, String search) {
        return hrTrainRecordMapper.getHrTrainRecordApprovedList(orgId, accountId, channel, courseType, beginTime, endTime, search);
    }

    /**
     * @param pageParam
     * @param channel
     * @param courseType
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrTrainRecordApprovedList
     * @Description:  获取待审批记录
     */
    public PageInfo<Map<String, String>> getHrTrainRecordApprovedList(PageParam pageParam, String channel, String courseType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrTrainRecordApprovedList(pageParam.getOrgId(), pageParam.getAccountId(), channel, courseType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @param channel
     * @param courseType
     * @param status
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrTrainRecordApprovedOldList
     * @Description:  获取历史审批记录
     */
    public List<Map<String, String>> getHrTrainRecordApprovedOldList(String orgId, String accountId, String channel, String courseType, String status, String beginTime, String endTime, String search) {
        return hrTrainRecordMapper.getHrTrainRecordApprovedOldList(orgId, accountId, channel, courseType, status, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param channel
     * @param courseType
     * @param status
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrTrainRecordApprovedOldList
     * @Description:  获取历史审批记录
     */
    public PageInfo<Map<String, String>> getHrTrainRecordApprovedOldList(PageParam pageParam, String channel, String courseType, String status, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrTrainRecordApprovedOldList(pageParam.getOrgId(), pageParam.getAccountId(), channel, courseType, status, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
