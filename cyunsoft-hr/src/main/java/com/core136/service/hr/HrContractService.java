package com.core136.service.hr;

import com.core136.bean.account.Account;
import com.core136.bean.hr.HrContract;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.ExcelUtil;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrContractMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class HrContractService {
    private HrContractMapper hrContractMapper;

    @Autowired
    public void setHrContractMapper(HrContractMapper hrContractMapper) {
        this.hrContractMapper = hrContractMapper;
    }

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int insertHrContract(HrContract hrContract) {
        return hrContractMapper.insert(hrContract);
    }

    public int deleteHrContract(HrContract hrContract) {
        return hrContractMapper.delete(hrContract);
    }

    public int updateHrContract(Example example, HrContract hrContract) {
        return hrContractMapper.updateByExampleSelective(hrContract, example);
    }

    public HrContract selectOneHrContract(HrContract hrContract) {
        return hrContractMapper.selectOne(hrContract);
    }

    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param enterpries
     * @param contractType
     * @return List<Map < String, String>>
     * @Title: getHrContractList
     * @Description:  获取合同列表
     */
    public List<Map<String, String>> getHrContractList(String orgId, String userId, String beginTime, String endTime, String enterpries, String contractType) {
        return hrContractMapper.getHrContractList(orgId, userId, beginTime, endTime, enterpries, contractType);
    }

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getMyHrContractList
     * @Description:  查询自己的合同列表
     */
    public List<Map<String, String>> getMyHrContractList(String orgId, String accountId) {
        return hrContractMapper.getMyHrContractList(orgId, accountId);
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param enterpries
     * @param contractType
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrContractList
     * @Description:  获取合同列表
     */
    public PageInfo<Map<String, String>> getHrContractList(PageParam pageParam, String userId, String beginTime, String endTime, String enterpries, String contractType) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrContractList(pageParam.getOrgId(), userId, beginTime, endTime, enterpries, contractType);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyHrContractList
     * @Description:  查询自己的合同列表
     */
    public PageInfo<Map<String, String>> getMyHrContractList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyHrContractList(pageParam.getOrgId(), pageParam.getAccountId());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param account
     * @param file
     * @return IOException
     * RetDataBean
     * @Title: importHrContract
     * @Description:  人事合同导入
     */
    @Transactional(value = "generalTM")
    public RetDataBean importHrContract(Account account, MultipartFile file) throws IOException {
        Map<String, String> fieldMap = new HashMap<String, String>();
        fieldMap.put("排序号", "sort_no");
        fieldMap.put("应聘岗位", "pool_position");
        fieldMap.put("合同编号", "contract_code");
        fieldMap.put("合同类型", "contract_type");
        fieldMap.put("签约公司", "enterpries");
        fieldMap.put("签约方式", "sign_type");
        fieldMap.put("关联档案人员", "user_id");
        fieldMap.put("雇佣者姓名", "user_name");
        fieldMap.put("合同生效时间", "start_time");
        fieldMap.put("合同终止时间", "end_time");
        fieldMap.put("合同期限属性", "specialization");
        fieldMap.put("合同签订时间", "sign_time");
        fieldMap.put("备注", "remark");
        List<String> fieldList = new ArrayList<String>();
        List<String> titleList = new ArrayList<String>();
        for (Map.Entry<String, String> entry : fieldMap.entrySet()) {
            fieldList.add(entry.getValue());
            titleList.add(entry.getKey());
        }
        String[] fieldArr = new String[fieldList.size()];
        fieldList.toArray(fieldArr);
        String fieldString = StringUtils.join(fieldArr, ",");
        List<Map<String, String>> recordList = ExcelUtil.readExcel(file);
        for (int i = 0; i < recordList.size(); i++) {
            Map<String, String> tempMap = recordList.get(i);
            String valueString = "'" + SysTools.getGUID() + "',";
            for (int k = 0; k < titleList.size(); k++) {
                if (titleList.get(k).equals("关联档案人员")) {
                    if (StringUtils.isNotBlank(tempMap.get(titleList.get(k)))) {
                        String sql1 = "select user_id from hr_user_info where user_name ='" + tempMap.get(titleList.get(k)) + "' and org_id='" + account.getOrgId() + "'";
                        String userId = jdbcTemplate.queryForObject(sql1, String.class);
                        valueString += "'" + userId + "',";
                    } else {
                        valueString += "'',";
                    }
                } else {
                    valueString += "'" + tempMap.get(titleList.get(k)) + "',";
                }
            }
            valueString += "'" + SysTools.getTime("yyyy-MM-dd HH:mm:ss") + "',";
            valueString += "'" + account.getAccountId() + "',";
            valueString += "'" + account.getOrgId() + "'";
            String insertSql = "insert into hr_contract(contract_id," + fieldString + ",create_time,create_user,org_id) values" + "(" + valueString + ")";
            jdbcTemplate.execute(insertSql);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }


    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getDeskHrContractList
     * @Description:  获取快到期的合同列表
     */
    public List<Map<String, String>> getDeskHrContractList(String orgId) {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        Date d = new Date();
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(d);
        gc.set(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH), gc.get(Calendar.DATE));
        gc.add(2, 1);
        String endTime = sf.format(gc.getTime());
        String beginTime = SysTools.getTime("yyyy-MM-dd");
        return hrContractMapper.getDeskHrContractList(orgId, beginTime, endTime);
    }

}
