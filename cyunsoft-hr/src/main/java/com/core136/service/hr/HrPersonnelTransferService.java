package com.core136.service.hr;

import com.core136.bean.hr.HrPersonnelTransfer;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.hr.HrPersonnelTransferMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class HrPersonnelTransferService {
    private HrPersonnelTransferMapper hrPersonnelTransferMapper;

    @Autowired
    public void setHrPersonnelTransferMapper(HrPersonnelTransferMapper hrPersonnelTransferMapper) {
        this.hrPersonnelTransferMapper = hrPersonnelTransferMapper;
    }

    public int insertHrPersonnelTransfer(HrPersonnelTransfer hrPersonnelTransfer) {
        return hrPersonnelTransferMapper.insert(hrPersonnelTransfer);
    }

    public int deleteHrPersonnelTransfer(HrPersonnelTransfer hrPersonnelTransfer) {
        return hrPersonnelTransferMapper.delete(hrPersonnelTransfer);
    }

    public int updateHrPersonnelTransfer(Example example, HrPersonnelTransfer hrPersonnelTransfer) {
        return hrPersonnelTransferMapper.updateByExampleSelective(hrPersonnelTransfer, example);
    }

    public HrPersonnelTransfer selectOneHrPersonnelTransfer(HrPersonnelTransfer hrPersonnelTransfer) {
        return hrPersonnelTransferMapper.selectOne(hrPersonnelTransfer);
    }

    /**
     * @param orgId
     * @param userId
     * @param beginTime
     * @param endTime
     * @param transferType
     * @param search
     * @return List<Map < String, String>>
     * @Title: getHrPersonnelTransferList
     * @Description:  获取人员调动列表
     */
    public List<Map<String, String>> getHrPersonnelTransferList(String orgId, String userId, String beginTime, String endTime, String transferType, String search) {
        return hrPersonnelTransferMapper.getHrPersonnelTransferList(orgId, userId, beginTime, endTime, transferType, "%" + search + "%");
    }

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getMyHrPersonnelTransferList
     * @Description:  人个工作调动记录
     */
    public List<Map<String, String>> getMyHrPersonnelTransferList(String orgId, String accountId) {
        return hrPersonnelTransferMapper.getMyHrPersonnelTransferList(orgId, accountId);
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getMyHrPersonnelTransferList
     * @Description:  人个工作调动记录
     */
    public PageInfo<Map<String, String>> getMyHrPersonnelTransferList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyHrPersonnelTransferList(pageParam.getOrgId(), pageParam.getAccountId());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param pageParam
     * @param userId
     * @param beginTime
     * @param endTime
     * @param transferType
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getHrPersonnelTransferList
     * @Description:  获取人员调动列表
     */
    public PageInfo<Map<String, String>> getHrPersonnelTransferList(PageParam pageParam, String userId, String beginTime, String endTime, String transferType) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getHrPersonnelTransferList(pageParam.getOrgId(), userId, beginTime, endTime, transferType, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
