$(function () {
    $.ajax({
        url: "/ret/safetyget/getSafetyTeamById",
        type: "post",
        dataType: "json",
        data: {
            teamId: teamId
        },
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let id in info) {
                   if (id == "teamType") {
                        $("#teamType").html(getSafetyCodeClassName(info[id], "teamType"));
                    } else if (id == "entId") {
                        $.ajax({
                            url: "/ret/safetyget/getSafetyEntInfoById",
                            type: "post",
                            dataType: "json",
                            data: {entId: info[id]},
                            success: function (res) {
                                let entInfo = res.list;
                                if (entInfo) {
                                    $("#entId").html(entInfo.entName);
                                }
                            }
                        });
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
