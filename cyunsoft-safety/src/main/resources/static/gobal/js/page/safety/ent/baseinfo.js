$(function () {
    jeDate("#incorporation", {
        format: "YYYY-MM-DD"
    });
    jeDate("#businessTerm", {
        format: "YYYY-MM-DD"
    });
    $('#businessScope').summernote({height: 200});
    $(".js-add-save").unbind("click").click(function () {
        createEnt();
    })
})

function createEnt() {
    if($("#entName").val()=="")
    {
        layer.msg("企业各称不能为空！");
        return;
    }
    $.ajax({
        url: "/set/safetyset/insertSafetyEntInfo",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            type: $("#type").val(),
            entName: $("#entName").val(),
            oldEntName: $("#oldEntName").val(),
            capital: $("#capital").val(),
            tel: $("#tel").val(),
            incorporation: $("#incorporation").val(),
            businessTerm: $("#businessTerm").val(),
            credit: $("#credit").val(),
            legalPerson: $("#legalPerson").val(),
            address: $("#address").val(),
            businessScope: $("#businessScope").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
