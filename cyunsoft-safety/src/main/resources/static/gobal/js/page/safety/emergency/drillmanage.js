$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD hh:mm"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD hh:mm"
    });
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    getSafetyCodeClass("drillType", "drillType");
    getSafetyCodeClass("drillTypeQuery", "drillType");
    $('#remark').summernote({height: 200});
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/safetyget/getSafetyDrillList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        sortOrder: "desc",
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'drillId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: sysmsg['SORT_NO'],//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'title',
                title: '演练标题',
                width: '200px',
                formatter: function (value, row, index) {
                    return "<a style=\"cursor: pointer;\" href=\"javascript:void(0);details('" + row.drillId + "');\">" + value + "</a>";
                }
            },
            {
                field: 'drillType',
                title: '演练类型',
                width: '50px',
                formatter: function (value, row, index) {
                    return getSafetyCodeClassName(value, "drillType");
                }
            }, {
                field: 'sponsor',
                title: '主办方',
                width: '150px'
            },
            {
                field: 'beginTime',
                width: '200px',
                title: '演练时间',
                formatter: function (value, row, index) {
                    return value+"至"+row.endTime;
                }
            },
            {
                field: 'createTime',
                width: '100px',
                title: '创建时间'
            },
            {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '100px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.drillId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        type: $("#typeStatus").val()
    };
    return temp;
}

function createOptBtn(drillId) {
    let html = "<a href=\"javascript:void(0);edit('" + drillId + "')\" class=\"btn btn-primary btn-xs\" >编辑</a>&nbsp;&nbsp;";
    html += "<a href=\"javascript:void(0);deleteDrill('" + drillId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function edit(drillId) {
    $("#listdiv").hide();
    $("#drilldiv").show();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    document.getElementById("form1").reset();
    $("#remark").code("")
    $("#attach").attr("data_value", "");
    $("#show_attach").empty();
    $.ajax({
        url: "/ret/safetyget/getSafetyDrillById",
        type: "post",
        dataType: "json",
        data: {
            drillId: drillId
        },
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 4);
                    } else if (id == "remark") {
                        $("#remark").code(info[id]);
                    } else {
                        $("#" + id).val(info[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updateDrill(drillId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })

}

function goback() {
    $("#drilldiv").hide();
    $("#listdiv").show();
}


function updateDrill(drillId) {
    if($("#title").val()=="")
    {
        layer.msg("演练标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/safetyset/updateSafetyDrill",
        type: "post",
        dataType: "json",
        data: {
            drillId: drillId,
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            drillType: $("#drillType").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            sponsor: $("#sponsor").val(),
            address: $("#address").val(),
            remark: $("#remark").code(),
            joinOrg: $("#joinOrg").val(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $('#myTable').bootstrapTable('refresh');
                goback();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function deleteDrill(drillId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/safetyset/deleteSafetyDrill",
            type: "post",
            dataType: "json",
            data: {drillId: drillId},
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $('#myTable').bootstrapTable('refresh');
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else if (data.status == "500") {
                    console.log(data.msg);
                }
            }
        })
    }
}

function details(drillId) {
    window.open("/app/core/safety/drilldetails?drillId=" + drillId);
}
