$(function () {
    $.ajax({
        url: "/ret/safetyget/getSafetyTrainById",
        type: "post",
        dataType: "json",
        data: {
            trainId: trainId
        },
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 1);
                    } else if (id == "trainType") {
                        $("#trainType").html(getSafetyCodeClassName(info[id], "trainType"));
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
