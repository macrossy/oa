$(function () {
    $('#remark').summernote({height: 200});
    getSafetyCodeClass("trainTypeQuery", "trainType");
    getSafetyCodeClass("trainType", "trainType");
    query();
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/safetyget/getSafetyTrainList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        sortOrder: "desc",
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'trainId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: sysmsg['SORT_NO'],//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'title',
                title: '培训标题',
                //sortable : true,
                width: '150px',
                formatter: function (value, row, index) {
                    return "<a style=\"cursor: pointer;\" href=\"javascript:void(0);details('" + row.trainId + "');\">" + value + "</a>";
                }
            },{
                field: 'subheading',
                title: '培训摘要',
                width: '150px'
            },
            {
                field: 'trainType',
                title: '培训类型',
                width: '50px',
                formatter: function (value, row, index) {
                    return getSafetyCodeClassName(value, "trainType");
                }
            },
            {
                field: 'sponsor',
                title: '主办方',
                width: '100px'
            },
            {
                field: 'linkMan',
                width: '100px',
                title: '联系人'
            },
            {
                field: 'beginTime',
                width: '150px',
                title: '培训时间',
                formatter: function (value, row, index) {
                    return value+"至"+row.endTime;
                }
            },
            {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '100px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.trainId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        trainType: $("#trainTypeQuery").val(),
        beginTime: $("#beginTimeQuery").val(),
        endTime: $("#endTimeQuery").val()
    };
    return temp;
}

function createOptBtn(trainId) {
    let html = "<a href=\"javascript:void(0);edit('" + trainId + "')\" class=\"btn btn-primary btn-xs\" >编辑</a>&nbsp;&nbsp;";
    html += "<a href=\"javascript:void(0);deleteTrain('" + trainId + "')\" class=\"btn btn-darkorange btn-xs\" >删除</a>";
    return html;
}

function edit(trainId) {
    $("#listdiv").hide();
    $("#traindiv").show();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    document.getElementById("form1").reset();
    $("#remark").code("")
    $("#attach").attr("data_value", "");
    $("#show_attach").empty();
    $.ajax({
        url: "/ret/safetyget/getSafetyTrainById",
        type: "post",
        dataType: "json",
        data: {
            trainId: trainId
        },
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 4);
                    } else if (id == "remark") {
                        $("#remark").code(info[id]);
                    }else {
                        $("#" + id).val(info[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updateTrain(trainId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })

}

function goback() {
    $("#traindiv").hide();
    $("#listdiv").show();
}


function updateTrain(trainId) {
    if($("#title").val()=="")
    {
        layer.msg("培训标题不能为空！");
        return;
    }
    $.ajax({
        url: "/set/safetyset/updateSafetyTrain",
        type: "post",
        dataType: "json",
        data: {
            trainId: trainId,
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            trainType: $("#trainType").val(),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            joinUser: $("#joinUser").val(),
            entName: $("#entName").val(),
            sponsor: $("#sponsor").val(),
            linkMan :$("#linkMan").val(),
            tel:$("#tel").val(),
            address: $("#address").val(),
            remark: $("#remark").code(),
            attach: $("#attach").attr("data_value")
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                $('#myTable').bootstrapTable('refresh');
                goback();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function deleteTrain(trainId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/safetyset/deleteSafetyTrain",
            type: "post",
            dataType: "json",
            data: {trainId: trainId},
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $('#myTable').bootstrapTable('refresh');
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else if (data.status == "500") {
                    console.log(data.msg);
                }
            }
        })
    }
}

function details(trainId) {
    window.open("/app/core/safety/traildetails?trainId=" + trainId);
}
