package com.core136.mapper.safety;

import com.core136.bean.safety.SafetyRegulator;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SafetyRegulatorMapper extends MyMapper<SafetyRegulator> {
    /**
     *获取监管机构列表
     * @param orgId
     * @param safetyType
     * @param search
     * @return
     */
    List<Map<String,String>> getSafetyRegulatorList(@Param(value = "orgId") String orgId, @Param(value = "safetyType") String safetyType, @Param(value = "search") String search);
}
