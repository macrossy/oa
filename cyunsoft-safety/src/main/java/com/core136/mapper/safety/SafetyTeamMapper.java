package com.core136.mapper.safety;

import com.core136.bean.safety.SafetyTeam;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SafetyTeamMapper extends MyMapper<SafetyTeam> {
    /**
     * 获取应急小组列表
     * @param orgId
     * @param teamType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String,String>> getSafetyTeamList(@Param(value="orgId")String orgId, @Param(value="teamType")String teamType,
                                                      @Param(value="beginTime")String beginTime, @Param(value="endTime")String endTime, @Param(value="search")String search);
}
