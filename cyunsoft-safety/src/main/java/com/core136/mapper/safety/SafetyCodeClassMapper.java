package com.core136.mapper.safety;

import com.core136.bean.safety.SafetyCodeClass;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SafetyCodeClassMapper extends MyMapper<SafetyCodeClass> {

    public List<Map<String, Object>> getCodeClassByModule(@Param(value = "orgId") String orgId, @Param(value = "module") String module);
}
