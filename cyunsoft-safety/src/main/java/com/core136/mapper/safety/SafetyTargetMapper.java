package com.core136.mapper.safety;

import com.core136.bean.safety.SafetyTarget;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SafetyTargetMapper extends MyMapper<SafetyTarget> {
    /**
     * 获取管理目标列表
     * @param orgId
     * @param targetYear
     * @param search
     * @return
     */
    List<Map<String,String>> getSafetyTargetList(@Param(value = "orgId") String orgId, @Param(value = "targetSort") String targetSort,
                                                 @Param(value = "targetYear") String targetYear, @Param(value = "search") String search);
}
