package com.core136.mapper.safety;

import com.core136.bean.safety.SafetyEvent;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SafetyEventMapper extends MyMapper<SafetyEvent> {
    /**
     * 获取重大事件列表
     * @param orgId
     * @param eventType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String,String>> getSafetyEventList(@Param(value="orgId")String orgId,@Param(value="eventType")String eventType,
                                                      @Param(value="beginTime")String beginTime,@Param(value="endTime")String endTime,@Param(value="search")String search);
}
