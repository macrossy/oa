package com.core136.mapper.safety;

import com.core136.bean.safety.SafetyEntMember;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SafetyEntMemberMapper extends MyMapper<SafetyEntMember> {

    /**
     * 获取企业人员列表
     *
     * @param orgId
     * @param post
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getEntMemberList(@Param(value = "orgId") String orgId, @Param(value = "post") String post,
                                                      @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

}
