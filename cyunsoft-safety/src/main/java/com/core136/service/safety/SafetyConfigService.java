package com.core136.service.safety;

import com.core136.bean.account.Account;
import com.core136.bean.safety.SafetyConfig;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.safety.SafetyConfigMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class SafetyConfigService {
    private SafetyConfigMapper safetyConfigMapper;

    @Autowired
    public void setSafetyConfigMapper(SafetyConfigMapper safetyConfigMapper) {
        this.safetyConfigMapper = safetyConfigMapper;
    }

    public int insertSafetyConfig(SafetyConfig safetyConfig) {
        return safetyConfigMapper.insert(safetyConfig);
    }

    public int deleteSafetyConfig(SafetyConfig safetyConfig) {
        return safetyConfigMapper.delete(safetyConfig);
    }

    public SafetyConfig selectOneSafetyConfig(SafetyConfig safetyConfig) {
        return safetyConfigMapper.selectOne(safetyConfig);
    }

    public int updateSafetyConfig(Example example, SafetyConfig safetyConfig) {
        return safetyConfigMapper.updateByExampleSelective(safetyConfig, safetyConfig);
    }

    public int getCount(SafetyConfig safetyConfig) {
        return safetyConfigMapper.selectCount(safetyConfig);
    }

    /**
     * 设置监管参数
     *
     * @param account
     * @param safetyConfig
     * @return
     */
    public RetDataBean setSafetyConfig(Account account, SafetyConfig safetyConfig) {
        SafetyConfig sc = new SafetyConfig();
        sc.setOrgId(safetyConfig.getOrgId());
        if (getCount(sc) > 0) {
            Example example = new Example(SafetyConfig.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, updateSafetyConfig(example, safetyConfig));
        } else {
            safetyConfig.setConfigId(SysTools.getGUID());
            safetyConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            safetyConfig.setCreateUser(account.getCreateUser());
            safetyConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, insertSafetyConfig(safetyConfig));
        }
    }

}
