package com.core136.service.safety;

import com.core136.bean.safety.SafetyDiary;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.safety.SafetyDiaryMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SafetyDiaryService {
    private SafetyDiaryMapper safetyDiaryMapper;

    @Autowired
    public void setSafetyDiaryMapper(SafetyDiaryMapper safetyDiaryMapper) {
        this.safetyDiaryMapper = safetyDiaryMapper;
    }

    public int insertSafetyDiary(SafetyDiary safetyDiary) {
        return safetyDiaryMapper.insert(safetyDiary);
    }

    public int deleteSafetyDiary(SafetyDiary safetyDiary) {
        return safetyDiaryMapper.delete(safetyDiary);
    }

    public SafetyDiary selectOneSafetyDiary(SafetyDiary safetyDiary) {
        return safetyDiaryMapper.selectOne(safetyDiary);
    }

    public int updateSafetyDiary(Example example, SafetyDiary safetyDiary) {
        return safetyDiaryMapper.updateByExampleSelective(safetyDiary, example);
    }

    /**
     * 获取安全日志列表
     * @param orgId
     * @param diaryType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String,String>> getSafetyDiaryList(String orgId,String diaryType,String beginTime,String endTime,String search)
    {
        return safetyDiaryMapper.getSafetyDiaryList(orgId,diaryType,beginTime,endTime,"%"+search+"%");
    }

    /**
     * 获取安全日志列表
     * @param pageParam
     * @param diaryType
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getSafetyDiaryList(PageParam pageParam, String diaryType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSafetyDiaryList(pageParam.getOrgId(), diaryType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
