package com.core136.service.safety;

import com.core136.bean.safety.SafetyEntMember;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.safety.SafetyEntMemberMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SafetyEntMemberService {
    private SafetyEntMemberMapper safetyEntMemberMapper;

    @Autowired
    public void setSafetyEntMemberMapper(SafetyEntMemberMapper safetyEntMemberMapper) {
        this.safetyEntMemberMapper = safetyEntMemberMapper;
    }

    public int insertSafetyEntMember(SafetyEntMember safetyEntMember) {
        return safetyEntMemberMapper.insert(safetyEntMember);
    }

    public int deleteSafetyEntMember(SafetyEntMember safetyEntMember) {
        return safetyEntMemberMapper.delete(safetyEntMember);
    }

    public SafetyEntMember selectOneSafetyEntMember(SafetyEntMember safetyEntMember) {
        return safetyEntMemberMapper.selectOne(safetyEntMember);
    }

    public int updateSafetyEntMember(Example example, SafetyEntMember safetyEntMember) {
        return safetyEntMemberMapper.updateByExampleSelective(safetyEntMember, example);
    }

    /**
     * 获取企业人员列表
     *
     * @param orgId
     * @param post
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getEntMemberList(String orgId, String post, String beginTime, String endTime, String search) {
        return safetyEntMemberMapper.getEntMemberList(orgId, post, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取企业人员列表
     *
     * @param pageParam
     * @param post
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getEntMemberList(PageParam pageParam, String post, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getEntMemberList(pageParam.getOrgId(), post, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
