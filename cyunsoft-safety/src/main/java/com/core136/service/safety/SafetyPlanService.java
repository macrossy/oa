package com.core136.service.safety;

import com.core136.bean.safety.SafetyPlan;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.safety.SafetyPlanMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SafetyPlanService {
    private SafetyPlanMapper safetyPlanMapper;
    @Autowired
    public void setSafetyPlanMapper(SafetyPlanMapper safetyPlanMapper)
    {
        this.safetyPlanMapper = safetyPlanMapper;
    }

    public int insertSafetyPlan(SafetyPlan safetyPlan)
    {
        return safetyPlanMapper.insert(safetyPlan);
    }

    public int deleteSafetyPlan(SafetyPlan safetyPlan)
    {
        return safetyPlanMapper.delete(safetyPlan);
    }

    public SafetyPlan selectOneSafetyPlan(SafetyPlan safetyPlan)
    {
        return safetyPlanMapper.selectOne(safetyPlan);
    }

    public int updateSafetyPlan(Example example,SafetyPlan safetyPlan)
    {
        return safetyPlanMapper.updateByExampleSelective(safetyPlan,example);
    }

    /**
     * 获取应急方案
     * @param orgId
     * @param planType
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String,String>> getSafetyPlanList(String orgId, String planType, String beginTime, String endTime, String search)
    {
        return safetyPlanMapper.getSafetyPlanList(orgId,planType,beginTime,endTime,"%"+search+"%");
    }

    /**
     * 获取应急方案
     * @param pageParam
     * @param planType
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getSafetyPlanList(PageParam pageParam, String planType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSafetyPlanList(pageParam.getOrgId(), planType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
