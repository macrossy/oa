package com.core136.service.safety;

import com.core136.bean.safety.SafetyTarget;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.safety.SafetyTargetMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SafetyTargetService {
    private SafetyTargetMapper safetyTargetMapper;
    @Autowired
    public void setSafetyTargetMapper(SafetyTargetMapper safetyTargetMapper)
    {
        this.safetyTargetMapper = safetyTargetMapper;
    }

    public int insertSafetyTarget(SafetyTarget safetyTarget)
    {
        return safetyTargetMapper.insert(safetyTarget);
    }

    public int deleteSafetyTarget(SafetyTarget safetyTarget)
    {
        return safetyTargetMapper.delete(safetyTarget);
    }

    public SafetyTarget selectOneSafetyTarget(SafetyTarget safetyTarget)
    {
        return safetyTargetMapper.selectOne(safetyTarget);
    }

    public int updateSafetyTarget(Example example,SafetyTarget safetyTarget)
    {
        return safetyTargetMapper.updateByExampleSelective(safetyTarget,example);
    }

    /**
     * 获取管理目标列表
     * @param orgId
     * @param targetSort
     * @param targetYear
     * @param search
     * @return
     */
    public List<Map<String,String>>getSafetyTargetList(String orgId,String targetSort,String targetYear,String search)
    {
        return safetyTargetMapper.getSafetyTargetList(orgId,targetSort,targetYear,"%"+search+"%");
    }

    /**
     * 获取管理目标列表
     * @param pageParam
     * @param targetSort
     * @param targetYear
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getSafetyTargetList(PageParam pageParam, String targetSort, String targetYear) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSafetyTargetList(pageParam.getOrgId(), targetSort, targetYear, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
