package com.core136.service.safety;

import com.core136.bean.safety.SafetyTeamMember;
import com.core136.mapper.safety.SafetyTeamMemberMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class SafetyTeamMemberService {
    private SafetyTeamMemberMapper safetyTeamMemberMapper;
    @Autowired
    public void setSafetyTeamMemberMapper(SafetyTeamMemberMapper safetyTeamMemberMapper)
    {
        this.safetyTeamMemberMapper = safetyTeamMemberMapper;
    }

    public int insertSafetyTeamMember(SafetyTeamMember safetyTeamMember)
    {
        return safetyTeamMemberMapper.insert(safetyTeamMember);
    }

    public int deleteSafetyTeamMember(SafetyTeamMember safetyTeamMember)
    {
        return safetyTeamMemberMapper.delete(safetyTeamMember);
    }

    public SafetyTeamMember selectOneSafetyTeamMember(SafetyTeamMember safetyTeamMember)
    {
        return safetyTeamMemberMapper.selectOne(safetyTeamMember);
    }

    public int updateSafetyTeamMember(Example example,SafetyTeamMember safetyTeamMember)
    {
        return safetyTeamMemberMapper.updateByExampleSelective(safetyTeamMember,example);
    }


}
