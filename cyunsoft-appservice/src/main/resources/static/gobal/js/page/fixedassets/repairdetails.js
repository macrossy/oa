$(function () {
    $.ajax({
        url: "/ret/fixedassetsget/getFixedAssetsRepairById",
        type: "post",
        dataType: "json",
        data: {repairId: repairId},
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "applicant" || id == "fixedUser") {
                        $("#" + id).html(getUserNameByStr(recordInfo[id]));
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
