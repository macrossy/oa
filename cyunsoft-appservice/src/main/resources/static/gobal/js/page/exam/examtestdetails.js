$(function () {
    $.ajax({
        url: "/ret/examget/getExamTestById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let id in info) {
                    if (id == "attach") {
                        $("#show_attach").html("");
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 1);
                    } else if (id == "deptId") {
                        $("#" + id).html(getDeptNameByDeptIds(info[id]));
                    } else if (id == "userPriv") {
                        $("#" + id).html(getUserNameByStr(info[id]));
                    } else if (id == "deptPriv") {
                        $("#" + id).html(getDeptNameByDeptIds(info[id]));
                    } else if (id == "levelPriv") {
                        $("#" + id).html(getUserLevelStr(info[id]));
                    } else if (id == "duration" || id == "passMark") {
                        $("#" + id).html(info[id] + "分");
                    } else if (id == "isOpen") {
                        if (info.isOpen == "1") {
                            $("#isOpen").html("开卷");
                        } else {
                            $("#isOpen").html("闭卷");
                        }
                    } else if (id == "questionRecordId") {
                        $.ajax({
                            url: "/ret/examget/getExamQuestionRecordById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {
                                recordId: info[id]
                            },
                            success: function (res) {
                                if (data.status == "200") {
                                    $("#" + id).html("《" + res.list.title + "》");
                                } else if (res.status == "100") {
                                    layer.msg(sysmsg[res.msg]);
                                } else {
                                    console.log(res.msg);
                                }
                            }
                        });
                    } else if (id == "sortId") {
                        $.ajax({
                            url: "/ret/examget/getExamTestSortById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {
                                sortId: info[id]
                            },
                            success: function (res) {
                                if (data.status == "200") {
                                    $("#" + id).html(res.list.sortName);
                                } else if (res.status == "100") {
                                    layer.msg(sysmsg[res.msg]);
                                } else {
                                    console.log(res.msg);
                                }

                            }
                        });
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})
