let ue = UE.getEditor("remark");
$(function () {
    $(".js-questions-item").unbind("click").click(function () {
        $("#questionsModal").modal("show");
    })
    $.ajax({
        url: "/ret/examget/getexamSortTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            $.fn.zTree.init($("#menuTree"), setting1, data);
        }
    });
    $("#sortId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $("#menuContent").hide();
    });

    $("#menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    $("#questionId").unbind("change").change(function () {
        let recordId = $(this).val();
        if (recordId != "") {
            $.ajax({
                url: "/ret/examget/getExamQuestionsById",
                type: "post",
                dataType: "json",
                data: {
                    recordId: recordId
                },
                success: function (data) {
                    if (data.status == "200") {
                        $("#questionTitle").html("试题标题：" + data.list.title + "<span style='float: right;margin-right: 10px;color: red;'>" + data.list.grade + "分</span>");
                        if (data.list.examType != "2") {
                            $("#content").show();
                            let childItemArr = JSON.parse(data.list.content);
                            createChildItem(childItemArr);
                        } else {
                            $("#content").hide();
                        }
                    } else if (data.status) {
                        layer.msg(sysmsg[data.msg]);
                    } else {
                        console.log(data.msg);
                    }
                }
            });
        } else {
            $("#questionTitle").html("");
            $("#child-item-table").html("");
        }
        $(".add-question").unbind("click").click(function () {
            let recordId = $("#questionId").val();
            $.ajax({
                url: "/ret/examget/getExamQuestionsById",
                type: "post",
                dataType: "json",
                data: {
                    recordId: recordId
                },
                success: function (data) {
                    if (data.status == "200") {
                        let index = $("#questionlist").find("tr").length;
                        let examType = "";
                        if (data.list.examType == "0") {
                            examType = "单选";
                        } else if (data.list.examType == "1") {
                            examType = "多选";
                        } else if (data.list.examType == "2") {
                            examType = "简述";
                        }
                        if (data.list.examType == "2") {
                            $("#questionlist").append("<tr data-value='" + recordId + "'><td>" + index + "</td><td>" + examType + "</td><td colspan='2'>" + data.list.title + "</td>" +
                                "<td><a onclick=\"deleteChildItem(this)\" class='btn btn-darkorange btn-xs'>删除</a>&nbsp;&nbsp;<a onclick=\"toup(this)\" class='btn btn-darkorange btn-xs'>上移</a>&nbsp;&nbsp;<a onclick=\"todown(this)\" class='btn btn-darkorange btn-xs'>下移</a></td></tr>");
                        } else {
                            let html = "";
                            let childItemArr = JSON.parse(data.list.content);
                            for (let i = 0; i < childItemArr.length; i++) {
                                html += childItemArr[i].optValue + "." + childItemArr[i].childTitle + "</br>";
                            }
                            $("#questionlist").append("<tr data-value='" + recordId + "'><td>" + index + "</td><td>" + examType + "</td><td>" + data.list.title + "</td>" +
                                "<td>" + html + "</td><td><a onclick=\"deleteChildItem(this)\" class='btn btn-darkorange btn-xs'>删除</a>&nbsp;&nbsp;<a onclick=\"toup(this)\" class='btn btn-darkorange btn-xs'>上移</a>&nbsp;&nbsp;<a onclick=\"todown(this)\" class='btn btn-darkorange btn-xs'>下移</a></td></tr>");
                        }

                    } else if (data.status == "100") {
                        layer.msg(sysmsg[data.msg]);
                    } else {
                        console.log(data.msg);
                    }
                    $("#questionsModal").modal("hide");
                }
            });

        })
    })
    $(".js-add-save").unbind("click").click(function () {
        insertExamQuestionsRecord();
    })
})


function insertExamQuestionsRecord() {
    $.ajax({
        url: "/set/examset/insertExamQuestionRecord",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            questionIds: getParam(),
            remark: ue.getContent()
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            }
        }
    })
}

function getParam() {
    let paramArr = [];
    $("#questionlist").find("tr").next().each(function () {
        let recordId = $(this).attr("data-value");
        paramArr.push(recordId);
    });
    return paramArr.join(",");
}

let setting1 = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/examget/getexamSortTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            let zTree = $.fn.zTree.getZTreeObj("menuTree"), nodes = zTree.getSelectedNodes(), v = "";
            let idem = $("#sortId");
            idem.attr("data-value", treeNode.sortId);
            idem.val(treeNode.sortName);
            getQuestionsList(treeNode.sortId)
        }
    }
};

function getQuestionsList(sortId) {
    $.ajax({
        url: "/ret/examget/getExamQuestListForSelectBySortId",
        type: "post",
        dataType: "json",
        data: {
            sortId: sortId
        },
        success: function (data) {
            if (data.status == "200") {
                let html = "<option value=''>请选择</option>";
                let infoList = data.list;
                for (let i = 0; i < infoList.length; i++) {
                    let examType = "";
                    if (infoList[i].examType == "1") {
                        examType = "单选";
                    } else if (infoList[i].examType == "2") {
                        examType = "多选";
                    } else {
                        examType = "简述";
                    }
                    html += "<option value='" + infoList[i].recordId + "'>【" + examType + "】" + infoList[i].title + "--分值：" + infoList[i].grade + "</option>";
                }
                $("#questionId").html(html);
            } else if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }

        }
    });
}

function createChildItem(childItemArr) {
    let html = "";
    for (let i = 0; i < childItemArr.length; i++) {
        html += "<tr><td>" + childItemArr[i].optValue + "</td>" +
            "<td>" + childItemArr[i].childTitle + "</td></tr>";
    }
    $("#child-item-table").html(html);
}

function deleteChildItem(Obj) {
    $(Obj).parent("td").parent("tr").remove();
}

function toup(Obj) {
    var before = $(Obj).parent().parent().prev();
    $(Obj).parent().parent().after(before);
}

function todown(Obj) {
    var next = $(Obj).parent().parent().next();
    $(Obj).parent().parent().before(next);
}
