$(function () {
    getAllSuperversion();
    getAllFinishSuperversionForDesk();
    getAllSuperversionProcessForDesk();
    getBiSuperversionStatusTypePie();
    getBiSuperversionByMonthLine();
})

function getAllSuperversionProcessForDesk() {
    $.ajax({
        url: "/ret/superversionget/getAllSuperversionProcessForDesk",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                let info = data.list;
                let html = "";
                for (let i = 0; i < info.length; i++) {
                    let title = info[i].title;
                    if (title.length > 30) {
                        title = title.substring(0, 30) + "...";
                    }
                    html += "<tr><td><span title='" + info[i].title + "'>【" + info[i].changeUesrName + "】<a href=\"javascript:void(0);doReadSuperversionProcess('" + info[i].superversionId + "','" + info[i].processId + "')\">" + title + "</a></span><span style='float:right;'>开始时间：&nbsp;&nbsp;"
                        + info[i].endTime + "&nbsp;&nbsp;延期到：&nbsp;&nbsp;" + info[i].endTime + "</span></td></tr>";
                }
                $("#superverdealylist").html(html);
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getBiSuperversionStatusTypePie() {
    $.ajax({
        url: "/ret/echartssuperversionget/getBiSuperversionStatusTypePie",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('main1'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function getBiSuperversionByMonthLine() {
    $.ajax({
        url: "/ret/echartssuperversionget/getBiSuperversionByMonthLine",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('main4'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function getAllFinishSuperversionForDesk() {
    $.ajax({
        url: "/ret/superversionget/getAllFinishSuperversionForDesk",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                let info = data.list;
                let html = "";
                for (let i = 0; i < info.length; i++) {
                    let title = info[i].title;
                    if (title.length > 30) {
                        title = title.substring(0, 30) + "...";
                    }
                    html += "<tr><td><span title='" + info[i].title + "'><a href=\"javascript:void(0);doReadSuperversion('" + info[i].superversionId + "')\">" + title + "</a>【" + info[i].typeName + "】</span><span style='float:right;'>开始时间：&nbsp;&nbsp;"
                        + info[i].beginTime + "&nbsp;&nbsp;完成时间：&nbsp;&nbsp;" + info[i].finishTime + "</span></td></tr>";
                }
                $("#finishSuperversion").html(html);
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getAllSuperversion() {
    $.ajax({
        url: "/ret/superversionget/getAllSuperversionForDesk",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                let info = data.list;
                let html = "";
                for (let i = 0; i < info.length; i++) {
                    let title = info[i].title;
                    if (title.length > 30) {
                        title = title.substring(0, 30) + "...";
                    }
                    html += "<tr>" +
                        "<td>" + (i + 1) + "</td>" +
                        "<td class='td_inline'><a href=\"javascript:void(0);doReadSuperversion('" + info[i].superversionId + "')\">" + title + "</a></td>" +
                        "<td>" + info[i].typeName + "</td>" +
                        "<td>" + info[i].leadUserName + "</td>" +
                        "<td>" + info[i].handedUserName + "</td>" +
                        "<td>" + info[i].beginTime + "</td>" +
                        "<td>" + info[i].endTime + "</td>";
                    if (info[i].status == "0") {
                        html += "<td>进行中</td>";
                    } else if (info[i].status == "1") {
                        html += "<td>延时</td>";
                    } else if (info[i].status == "2") {
                        html += "<td>完成</td>";
                    }
                    html += "</tr>";
                }
                $("#superversionbody").html(html);
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function doReadSuperversion(superversionId) {
    window.open("/app/core/superversion/superversiondetails?superversionId=" + superversionId)
}

function doReadSuperversionProcess(superversionId, processId) {
    window.open("/app/core/superversion/processdetails?processId=" + processId + "&superversionId=" + superversionId)
}
