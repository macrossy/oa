let oamsg = {
    'TITLE':'日誌標題',
    'DIARY_TYPE':'日誌類型',
    'DIARY_DATE':'指定日期',
    'DIARY_SUB_NAME':'下属姓名',
    'DIARY_PERSONAL':'個人工作日誌',
    'DIARY_DEPT':'部門工作匯報',
}