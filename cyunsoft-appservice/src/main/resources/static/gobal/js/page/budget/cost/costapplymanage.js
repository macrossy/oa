let ue = UE.getEditor("remark")
$(function () {
    jeDate("#payTime", {
        format: "YYYY-MM-DD",
    });
    $.ajax({
        url: "/ret/budgetget/getProjectTreeList",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "全部分类",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#tree1"), setting, newTreeNodes);
            var topNode = [{
                sortName: "全部",
                isParent: "false",
                sortId: ""
            }];
            var pTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#ztree1"), setting2, pTreeNodes);
        }
    });
    $.ajax({
        url: "/ret/budgetget/getBudgetAccountTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "TOP分类",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#menuTree"), setting1, newTreeNodes);
        }
    });
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD",
    });
    $("#budgetAccount").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("#projectId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#projectContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("#projectIdQuery").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent1").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $(".menuContent").hide();
    });
    $(".menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    query();
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    getCostApprovalUser();
});
var setting2 = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/budgetget/getProjectTreeList",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("ztree1"),
                nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#projectId");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};
var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/budgetget/getProjectTreeList",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("tree1"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }

            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#projectIdQuery");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};

var setting1 = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/budgetget/getBudgetAccountTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("menuTree"),
                nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#budgetAccount");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/budgetget/getBudgetCostApplyList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: true,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: true,// 启用搜索
        sortOrder: "asc",
        showColumns: true,// 是否显示 内容列下拉框
        showRefresh: true,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'pTitle',
            width: '150px',
            title: '项目名称'
        }, {
            field: 'title',
            width: '150px',
            title: '预算申请标题'
        }, {
            field: 'pTotalCost',
            width: '100px',
            title: '剩余预算'
        },
            {
                field: 'cTotalCost',
                title: '申请金额',
                width: '100px'
            },
            {
                field: 'budgetAccount',
                width: '100px',
                title: '预算科目',
                formatter: function (value, row, index) {
                    return getBudgetAccountName(value);
                }
            }, {
                field: 'approvalUser',
                width: '50px',
                title: '审批人',
                formatter: function (value, row, index) {
                    return getUserNameByStr(value);
                }
            }, {
                field: 'payTime',
                title: '支出日期',
                width: '50px'
            }, {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '120px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.recordId, row.status);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        projectId: $("#projectIdQuery").attr("data-value"),
        status: $("#status").val(),
        beginTime: $("#beginTimeQuery").val(),
        endTime: $("#endTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId, status) {
    var html = "";
    if (status == "0") {
        html += "<a href=\"javascript:void(0);edit('" + recordId + "')\" class=\"btn btn-primary btn-xs\">编辑</a>&nbsp;&nbsp;" +
            "<a href=\"javascript:void(0);delectRecord('" + recordId + "')\" class=\"btn btn-darkorange btn-xs\">删除</a>&nbsp;&nbsp;";
    }
    html += "<a href=\"javascript:void(0);details('" + recordId + "')\" class=\"btn btn-sky btn-xs\" >详情</a>";
    return html;
}

function edit(recordId) {
    document.getElementById("form1").reset();
    $("#show_attach").empty();
    $("#attach").attr("data-value", "");
    $("#budgetAccount").attr("data-value", "");
    $("#applyUser").attr("data-value", "");
    $("#projectId").attr("data-value", "");
    ue.setContent("")
    $("#listdiv").hide();
    $("#creatediv").show();
    $(".js-back-btn").unbind("click").click(function () {
        goback();
    })
    $.ajax({
        url: "/ret/budgetget/getBudgetCostApplyById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var info = data.list;
                for (var id in info) {
                    if (id == "remark") {
                        ue.setContent(info[id]);
                    } else if (id == "attach") {
                        $("#show_attach").html("");
                        $("#attach").attr("data_value", info[id]);
                        createAttach("attach", 4);
                    } else if (id == "budgetAccount") {
                        $("#budgetAccount").attr("data-value", info[id]);
                        $("#budgetAccount").val(getBudgetAccountName(info[id]));
                    } else if (id == "applyUser") {
                        $("#" + id).attr("data-value", info[id]);
                        $("#" + id).val(getUserNameByStr(info[id]));
                    } else if (id == "projectId") {
                        $("#projectId").attr("data-value", info[id]);
                        $.ajax({
                            url: "/ret/budgetget/getBudgetProjectById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {projectId: info[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    $("#projectId").val(res.list.title);
                                } else if (res.status == "100") {
                                    layer.msg(res.msg)
                                } else {
                                    console.log(res.msg)
                                }
                            }
                        });
                    } else {
                        $("#" + id).val(info[id]);
                    }
                }
                $(".js-update-save").unbind("click").click(function () {
                    updateBudgetCostApply(recordId);
                })
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function updateBudgetCostApply(recordId) {
    $.ajax({
        url: "/set/budgetset/updateBudgetCostApply",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId,
            title: $("#title").val(),
            totalCost: $("#totalCost").val(),
            payTime: $("#payTime").val(),
            projectId: $("#projectId").attr("data-value"),
            budgetAccount: $("#budgetAccount").attr("data-value"),
            applyUser: $("#applyUser").attr("data-value"),
            approvalUser: $("#approvalUser").val(),
            status: '0',
            attach: $("#attach").attr("data_value"),
            remark: ue.getContent()
        },
        success: function (data) {
            if (data.status == "200") {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function delectRecord(recordId) {
    if (confirm(sysmsg['OPT_CONFIRM_DELETE'])) {
        $.ajax({
            url: "/set/budgetset/deleteBudgetCostApply",
            type: "post",
            dataType: "json",
            data: {
                recordId: recordId,
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else if (data.status == "500") {
                    console.log(data.msg);
                }
            }
        })
    }
}


function details(recordId) {
    window.open("/app/core/budget/costapplydetails?recordId=" + recordId);
}

function getBudgetAccountName(budgetAccountId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/budgetget/getBudgetAccountById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            budgetAccountId: budgetAccountId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                returnStr = data.list.title;
            }
        }
    });
    return returnStr;
}

function goback() {
    $("#creatediv").hide();
    $("#listdiv").show();
}

function getCostApprovalUser() {
    $.ajax({
        url: "/ret/budgetget/getCostApprovalUser",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var list = data.list;
                var html1 = "";
                for (var i = 0; i < list.length; i++) {
                    html1 += "<option value=\"" + list[i].accountId + "\">" + list[i].userName + "</option>"
                }
                $("#approvalUser").html(html1);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
