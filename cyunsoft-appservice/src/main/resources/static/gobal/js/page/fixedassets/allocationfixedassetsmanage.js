$(function () {
    query();
    $.ajax({
        url: "/ret/fixedassetsget/getFixedAssetSortTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            var topNode = [{
                sortName: "全部分类",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#menuTree"), setting1, newTreeNodes);
        }
    });
    $("#sortId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $("#menuContent").hide();
    });

    $("#menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    $(".js-query-but").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    });
});

var setting1 = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/fixedassetsget/getFixedAssetSortTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "sortLevel",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("menuTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            var nameem = $("#sortId");
            nameem.val(v);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            nameem.attr("data-value", vid);
        }
    }
};

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/fixedassetsget/getFixedAssetsAllocationOldList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'assetsId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'assetsName',
                title: '固定资产名称',
                sortable: true,
                width: '100px'
            },
            {
                field: 'assetsCode',
                width: '100px',
                title: '固定资产编号'
            }, {
                field: 'brand',
                title: '品牌',
                width: '50px'
            },
            {
                field: 'model',
                width: '100px',
                title: '型号'
            },
            {
                field: 'oldOwnDept',
                width: '100px',
                title: '原始部门',
                formatter: function (value, row, index) {
                    return getDeptNameByDeptIds(value);
                }
            },
            {
                field: 'newOwnDept',
                width: '100px',
                title: '调拨部门',
                formatter: function (value, row, index) {
                    return getDeptNameByDeptIds(value);
                }
            },

            {
                field: 'createUser',
                width: '50px',
                title: '调拨人',
                formatter: function (value, row, index) {
                    return getUserNameByStr(value);
                }
            }, {
                field: 'createTime',
                width: '100px',
                title: '调拨时间'
            },
            {
                field: 'opt',
                title: '操作',
                align: 'center',
                width: '100px',
                formatter: function (value, row, index) {
                    return createOptBtn(row.assetsId, row.applyId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        sortId: $("#sortId").attr("data-value"),
        deptId: $("#deptId").attr("data-value")
    };
    return temp;
};

function createOptBtn(assetsId, applyId) {
    var html = "<a href=\"javascript:void(0);readdetails('" + assetsId + "','" + applyId + "')\" class=\"btn btn-sky btn-xs\" >资产详情</a>";
    return html;
}

function readdetails(assetsId, applyId) {
    window.open("/app/core/fixedassets/detailsfixedassetsandapply?assetsId=" + assetsId + "&applyId=" + applyId);
}
