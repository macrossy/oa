let ue = UE.getEditor("remark")
$(function () {
    jeDate("#beginTime", {
        format: "YYYY-MM-DD hh:mm",
        minDate: getSysDate(),
        isinitVal: true
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD hh:mm",
        minDate: getSysDate()
    });
    $(".js-add-save").unbind("click").click(function () {
        addExamTest();
    })
    $("#sortId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $("#menuContent").hide();
    });
    $("#menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    $.ajax({
        url: "/ret/examget/getexamTestSortTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            $.fn.zTree.init($("#menuTree"), setting1, data);
        }
    });
    getExamQuestionRecordForSelect();
})

function addExamTest() {
    $.ajax({
        url: "/set/examset/insertExamTest",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            title: $("#title").val(),
            questionRecordId: $("#questionRecordId").val(),
            isOpen: getCheckBoxValue("isOpen"),
            deptId: $("#deptId").attr("data-value"),
            sortId: $("#sortId").attr("data-value"),
            userPriv: $("#userPriv").attr("data-value"),
            deptPriv: $("#userPriv").attr("data-value"),
            levelPriv: $("#levelPriv").attr("data-value"),
            beginTime: $("#beginTime").val(),
            endTime: $("#endTime").val(),
            duration: $("#duration").val(),
            passMark: $("#passMark").val(),
            attach: $("#attach").attr("data_value"),
            remark: ue.getContent()
        },
        success: function (data) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else {
                console.log(data.msg);
            }
        }
    });
}

let setting1 = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/examget/getexamTestSortTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("menuTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#sortId");
            idem.attr("data-value", vid);
            idem.val(v);
        }
    }
};

function getExamQuestionRecordForSelect() {
    $.ajax({
        url: "/ret/examget/getExamQuestionRecordForSelect",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                for (let i = 0; i < data.list.length; i++) {
                    $("#questionRecordId").append("<option value='" + data.list[i].recordId + "'>" + data.list[i].title + "</option>")
                }
            } else if (data.status == "500") {
                console.log(data.msg);
            } else {
                layer.msg(sysmsg[data.msg]);
            }
        }
    });
}
