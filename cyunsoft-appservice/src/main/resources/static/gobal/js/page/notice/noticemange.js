$(function () {
    query();
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    getCodeClass("noticeType", "notice");
    $(".js-query-but").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    });
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/noticeget/getNoticeManageList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        sortOrder: "desc",
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'noticeId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: sysmsg['SORT_NO'],//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'noticeTitle',
                title: noticemsg['NOTICE_TITLE'],
                width: '200px',
                formatter: function (value, row, index) {
                    return "<a href=\"javascript:void(0);readNotice('" + row.noticeId + "')\">" + value + "</a>";
                }
            },
            {
                field: 'noticeType',
                width: '50px',
                title: noticemsg['NOTICE_TYPE'],
                formatter: function (value, row, index) {
                    return getCodeClassName(value, "notice");
                }
            },
            {
                field: 'status',
                title: sysmsg['STATUS'],
                width: '50px',
                formatter: function (value, row, index) {
                    if (value == "0") {
                        return sysmsg['IN_APPROVAL'];
                    } else if (value == "1") {
                        return sysmsg['IN_EFFECT'];
                    } else if (value == "2") {
                        return sysmsg['NOT_PASS'];
                    }

                }
            },
            {
                field: 'createUser',
                width: '50px',
                title: sysmsg['CREATE_USER']
            },
            {
                field: 'createTime',
                width: '100px',
                title: sysmsg['CREATE_TIME']
            },
            {
                field: 'sendTime',
                width: '50px',
                title: sysmsg['TIME_TAKING_EFFECT']
            },
            {
                field: 'endTime',
                width: '50px',
                title: sysmsg['END_TIME']
            },
            {
                field: '',
                width: '150px',
                align: 'center',
                title: sysmsg['OPT'],
                formatter: function (value, row, index) {
                    return createOptBtn(row.noticeId, row.status);
                }
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        noticeType: $("#noticeType").val(),
        beginTime: $("#beginTime").val(),
        endTime: $("#endTime").val()
    };
    return temp;
};

function createOptBtn(noticeId, status) {
    let html = "<a href=\"javascript:void(0);edit('" + noticeId + "')\" class=\"btn btn-success btn-xs\" >" + sysmsg['OPT_EDIT'] + "</a>&nbsp;&nbsp;";
    if (status == "1") {
        html += "<a href=\"javascript:void(0);setNotice('" + noticeId + "','0')\" class=\"btn btn-primary btn-xs\">" + sysmsg['TERMINATION'] + "</a>&nbsp;&nbsp;";
    } else {
        if (status != "2") {
            html += "<a href=\"javascript:void(0);setNotice('" + noticeId + "','1')\" class=\"btn btn-palegreen btn-xs\">" + sysmsg['EFFECT'] + "</a>&nbsp;&nbsp;";
        }
    }
    html += "<a href=\"javascript:void(0);readStatus('" + noticeId + "')\" class=\"btn btn-darkorange btn-xs\" >查阅情况</a>&nbsp;&nbsp;";
    html += "<a href=\"javascript:void(0);del('" + noticeId + "')\" class=\"btn btn-darkorange btn-xs\" >" + sysmsg['OPT_DELETE'] + "</a>";
    return html;
}

function readStatus(noticeId) {
    window.open("/app/core/notice/readstatus?noticeId=" + noticeId);
}


function edit(noticeId) {
    open("/app/core/notice/index?view=edit&noticeId=" + noticeId, "_self");
}

function setNotice(noticeId, status) {
    $.ajax({
        url: "/set/noticeset/setNoticeStatus",
        type: "post",
        dataType: "json",
        data: {
            noticeId: noticeId,
            status: status
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                layer.msg(sysmsg[data.msg]);
                $('#myTable').bootstrapTable('refresh');
            }
        }
    })
}


function readNotice(noticeId) {
    window.open("/app/core/notice/details?noticeId=" + noticeId);
}

function del(noticeId) {
    if (window.confirm("确定删除选中的通知公告吗？")) {
        $.ajax({
            url: "/set/noticeset/delNotice",
            type: "post",
            dataType: "json",
            data: {noticeId: noticeId},
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                    $('#myTable').bootstrapTable('refresh');
                }
            }
        })
    } else {
        return;
    }

}
