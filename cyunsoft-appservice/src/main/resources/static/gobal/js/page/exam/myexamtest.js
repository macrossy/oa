$(function () {
    query();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/examget/getMyExamTestList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        sortOrder: "asc",
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'recordId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            title: '考试标题',
            sortable: true,
            width: '150px',
            formatter:function (value,row,index)
            {
                return "<a href=\"javascript:void(0);details('" + row.recordId + "')\" style='cursor: pointer' >"+value+"</a>"
            }
        }, {
            field: 'questionTitle',
            title: '试卷标题',
            width: '150px',
            formatter: function (value, row, index) {
                return "《" + value + "》";
            }
        }, {
            field: 'deptId',
            width: '80px',
            title: '主考部门',
            formatter: function (value, row, index) {
                return getDeptNameByDeptIds(value);
            }
        }, {
            field: 'sortName',
            width: '80px',
            title: '试卷分类'
        }, {
            field: 'isOpen',
            title: '是否开卷',
            width: '50px',
            formatter: function (value, row, index) {
                if (value == "1") {
                    return "是";
                } else {
                    return "否";
                }
            }
        }, {
            field: 'beginTime',
            width: '100px',
            title: '开始时间'
        }, {
            field: 'endTime',
            title: '结束时间',
            width: '100px'
        }, {
            field: 'status',
            title: '状态',
            width: '50px',
            formatter: function (value, row, index) {
                if (value == "false") {
                    return "结束";
                } else if (value == "true") {
                    return "开考"
                } else {
                    return "未知";
                }
            }
        }, {
            field: 'duration',
            width: '50px',
            title: '考试时长',
            formatter: function (value, row, index) {
                return value + "分钟";
            }
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '50px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId, row.status);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                    //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    let temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function createOptBtn(recordId, status) {
    let html = "";
    if (status == "true") {
        html += "<a href=\"javascript:void(0);doExamTest('" + recordId + "')\" class=\"btn btn-success btn-xs\" >考试</a>";
    } else {
        html += "<a href=\"javascript:void(0);getMyGrade('" + recordId + "')\" class=\"btn btn-magenta btn-xs\" >查分</a>";
    }
    return html;
}

function details(recordId) {
    window.open("/app/core/exam/examtestdetails?recordId=" + recordId);
}

function doExamTest(recordId) {
    window.open("/app/core/exam/begintest?recordId=" + recordId);
}

function getMyGrade(examTestId) {
    $.ajax({
        url: "/ret/examget/getMyGrade",
        type: "post",
        dataType: "json",
        data: {examTestId: examTestId},
        success: function (data) {
            if (data.status == "200") {
                alert(data.redirect);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
