$(function () {
    $.ajax({
        url: "/ret/meetingget/getMeetingNotesInfo",
        type: "post",
        dataType: "json",
        data: {
            notesId: notesId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var info = data.list;
                for (var id in info) {
                    if (id == "attach") {
                        $("#meetingattach").attr("data_value", info.attach);
                        createAttach("meetingattach", info.attachPriv);
                    } else if (id == "userPriv") {
                        $("#" + id).html(getUserNameByStr(info[id]))
                    } else if (id == "deptPriv") {
                        $("#" + id).html(getDeptNameByDeptIds(info[id]))
                    } else if (id == "levelPriv") {
                        $("#" + id).html(getUserLevelStr(info[id]))
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            }
        }
    });
});
