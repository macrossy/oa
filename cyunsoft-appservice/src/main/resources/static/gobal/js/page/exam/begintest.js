$(function () {
    $.ajax({
        url: "/ret/examget/getExamTestById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                $("#title").html("《" + data.list.title + "》");
                $("#passMark").html("合格分数：" + data.list.passMark);
                let duration = data.list.duration;
                let now = new Date;
                now.setMinutes(now.getMinutes() + duration);
                let downTime = formatTime(now);
                forDiwnCount(downTime);
                let questionRecordId = data.list.questionRecordId;
                getExamQuestionRecordInfo(questionRecordId);
            } else if (data.status == "100") {
                console.log(layer.msg(data.msg));
            } else {
                console.log(data.msg);
            }
        }
    });
    $(".js-jiaojuan").unbind("click").click(function () {
        addExamGread();
    })
})

function forDiwnCount(timeStr) {

    let arr = timeStr.split(" ");
    let temp1 = arr[0];
    let temparr = temp1.split("-");
    let resTime = temparr[1] + "/" + temparr[2] + "/" + temparr[0] + " " + arr[1];
    $('.countdown').downCount({
        date: resTime,
        offset: +10
    }, function () {
        $.ajax({
            url: "/set/examset/addExamGread",
            type: "post",
            dataType: "json",
            data: {
                examTestId: recordId,
                result: getAnswerParam()
            },
            success: function (data) {
                if (data.status == "200") {
                    alert("您的客观题分数为：" + data.list + "分")
                    window.close();
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
        alert('考试时长已到,试卷已自动提交!');
    });
}

function getExamQuestionRecordInfo(recordId) {
    $.ajax({
        url: "/ret/examget/getExamQuestionRecordById",
        type: "post",
        dataType: "json",
        data: {
            recordId: recordId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                getExamQuestionListByIds(data.list.questionIds)
            }
        }
    })
}

function getExamQuestionListByIds(recordIds) {
    $.ajax({
        url: "/ret/examget/getExamQuestionListByIds",
        type: "post",
        dataType: "json",
        data: {
            recordIds: recordIds
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                let listInfo = data.list;
                for (let i = 0; i < listInfo.length; i++) {
                    let info = listInfo[i];
                    let index = $("#questionlist").find("tr").length;
                    let examType = "";
                    if (info.examType == "0") {
                        examType = "单选";
                    } else if (info.examType == "1") {
                        examType = "多选";
                    } else if (info.examType == "2") {
                        examType = "简述";
                    }
                    let childItemArr = [];
                    let html = "";
                    let html1 = "<div style='display: inline-block;font-size: 16px;font-weight: 600'>解答：</div>";
                    if (info.examType == "0") {
                        childItemArr = JSON.parse(info.content);
                        html1 += "<div class=\"radio js-answer\" style='display: inline-block' data-value='" + info.recordId + "' data-type='radio'>";
                    } else if (info.examType == "1") {
                        childItemArr = JSON.parse(info.content);
                        html1 += "<div class=\"checkbox js-answer\" style='display: inline-block' data-value='" + info.recordId + "' data-type='checkbox'>";
                    } else {
                        html1 += "<div class='js-answer' data-value='" + info.recordId + "' data-type='textarea'><textarea id='name_" + info.recordId + "' rows='5' style='width: 100%;'></textarea>";
                    }
                    for (let i = 0; i < childItemArr.length; i++) {
                        html += "<div style='margin-top:5px;font-size: 14px;'>" + childItemArr[i].optValue + "、&nbsp;&nbsp;" + childItemArr[i].childTitle + "</div>";
                        if (info.examType == "0") {
                            html1 += "<label>\n" +
                                "    <input type=\"radio\" value='" + childItemArr[i].optValue + "' name='name_" + info.recordId + "'>\n" +
                                "    <span class=\"text\">" + childItemArr[i].optValue + " </span>\n" +
                                " </label>"
                        } else if (info.examType == "1") {
                            html1 += "<label>\n" +
                                "   <input type=\"checkbox\" class=\"colored-success\" value='" + childItemArr[i].optValue + "' name='name_" + info.recordId + "'>\n" +
                                "  <span class=\"text\">" + childItemArr[i].optValue + "</span>\n" +
                                " </label>"
                        }
                    }
                    html1 += "</div>"
                    $("#questionlist").append("<tr data-value='" + info.recordId + "'><td><div style='font-size: 16px;font-weight: 600;'>" + (index + 1) + "、【" + examType + "】" + info.title + "<span style='float: right;margin-right: 10px;'>分值：" + info.grade + "分</span></div>" +
                        "<div>" + html + "</div>" +
                        "<div>" + html1 + "</div></tr>");
                }
            }
        }
    })
}

function getAnswerParam() {
    let paramArr = [];
    $(".js-answer").each(function () {
        let tempJson = {};
        let dataValue = $(this).attr("data-value");
        let type = $(this).attr("data-type");
        tempJson.recordId = dataValue;
        tempJson.type = type;
        if (type == "checkbox") {
            let answer = getCheckBoxValue("name_" + dataValue);
            tempJson.answer = answer;
        } else if (type == "radio") {
            let answer = getCheckBoxValue("name_" + dataValue);
            tempJson.answer = answer;
        } else if (type == "textarea") {
            let answer = $("#name_" + dataValue).val();
            tempJson.answer = answer;
        }
        paramArr.push(tempJson);
    })
    return JSON.stringify(paramArr);
}

function addExamGread() {
    if (confirm("您确定交卷吗？")) {
        $.ajax({
            url: "/set/examset/addExamGread",
            type: "post",
            dataType: "json",
            data: {
                examTestId: recordId,
                beginTime: formatTime(new Date()),
                result: getAnswerParam()
            },
            success: function (data) {
                if (data.status == "200") {
                    alert("您的客观题分数为：" + data.list + "分")
                    window.close();
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    }
}
