var $table = $('#table');
let userList;
$(function () {
    $.ajax({
        url: "/ret/noticeget/getNoticeReadStatus",
        type: "post",
        dataType: "json",
        data: {noticeId: noticeId},
        success: function (data) {
            if (data.status == "200") {
                userList = data.list;
                $.ajax({
                    url: "/ret/unitget/getUnitDeptTableTreeList",
                    type: "post",
                    dataType: "json",
                    success: function (res) {
                        if (data.status == "200") {
                            createTableTree(res.list);
                        } else if (data.status == "100") {
                            layer.msg(sysmsg[res.msg]);
                        } else {
                            console.log(res.msg);
                        }
                    }
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
});

function createTableTree(res) {
    $table.bootstrapTable({
        data: res,
        idField: 'id',
        striped: true,//隔行换色
        cache: false,//禁用缓存
        dataType: 'json',
        sortable: true,//排序
        search: false,//启用搜索
        showColumns: false,//是否显示 内容列下拉框
        showRefresh: false,//显示刷新按钮
        columns: [
            {
                field: 'check',
                checkbox: true,
                formatter: function (value, row, index) {
                    if (row.check == true) {
                        return {checked: true};
                    }
                }
            },
            {
                field: 'name',
                title: '部门名称',
                width: '250px'
            },
            {
                title: '未查看',
                align: "center",
                width: '400px',
                formatter: function (value, row, index) {
                    return getNotReadUser(row.id)
                }
            },
            {
                title: '已查看',
                align: "center",
                width: '400px',
                formatter: function (value, row, index) {
                    return getReadedUser(row.id)
                }
            }
        ],
        treeShowField: 'name',
        parentIdField: 'pid',
        onResetView: function (data) {
            $table.treegrid({
                initialState: 'collapsed',// 所有节点都折叠
                treeColumn: 1,
                onChange: function () {
                    $table.bootstrapTable('resetWidth');
                }
            });
            $table.treegrid('getRootNodes').treegrid('expand');
        },
        onCheck: function (row) {
            var datas = $table.bootstrapTable('getData');
            selectChilds(datas, row, "id", "pid", true);
            selectParentChecked(datas, row, "id", "pid")
            $table.bootstrapTable('load', datas);
        },

        onUncheck: function (row) {
            var datas = $table.bootstrapTable('getData');
            selectChilds(datas, row, "id", "pid", false);
            $table.bootstrapTable('load', datas);
        }
    });
}

/*
       *    选中父项时，同时选中子项
* @param datas 所有的数据
* @param row 当前数据
* @param id id 字段名
* @param pid 父id字段名
*/
function selectChilds(datas, row, id, pid, checked) {
    for (var i in datas) {
        if (datas[i][pid] == row[id]) {
            datas[i].check = checked;
            selectChilds(datas, datas[i], id, pid, checked);
        }
        ;
    }
}

function selectParentChecked(datas, row, id, pid) {
    for (var i in datas) {
        if (datas[i][id] == row[pid]) {
            datas[i].check = true;
            selectParentChecked(datas, datas[i], id, pid);
        }
        ;
    }
}

function getReadedUser(deptId) {
    let resArr = [];
    for (i = 0; i < userList.length; i++) {
        if (userList[i].deptId == deptId && userList[i].isRead == 'true') {
            resArr.push(userList[i].userName);
        }
    }
    return resArr.join(",");
}

function getNotReadUser(deptId) {
    let resArr = [];
    for (i = 0; i < userList.length; i++) {
        if (userList[i].deptId == deptId && userList[i].isRead == 'false') {
            resArr.push(userList[i].userName);
        }
    }
    return resArr.join(",");
}
