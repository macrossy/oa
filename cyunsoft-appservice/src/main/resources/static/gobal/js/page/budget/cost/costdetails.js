$(function () {
    $.ajax({
        url: "/ret/budgetget/getBudgetCostById",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "projectId" || id == "childProjectId") {
                        $.ajax({
                            url: "/ret/budgetget/getBudgetProjectById",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {projectId: recordInfo[id]},
                            success: function (res) {
                                if (res.status == "200") {
                                    $("#" + id).html(res.list.title);
                                } else if (res.status == "100") {
                                    layer.msg(sysmsg[res.msg]);
                                } else {
                                    console.log(res.msg);
                                }
                            }
                        });
                    } else if (id == "runId") {

                    } else if (id == "budgetAccount") {
                        $("#" + id).html(getBudgetAccountName(recordInfo[id]));
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
})

function getBudgetAccountName(budgetAccountId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/budgetget/getBudgetAccountById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            budgetAccountId: budgetAccountId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                returnStr = data.list.title;
            }
        }
    });
    return returnStr;
}
