$(function () {
    query();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/oaget/getMyVoteListForVote',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: false,// 是否显示 数据条数选择框
        sortable: true,// 排序
        sortOrder: "desc",
        search: false,// 启用搜索
        showColumns: false,// 是否显示 内容列下拉框
        showRefresh: false,// 显示刷新按钮
        idField: 'voteId',// key值栏位
        clickToSelect: true,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'title',
            title: '投票标题',
            sortable: true,
            width: '150px'
        }, {
            field: 'voteType',
            title: '投票类型',
            sortable: true,
            width: '50px',
            formatter: function (value, row, index) {
                return getCodeClassName(value, "vote");
            }
        }, {
            field: 'readRes',
            title: '是否允许查看结果',
            width: '50px',
            formatter: function (value, row, index) {
                if (value == "0") {
                    return "允许查看结果";
                } else if (value == "1") {
                    return "禁止查看结果";
                }
            }
        }, {
            field: 'startTime',
            title: '起始日期',
            width: '50px'
        }, {
            field: 'endTime',
            width: '100px',
            title: '终止时间'
        }, {
            field: 'createTime',
            width: '100px',
            title: '创建时间'
        }, {
            field: 'createUser',
            width: '50px',
            title: '创建人',
            formatter: function (value, row, index) {
                return getUserNameByStr(value);
            }
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '100px',
            formatter: function (value, row, index) {
                return createOptBtn(row.voteId, row.readRes);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function createOptBtn(voteId, readRes) {
    var html = "<a href=\"javascript:void(0);dovoite('" + voteId + "')\" class=\"btn btn-sky btn-xs\" >投票</a>&nbsp;&nbsp;";
    if (readRes == "0") {
        html += "<a href=\"javascript:void(0);readres('" + voteId + "')\" class=\"btn btn-primary btn-xs\">查看结果</a>";
    }
    return html;
}

function dovoite(voteId) {
    window.open("/app/core/vote/myvote?view=dovote&voteId=" + voteId);
}

function readres(voteId) {
    window.open("/app/core/vote/myvote?view=readres&voteId=" + voteId);
}
