$(function () {
    getAllKnowledgeInfo();
})

function getAllKnowledgeInfo() {
    $.ajax({
        url: "/ret/knowledgeget/getAllKnowledgeInfo",
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log(data);
            if (data.status == "200") {
                var attachInfo = data.list.attachInfo;
                $("#attachInfo").html("文件总大小：" + attachInfo.totalFileSize + "kb,文件总数：" + attachInfo.totalCount + "个");
                $("#indexPath").html(data.list.indexPath);
                $("#hostWordCount").html(data.list.hostWordCount + "个");
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
