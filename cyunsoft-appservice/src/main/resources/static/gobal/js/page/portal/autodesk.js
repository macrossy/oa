$(function () {
    $(".preview").each(function () {
        $(this).remove();
    })
    $("body").removeClass("edit");
    $("body").addClass("devpreview sourcepreview");
    removeMenuClasses();
    $(".lyrow").each(function () {
        let height = $(this).height();
        $(this).css("height", (height-30)+"px");
    })
    var arr = [];
    $(".js-module").each(function () {
        arr.push($(this).attr("module"));
    })
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == "news") {
            $.ajax({
                url: "/ret/oaget/getMyPicNewsListForDesk",
                type: "post",
                dataType: "json",
                success: function (data) {
                    if (data.status == "500") {
                        console.log(data.msg);
                    } else if (data.status == "100") {
                        layer.msg(sysmsg[data.msg]);
                    } else {
                        var newsInfo = data.list;
                        for (var i = 0; i < newsInfo.length; i++) {
                            $("#pol")
                                .append(
                                    " <img src="
                                    + newsInfo[i].imgUrl
                                    + " class=\"newsimg\" onclick=\"readNews('"
                                    + newsInfo[i].newsId
                                    + "')\">")
                            if (i == 0) {
                                $("#num").append(
                                    " <span class='cut'> 1</span>")
                            } else {
                                $("#num").append(
                                    " <span class='cut'>" + (i + 1)
                                    + "</span>")
                            }
                        }
                        var CRT = 0;
                        var w = $(".newsimg").width() + 39, pol = $("#pol"), spans = $("#num span");
                        spans.hover(function () {
                            var me = $(this);
                            me.addClass("cut").siblings(".cut")
                                .removeClass("cut");
                            spans.eq(CRT).clearQueue();
                            pol.stop().animate(
                                {
                                    left: "-" + w * (CRT = me.index()) + "px"
                                }, "slow");
                        }, function () {
                            anony();
                        });
                        var anony = function () {
                            CRT++;
                            CRT = CRT > spans.length - 1 ? 0 : CRT;
                            spans.eq(CRT).clearQueue().delay(5000).queue(
                                function () {
                                    spans
                                        .eq(CRT)
                                        .triggerHandler(
                                            "mouseover");
                                    anony();
                                });
                        };
                        anony();
                    }
                }
            })
            getMyNews();
            if (msgtime > 0) {
                setInterval("getMyNews()", msgtime);
            }
        } else if (arr[i] == "notice") {
            getMyNotice();
            if (msgtime > 0) {
                setInterval("getMyNotice()", msgtime);
            }
        } else if (arr[i] == "bpm") {
            getMyBpm();
            if (msgtime > 0) {
                setInterval("getMyBpm()", msgtime);
            }
        } else if (arr[i] == "myTask") {
            getMyTask();
            if (msgtime > 0) {
                setInterval("getMyTask()", msgtime);
            }
        } else if (arr[i] == "myMeeting") {
            getMyMeeting();
            if (msgtime > 0) {
                setInterval("getMyMeeting()", msgtime);
            }
        } else if (arr[i] == "myFile") {
            getMyFile();
            if (msgtime > 0) {
                setInterval("getMyFile()", msgtime);
            }
        } else if (arr[i] == "document1") {
            getMyDocument1();
            if (msgtime > 0) {
                setInterval("getMyDocument1()", msgtime);
            }
        } else if (arr[i] == "document2") {
            getMyDocument2();
            if (msgtime > 0) {
                setInterval("getMyDocument2()", msgtime);
            }
        } else if (arr[i] == "superversion") {
            getSuperversionListForDesk();
            if (msgtime > 0) {
                setInterval("getSuperversionListForDesk()", msgtime);
            }
        } else if (arr[i] == "calendar") {
            getMyCalendar();
            if (msgtime > 0) {
                setInterval("getMyCalendar()", msgtime);
            }
        } else if (arr[i] == "leadactivity") {
            getLeadActivity();
            if (msgtime > 0) {
                setInterval("getLeadActivity()", msgtime);
            }
        } else if (arr[i] == "bpmworkretention") {
            getBpmZl();
            if (msgtime > 0) {
                setInterval("getBpmZl()", msgtime);
            }
        }else if (arr[i] == "fileMap") {
            getMyFileMapForDesk("1");
            if (msgtime > 0) {
                setInterval("getMyFileMapForDesk('1')", msgtime);
            }
        }else if (arr[i] == "email") {
            getMyEmail();
            if (msgtime > 0) {
                setInterval("getMyEmail()", msgtime);
            }
        }
    }
})

function getMyFileMapForDesk(readStatus)
{
    $.ajax({
        url: "/ret/filemapget/getMyFileMapForDesk",
        type: "post",
        dataType: "json",
        data:{readStatus:readStatus},
        success: function (data) {
            if (data.status == "200") {
                let infoList = data.list;
                let html="";
                for(let i=0;i<infoList.length;i++)
                {
                    var fileName = infoList[i].fileName;
                    html += "<tr><td class='td_inline'>";
                    html += "<span style='position: absolute;display: block;width: 65%'>";
                    if(readStatus!="2")
                    {
                        html += "<li class=\"label label-darkorange\">new</li>&nbsp;&nbsp;";
                    }
                    html += "<a href=\"javascript:void(0);openFileMapOnLine('" + infoList[i].extName + "','" + infoList[i].attachId + "','1','','" + infoList[i].recordId + "')\">" + fileName + "</a>";
                    html += "</span><span style='float:right;'>"
                        + infoList[i].createTime
                        + "&nbsp;&nbsp;"
                        + infoList[i].createUserName
                        + "</span></td></tr>";
                }
                $("#fileMapRecord").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    });
}

function getMyFileMap(Obj,flag)
{
    $("#fileMap").attr("data-value", flag);
    $(".js-filemap").each(function () {
        $(this).removeClass("onaction");
    })
    $(Obj).addClass("onaction");
    if (flag == "1") {
        $(".js-fileopenmore").attr("data-value",
            "/app/core/filemap/filemapsearch");
        $(".js-fileopenmore").attr("data-title", "待阅文件");
        getMyFileMapForDesk("1");
    } else if (flag == "2") {
        $(".js-fileopenmore").attr("data-value",
            "/app/core/filemap/filemapsearch");
        $(".js-fileopenmore").attr("data-title", "已阅文件");
        getMyFileMapForDesk("2");
    }
}

function refreshFileMap() {
    var flag = $("#fileMap").attr("data-value");
    if (flag == "1") {
        getMyFileMapForDesk("1");
    } else if (flag == "2") {
        getMyFileMapForDesk("2");
    }
}

function removeMenuClasses() {
    $("#menu-layoutit li button").removeClass("active")
}

function getLeadActivity() {
    $.ajax({
        url: "/ret/oaget/getLeadActivityLsitForDesk",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var infoList = data.list;
                var html = "";
                for (var i = 0; i < infoList.length; i++) {
                    var title = infoList[i].title;
                    html += "<tr><td class='td_inline'><span style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);readActivity('"
                        + infoList[i].recordId
                        + "')\">" + infoList[i].leaderUserName + "&nbsp;&nbsp;《" + title + "》</a>"
                        + "</span><span style='float:right;'>时间:"
                        + infoList[i].beginTime
                        + "--"
                        + infoList[i].endTime
                        + "</span></td></tr>";
                }
                $("#leadbody").html(html);
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function readActivity(recordId) {
    window.open("/app/core/leadactivity/details?recordId=" + recordId);
}

function readMeeting(meetingId) {
    window.open("/app/core/meeting/meetingdetails?meetingId=" + meetingId);
}

function getMyMeeting() {
    $.ajax({
        url: "/ret/meetingget/getMyMeetingListForDesk",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var meetingList = data.list;
                var html = "";
                for (var i = 0; i < meetingList.length; i++) {
                    var subject = meetingList[i].subject;
                    html += "<tr><td class='td_inline'><span title='" + meetingList[i].name + "' style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);readMeeting('"
                        + meetingList[i].meetingId
                        + "')\">" + subject + "</a>"
                        + "</span><span style='float:right;'>开会时间:"
                        + meetingList[i].beginTime
                        + "--"
                        + meetingList[i].endTime
                        + "</span></td></tr>";
                }
                $("#meetingbody").html(html);

            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}


function getMyFile() {
    $
        .ajax({
            url: "/ret/fileget/getMyPublicFolderInPrivForDesk",
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.status == 200) {
                    var fileList = data.list;
                    var html = "";
                    for (var i = 0; i < fileList.length; i++) {
                        html += "<tr><td class='td_inline'><span><a href=\"javascript:void(0);gopublicfile('"
                            + fileList[i].folderId
                            + "')\">" + fileList[i].folderName + "</a></span></td></tr>";
                    }
                    $("#filebody").html(html);

                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function gopublicfile(folderId) {
    var url = "/app/core/file/publicfile?folderId=" + folderId;
    parent.openNewTabs(url, "公共文件柜");
}

function getMyBpm() {
    $
        .ajax({
            url: "/ret/bpmget/getMyProcessListForDesk",
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.status == 200) {
                    var infoList = data.list;
                    var html = "";
                    for (var i = 0; i < infoList.length; i++) {
                        let newHtml="";
                        if (infoList[i].recTime == undefined||infoList[i].recTime == "") {
                            newHtml += "<li class=\"label label-darkorange\">new</li>&nbsp;&nbsp;";
                        }
                        var flowTitle = infoList[i].flowTitle;
                        html += "<tr><td class='td_inline'><span title=\"" + infoList[i].prcsName + "\" style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);doBpm('"
                            + infoList[i].runId + "','"
                            + infoList[i].runProcessId + "','"
                            + infoList[i].flowTitle.replace("\'","")
                            + "')\">NO:"+infoList[i].id+"&nbsp;&nbsp;"+ flowTitle + "</a>"
                            + "</span><span style='float:right'>"
                            + infoList[i].createTime + "&nbsp;&nbsp;"
                            + infoList[i].createUser
                            + "</span></td></tr>";
                    }
                    $("#bpmtbody").html(html);

                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function getMyBpmSendToMeForDesk() {
    $
        .ajax({
            url: "/ret/bpmget/getMySendToListForDesk",
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.status == 200) {
                    var infoList = data.list;
                    var html = "";
                    for (var i = 0; i < infoList.length; i++) {
                        var flowTitle = infoList[i].flowTitle;
                        html += "<tr><td class='td_inline'><span title=\"" + infoList[i].flowName + "\" style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);readsendtobpm('"
                            + infoList[i].runId + "','"
                            + infoList[i].flowId + "','"
                            + infoList[i].sendToId
                            + "')\">NO:"+infoList[i].id+"&nbsp;&nbsp;"+ flowTitle + "</a>"
                            + "</span><span style='float:right;'>"
                            + infoList[i].createTime + "&nbsp;&nbsp;"
                            + infoList[i].createUserName
                            + "</span></td></tr>";
                    }
                    $("#bpmtbody").html(html);
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function readsendtobpm(runId, flowId, sendToId) {
    $.ajax({
        url: "/set/bpmset/updateSendToStatus",
        type: "post",
        dataType: "json",
        data: {
            sendToId: sendToId
        },
        success: function (data) {
            if (data.status == "200") {
                window.open("/app/core/bpm/bpmread?runId=" + runId + "&flowId="
                    + flowId);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getMyDocument1() {
    $.ajax({
        url: "/ret/documentget/getMyProcessListForDesk",
        type: "post",
        dataType: "json",
        data: {
            documentType: 1
        },
        success: function (data) {
            if (data.status == 200) {
                var infoList = data.list;
                var html = "";
                for (var i = 0; i < infoList.length; i++) {
                    let newHtml="";
                    if (infoList[i].recTime == undefined||infoList[i].recTime == "") {
                        newHtml += "<li class=\"label label-darkorange\">new</li>&nbsp;&nbsp;";
                    }
                    var flowTitle = infoList[i].flowTitle;
                    html += "<tr><td class='td_inline'><span title='" + infoList[i].prcsName + "' style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);doDocument('"
                        + infoList[i].runId + "','"
                        + infoList[i].runProcessId + "','"
                        + infoList[i].flowTitle
                        + "')\">"+newHtml+"NO:"+infoList[i].id+"&nbsp;&nbsp;"+ flowTitle + "</a>"
                        + "</span><span style=\"float:right\">"
                        + infoList[i].createTime + "&nbsp;&nbsp;"
                        + infoList[i].createUser
                        + "<span></td></tr>";
                }
                $("#documentbody1").html(html);

            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getMyDocument2() {
    $.ajax({
        url: "/ret/documentget/getMyProcessListForDesk",
        type: "post",
        dataType: "json",
        data: {
            documentType: 2
        },
        success: function (data) {
            if (data.status == 200) {
                var infoList = data.list;
                var html = "";
                for (var i = 0; i < infoList.length; i++) {
                    let newHtml="";
                    if (infoList[i].recTime == undefined||infoList[i].recTime == "") {
                        newHtml += "<li class=\"label label-darkorange\">new</li>&nbsp;&nbsp;";
                    }
                    var flowTitle = infoList[i].flowTitle;
                    html += "<tr><td class='td_inline'><span title='" + infoList[i].prcsName + "' style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);doDocument('"
                        + infoList[i].runId + "','"
                        + infoList[i].runProcessId + "','"
                        + infoList[i].flowTitle
                        + "')\">"+newHtml+"NO:"+infoList[i].id+"&nbsp;&nbsp;"+ flowTitle + "</a>"
                        + "</span><span style=\"float:right\">"
                        + infoList[i].createTime
                        + "&nbsp;&nbsp;"
                        + infoList[i].createUser
                        + "</span></td></tr>";
                }
                $("#documentbody2").html(html);

            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}
function getMyDocumentSendTo1() {
    $
        .ajax({
            url: "/ret/documentget/getMySendToListForDesk",
            type: "post",
            dataType: "json",
            data: {
                documentType: 1
            },
            success: function (data) {
                if (data.status == 200) {
                    var infoList = data.list;
                    var html = "";
                    for (var i = 0; i < infoList.length; i++) {
                        var flowTitle = infoList[i].flowTitle;
                        html += "<tr><td class='td_inline'><span title='" + infoList[i].flowName + "' style='position: absolute;display: block;width: 65%'>"
                            + "<a href=\"javascript:void(0);readsendtodocument('"
                            + infoList[i].runId + "','"
                            + infoList[i].flowId + "','"
                            + infoList[i].sendToId
                            + "')\">NO:"+infoList[i].id+"&nbsp;&nbsp;"+ flowTitle + "</a>"
                            + "</span><span style='float:right;'>"
                            + infoList[i].createTime
                            + "&nbsp;&nbsp;"
                            + infoList[i].createUserName
                            + "</span></td></tr>";
                    }
                    $("#documentbody1").html(html);
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function getMyDocumentSendTo2() {
    $
        .ajax({
            url: "/ret/documentget/getMySendToListForDesk",
            type: "post",
            dataType: "json",
            data: {
                documentType: 2
            },
            success: function (data) {
                if (data.status == 200) {
                    var infoList = data.list;
                    var html = "";
                    for (var i = 0; i < infoList.length; i++) {
                        var flowTitle = infoList[i].flowTitle;
                        html += "<tr><td class='td_inline'><span title='" + infoList[i].flowName + "' style='position: absolute;display: block;width: 65%'>"
                            + "<a href=\"javascript:void(0);readsendtodocument('"
                            + infoList[i].runId + "','"
                            + infoList[i].flowId + "','"
                            + infoList[i].sendToId
                            + "')\">NO:"+infoList[i].id+"&nbsp;&nbsp;"+ flowTitle + "</a>"
                            + "</span><span style='float:right;'>"
                            + infoList[i].createTime
                            + "&nbsp;&nbsp;"
                            + infoList[i].createUserName
                            + "</span></td></tr>";
                    }
                    $("#documentbody2").html(html);
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function readsendtodocument(runId, flowId, sendToId) {
    $.ajax({
        url: "/set/documentset/updateSendToStatus",
        type: "post",
        dataType: "json",
        data: {
            sendToId: sendToId
        },
        success: function (data) {
            if (data.status == "200") {
                window.open("/app/core/document/documentread?runId=" + runId
                    + "&flowId=" + flowId);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getMyEmail() {
    $.ajax({
        url: "/ret/oaget/getEmailListForDesk",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var datalist = data.list;
                var html = "";
                for (var i = 0; i < datalist.length; i++) {
                    var subject = datalist[i].subject;
                    html += "<tr><td class='td_inline'><span style='position: absolute;display: block;width: 65%'>";
                    if (datalist[i].readTime == null) {
                        html += "<li class=\"label label-darkorange\">new</li>&nbsp;&nbsp;";
                    }
                    html +="<a href=\"javascript:void(0);readEmail('"+ datalist[i].emailId+ "')\">" + subject + "</a>";

                    html += "</span><span style='float:right;'>"
                        + datalist[i].sendTime
                        + "&nbsp;&nbsp;"
                        + datalist[i].fromUserName
                        + "</span></td></tr>";
                }
                $("#emailtbody").html(html);
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getMyCalendar() {

    $.ajax({
        url: "/ret/oaget/getMyCalendarListForDesk",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var datalist = data.list;
                var html = "";
                for (var i = 0; i < datalist.length; i++) {
                    html += "<tr><td class='td_inline'>" + datalist[i].title;
                    html += "</td><td style='width:150px'>"
                        + datalist[i].calendarStart
                        + "</td><td style='width:80px'>"
                        + datalist[i].createUserName + "</td></tr>";
                }
                $("#calendartbody").html(html);
            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getMyTask() {
    $
        .ajax({
            url: "/ret/taskget/getTaskListForDesk2",
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.status == 200) {
                    var taskList = data.list;
                    var html = "";
                    for (var i = 0; i < taskList.length; i++) {
                        var text1 = taskList[i].text1;
                        html += "<tr><td class='td_inline'><span title='" + taskList[i].taskName + "' style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);doTask('"
                            + taskList[i].taskDataId
                            + "')\">" + text1 + "</a></span><span style='float:right;'>"
                            + taskList[i].startDate
                            + "&nbsp;&nbsp;进度："
                            + (taskList[i].progress * 100)
                            + "%</span></td></tr>";
                    }
                    $("#mytaskbody").html(html);

                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function getMyNews() {
    $
        .ajax({
            url: "/ret/oaget/getMyNewsListForDesk",
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.status == 200) {
                    var newsList = data.list;
                    var html = "";
                    for (var i = 0; i < newsList.length; i++) {
                        html += "<tr><td class='td_inline'><span>";
                        if (newsList[i].readStatus == "true") {
                            html += "<li class=\"label label-darkorange\">new</li>&nbsp;&nbsp;";
                        }
                        var newsTitle = newsList[i].newsTitle;
                        if (newsTitle.length > 20) {
                            newsTitle = newsTitle.substring(0, 20) + "...";
                        }
                        html += "<a href=\"javascript:void(0);readNews('" + newsList[i].newsId + "')\">" + newsTitle + "</a><span style=\"float:right;\">" + newsList[i].sendTime + "&nbsp;&nbsp;" + newsList[i].createUser + "</span>"
                            + "</td></tr>";
                    }
                    $("#newstbody").html(html);

                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function getMyNotice() {
    $
        .ajax({
            url: "/ret/noticeget/getMyNoticeListForDesk",
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.status == 200) {
                    var noticeList = data.list;
                    var html = "";
                    for (var i = 0; i < noticeList.length; i++) {
                        var noticeTitle = noticeList[i].noticeTitle;
                        html += "<tr><td class='td_inline'><span style='position: absolute;display: block;width: 65%'>";
                        if (noticeList[i].readStatus == "true") {
                            html += "<li class=\"label label-darkorange\">new</li>&nbsp;&nbsp;";
                        }
                        html += "<a href=\"javascript:void(0);readNotice('"
                            + noticeList[i].noticeId
                            + "')\">" + noticeTitle + "</a>";
                        html += "</span><span style='float:right;'>"
                            + noticeList[i].sendTime
                            + "&nbsp;&nbsp;"
                            + noticeList[i].createUser
                            + "</span></td></tr>";
                    }
                    $("#noticetbody").html(html);

                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function readEmail(eamilId) {
    parent.openNewTabs("/app/core/oa/emaildetails?emailId=" + eamilId, "电子邮件");
}

function readNews(newsId) {
    window.open("/app/core/news/readnews?newsId=" + newsId);
}

function doBpm(runId, runProcessId, title) {
    parent.openNewTabs("/app/core/bpm/dowork?runId=" + runId + "&runProcessId="
        + runProcessId, title);
}

function doTask(taskDataId) {
    window.open("/app/core/task/taskdatadetails?taskDataId=" + taskDataId);
}

function doDocument(runId, runProcessId, title) {
    parent.openNewTabs("/app/core/document/dowork?runId=" + runId
        + "&runProcessId=" + runProcessId, title);
}

function readNotice(noticeId) {
    window.open("/app/core/notice/details?noticeId=" + noticeId);
}

function refreshdocument1() {
    var flag = $("#documentdeskdiv1").attr("data-value");
    if (flag == "1") {
        getMyDocument1();
    } else if (flag == "2") {
        getMyDocumentSendTo1();
    } else if (flag == "3") {
        getDraftsDocumentFlowListForDesk1()
    }
}

function refreshdocument2() {
    var flag = $("#documentdeskdiv2").attr("data-value");
    if (flag == "1") {
        getMyDocument1();
    } else if (flag == "2") {
        getMyDocumentSendTo2();
    } else if (flag == "3") {
        getDraftsDocumentFlowListForDesk2()
    }
}

function getdocumentlist1(Obj, flag) {
    $("#documentdeskdiv1").attr("data-value", flag);
    $(".js-document1").each(function () {
        $(this).removeClass("onaction");
    })
    $(Obj).addClass("onaction");
    if (flag == "1") {
        $(".js-document1openmore").attr("data-value",
            "/app/core/document/receiptapproved");
        $(".js-document1openmore").attr("data-title", "公文传阅");
        getMyDocument1();
    } else if (flag == "2") {
        $(".js-document1openmore").attr("data-value",
            "/app/core/document/sendtome");
        $(".js-document1openmore").attr("data-title", "公文抄送");
        getMyDocumentSendTo1();
    } else if (flag == "3") {
        $(".js-document1openmore").attr("data-value",
            "/app/core/document/receiptapproved?view=draft");
        $(".js-document1openmore").attr("data-title", "草稿箱");
        getDraftsDocumentFlowListForDesk1();
    }
}

function getdocumentlist2(Obj, flag) {
    $("#documentdeskdiv2").attr("data-value", flag);
    $(".js-document2").each(function () {
        $(this).removeClass("onaction");
    })
    $(Obj).addClass("onaction");
    if (flag == "1") {
        $(".js-document2openmore").attr("data-value",
            "/app/core/document/dispatchapproved");
        $(".js-document2openmore").attr("data-title", "发文审批");
        getMyDocument2();
    } else if (flag == "2") {
        $(".js-document2openmore").attr("data-value",
            "/app/core/document/sendtome");
        $(".js-document2openmore").attr("data-title", "公文抄送");
        getMyDocumentSendTo2();
    } else if (flag == "3") {
        $(".js-document2openmore").attr("data-value",
            "/app/core/document/dispatchapproved?view=draft");
        $(".js-document2openmore").attr("data-title", "草稿箱");
        getDraftsDocumentFlowListForDesk2();
    }
}

function getbpmlist(Obj, flag) {
    $("#bpmdeskdiv1").attr("data-value", flag);
    $(".js-bpm").each(function () {
        $(this).removeClass("onaction");
    })
    $(Obj).addClass("onaction");
    if (flag == "1") {
        $(".js-bpmopenmore").attr("data-value", "/app/core/bpm/doprocess");
        $(".js-bpmopenmore").attr("data-title", "流程审批");
        getMyBpm();
    } else if (flag == "2") {
        $(".js-bpmopenmore").attr("data-value",
            "/app/core/bpm/doprocess?view=sendtome");
        $(".js-bpmopenmore").attr("data-title", "流程审批");
        getMyBpmSendToMeForDesk();
    } else if (flag == "3") {
        $(".js-bpmopenmore").attr("data-value",
            "/app/core/bpm/doprocess?view=drafts");
        $(".js-bpmopenmore").attr("data-title", "草稿箱");
        getDraftsBpmFlowListForDesk();
    }
}

function refreshbpm() {
    var flag = $("#bpmdeskdiv1").attr("data-value");
    if (flag == "1") {
        getMyBpm();
    } else if (flag == "2") {
        getMyBpmSendToMeForDesk();
    } else if (flag == "3") {
        getDraftsBpmFlowListForDesk();
    }
}

function getDraftsBpmFlowListForDesk() {
    $
        .ajax({
            url: "/ret/bpmget/getDraftsBpmFlowListForDesk",
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.status == 200) {
                    var infoList = data.list;
                    var html = "";
                    for (var i = 0; i < infoList.length; i++) {
                        var flowTitle = infoList[i].flowTitle;
                        html += "<tr><td class='td_inline'><span title=\"" + infoList[i].flowName + "\" style='position: absolute;display: block;width: 65%'>" +
                            "<a href=\"javascript:void(0);window.open('/app/core/bpm/bpmread?runId=" + infoList[i].runId + "&flowId=" + infoList[i].flowId + "')\">NO:"+infoList[i].id+"&nbsp;&nbsp;"+ flowTitle + "</a>"
                            + "</span><span style='float:right'>"
                            + infoList[i].createTime + "&nbsp;&nbsp;"
                            + "<a href=\"javascript:void(0);setBpmEmptyStatus('" + infoList[i].runId + "')\" class=\"btn btn-sky btn-xs\" >加入待办</a>"
                            + "</span></td></tr>";
                    }
                    $("#bpmtbody").html(html);
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function openMore(Obj) {
    var url = $(Obj).attr("data-value");
    var title = $(Obj).attr("data-title");
    parent.openNewTabs(url, title)
}

function getsuperversion(Obj, flag) {
    $("#supdeskdiv").attr("data-value", flag);
    $(".js-sup").each(function () {
        $(this).removeClass("onaction");
    })
    $(Obj).addClass("onaction");
    if (flag == "1") {
        $(".js-supopenmore").attr("data-value",
            "/app/core/superversion/process");
        $(".js-supopenmore").attr("data-title", "督办处理");
        getSuperversionListForDesk();
    } else if (flag == "2") {
        $(".js-supopenmore").attr("data-value",
            "/app/core/superversion/leadmanage");
        $(".js-supopenmore").attr("data-title", "督查管控");
        getManageSuperversionListForDesk();
    }
}

function refreshsup() {
    var flag = $("#supdeskdiv").attr("data-value");
    if (flag == "1") {
        getSuperversionListForDesk();
    } else if (flag == "2") {
        getManageSuperversionListForDesk();
    }
}

function getSuperversionListForDesk() {
    $
        .ajax({
            url: "/ret/superversionget/getSupperversionPorcessListForDesk",
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.status == 200) {
                    var infoList = data.list;
                    var html = "";
                    for (var i = 0; i < infoList.length; i++) {
                        var title = infoList[i].title;
                        var status = infoList[i].status == '0' ? '进行中' : '延期中';
                        html += "<tr><td class='td_inline'><span title=\"" + infoList[i].typeName + "\" style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);readsuperversion('"+ infoList[i].superversionId+ "')\">"
                            + title + "</a></span><span style=\"float:right;\">截止:" + infoList[i].endTime + "&nbsp;&nbsp;督查:" + infoList[i].leadUserName + "</span></td></tr>"
                    }
                    $("#supbody").html(html);
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function getManageSuperversionListForDesk() {
    $
        .ajax({
            url: "/ret/superversionget/getManageSuperversionListForDesk",
            type: "post",
            dataType: "json",
            success: function (data) {
                if (data.status == 200) {
                    var infoList = data.list;
                    var html = "";
                    for (var i = 0; i < infoList.length; i++) {
                        var title = infoList[i].title;
                        html += "<tr><td class='td_inline'><span title=\"" + infoList[i].typeName + "\" style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);readsuperversion('"
                            + infoList[i].superversionId
                            + "')\">" + title + "</a></span><span style=\"float:right;\">截止:" + infoList[i].endTime + "&nbsp;&nbsp;牵头:" + infoList[i].handedUserName + "</span></td></tr>"
                    }
                    $("#supbody").html(html);
                } else if (data.status == 100) {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
}

function getBpmZl() {
    $.ajax({
        url: "/ret/echartsbpmget/getUserWorkForBar",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                var myChart = echarts.init(document.getElementById('bpmzl'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function readsuperversion(supervresionId) {
    window.open("/app/core/superversion/superversiondetails?superversionId="
        + supervresionId);
}


function getDraftsDocumentFlowListForDesk1() {
    $.ajax({
        url: "/ret/documentget/getDraftsDocumentFlowListForDesk1",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var infoList = data.list;
                var html = "";
                for (var i = 0; i < infoList.length; i++) {
                    var flowTitle = infoList[i].flowTitle;
                    html += "<tr><td class='td_inline'><span title='" + infoList[i].flowName + "' style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);doDocument('"
                        + infoList[i].runId + "','"
                        + infoList[i].runProcessId + "','"
                        + infoList[i].flowTitle
                        + "')\">NO:"+infoList[i].id+"&nbsp;&nbsp;"+ flowTitle + "</a>"
                        + "</span><span style=\"float:right\">"
                        + infoList[i].createTime + "&nbsp;&nbsp;"
                        + infoList[i].createUser
                        + "<span></td></tr>";
                }
                $("#documentbody1").html(html);

            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function getDraftsDocumentFlowListForDesk2() {
    $.ajax({
        url: "/ret/documentget/getDraftsDocumentFlowListForDesk2",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == 200) {
                var infoList = data.list;
                var html = "";
                for (var i = 0; i < infoList.length; i++) {
                    var flowTitle = infoList[i].flowTitle;
                    html += "<tr><td class='td_inline'><span title='" + infoList[i].flowName + "' style='position: absolute;display: block;width: 65%'><a href=\"javascript:void(0);doDocument('"
                        + infoList[i].runId + "','"
                        + infoList[i].runProcessId + "','"
                        + infoList[i].flowTitle
                        + "')\">NO:"+infoList[i].id+"&nbsp;&nbsp;"+ flowTitle + "</a>"
                        + "</span><span style=\"float:right\">"
                        + infoList[i].createTime + "&nbsp;&nbsp;"
                        + infoList[i].createUser
                        + "<span></td></tr>";
                }
                $("#documentbody2").html(html);

            } else if (data.status == 100) {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}

function setBpmEmptyStatus(runId) {
    if (confirm("您确定将当前记录加入待办吗？")) {
        $.ajax({
            url: "/set/bpmset/setBpmEmptyStatus",
            type: "post",
            dataType: "json",
            data: {runId: runId},
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    getDraftsBpmFlowListForDesk();
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data);
                }
            }
        });
    }
}
function openFileMapOnLine(extName, attachId, priv, fileId, recordId) {
    $.ajax({
        url: "/set/filemapset/setReadStatus",
        type: "post",
        dataType: "json",
        data: {recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                //layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
    fileId = fileId || '';
    var extName = extName.toUpperCase();
    if (extName == ".TXT" || extName == ".HTML") {
        window.open("/sys/file/readFile?attachId=" + attachId, "_blank");
    } else if (extName == ".DOC" || extName == ".DOT" || extName == ".DOCX" || extName == ".DOTX" || extName == ".WPS" || extName == ".UOF")//
    {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openword?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openword?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            window.open("/office/wps/openword?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == ".XLS" || extName == ".XLSX" || extName == ".CSV" || extName == ".ET" || extName == ".ETT") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openexcel?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openexcel?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            window.open("/office/wps/openexcel?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == ".PPT" || extName == ".PPTX" || extName == ".DPS" || extName == ".DPT") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openppt?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openword?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            window.open("/office/wps/openppt?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == ".PDF") {
        if (officetype == "0") {
            POBrowser.openWindowModeless("/office/openpdf?attachId=" + attachId + "&openModeType=" + priv, "width=1200px;height=800px;");
        } else if (officetype == "1") {
            window.open("/office/goldgrid/openword?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "2") {
            if (ofdtype == "1") {
                window.open("/office/ofd/openfxofd?attachId=" + attachId + "&openModeType=" + priv);
            } else {
                window.open("/module/pdfjs/web/viewer.html?file=" + encodeURIComponent("/sys/file/getFileDown?attachId=" + attachId), "_blank");
            }
        } else if (officetype == "3") {
            window.open("/v1/weboffice/index?attachId=" + attachId + "&openModeType=" + priv);
        } else if (officetype == "4") {
            window.open("/app/core/onlyoffice?attachId=" + attachId + "&openModeType=" + priv + "&fileId=" + fileId);
        }
    } else if (extName == '.JPG' || extName == '.PNG' || extName == '.JPEG' || extName == '.BMP' || extName == '.TIT' || extName == '.GIF') {
        window.open("/sys/file/getImage?attachId=" + attachId, "_blank", "location=no");
    } else if (extName == ".OFD") {
        if (ofdtype == "1") {
            window.open("/office/ofd/openfxofd?attachId=" + attachId + "&openModeType=" + priv, "_blank", "location=no");
        } else if (ofdtype == "2") {
            window.open("/office/ofd/openswofd?attachId=" + attachId + "&openModeType=" + priv, "_blank", "location=no");
        } else {
            window.open("/module/ofd/index.html?ofdFile=" + encodeURIComponent("/sys/file/getFileDown?attachId=" + attachId), "_blank", "location=no");
        }
    } else {
        layer.msg("文件格式不支持在线打开！");
    }
}
