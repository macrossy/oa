$(function () {
    $.ajax({
        url: "/ret/workplanget/getWorkPlanById",
        type: "post",
        dataType: "json",
        data: {planId: planId},
        success: function (data) {
            if (data.status == "200") {
                var workplan = data.list;
                for (var id in workplan) {
                    if (id == "attach") {
                        $("#workplanattach").attr("data_value", workplan.attach);
                        createAttach("workplanattach", 1);
                    } else if (id == "userPriv" || id == "joinUser" || id == "holdUser" || id == "supUser") {
                        $("#" + id).html(getUserNameByStr(workplan[id]));
                    } else if (id == "deptPriv") {
                        $("#" + id).html(getDeptNameByDeptIds(workplan[id]));
                    } else if (id == "levelPriv") {
                        $("#" + id).html(getUserLevelStr(workplan[id]));
                    } else if (id == "planType") {
                        $("#planType").html(getCodeClassName(workplan[id], "workplan"));
                    } else if (id == "remark") {
                        $("#remark").html(workplan[id]);
                    } else {
                        $("#" + id).html(workplan[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
    getWorkPlanProcess();
})

function getWorkPlanProcess() {
    $.ajax({
        url: "/ret/workplanget/getWorkPlanProcessList",
        type: "post",
        dataType: "json",
        data: {planId: planId},
        success: function (data) {
            if (data.status == "200") {
                for (var i = 0; i < data.list.length; i++) {
                    $("#processlist").append("<div class=\"external-event ui-draggable ui-draggable-handle\" style=\"border-left: 4px solid rgb(255, 206, 85); position: relative;\">" + data.list[i].remark + "</div>" +
                        "<div style=\"text-align: right;\"><span>反馈人：" + getUserNameByStr(data.list[i].createUser) + "</span><span style=\"margin-left: 20px;\">反馈时间：" + data.list[i].createTime + "</span></div>" +
                        "<div id=\"attach_" + i + "\" data_value=\"" + data.list[i].attach + "\"><div id=\"show_attach_" + i + "\"></div></div>");
                    createAttach("attach_" + i, 1);
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}
