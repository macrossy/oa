var zTree;
var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/budgetget/getProjectTreeList",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    }
};
$(function () {
    $.ajax({
        url: "/ret/budgetget/getProjectTreeList",
        type: "post",
        dataType: "json",
        success: function (data) {
            zTree = $.fn.zTree.init($("#tree"), setting, data);// 初始化树节点时，添加同步获取的数据
        }
    });
})


function zTreeOnClick(event, treeId, treeNode) {
    $("#myTable").bootstrapTable('destroy');
    query(treeNode.sortId)
    $.ajax({
        url: "/ret/budgetget/getBudgetProjectById",
        type: "post",
        dataType: "json",
        data: {projectId: treeNode.sortId},
        success: function (data) {
            if (data.status == "200") {
                var recordInfo = data.list;
                for (var id in recordInfo) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", recordInfo.attach);
                        createAttach("attach", 1);
                    } else if (id == "chargeUser" || id == "joinUser") {
                        $("#" + id).html(getUserNameByStr(recordInfo[id]));
                    } else if (id == "remark") {
                        $("#remark").html(recordInfo[id]);
                    } else if (id == "budgetAccount") {
                        $("#" + id).html(getBudgetAccountName(recordInfo[id]));
                    } else if (id == "projectType") {
                        $("#" + id).html(getBudgetTypeName(recordInfo[id]));
                    } else {
                        $("#" + id).html(recordInfo[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}


function query(projectId) {
    $("#myTable").bootstrapTable({
        url: '/ret/budgetget/getBudgetCostList?parentId=' + projectId,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',// 工具列
        striped: true,// 隔行换色
        cache: false,// 禁用缓存
        pagination: true,// 启动分页
        sidePagination: 'server',// 分页方式
        pageNumber: 1,// 初始化table时显示的页码
        pageSize: 10,// 每页条目
        showFooter: false,// 是否显示列脚
        showPaginationSwitch: false,// 是否显示 数据条数选择框
        sortable: true,// 排序
        search: false,// 启用搜索
        sortOrder: "asc",
        showColumns: false,// 是否显示 内容列下拉框
        showRefresh: false,// 显示刷新按钮
        idField: 'recordId',// key值栏位
        clickToSelect: false,// 点击选中checkbox
        pageList: [10, 20, 30, 50],// 可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',// 标题 可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'pTitle',
            width: '150px',
            title: '项目名称'
        }, {
            field: 'title',
            width: '150px',
            title: '费用标题'
        }, {
            field: 'projectCode',
            title: '项目编号',
            width: '100px'
        }, {
            field: 'pTotalCost',
            width: '100px',
            title: '项目预算'
        }, {
            field: 'cTotalCost',
            width: '100px',
            title: '费用支出'
        }, {
            field: 'projectType',
            width: '50px',
            title: '项目类型',
            formatter: function (value, row, index) {
                return getBudgetTypeName(value);
            }
        }, {
            field: 'happenTime',
            width: '100px',
            title: '支出时间'
        }, {
            field: 'chargeUser',
            width: '100px',
            title: '项目负责人',
            formatter: function (value, row, index) {
                return getUserNameByStr(value);
            }
        }, {
            field: 'budgetAccount',
            width: '50px',
            title: '预算科目',
            formatter: function (value, row, index) {
                return getBudgetAccountName(value);
            }
        }, {
            field: 'createUser',
            width: '50px',
            title: '上报人',
            formatter: function (value, row, index) {
                return getUserNameByStr(value);
            }
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '120px',
            formatter: function (value, row, index) {
                return createOptBtn(row.recordId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            // alert(row.SystemDesc);
        },
        onLoadSuccess: function (data) {
            var totalcosts = 0;
            for (var i = 0; i < data.rows.length; i++) {
                totalcosts += parseFloat(data.rows[i].cTotalCost);
            }
            var zs = parseFloat($("#totalCost").text())
            $("#paytotalcost").html(totalcosts);
            $("#restotalcost").html(zs - totalcosts);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, // 总页数,前面的key必须为"total"
                    rows: res.list.list
                    // 行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order
    };
    return temp;
};

function createOptBtn(recordId) {
    var html = "<a href=\"javascript:void(0);details('" + recordId + "')\" class=\"btn btn-sky btn-xs\" >详情</a>";
    return html;
}

function details(recordId) {
    window.open("/app/core/budget/costdetails?recordId=" + recordId);
}

function getBudgetTypeName(budgetTypeId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/budgetget/getBudgetTypeById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            budgetTypeId: budgetTypeId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                returnStr = data.list.title;
            }
        }
    });
    return returnStr;
}


function getBudgetAccountName(budgetAccountId) {
    var returnStr = "";
    $.ajax({
        url: "/ret/budgetget/getBudgetAccountById",
        type: "post",
        dataType: "json",
        async: false,
        data: {
            budgetAccountId: budgetAccountId
        },
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                returnStr = data.list.title;
            }
        }
    });
    return returnStr;
}

