$(function () {
    $.ajax({
        url: "/ret/oaget/getVoteItemsList",
        type: "post",
        dataType: "json",
        data: {voteId: voteId},
        success: function (data) {
            if (data.status == "200") {
                var itemList = data.list;
                console.log(itemList);
                for (var i = 0; i < itemList.length; i++) {
                    var title = itemList[i].title;
                    if (itemList[i].remark) {
                        title += "<br/><span style='margin-left: 20px;'>说明：" + itemList[i].remark + "</span>";
                    }

                    var optType = itemList[i].optType;
                    var recordId = itemList[i].recordId;
                    var childItem = getChildItem(voteId, itemList[i].recordId);
                    if (optType == "1") {
                        getRadioItem(i, recordId, title, childItem)
                    } else if (optType == "2") {
                        getCheckBoxItem(i, recordId, title, childItem)
                    }
                }

            } else if (data.statu == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
    $(".js-add-save").unbind("click").click(function () {
        setVoteValue();
    });
    getVoteInfo();
})


function getChildItem(voteId, recordId) {
    var jsonArr;
    $.ajax({
        url: "/ret/oaget/getVoteChildItemsList",
        type: "post",
        dataType: "json",
        async: false,
        data: {voteId: voteId, recordId: recordId},
        success: function (data) {
            if (data.status == "200") {
                jsonArr = data.list;
            } else if (data.statu == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
    return jsonArr;
}


function getVoteValueList() {
    var returtArr = [];
    $(".itemrow").each(function () {
        var json = {};
        var optType = $(this).attr("optType");
        var itemId = $(this).attr("data-id");
        var value = 0;
        var comment = "";
        if (optType == "1") {
            value = $("input:radio[name='" + itemId + "']:checked").val()
        }
        if (optType == "2") {
            var returnStr = [];
            $('input[name="' + itemId + '"]:checked').each(function () {
                returnStr.push($(this).val());
            });
            value = returnStr;
        }
        if (value == "") {
            layer.msg("还有项未投票！");
            return;
        }
        json.itemId = itemId;
        json.optType = optType;
        json.value = value;
        json.comment = comment;
        returtArr.push(json);
    })
    return returtArr;
}

function sum(arr) {
    var len = arr.length;
    if (len == 0) {
        return 0;
    } else if (len == 1) {
        return arr[0];
    } else {
        return arr[0] + sum(arr.slice(1));
    }
}

function setVoteValue() {
    if (confirm("投票完成后不可再次修改？")) {
        $.ajax({
            url: "/ret/oaget/getVoteResultCount",
            type: "post",
            dataType: "json",
            data: {
                voteId: voteId
            },
            success: function (data) {
                if (data.status == "200") {
                    if (data.list == 0) {
                        $.ajax({
                            url: "/set/oaset/addVoteResuletRecord",
                            type: "post",
                            dataType: "json",
                            data: {
                                voteId: voteId,
                                result: JSON.stringify(getVoteValueList())
                            },
                            success: function (res) {
                                if (res.status == "200") {
                                    window.close();
                                } else if (res.status == "100") {
                                    layer.msg(sysmsg[res.msg]);
                                } else {
                                    console.log(res.msg);
                                }
                            }
                        });
                    } else {
                        console.log("您已投过票，不能再投票");
                        layer.msg("您已投过票，不能再投票！");
                    }
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else if (data.status == "500") {
                    console.log(data.msg);
                }
            }
        })
    }
}


function getVoteInfo() {
    $.ajax({
        url: "/ret/oaget/getVoteById",
        type: "post",
        dataType: "json",
        data: {
            voteId: voteId
        },
        success: function (data) {
            if (data.status == "200") {
                $("#voteTitle").html(data.list.title);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else if (data.status == "500") {
                console.log(data.msg);
            }
        }
    })
}

function getRadioItem(index, recordId, title, arr) {
    var html = "<tr  class='itemrow' data-id=\"" + recordId + "\" optType='1'><td>" + (index + 1) + ":" + title;
    for (var i = 0; i < arr.length; i++) {
        html += "<div class=\"radio\"><label><input name=\"" + recordId + "\" type=\"radio\" value=\"" + arr[i].recordId + "\">" +
            "<span class=\"text\">【" + arr[i].title + "】&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + arr[i].remark + "</span></label></div>";
    }
    html += "</td></tr>"
    $("#child-item-table").append(html);
}

function getCheckBoxItem(index, recordId, title, arr) {
    var html = "<tr class='itemrow' data-id=\"" + recordId + "\" optType='2'><td>" + (index + 1) + ":" + title;
    for (var i = 0; i < arr.length; i++) {
        html += "<div class=\"checkbox\"><label><input name=\"" + recordId + "\" type=\"checkbox\" value=\"" + arr[i].recordId + "\">" +
            "<span class=\"text\">【" + arr[i].title + "】&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + arr[i].remark + "</span></label></div>";
    }
    html += "</td></tr>"
    $("#child-item-table").append(html);
}
