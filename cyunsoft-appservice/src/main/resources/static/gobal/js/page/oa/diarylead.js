$(function () {
    query();
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    $(".js-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    });
    getsubordinates();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/oaget/getMySubordinatesDiaryList',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'diaryId',//key值栏位
        clickToSelect: false,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [
            {
                field: 'num',
                title: sysmsg['SORT_NO'],//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            }, {
                field: 'title',
                title: oamsg['TITLE'],
                width: '200px',
                formatter: function (value, row, index) {
                    return "<a href=\"javascript:void(0);readDiary('" + row.diaryId + "')\" style='cursor: pointer;' >" + value + "</a>";
                }
            }, {
                field: 'diaryDay',
                title: oamsg['DIARY_DATE'],
                sortable: true,
                width: '100px'
            },
            {
                field: 'diaryType',
                width: '100px',
                title: oamsg['DIARY_TYPE'],
                formatter: function (value, row, index) {
                    if (value == "1") {
                        return oamsg['DIARY_PERSONAL'];
                    } else if (value == "") {
                        return oamsg['DIARY_DEPT'];
                    } else {
                        return sysmsg['UNKNOWN'];
                    }
                }
            },
            {
                field: 'createUserName',
                width: '100px',
                title: oamsg['DIARY_SUB_NAME']
            },
            {
                field: 'createTime',
                width: '100px',
                title: sysmsg['CREATE_TIME']
            }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        accountId: $("#accountId").val(),
        beginTime: $("#beginTime").val(),
        endTime: $("#endTime").val()
    };
    return temp;
};

function readDiary(diaryId) {
    window.open("/app/core/diary/readdiary?diaryId=" + diaryId);
}


function getsubordinates() {
    $.ajax({
        url: "/ret/sysget/getMySubordinates",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                var html = "<option value=''>全部</option>"
                for (var i = 0; i < data.list.length; i++) {
                    html += "<option value='" + data.list[i].accountId + "'>" + data.list[i].userName + "</option>"
                }
                $("#accountId").html(html);
            }
        }
    })
}
