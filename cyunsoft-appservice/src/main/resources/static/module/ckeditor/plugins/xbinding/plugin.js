CKEDITOR.plugins.add('xbinding', {
    init: function (editor) {
        editor.addCommand('xbinding', {
            exec: function () {
                $("#xplugin").html(xbindingmodal);
                getDbSource();
                if (bpmfield == "auto") {
                    $(".autodiv").each(function () {
                        $(this).remove();
                    });
                }
                $("#dataSource").unbind("change").change(function(){
                    if($(this).val()=="12")
                    {
                        $(".dbDiv").show();
                    }else
                    {
                        $(".dbDiv").hide();
                    }
                });
                var selection = top.editor.getSelection();
                var element = selection.getSelectedElement();
                if (element) {
                    var xtype = element.getAttribute('xtype');
                    if (xtype != "xbinding") {
                        return;
                    }
                    $("#name").val(element.getAttribute('name'));
                    $("#name").attr("readonly", "readonly");
                    $("#dataType").val(element.getAttribute('dataType'));
                    $("#style").val(element.getAttribute('style'));
                    $("#title").val(element.getAttribute('title'));
                    var model = element.getAttribute('model');
                    var json = JSON.parse(model);
                    if(json.dataSource=="12")
                    {
                        $("#dbSource").val(json.dbSource);
                        $("#sql").val(json.sql);
                        $(".dbDiv").show();
                    }
                    $("#dataSource").val(json.dataSource);
                    $("#binDingMap").val(json.binDingMap);
                } else {
                    getDbSource();
                    $("#dataSource").unbind("change").change(function(){
                        if($(this).val()=="12")
                        {
                            $(".dbDiv").show();
                        }else
                        {
                            $(".dbDiv").hide();
                        }
                    });
                    if (bpmfield == "auto") {
                        var content = editor.getData();
                        $("#name").attr("readonly", "readonly");
                        var maxName = 0;
                        $(content).find('*[xtype]').each(function () {
                            var thisName = $(this).attr("name");
                            var thisCount = thisName.substring(7, thisName.length);
                            if (parseInt(thisCount) > maxName) {
                                maxName = parseInt(thisCount);
                            }
                        })
                        $("#name").val(getFieldNameAuto(maxName));
                    }
                }
                $(".js-saveinputbtn").unbind('click').click(function () {
                    var name = $("#name").val();
                    var dataType = $("#dataType").val();
                    var style = $("#style").val();
                    var title = $("#title").val();
                    var dataSource = $("#dataSource").val();
                    var binDingMap = $("#binDingMap").val();
                    var json = {};
                    json.dataSource = dataSource;
                    json.binDingMap = binDingMap;
                    if(dataSource=="12")
                    {
                        var dbSource = $("#dbSource").val();
                        var sql = $("#sql").val().replace(/\'/g, "&#39;");
                        json.dbSource = dbSource;
                        json.sql = sql;
                    }
                    var modelStr = JSON.stringify(json)
                    var html = "<img src='/module/ckeditor/plugins/xbinding/search.png'" +
                        "xtype='xbinding' " +
                        "title='" + title + "' " +
                        "name='" + name + "' " +
                        "dataType='" + dataType + "' " +
                        "model='" + modelStr + "' " +
                        "style='" + style + "'/>";
                    if (title == "") {
                        layer.msg("字段标题不能为空!");
                        return false;
                    }
                    if ($("#name").attr("readonly") == "readonly") {
                        editor.insertHtml(html);
                        $("#xmodal").modal("hide");
                    } else {
                        var content = editor.getData();
                        $(content).find('*[xtype]').each(function () {
                            fieldList[$(this).attr("name")] = $(this).attr("title");
                        });
                        if (fieldList.hasOwnProperty(name)) {
                            layer.msg(title + "字段已存在,请检查!");
                            return false;
                        }
                        editor.insertHtml(html);
                        $("#xmodal").modal("hide");
                    }
                });
                $('#xmodal').modal("show");
            }
        });
        if (editor.addMenuItems) {
            editor.addMenuItems({
                xbinding: {
                    label: "绑定组件",
                    command: 'xbinding',
                    group: 'xbinding',
                    order: 1
                }
            });
        }
        if (editor.contextMenu) {
            editor.contextMenu.addListener(function (element, selection) {
                if (!element || element.isReadOnly()) return null;
                var isInput = element.hasAscendant('img', 1);
                if (isInput && element.getAttribute('xtype') == "xbinding") {
                    return {
                        xbinding: CKEDITOR.TRISTATE_OFF
                    };
                }
                return null;
            });
        }
    }
});

function getDbSource() {
    $.ajax({
        url: "/ret/sysget/getDbSourceList",
        type: "POST",
        dataType: "json",
        async: false,
        success: function (data) {
            if (data.status == 200) {
                var html = "<option value='SYS_DB'>当前系统</option>";
                for (var i = 0; i < data.list.length; i++) {
                    html += "<option value='" + data.list[i].dbSourceId + "'>" + data.list[i].dbSourceName + "</option>";
                }
                $("#dbSource").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
}


var xbindingmodal = ['	 <div class="modal fade bs-example-modal-lg" id="xmodal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">',
    '        <div class="modal-dialog">',
    '            <div class="modal-content">',
    '                <div class="modal-header">',
    '                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>',
    '                    <h4 class="modal-title" id="myLargeModalLabel">绑定组件</h4>',
    '                </div>',
    '                <div class="modal-body">',
    '                <form class="form-horizontal" role="form" id="xbindingform">',
    '                	<div class="form-group">',
    '	                    <label class="col-sm-2 control-label no-padding-right">控件标题</label>',
    '	                    <div class="col-sm-10">',
    '	                    <input type="text" class="form-control" id="title" name="title" placeholder="控件标题">',
    '	                    </div>',
    '	                </div>',
    '	                <div class="form-group">',
    '	                    <label class="col-sm-2 control-label no-padding-right">字段名称</label>',
    '	                    <div class="col-sm-10">',
    '	                    <input type="text" class="form-control" id="name" name="name" placeholder="字段名称">',
    '	                </div>',
    '	                </div>',
    '	                <div class="form-group autodiv">',
    '	                    <label class="col-sm-2 control-label no-padding-right">字段类型</label>',
    '	                    <div class="col-sm-10">',
    '	                    <select class="form-control" id="dataType" name="dataType" placeholder="字段类型">',
    '							<option>请选择</option>',
    '							<option value="int">数值型</option>',
    '							<option value="varchar">字符型</option>',
    '							<option value="text">文本型</option>',
    '						</select>',
    '	                	</div>',
    '	                </div>',
    '	                <div class="form-group">',
    '	                    <label class="col-sm-2 control-label no-padding-right">数据来源</label>',
    '	                    <div class="col-sm-10">',
    '	                    <select id="dataSource" name="dataSource" style="border-radius: 0px">' +
    '                           <option value="0">人员选择[单选]</option>' +
    '                           <option value="1">人员选择[多选]</option>' +
    '                           <option value="2">部门选择[单选]</option>' +
    '                           <option value="3">部门选择[多选]</option>' +
    '                           <option value="4">行政级别[单选]</option>' +
    '                           <option value="5">行政级别[多选]</option>' +
    '                           <option value="6">HR人员选择[单选]</option>' +
    '                           <option value="7">HR人员选择[多选]</option>' +
    '                           <option value="8">HR部门选择[单选]</option>' +
    '                           <option value="9">HR部门选择[多选]</option>' +
    '                           <option value="10">HR行政级别[单选]</option>' +
    '                           <option value="11">HR行政级别[多选]</option>' +
    '                           <option value="12">Sql查询</option>' +
    '                       </select>',
    '	               		</div>',
    '	                </div>',
    '	                <div class="dbDiv" style="display: none;">',
    '	                <div class="form-group">',
    '	                    <label class="col-sm-2 control-label no-padding-right">数据源</label>',
    '	                    <div class="col-sm-10">',
    '	                    <select id="dbSource" name="dbSource" style="border-radius: 0px"></select>',
    '	                </div>',
    '	                </div>',
    '	                <div class="form-group">',
    '	                    <label class="col-sm-2 control-label no-padding-right">执行SQL</label>',
    '	                    <div class="col-sm-10">',
    '	                    <textarea rows="5" class="form-control" id="sql" name="sql"></textarea>',
    '	                    </div>',
    '	                </div>',
    '	                </div>',
    '	                <div class="form-group">',
    '	                    <label class="col-sm-2 control-label no-padding-right">绑定对像</label>',
    '	                    <div class="col-sm-10">',
    '	                    <textarea rows="5" class="form-control" id="binDingMap" name="binDingMap"></textarea>',
    '	               		</div>',
    '	                </div>',
    '	                <div class="form-group">',
    '	                    <label class="col-sm-2 control-label no-padding-right">控件样式</label>',
    '	                    <div class="col-sm-10">',
    '	                    <textarea rows="5" class="form-control" id="style" name="style"></textarea>',
    '	                </div>',
    '	                </div>',
    '                </form>',
    '                </div>',
    '                <div class="modal-footer">',
    '                  <button type="button" class="btn btn-warning" data-dismiss="modal">取消</button>',
    '                 <button type="button" class="btn btn-primary js-saveinputbtn">确定</button>',
    '                </div>',
    '            </div>',
    '        </div>',
    '    </div>'].join("");
