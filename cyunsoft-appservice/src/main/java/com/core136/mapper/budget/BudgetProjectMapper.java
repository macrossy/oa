package com.core136.mapper.budget;

import com.core136.bean.budget.BudgetProject;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface BudgetProjectMapper extends MyMapper<BudgetProject> {
    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getProjectTreeList
     * @Description:  获取项目列表
     */
    public List<Map<String, Object>> getProjectTreeList(@Param(value = "orgId") String orgId, @Param(value = "levelId") String levelId);

    /**
     * @param orgId
     * @param projectType
     * @param chargeUser
     * @param budgetAccount
     * @param search
     * @return List<Map < String, String>>
     * @Title: getBudgetProjectList
     * @Description:  获取项目列表
     */
    public List<Map<String, String>> getBudgetProjectList(@Param(value = "orgId") String orgId, @Param(value = "projectType") String projectType,
                                                          @Param(value = "chargeUser") String chargeUser, @Param(value = "budgetAccount") String budgetAccount, @Param(value = "opFlag") String opFlag,
                                                          @Param(value = "accountId") String accountId, @Param(value = "search") String search
    );

    /**
     * @param orgId
     * @param parentId
     * @return List<Map < String, String>>
     * @Title: getBudgetChildProjectList
     * @Description:  获取子项目列表
     */
    public List<Map<String, String>> getBudgetChildProjectList(@Param(value = "orgId") String orgId, @Param(value = "parentId") String parentId);

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getBudgetProjectListForProject
     * @Description:  获取主项目列表
     */
    public List<Map<String, String>> getBudgetProjectListForProject(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag, @Param(value = "accountId") String accountId);

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param parenetId
     * @return List<Map < String, String>>
     * @Title: getBudgetChildProjectListForProject
     * @Description:  获取子项目列表
     */
    public List<Map<String, String>> getBudgetChildProjectListForProject(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag, @Param(value = "accountId") String accountId, @Param(value = "parentId") String parenetId);


}
