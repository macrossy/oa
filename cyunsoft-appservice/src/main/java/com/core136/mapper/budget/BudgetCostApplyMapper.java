package com.core136.mapper.budget;

import com.core136.bean.budget.BudgetCostApply;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface BudgetCostApplyMapper extends MyMapper<BudgetCostApply> {
    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param projectId
     * @param beginTime
     * @param endTime
     * @param status
     * @param search
     * @return List<Map < String, String>>
     * @Title: getBudgetCostApplyList
     * @Description:  获取申请记录列表
     */
    public List<Map<String, String>> getBudgetCostApplyList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag,
                                                            @Param(value = "accountId") String accountId, @Param(value = "projectId") String projectId,
                                                            @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                            @Param(value = "status") String status, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param accountId
     * @param projectId
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getBudgetCostApprpvalList
     * @Description:  获取预算费用审批记录
     */
    public List<Map<String, String>> getBudgetCostApprpvalList(@Param(value = "orgId") String orgId,
                                                               @Param(value = "accountId") String accountId, @Param(value = "projectId") String projectId,
                                                               @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param accountId
     * @param projectId
     * @param beginTime
     * @param endTime
     * @param status
     * @param search
     * @return List<Map < String, String>>
     * @Title: getBudgetCostApprpvalOldList
     * @Description:  获取预算费用历史审批记录
     */
    public List<Map<String, String>> getBudgetCostApprpvalOldList(@Param(value = "orgId") String orgId,
                                                                  @Param(value = "accountId") String accountId, @Param(value = "projectId") String projectId,
                                                                  @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime,
                                                                  @Param(value = "status") String status, @Param(value = "search") String search);

}
