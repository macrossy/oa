package com.core136.mapper.budget;

import com.core136.bean.budget.BudgetAdjustment;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface BudgetAdjustmentMapper extends MyMapper<BudgetAdjustment> {

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param status
     * @param projectId
     * @param approvalUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getAdjustmentApplayList
     * @Description:  获取申请列表
     */
    public List<Map<String, String>> getAdjustmentApplayList(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag,
                                                             @Param(value = "accountId") String accountId, @Param(value = "status") String status,
                                                             @Param(value = "projectId") String projectId, @Param(value = "approvalUser") String approvalUser, @Param(value = "beginTime") String beginTime,
                                                             @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param accountId
     * @param projectId
     * @param applyUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getAdjustmentApprovalList
     * @Description:  获取等审批列表
     */
    public List<Map<String, String>> getAdjustmentApprovalList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                               @Param(value = "projectId") String projectId, @Param(value = "applyUser") String applyUser, @Param(value = "beginTime") String beginTime,
                                                               @Param(value = "endTime") String endTime, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param accountId
     * @param projectId
     * @param applyUser
     * @param beginTime
     * @param endTime
     * @param status
     * @param search
     * @return List<Map < String, String>>
     * @Title: getAdjustmentApprovalOldList
     * @Description:  获取等审批记录查询
     */
    public List<Map<String, String>> getAdjustmentApprovalOldList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                                  @Param(value = "projectId") String projectId, @Param(value = "applyUser") String applyUser, @Param(value = "beginTime") String beginTime,
                                                                  @Param(value = "endTime") String endTime, @Param(value = "status") String status, @Param(value = "search") String search);

}
