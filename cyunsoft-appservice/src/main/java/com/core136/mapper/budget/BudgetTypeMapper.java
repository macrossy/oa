package com.core136.mapper.budget;

import com.core136.bean.budget.BudgetType;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface BudgetTypeMapper extends MyMapper<BudgetType> {

    public List<Map<String, String>> getBudgetTypeList(@Param(value = "orgId") String orgId);

    public List<Map<String, String>> getBudgetTypeListForSelect(@Param(value = "orgId") String orgId);

}
