package com.core136.mapper.oa;

import com.core136.bean.oa.AddressBook;
import com.core136.common.dbutils.MyMapper;
import io.lettuce.core.dynamic.annotation.Param;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface AddressBookMapper extends MyMapper<AddressBook> {

    /**
     * @param orgId
     * @param accountId
     * @param bookType
     * @return List<AddressBook>
     * @Title: getMyShareAddressBookList
     * @Description:  获取他人共享的人员列表
     */
    public List<AddressBook> getMyShareAddressBookList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "bookType") String bookType);

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getQueryAddressBookList
     * @Description:  查询通讯录人员
     */
    public List<Map<String, String>> getQueryAddressBookList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param levelId
     * @return List<Map < String, String>>
     * @Title: getMyFriendsByLevel
     * @Description:  获取同事列表
     */
    public List<Map<String, String>> getMyFriendsByLevel(@Param(value = "orgId") String orgId, @Param(value = "levelId") String levelId);

    /**
     * @param orgId
     * @param accountId
     * @return Map<String, String>
     * @Title: getMyFriendsInfo
     * @Description:  获取同事联系方式
     */
    public Map<String, String> getMyFriendsInfo(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

}
