package com.core136.mapper.oa;

import com.core136.bean.oa.VoteResult;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface VoteResultMapper extends MyMapper<VoteResult> {
    /**
     * @param orgId
     * @param voteId
     * @return List<Map < String, String>>
     * @Title: getVoteResult
     * @Description:  获取投票结果
     */
    public List<Map<String, String>> getVoteResult(@Param(value = "orgId") String orgId, @Param(value = "voteId") String voteId);

    /**
     * @param orgId
     * @param voteId
     * @return List<Map < String, String>>
     * @Title: voteDetailsForUser
     * @Description:  获取投票人员列表
     */
    public List<Map<String, String>> voteDetailsForUser(@Param(value = "orgId") String orgId, @Param(value = "voteId") String voteId, @Param(value = "recordId") String recordId);
}
