package com.core136.mapper.oa;

import com.core136.bean.oa.LeaderMailboxConfig;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LeaderMailboxConfigMapper extends MyMapper<LeaderMailboxConfig> {

}
