package com.core136.mapper.oa;

import com.core136.bean.oa.DiaryComments;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface DiaryCommentsMapper extends MyMapper<DiaryComments> {

    /**
     * @Title: getDiaryCommentsList
     * @Description:  获取评论列表
     * @param: orgId
     * @param: diaryId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getDiaryCommentsList(@Param(value = "orgId") String orgId, @Param(value = "diaryId") String diaryId);

    /**
     * @Title: getMyDiaryCommentsCount
     * @Description:  获取评论数
     * @param: orgId
     * @param: accountId
     * @param: @return
     * @return: Integer
     */
    public Integer getMyDiaryCommentsCount(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId);

}
