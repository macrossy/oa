package com.core136.mapper.oa;

import com.core136.bean.oa.Shortcut;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ShortcutMapper extends MyMapper<Shortcut> {

}
