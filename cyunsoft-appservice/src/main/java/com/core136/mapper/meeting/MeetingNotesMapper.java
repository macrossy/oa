/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: MeetingNotesMapper.java
 * @Package com.core136.mapper.meeting
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月31日 上午10:55:49
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.mapper.meeting;

import com.core136.bean.meeting.MeetingNotes;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 *
 */
@Mapper
public interface MeetingNotesMapper extends MyMapper<MeetingNotes> {

    /**
     *
     * @Title: getMeetingNotesList
     * @Description:  获取会议记要列表
     * @param: orgId
     * @param: opFlag
     * @param: accountId
     * @param: beginTime
     * @param: endTime
     * @param: roomId
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>

     */
    public List<Map<String, String>> getMeetingNotesList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "accountId") String accountId,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "search") String search
    );

    /**
     *
     * @Title: queryMeetingNotesList
     * @Description:  会议记要查询
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param deptId
     * @param levelPriv
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     * List<Map < String, String>>

     */
    public List<Map<String, String>> queryMeetingNotesList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "accountId") String accountId,
            @Param(value = "deptId") String deptId,
            @Param(value = "levelPriv") String levelPriv,
            @Param(value = "beginTime") String beginTime,
            @Param(value = "endTime") String endTime,
            @Param(value = "search") String search
    );


    /**
     *
     * @Title: getMeetingNotesInfo
     * @Description:  获取会议记要详情
     * @param orgId
     * @param notesId
     * @return
     * Map<String, String>

     */
    public Map<String, String> getMeetingNotesInfo(@Param(value = "orgId") String orgId, @Param(value = "notesId") String notesId);
}
