package com.core136.mapper.exam;

import com.core136.bean.exam.ExamQuestionRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ExamQuestionRecordMapper extends MyMapper<ExamQuestionRecord> {

    /**
     * 获取试卷列表
     *
     * @param orgId
     * @param search
     * @return
     */
    public List<Map<String, String>> getExamQuestionsRecordList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);

    /**
     * 获取试卷列表
     *
     * @param orgId
     * @return
     */
    public List<Map<String, String>> getExamQuestionRecordForSelect(@Param(value = "orgId") String orgId);

}
