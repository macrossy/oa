package com.core136.mapper.vehicle;

import com.core136.bean.vehicle.VehicleOperator;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface VehicleOperatorMapper extends MyMapper<VehicleOperator> {

}
