package com.core136.mapper.vehicle;

import com.core136.bean.vehicle.VehicleDriver;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface VehicleDriverMapper extends MyMapper<VehicleDriver> {

    public List<Map<String, String>> getVehicleDriverList(@Param(value = "orgId") String orgId);

    public List<Map<String, String>> getVehicleDriverListForSelect(@Param(value = "orgId") String orgId);


}
