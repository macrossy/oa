package com.core136.mapper.echarts;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface EchartsHrMapper {

    /**
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title: getHighsetShoolPie
     * @Description:  获取HR学历占比
     */
    public List<Map<String, Object>> getHighsetShoolPie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title: getWorkTypeBar
     * @Description:  获取HR人员的工种占比
     */
    public List<Map<String, Object>> getWorkTypeBar(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getNativePlacePie
     * @Description:  HR人员籍贯占比
     */
    public List<Map<String, String>> getNativePlacePie(@Param(value = "orgId") String orgId);

    /**
     * @param orgId
     * @param deptId
     * @param module
     * @param groupBy
     * @return List<Map < String, String>>
     * @Title: getBaseInfoAnalysis
     * @Description:  人事档案分析
     */
    public List<Map<String, String>> getBaseInfoAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy
    );

    /**
     * @param orgId
     * @param deptId
     * @param module
     * @param groupBy
     * @return List<Map < String, String>>
     * @Title: getContractAnalysis
     * @Description:  人事合同分析
     */
    public List<Map<String, String>> getContractAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

    /**
     * @param orgId
     * @param deptId
     * @param module
     * @param groupBy
     * @return List<Map < String, String>>
     * @Title: getIncentiveAnalysis
     * @Description:  合同奖惩分析
     */
    public List<Map<String, String>> getIncentiveAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

    /**
     * @param orgId
     * @param deptId
     * @param module
     * @param groupBy
     * @return List<Map < String, String>>
     * @Title: getLicenceAnalysis
     * @Description: 证照分析
     */
    public List<Map<String, String>> getLicenceAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

    /**
     * @param orgId
     * @param deptId
     * @param module
     * @param groupBy
     * @return List<Map < String, String>>
     * @Title: getLearnAnalysis
     * @Description:  学习经历分析
     */
    public List<Map<String, String>> getLearnAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

    /**
     * @param orgId
     * @param deptId
     * @param module
     * @param groupBy
     * @return List<Map < String, String>>
     * @Title: getSkillsAnalysis
     * @Description:  劳动技能分析
     */
    public List<Map<String, String>> getSkillsAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

    /**
     * @param orgId
     * @param deptId
     * @param module
     * @param groupBy
     * @return List<Map < String, String>>
     * @Title: getTransferAnalysis
     * @Description:  人事调动分析
     */
    public List<Map<String, String>> getTransferAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

    /**
     * @param orgId
     * @param deptId
     * @param module
     * @param groupBy
     * @return List<Map < String, String>>
     * @Title: getLevelAnalysis
     * @Description:  离职分析
     */
    public List<Map<String, String>> getLevelAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

    /**
     * @param orgId
     * @param deptId
     * @param module
     * @param groupBy
     * @return List<Map < String, String>>
     * @Title: getReinstatAnalysis
     * @Description:  人员复职分析
     */
    public List<Map<String, String>> getReinstatAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

    /**
     * @param orgId
     * @param deptId
     * @param module
     * @param groupBy
     * @return List<Map < String, String>>
     * @Title: getEvaluationAnalysis
     * @Description:  职称评定分析
     */
    public List<Map<String, String>> getEvaluationAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);

    /**
     * @param orgId
     * @param deptId
     * @param module
     * @param groupBy
     * @return List<Map < String, String>>
     * @Title: getCareAnalysis
     * @Description:  人员关怀分析
     */
    public List<Map<String, String>> getCareAnalysis(
            @Param(value = "orgId") String orgId,
            @Param(value = "deptId") String deptId,
            @Param(value = "module") String module,
            @Param(value = "groupBy") String groupBy);


}
