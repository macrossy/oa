package com.core136.mapper.discuss;

import com.core136.bean.discuss.DiscussRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface DiscussRecordMapper extends MyMapper<DiscussRecord> {

    /**
     * 获取帖子与子帖
     *
     * @param orgId
     * @param recordId
     * @return
     */
    public List<Map<String, String>> getDiscussRecordListById(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "recordId") String recordId);

    /**
     * @param orgId
     * @param discussId
     * @return Map<String, String>
     * @Title: getLastDiscussRecordTime
     * @Description:  获取最后发帖时间
     */
    public Map<String, String> getLastDiscussRecordTime(@Param(value = "orgId") String orgId, @Param(value = "discussId") String discussId);

    /**
     * @param orgId
     * @param discussId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getTopDiscussRecordList
     * @Description:  获取讨论主题列表
     */
    public List<Map<String, String>> getTopDiscussRecordList(@Param(value = "orgId") String orgId, @Param(value = "discussId") String discussId, @Param(value = "accountId") String accountId, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getTopDiscussRecordApprovalList
     * @Description:  获取待审批讨论区主题列表
     */
    public List<Map<String, String>> getTopDiscussRecordApprovalList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId, @Param(value = "search") String search);


}
