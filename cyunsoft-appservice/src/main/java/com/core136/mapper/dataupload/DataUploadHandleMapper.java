package com.core136.mapper.dataupload;

import com.core136.bean.dataupload.DataUploadHandle;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DataUploadHandleMapper extends MyMapper<DataUploadHandle> {

}
