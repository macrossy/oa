package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesBorrowVolume;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ArchivesBorrowVolumeMapper extends MyMapper<ArchivesBorrowVolume> {

    /**
     * @param orgId
     * @param approvalStatus
     * @param opFlag
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getArchivesBorrowVolumeList
     * @Description:  案卷借阅记录
     */
    public List<Map<String, String>> getArchivesBorrowVolumeList(@Param(value = "orgId") String orgId,
                                                                 @Param(value = "approvalStatus") String approvalStatus,
                                                                 @Param(value = "opFlag") String opFlag,
                                                                 @Param(value = "accountId") String accountId);

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getArchivesBorrowVolumeApprovalList
     * @Description:  获取待审批记录
     */
    public List<Map<String, String>> getArchivesBorrowVolumeApprovalList(@Param(value = "orgId") String orgId,
                                                                         @Param(value = "opFlag") String opFlag,
                                                                         @Param(value = "accountId") String accountId);

    /**
     * @param orgId
     * @param opFlag
     * @param approvalStatus
     * @param accountId
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getArchivesBorrowVolumeApprovalOldList
     * @Description:  获取历史审批记录
     */
    public List<Map<String, String>> getArchivesBorrowVolumeApprovalOldList(@Param(value = "orgId") String orgId,
                                                                            @Param(value = "opFlag") String opFlag, @Param(value = "approvalStatus") String approvalStatus,
                                                                            @Param(value = "accountId") String accountId, @Param(value = "createUser") String createUser,
                                                                            @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

}
