package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesBorrowFile;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ArchivesBorrowFileMapper extends MyMapper<ArchivesBorrowFile> {

    /**
     * @param orgId
     * @param approvalStatus
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getArchivesBorrowFileList
     * @Description:  文件借阅记录
     */
    public List<Map<String, String>> getArchivesBorrowFileList(@Param(value = "orgId") String orgId,
                                                               @Param(value = "approvalStatus") String approvalStatus,
                                                               @Param(value = "opFlag") String opFlag,
                                                               @Param(value = "accountId") String accountId);

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getArchivesBorrowFileApprovalList
     * @Description:  获取待审批记录
     */
    public List<Map<String, String>> getArchivesBorrowFileApprovalList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "accountId") String accountId);

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getArchivesBorrowFileApprovalOldList
     * @Description:  查询历史审批记录
     */
    public List<Map<String, String>> getArchivesBorrowFileApprovalOldList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag, @Param(value = "approvalStatus") String approvalStatus,
            @Param(value = "accountId") String accountId, @Param(value = "createUser") String createUser,
            @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

}
