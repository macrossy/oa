package com.core136.mapper.archives;

import com.core136.bean.archives.ArchivesVolume;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ArchivesVolumeMapper extends MyMapper<ArchivesVolume> {

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getArchivesVolumeList
     * @Description:  获取案卷列表
     */
    public List<Map<String, String>> getArchivesVolumeList(
            @Param(value = "orgId") String orgId,
            @Param(value = "opFlag") String opFlag,
            @Param(value = "accountId") String accountId,
            @Param(value = "search") String search
    );

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getArchivesVolumeListForSelect
     * @Description:  获取案卷列表
     */
    public List<Map<String, String>> getArchivesVolumeListForSelect(@Param(value = "orgId") String orgId, @Param(value = "opFlag") String opFlag, @Param(value = "accountId") String accountId);

    /**
     * @param orgId
     * @param repositoryId
     * @return List<Map < String, String>>
     * @Title: getArchivesVolumeByRepositoryId
     * @Description:  获取卷库下的案卷列表
     */
    public List<Map<String, String>> getArchivesVolumeByRepositoryId(@Param(value = "orgId") String orgId, @Param(value = "repositoryId") String repositoryId);


}
