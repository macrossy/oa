package com.core136.mapper.officesupplies;

import com.core136.bean.officesupplies.OfficeSuppliesGrant;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface OfficeSuppliesGrantMapper extends MyMapper<OfficeSuppliesGrant> {
    /**
     * @Title: getGrantCount
     * @Description:  统计已发放了多少办公用品
     * @param: orgId
     * @param: applyId
     * @param: @return
     * @return: int
     */
    public int getGrantCount(@Param(value = "orgId") String orgId, @Param(value = "applyId") String applyId);

    /**
     * @param orgId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getGrantOfficeList
     * @Description:  获取办公用品发放列表
     */
    public List<Map<String, String>> getGrantOfficeList(@Param(value = "orgId") String orgId, @Param(value = "accountId") String accountId,
                                                        @Param(value = "beginTime") String beginTime, @Param(value = "endTime") String endTime);

}
