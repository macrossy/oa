package com.core136.mapper.news;

import com.core136.bean.news.NewsComments;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface NewsCommentsMapper extends MyMapper<NewsComments> {

    /**
     * @Title: getCommentsList
     * @Description:  获取新闻下的所有评论
     * @param: orgId
     * @param: newsId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getCommentsList(@Param(value = "orgId") String orgId, @Param(value = "newsId") String newsId);

}
