package com.core136.service.fixedassets;

import com.core136.bean.fixedassets.FixedAssets;
import com.core136.bean.fixedassets.FixedAssetsAllocation;
import com.core136.bean.fixedassets.FixedAssetsApply;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.fixedassets.FixedAssetsAllocationMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class FixedAssetsAllocationService {
    private FixedAssetsAllocationMapper fixedAssetsAllocationMapper;

    @Autowired
    public void setFixedAssetsAllocationMapper(FixedAssetsAllocationMapper fixedAssetsAllocationMapper) {
        this.fixedAssetsAllocationMapper = fixedAssetsAllocationMapper;
    }

    private FixedAssetsService fixedAssetsService;

    @Autowired
    public void setFixedAssetsService(FixedAssetsService fixedAssetsService) {
        this.fixedAssetsService = fixedAssetsService;
    }

    private FixedAssetsApplyService fixedAssetsApplyService;

    @Autowired
    public void setFixedAssetsApplyService(FixedAssetsApplyService fixedAssetsApplyService) {
        this.fixedAssetsApplyService = fixedAssetsApplyService;
    }

    public int insertFixedAssetsAllocation(FixedAssetsAllocation fixedAssetsAllocation) {
        return fixedAssetsAllocationMapper.insert(fixedAssetsAllocation);
    }

    public int deleteFixedAssetsAllocation(FixedAssetsAllocation fixedAssetsAllocation) {
        return fixedAssetsAllocationMapper.delete(fixedAssetsAllocation);
    }

    public int updateFixedAssetsAllocation(Example example, FixedAssetsAllocation fixedAssetsAllocation) {
        return fixedAssetsAllocationMapper.updateByExampleSelective(fixedAssetsAllocation, example);
    }

    public FixedAssetsAllocation selectOneFixedAssetsAllocation(FixedAssetsAllocation fixedAssetsAllocation) {
        return fixedAssetsAllocationMapper.selectOne(fixedAssetsAllocation);
    }

    /**
     * @Title: addFixedAssetsAllocation
     * @Description:  调拨
     */
    @Transactional(value = "generalTM")
    public RetDataBean addFixedAssetsAllocation(FixedAssetsAllocation fixedAssetsAllocation, String applyId) {
        FixedAssets fixedAssets = new FixedAssets();
        fixedAssets.setAssetsId(fixedAssetsAllocation.getAssetsId());
        fixedAssets.setOrgId(fixedAssetsAllocation.getOrgId());
        fixedAssets = fixedAssetsService.selectOneFixedAssets(fixedAssets);
        if (fixedAssets.getStatus().equals("0")) {
            FixedAssetsApply fixedAssetsApply = new FixedAssetsApply();
            fixedAssetsApply.setOrgId(fixedAssetsAllocation.getOrgId());
            fixedAssetsApply.setApplyId(applyId);
            fixedAssetsApply = fixedAssetsApplyService.selectOneFixedAssetsApply(fixedAssetsApply);
            fixedAssetsAllocation.setOldOwnDept(fixedAssets.getOwnDept());
            fixedAssetsAllocation.setNewOwnDept(fixedAssetsApply.getUsedDept());
            insertFixedAssetsAllocation(fixedAssetsAllocation);
            fixedAssets.setStatus("1");
            fixedAssets.setOwnDept(fixedAssetsAllocation.getNewOwnDept());
            Example example = new Example(FixedAssets.class);
            example.createCriteria().andEqualTo("orgId", fixedAssets.getOrgId()).andEqualTo("assetsId", fixedAssets.getAssetsId());
            fixedAssetsService.updateFixedAssets(example, fixedAssets);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
        } else if (fixedAssets.getStatus().equals("1")) {
            return RetDataTools.NotOk(MessageCode.MSG_00029);
        } else if (fixedAssets.getStatus().equals("2")) {
            return RetDataTools.NotOk(MessageCode.MSG_00030);
        } else {
            return RetDataTools.NotOk(MessageCode.MSG_00031);
        }

    }

    /**
     * @param orgId
     * @param deptId
     * @param sortId
     * @param search
     * @return List<Map < String, String>>
     * @Title: getFixedAssetsAllocationOldList
     * @Description:  历史调拨记录
     */
    public List<Map<String, String>> getFixedAssetsAllocationOldList(String orgId, String deptId, String sortId, String search) {
        return fixedAssetsAllocationMapper.getFixedAssetsAllocationOldList(orgId, deptId, sortId, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param deptId
     * @param sortId
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getFixedAssetsAllocationOldList
     * @Description:  历史调拨记录
     */
    public PageInfo<Map<String, String>> getFixedAssetsAllocationOldList(PageParam pageParam, String deptId, String sortId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getFixedAssetsAllocationOldList(pageParam.getOrgId(), deptId, sortId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
