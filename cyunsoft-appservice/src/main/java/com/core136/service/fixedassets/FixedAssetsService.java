/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: FixedAssetsService.java
 * @Package com.core136.service.fixedassets
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月25日 下午5:10:51
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.service.fixedassets;

import com.core136.bean.fixedassets.FixedAssets;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.fixedassets.FixedAssetsMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@Service
public class FixedAssetsService {
    private FixedAssetsMapper fixedAssetsMapper;

    @Autowired
    public void setFixedAssetsMapper(FixedAssetsMapper fixedAssetsMapper) {
        this.fixedAssetsMapper = fixedAssetsMapper;
    }

    public int insertFixedAssets(FixedAssets fixedAssets) {
        return fixedAssetsMapper.insert(fixedAssets);
    }


    public int deleteFixedAssets(FixedAssets fixedAssets) {
        return fixedAssetsMapper.delete(fixedAssets);
    }

    public int updateFixedAssets(Example example, FixedAssets fixedAssets) {
        return fixedAssetsMapper.updateByExampleSelective(fixedAssets, example);
    }

    public FixedAssets selectOneFixedAssets(FixedAssets fixedAssets) {
        return fixedAssetsMapper.selectOne(fixedAssets);
    }

    /**
     * @Title: getFixedAssetsList
     * @Description:  固定资产列表
     * @param: orgId
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getFixedAssetsList(String orgId, String beginTime, String endTime, String sortId, String search) {
        return fixedAssetsMapper.getFixedAssetsList(orgId, beginTime, endTime, sortId, "%" + search + "%");
    }

    /**
     * @Title: getAllocationList
     * @Description:  获取调拨列表
     * @param: orgId
     * @param: beginTime
     * @param: endTime
     * @param: sortId
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getAllocationList(String orgId, String beginTime, String endTime, String sortId, String applyUser, String search) {
        return fixedAssetsMapper.getAllocationList(orgId, beginTime, endTime, sortId, applyUser, "%" + search + "%");
    }

    /**
     * @throws Exception
     * @Title: getFixedAssetsList
     * @Description:  固定资产列表
     * @param: pageParam
     * @param: @return
     * @return: PageInfo<Map < String, String>>
     */
    public PageInfo<Map<String, String>> getFixedAssetsList(PageParam pageParam, String beginTime, String endTime, String sortId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getFixedAssetsList(pageParam.getOrgId(), beginTime, endTime, sortId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @throws Exception
     * @Title: getAllocationList
     * @Description:  获取调拨列表
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: sortId
     * @param: @return
     * @return: PageInfo<Map < String, String>>
     */
    public PageInfo<Map<String, String>> getAllocationList(PageParam pageParam, String beginTime, String endTime, String sortId, String applyUser) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAllocationList(pageParam.getOrgId(), beginTime, endTime, sortId, applyUser, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @Title: queryFixedAssetsList
     * @Description:  查询固定资产列表
     * @param: orgId
     * @param: beginTime
     * @param: endTime
     * @param: sortId
     * @param: ownDept
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> queryFixedAssetsList(String orgId, String beginTime, String endTime, String sortId, String ownDept, String status, String search) {
        return fixedAssetsMapper.queryFixedAssetsList(orgId, beginTime, endTime, sortId, ownDept, status, "%" + search + "%");
    }

    /**
     * @throws Exception
     * @Title: queryFixedAssetsList
     * @Description:  查询固定资产列表
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: sortId
     * @param: ownDept
     * @param: @return
     * @return: PageInfo<Map < String, String>>
     */
    public PageInfo<Map<String, String>> queryFixedAssetsList(PageParam pageParam, String beginTime, String endTime, String sortId, String ownDept, String status) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = queryFixedAssetsList(pageParam.getOrgId(), beginTime, endTime, sortId, ownDept, status, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @Title: getApplyFixedAssetsList
     * @Description:  获取可申请的固定资产的列表
     * @param: orgId
     * @param: sortId
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getApplyFixedAssetsList(String orgId, String sortId, String search) {
        return fixedAssetsMapper.getApplyFixedAssetsList(orgId, sortId, "%" + search + "%");
    }

    /**
     * @throws Exception
     * @Title: getApplyFixedAssetsList
     * @Description:  获取可申请的固定资产的列表
     * @param: pageParam
     * @param: sortId
     * @param: @return
     * @return: PageInfo<Map < String, String>>
     */
    public PageInfo<Map<String, String>> getApplyFixedAssetsList(PageParam pageParam, String sortId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getApplyFixedAssetsList(pageParam.getOrgId(), sortId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
