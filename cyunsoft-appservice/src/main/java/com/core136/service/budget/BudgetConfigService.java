package com.core136.service.budget;

import com.core136.bean.account.Account;
import com.core136.bean.budget.BudgetConfig;
import com.core136.mapper.budget.BudgetConfigMapper;
import com.core136.service.account.UserInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class BudgetConfigService {
    private BudgetConfigMapper budgetConfigMapper;

    @Autowired
    public void setBudgetConfigMapper(BudgetConfigMapper budgetConfigMapper) {
        this.budgetConfigMapper = budgetConfigMapper;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    public int insertBudgetConfig(BudgetConfig budgetConfig) {
        return budgetConfigMapper.insert(budgetConfig);
    }

    public int deleteBudgetConfig(BudgetConfig budgetConfig) {
        return budgetConfigMapper.delete(budgetConfig);
    }

    public int updateBudgetConfig(Example example, BudgetConfig budgetConfig) {
        return budgetConfigMapper.updateByExampleSelective(budgetConfig, example);
    }

    public BudgetConfig selectOneBudgetConfig(BudgetConfig budgetConfig) {
        return budgetConfigMapper.selectOne(budgetConfig);
    }

    public int getConfigCount(BudgetConfig budgetConfig) {
        return budgetConfigMapper.selectCount(budgetConfig);
    }

    /**
     * @param budgetConfig
     * @return int
     * @Title: setBudgetConfig
     * @Description:  设置预算配置
     */
    public int setBudgetConfig(BudgetConfig budgetConfig) {
        BudgetConfig budgetConfig1 = new BudgetConfig();
        budgetConfig1.setOrgId(budgetConfig.getOrgId());
        if (getConfigCount(budgetConfig1) > 0) {
            Example example = new Example(BudgetConfig.class);
            example.createCriteria().andEqualTo("orgId", budgetConfig.getOrgId());
            return updateBudgetConfig(example, budgetConfig);
        } else {
            return insertBudgetConfig(budgetConfig);
        }
    }

    /**
     * @param account
     * @return List<Map < String, String>>
     * @Title: getAdjustmentApprovalUser
     * @Description:  获取预算调整审批人员列表
     */
    public List<Map<String, String>> getAdjustmentApprovalUser(Account account) {
        List<Map<String, String>> returnList = new ArrayList<Map<String, String>>();
        BudgetConfig budgetConfig = new BudgetConfig();
        budgetConfig.setOrgId(account.getOrgId());
        budgetConfig = selectOneBudgetConfig(budgetConfig);
        if (budgetConfig != null) {
            String accountStr = budgetConfig.getAdjustmentApprovalUser();
            if (StringUtils.isNotBlank(accountStr)) {
                String[] accountArr = accountStr.split(",");
                List<String> accounrList = Arrays.asList(accountArr);
                returnList = userInfoService.getAllUserInfoByAccountList(account.getOrgId(), accounrList);
            }
        }
        return returnList;
    }


    /**
     * @param account
     * @return List<Map < String, String>>
     * @Title: getCostApprovalUser
     * @Description:  获取费用预算申请审批人
     */
    public List<Map<String, String>> getCostApprovalUser(Account account) {
        List<Map<String, String>> returnList = new ArrayList<Map<String, String>>();
        BudgetConfig budgetConfig = new BudgetConfig();
        budgetConfig.setOrgId(account.getOrgId());
        budgetConfig = selectOneBudgetConfig(budgetConfig);
        if (budgetConfig != null) {
            String accountStr = budgetConfig.getCostApprovalUser();
            if (StringUtils.isNotBlank(accountStr)) {
                String[] accountArr = accountStr.split(",");
                List<String> accounrList = Arrays.asList(accountArr);
                returnList = userInfoService.getAllUserInfoByAccountList(account.getOrgId(), accounrList);
            }
        }
        return returnList;
    }

}
