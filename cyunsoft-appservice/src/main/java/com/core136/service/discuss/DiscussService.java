package com.core136.service.discuss;

import com.core136.bean.discuss.Discuss;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.discuss.DiscussMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class DiscussService {
    private DiscussMapper discussMapper;

    @Autowired
    public void setDiscussMapper(DiscussMapper discussMapper) {
        this.discussMapper = discussMapper;
    }

    public int insertDiscuss(Discuss discuss) {
        return discussMapper.insert(discuss);
    }

    public int deleteDiscuss(Discuss discuss) {
        return discussMapper.delete(discuss);
    }

    public int updateDiscuss(Example example, Discuss discuss) {
        return discussMapper.updateByExampleSelective(discuss, example);
    }

    public Discuss selectOneDiscuss(Discuss discuss) {
        return discussMapper.selectOne(discuss);
    }

    public List<Map<String, String>> getDiscussList(String orgId, String accountId, String search) {
        return discussMapper.getDiscussList(orgId, accountId, search);
    }

    public PageInfo<Map<String, String>> getDiscussList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getDiscussList(pageParam.getOrgId(), pageParam.getAccountId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


    public List<Map<String, String>> getMyDiscussList(String orgId, String userPriv, String deptPriv, String levelPriv) {
        return discussMapper.getMyDiscussList(orgId, userPriv, deptPriv, levelPriv);
    }

}
