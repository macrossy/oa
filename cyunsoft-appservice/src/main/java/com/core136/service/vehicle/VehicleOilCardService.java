package com.core136.service.vehicle;

import com.core136.bean.sys.PageParam;
import com.core136.bean.vehicle.VehicleOilCard;
import com.core136.common.utils.SysTools;
import com.core136.mapper.vehicle.VehicleOilCardMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class VehicleOilCardService {

    private VehicleOilCardMapper vehicleOilCardMapper;

    @Autowired
    public void setVehicleOilCardMapper(VehicleOilCardMapper vehicleOilCardMapper) {
        this.vehicleOilCardMapper = vehicleOilCardMapper;
    }

    public int insertVehicleOilCard(VehicleOilCard vehicleOilCard) {
        return vehicleOilCardMapper.insert(vehicleOilCard);
    }

    public int deleteVehicleOilCard(VehicleOilCard vehicleOilCard) {
        return vehicleOilCardMapper.delete(vehicleOilCard);
    }

    public int updateVehicleOilCard(Example example, VehicleOilCard vehicleOilCard) {
        return vehicleOilCardMapper.updateByExampleSelective(vehicleOilCard, example);
    }

    public VehicleOilCard selectOneVehicleOilCard(VehicleOilCard vehicleOilCard) {
        return vehicleOilCardMapper.selectOne(vehicleOilCard);
    }

    /**
     * @param orgId
     * @param oilType
     * @param beginTime
     * @param endTime
     * @param search
     * @return List<Map < String, String>>
     * @Title: getVehicleOilCardList
     * @Description:  获取油卡列表
     */
    public List<Map<String, String>> getVehicleOilCardList(String orgId, String oilType, String beginTime, String endTime, String search) {
        return vehicleOilCardMapper.getVehicleOilCardList(orgId, oilType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param oilType
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getVehicleOilCardList
     * @Description:  获取油卡列表
     */
    public PageInfo<Map<String, String>> getVehicleOilCardList(PageParam pageParam, String oilType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getVehicleOilCardList(pageParam.getOrgId(), oilType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getCanUsedOilCardList
     * @Description:  获取可用油卡列表
     */
    public List<Map<String, String>> getCanUsedOilCardList(String orgId) {
        return vehicleOilCardMapper.getCanUsedOilCardList(orgId);
    }
}
