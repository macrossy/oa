package com.core136.service.exam;

import com.core136.bean.exam.ExamTestSort;
import com.core136.mapper.exam.ExamTestSortMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ExamTestSortService {

    private ExamTestSortMapper examTestSortMapper;

    @Autowired
    public void setExamTestSortMapper(ExamTestSortMapper examTestSortMapper) {
        this.examTestSortMapper = examTestSortMapper;
    }

    public int insertExamTestSort(ExamTestSort examTestSort) {
        return examTestSortMapper.insert(examTestSort);
    }

    public int deleteExamTestSort(ExamTestSort examTestSort) {
        return examTestSortMapper.delete(examTestSort);
    }

    public int updateExamTestSort(Example example, ExamTestSort examTestSort) {
        return examTestSortMapper.updateByExampleSelective(examTestSort, example);
    }

    public ExamTestSort selectOneExamTestSort(ExamTestSort examTestSort) {
        return examTestSortMapper.selectOne(examTestSort);
    }

    public List<Map<String, String>> getExamTestSortTree(String orgId, String levelId) {
        return examTestSortMapper.getExamTestSortTree(orgId, levelId);
    }

    public int isExistChild(String orgId, String sortId) {
        return examTestSortMapper.isExistChild(orgId, sortId);
    }

}
