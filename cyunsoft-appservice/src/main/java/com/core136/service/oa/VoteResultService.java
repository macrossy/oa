package com.core136.service.oa;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.Account;
import com.core136.bean.oa.VoteResult;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.oa.VoteResultMapper;
import com.core136.service.account.AccountService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class VoteResultService {
    private VoteResultMapper voteResultMapper;

    @Autowired
    public void setVoteResultMapper(VoteResultMapper voteResultMapper) {
        this.voteResultMapper = voteResultMapper;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    public int insertVoteResult(VoteResult voteResult) {
        return voteResultMapper.insert(voteResult);
    }

    public int deleteVoteResult(VoteResult voteResult) {
        return voteResultMapper.delete(voteResult);
    }

    public int updateVoteResult(Example example, VoteResult voteResult) {
        return voteResultMapper.updateByExampleSelective(voteResult, example);
    }

    public VoteResult selectOneVoteResult(VoteResult voteResult) {
        return voteResultMapper.selectOne(voteResult);
    }

    /**
     * @param orgId
     * @param voteId
     * @return List<Map < String, String>>
     * @Title: getVoteResult
     * @Description:  获取投票结果
     */
    public List<Map<String, String>> getVoteResult(String orgId, String voteId) {
        return voteResultMapper.getVoteResult(orgId, voteId);
    }


    /**
     * @param voteResult
     * @return int
     * @Title: getVoteResultCount
     * @Description:  判断是否已经投过票
     */
    public int getVoteResultCount(VoteResult voteResult) {
        return voteResultMapper.selectCount(voteResult);
    }

    /**
     * @param account
     * @param result
     * @return RetDataBean
     * @Title: addVoteResuletRecord
     * @Description:  人员投票
     */
    public RetDataBean addVoteResuletRecord(Account account, String voteId, String result) {
        if (StringUtils.isNotBlank(result)) {
            JSONArray jsonArray = JSONArray.parseArray(result);
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject josn = jsonArray.getJSONObject(i);
                JSONArray jsonArrayChild = josn.getJSONArray("value");
                for (int k = 0; k < jsonArrayChild.size(); k++) {
                    VoteResult voteResult = new VoteResult();
                    voteResult.setRecordId(SysTools.getGUID());
                    voteResult.setItemId(jsonArrayChild.getString(k));
                    voteResult.setVoteId(voteId);
                    voteResult.setParentId(josn.getString("itemId"));
                    voteResult.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                    voteResult.setCreateUser(account.getAccountId());
                    voteResult.setOrgId(account.getOrgId());
                    insertVoteResult(voteResult);
                }
            }
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }

    /**
     * @param orgId
     * @param voteId
     * @return List<Map < String, String>>
     * @Title: voteDetailsForUser
     * @Description:  获取投票人员列表
     */
    public List<Map<String, String>> voteDetailsForUser(String orgId, String voteId, String recordId) {
        List<Map<String, String>> userList = voteResultMapper.voteDetailsForUser(orgId, voteId, recordId);
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < userList.size(); i++) {
            list.add(userList.get(i).get("createUser"));
        }
        return accountService.getUserNamesByAccountIds(list, orgId);
    }


}
