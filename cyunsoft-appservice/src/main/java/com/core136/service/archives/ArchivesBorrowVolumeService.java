package com.core136.service.archives;

import com.core136.bean.account.Account;
import com.core136.bean.archives.ArchivesBorrowVolume;
import com.core136.bean.archives.ArchivesFile;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesBorrowVolumeMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

@Service
public class ArchivesBorrowVolumeService {
    private ArchivesBorrowVolumeMapper archivesBorrowVolumeMapper;

    @Autowired
    public void setArchivesBorrowVolumeMapper(ArchivesBorrowVolumeMapper archivesBorrowVolumeMapper) {
        this.archivesBorrowVolumeMapper = archivesBorrowVolumeMapper;
    }

    private ArchivesBorrowVolumeService archivesBorrowVolumeService;

    @Autowired
    public void setArchivesBorrowVolumeService(ArchivesBorrowVolumeService archivesBorrowVolumeService) {
        this.archivesBorrowVolumeService = archivesBorrowVolumeService;
    }

    private ArchivesFileService archivesFileService;

    @Autowired
    public void setArchivesFileService(ArchivesFileService archivesFileService) {
        this.archivesFileService = archivesFileService;
    }

    private ArchivesVolumeService archivesVolumeService;

    @Autowired
    public void setArchivesVolumeService(ArchivesVolumeService archivesVolumeService) {
        this.archivesVolumeService = archivesVolumeService;
    }

    public int insertArchivesBorrowVolume(ArchivesBorrowVolume archivesBorrowVolume) {
        return archivesBorrowVolumeMapper.insert(archivesBorrowVolume);
    }

    public int deleteArchivesBorrowVolume(ArchivesBorrowVolume archivesBorrowVolume) {
        return archivesBorrowVolumeMapper.delete(archivesBorrowVolume);
    }

    public int updateArchivesBorrowVolume(Example example, ArchivesBorrowVolume archivesBorrowVolume) {
        return archivesBorrowVolumeMapper.updateByExampleSelective(archivesBorrowVolume, example);
    }

    public ArchivesBorrowVolume selectOneArchivesBorrowVolume(ArchivesBorrowVolume archivesBorrowVolume) {
        return archivesBorrowVolumeMapper.selectOne(archivesBorrowVolume);
    }

    /**
     * @param orgId
     * @param approvalStatus
     * @param opFlag
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getArchivesBorrowVolumeList
     * @Description:  案卷借阅记录
     */
    public List<Map<String, String>> getArchivesBorrowVolumeList(String orgId, String approvalStatus, String opFlag, String accountId) {
        return archivesBorrowVolumeMapper.getArchivesBorrowVolumeList(orgId, approvalStatus, opFlag, accountId);
    }

    /**
     * @param pageParam
     * @param approvalStatus
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getArchivesBorrowVolumeList
     * @Description:   案卷借阅记录
     */
    public PageInfo<Map<String, String>> getArchivesBorrowVolumeList(PageParam pageParam, String approvalStatus) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArchivesBorrowVolumeList(pageParam.getOrgId(), approvalStatus, pageParam.getOpFlag(), pageParam.getAccountId());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getArchivesBorrowVolumeApprovalList
     * @Description:  获取待审批记录
     */
    public List<Map<String, String>> getArchivesBorrowVolumeApprovalList(String orgId, String opFlag, String accountId) {
        return archivesBorrowVolumeMapper.getArchivesBorrowVolumeApprovalList(orgId, opFlag, accountId);
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getArchivesBorrowVolumeApprovalList
     * @Description:  获取待审批记录
     */
    public PageInfo<Map<String, String>> getArchivesBorrowVolumeApprovalList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArchivesBorrowVolumeApprovalList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param account
     * @param archivesBorrowVolume
     * @param archivesFile
     * @return ParseException
     * ArchivesFile
     * @Title: getApprovalVolumeFile
     * @Description:  获取案卷借阅文件详情
     */
    public ArchivesFile getApprovalVolumeFile(Account account, ArchivesBorrowVolume archivesBorrowVolume, ArchivesFile archivesFile) throws ParseException {
        archivesBorrowVolume = archivesBorrowVolumeService.selectOneArchivesBorrowVolume(archivesBorrowVolume);
        archivesFile = archivesFileService.selectOneArchivesFile(archivesFile);
        return archivesVolumeService.getApprovalVolumeFile(account, archivesBorrowVolume, archivesFile);
    }

    /**
     * @param orgId
     * @param opFlag
     * @param approvalStatus
     * @param accountId
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getArchivesBorrowVolumeApprovalOldList
     * @Description:  获取历史审批记录
     */
    public List<Map<String, String>> getArchivesBorrowVolumeApprovalOldList(String orgId, String opFlag, String approvalStatus, String accountId, String createUser, String beginTime, String endTime) {
        return archivesBorrowVolumeMapper.getArchivesBorrowVolumeApprovalOldList(orgId, opFlag, approvalStatus, accountId, createUser, beginTime, endTime);
    }

    /**
     * @param pageParam
     * @param approvalStatus
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getArchivesBorrowVolumeApprovalOldList
     * @Description:  获取历史审批记录
     */
    public PageInfo<Map<String, String>> getArchivesBorrowVolumeApprovalOldList(PageParam pageParam, String approvalStatus, String createUser, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArchivesBorrowVolumeApprovalOldList(pageParam.getOrgId(), pageParam.getOpFlag(), approvalStatus, pageParam.getAccountId(), createUser, beginTime, endTime);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
