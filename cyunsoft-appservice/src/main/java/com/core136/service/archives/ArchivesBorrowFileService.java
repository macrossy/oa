package com.core136.service.archives;

import com.core136.bean.account.Account;
import com.core136.bean.archives.ArchivesBorrowFile;
import com.core136.bean.archives.ArchivesFile;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesBorrowFileMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

@Service
public class ArchivesBorrowFileService {
    private ArchivesBorrowFileMapper archivesBorrowFileMapper;

    @Autowired
    public void setArchivesBorrowFileMapper(ArchivesBorrowFileMapper archivesBorrowFileMapper) {
        this.archivesBorrowFileMapper = archivesBorrowFileMapper;
    }

    private ArchivesFileService archivesFileService;

    @Autowired
    public void setArchivesFileService(ArchivesFileService archivesFileService) {
        this.archivesFileService = archivesFileService;
    }

    public int insertArchivesBorrowFile(ArchivesBorrowFile archivesBorrowFile) {
        return archivesBorrowFileMapper.insert(archivesBorrowFile);
    }

    public int deleteArchivesBorrowFile(ArchivesBorrowFile archivesBorrowFile) {
        return archivesBorrowFileMapper.delete(archivesBorrowFile);
    }

    public int updateArchivesBorrowFile(Example example, ArchivesBorrowFile archivesBorrowFile) {
        return archivesBorrowFileMapper.updateByExampleSelective(archivesBorrowFile, example);
    }

    public ArchivesBorrowFile selectOneArchivesBorrowFile(ArchivesBorrowFile archivesBorrowFile) {
        return archivesBorrowFileMapper.selectOne(archivesBorrowFile);
    }

    /**
     * @param orgId
     * @param approvalStatus
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getArchivesBorrowFileList
     * @Description:  文件借阅记录
     */
    public List<Map<String, String>> getArchivesBorrowFileList(String orgId, String approvalStatus, String opFlag, String accountId) {
        return archivesBorrowFileMapper.getArchivesBorrowFileList(orgId, approvalStatus, opFlag, accountId);
    }

    /**
     * @param pageParam
     * @param approvalStatus
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getArchivesBorrowFileList
     * @Description:  文件借阅记录
     */
    public PageInfo<Map<String, String>> getArchivesBorrowFileList(PageParam pageParam, String approvalStatus) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArchivesBorrowFileList(pageParam.getOrgId(), approvalStatus, pageParam.getOpFlag(), pageParam.getAccountId());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @return List<Map < String, String>>
     * @Title: getArchivesBorrowFileApprovalList
     * @Description:  获取待审批记录
     */
    public List<Map<String, String>> getArchivesBorrowFileApprovalList(String orgId, String opFlag, String accountId) {
        return archivesBorrowFileMapper.getArchivesBorrowFileApprovalList(orgId, opFlag, accountId);
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getArchivesBorrowFileApprovalList
     * @Description:  获取待审批记录
     */
    public PageInfo<Map<String, String>> getArchivesBorrowFileApprovalList(PageParam pageParam) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArchivesBorrowFileApprovalList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * ParseException
     *
     * @param account
     * @param archivesBorrowFile
     * @return ArchivesFile
     * @Title: getApprovalFile
     * @Description:  获取借阅文件详情
     */
    public ArchivesFile getApprovalFile(Account account, ArchivesBorrowFile archivesBorrowFile) throws ParseException {
        archivesBorrowFile = selectOneArchivesBorrowFile(archivesBorrowFile);
        return archivesFileService.getApprovalFile(account, archivesBorrowFile);
    }

    /**
     * @param orgId
     * @param opFlag
     * @param accountId
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getArchivesBorrowFileApprovalOldList
     * @Description:  获取历史审批记录
     */
    public List<Map<String, String>> getArchivesBorrowFileApprovalOldList(String orgId, String opFlag, String approvalStatus, String accountId, String createUser, String beginTime, String endTime) {
        return archivesBorrowFileMapper.getArchivesBorrowFileApprovalOldList(orgId, opFlag, approvalStatus, accountId, createUser, beginTime, endTime);
    }

    /**
     * @param pageParam
     * @param createUser
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getArchivesBorrowFileApprovalOldList
     * @Description:  获取历史审批记录
     */
    public PageInfo<Map<String, String>> getArchivesBorrowFileApprovalOldList(PageParam pageParam, String approvalStatus, String createUser, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArchivesBorrowFileApprovalOldList(pageParam.getOrgId(), pageParam.getOpFlag(), approvalStatus, pageParam.getAccountId(), createUser, beginTime, endTime);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


}
