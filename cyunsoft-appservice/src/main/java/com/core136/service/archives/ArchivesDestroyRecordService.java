package com.core136.service.archives;

import com.core136.bean.archives.ArchivesDestroyRecord;
import com.core136.bean.archives.ArchivesFile;
import com.core136.bean.archives.ArchivesVolume;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.archives.ArchivesDestroyRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ArchivesDestroyRecordService {

    private ArchivesDestroyRecordMapper archivesDestroyRecordMapper;

    @Autowired
    public void setArchivesDestroyRecordMapper(ArchivesDestroyRecordMapper archivesDestroyRecordMapper) {
        this.archivesDestroyRecordMapper = archivesDestroyRecordMapper;
    }

    private ArchivesFileService archivesFileService;

    @Autowired
    public void setArchivesFileService(ArchivesFileService archivesFileService) {
        this.archivesFileService = archivesFileService;
    }

    private ArchivesVolumeService archivesVolumeService;

    @Autowired
    public void setArchivesVolumeService(ArchivesVolumeService archivesVolumeService) {
        this.archivesVolumeService = archivesVolumeService;
    }

    public int insertArchivesDestroyRecord(ArchivesDestroyRecord archivesDestroyRecord) {
        return archivesDestroyRecordMapper.insert(archivesDestroyRecord);
    }

    public int deleteArchivesDestroyRecord(ArchivesDestroyRecord archivesDestroyRecord) {
        return archivesDestroyRecordMapper.delete(archivesDestroyRecord);
    }

    public int updateArchivesDestroyRecord(Example example, ArchivesDestroyRecord archivesDestroyRecord) {
        return archivesDestroyRecordMapper.updateByExampleSelective(archivesDestroyRecord, example);
    }

    public ArchivesDestroyRecord selectOneArchivesDestroyRecord(ArchivesDestroyRecord archivesDestroyRecord) {
        return archivesDestroyRecordMapper.selectOne(archivesDestroyRecord);
    }

    /**
     * @param archivesDestroyRecord
     * @return int
     * @Title: destroyArchives
     * @Description:  销毁档案
     */
    @Transactional(value = "generalTM")
    public int destroyArchives(ArchivesDestroyRecord archivesDestroyRecord) {
        if (archivesDestroyRecord.getArchivesType().equals("1")) {
            ArchivesFile archivesFile = new ArchivesFile();
            archivesFile.setOrgId(archivesDestroyRecord.getOrgId());
            archivesFile.setFileId(archivesDestroyRecord.getArchivesId());
            archivesFile.setDestoryFlag("1");
            Example example = new Example(ArchivesFile.class);
            example.createCriteria().andEqualTo("orgId", archivesFile.getOrgId()).andEqualTo("fileId", archivesFile.getFileId());
            archivesFileService.updateArchivesFile(example, archivesFile);
        } else if (archivesDestroyRecord.getArchivesType().equals("2")) {
            ArchivesVolume archivesVolume = new ArchivesVolume();
            archivesVolume.setOrgId(archivesDestroyRecord.getOrgId());
            archivesVolume.setVolumeId(archivesDestroyRecord.getArchivesId());
            archivesVolume.setDestoryFlag("1");
            Example example = new Example(ArchivesVolume.class);
            example.createCriteria().andEqualTo("orgId", archivesVolume.getOrgId()).andEqualTo("volumeId", archivesVolume.getVolumeId());
            archivesVolumeService.updateArchivesVolume(example, archivesVolume);
        }
        return insertArchivesDestroyRecord(archivesDestroyRecord);
    }


    /**
     * @param orgId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getArchivesDestoryFileList
     * @Description:  获取销毁记录
     */
    public List<Map<String, String>> getArchivesDestoryFileList(String orgId, String accountId, String beginTime, String endTime, String search) {
        return archivesDestroyRecordMapper.getArchivesDestoryFileList(orgId, accountId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getArchivesDestoryFileList
     * @Description:  获取销毁记录
     */
    public PageInfo<Map<String, String>> getArchivesDestoryFileList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArchivesDestoryFileList(pageParam.getOrgId(), pageParam.getAccountId(), beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @param accountId
     * @param beginTime
     * @param endTime
     * @return List<Map < String, String>>
     * @Title: getArchivesDestoryVolumeList
     * @Description:  获取销毁记录
     */
    public List<Map<String, String>> getArchivesDestoryVolumeList(String orgId, String accountId, String beginTime, String endTime, String search) {
        return archivesDestroyRecordMapper.getArchivesDestoryVolumeList(orgId, accountId, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getArchivesDestoryVolumeList
     * @Description:  获取销毁记录
     */
    public PageInfo<Map<String, String>> getArchivesDestoryVolumeList(PageParam pageParam, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getArchivesDestoryVolumeList(pageParam.getOrgId(), pageParam.getAccountId(), beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }
}
