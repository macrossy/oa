package com.core136.service.mobile;

import com.core136.bean.account.AUserInfo;
import com.core136.bean.account.Account;
import com.core136.common.SysRunConfig;
import com.core136.common.enums.EventTypeEnums;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.account.UnitService;
import com.core136.service.account.UserInfoService;
import com.core136.service.account.UserPrivService;
import com.core136.service.sys.SysLogService;
import com.core136.service.sys.SysMenuService;
import com.core136.unit.RedisUtil;
import com.taobao.api.ApiException;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class MobileLoginService {

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private UserPrivService userPrivService;

    @Autowired
    public void setUserPrivService(UserPrivService userPrivService) {
        this.userPrivService = userPrivService;
    }

    private SysMenuService sysMenuService;

    @Autowired
    public void setSysMenuService(SysMenuService sysMenuService) {
        this.sysMenuService = sysMenuService;
    }

    private SysLogService sysLogService;

    @Autowired
    public void setSysLogService(SysLogService sysLogService) {
        this.sysLogService = sysLogService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private UnitService unitService;

    @Autowired
    public void setUnitService(UnitService unitService) {
        this.unitService = unitService;
    }

    private RedisUtil redisUtil;

    @Autowired
    public void setRedisUtil(RedisUtil redisUtil) {
        this.redisUtil = redisUtil;
    }

    /**
     * @param request
     * @param accountId void
     * @Title: mobileLogin
     * @Description:  移动端登陆
     */
    public boolean mobileLogin(HttpServletRequest request, String accountId, String passWord) throws ApiException {
        boolean isRegist = SysRunConfig.getIsRegist();
        Account account = new Account();
        String ip = SysTools.getIpAddress(request);
        if (isRegist) {
            SysTools.reloadAuthorizing(accountId);
            UsernamePasswordToken userToken = new UsernamePasswordToken(accountId, passWord);
            Subject subject = SecurityUtils.getSubject();
            subject.login(userToken);
            AUserInfo aUserInfo = accountService.getRedisAUserInfo();
            account.setAccountId(accountId);
            if(aUserInfo!=null)
            {
                sysLogService.createLog(ip, account, EventTypeEnums.SYS_LOG_LOGIN, "移动端登陆成功");
            }else
            {
                sysLogService.createLog(ip, account, EventTypeEnums.SYS_LOG_LOGIN, "移动端登陆失败");
            }
            return true;
        } else {
            account.setAccountId(accountId);
            sysLogService.createLog(ip, account, EventTypeEnums.SYS_LOG_LOGIN, "移动端登陆失败");
            return false;
        }
    }
}
