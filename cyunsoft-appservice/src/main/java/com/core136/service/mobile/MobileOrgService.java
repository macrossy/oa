package com.core136.service.mobile;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.UnitDept;
import com.core136.service.account.UnitDeptService;
import com.core136.service.account.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class MobileOrgService {
    private UnitDeptService unitDeptService;

    @Autowired
    public void setUnitDeptService(UnitDeptService unitDeptService) {
        this.unitDeptService = unitDeptService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    /**
     * @param orgId
     * @param deptId
     * @return JSONArray
     * @Title: getDeptAndUserInfoForMobile
     * @Description:  获取APP组织机
     */
    public JSONArray getDeptAndUserInfoForMobile(String orgId, String deptId) {
        JSONArray jsonArray = new JSONArray();
        Example example = new Example(UnitDept.class);
        example.createCriteria().andEqualTo("orgId", orgId).andEqualTo("orgLevelId", deptId);
        List<UnitDept> deptlist = unitDeptService.getDeptList(example);
        for (int i = 0; i < deptlist.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", deptlist.get(i).getDeptId());
            jsonObject.put("text", deptlist.get(i).getDeptName());
            jsonObject.put("isParent", true);
            jsonArray.add(jsonObject);
        }

        List<Map<String, Object>> userList = userInfoService.getSelectUserByDeptId(deptId, orgId);
        for (int i = 0; i < userList.size(); i++) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", userList.get(i).get("accountId"));
            jsonObject.put("text", userList.get(i).get("userName"));
            jsonObject.put("sex", userList.get(i).get("sex"));
            jsonObject.put("headImg", userList.get(i).get("headImg"));
            jsonObject.put("isParent", false);
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }


}

