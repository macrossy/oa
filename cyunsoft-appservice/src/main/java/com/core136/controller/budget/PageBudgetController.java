package com.core136.controller.budget;

import com.core136.bean.account.Account;
import com.core136.bean.budget.BudgetConfig;
import com.core136.service.account.AccountService;
import com.core136.service.budget.BudgetConfigService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/app/core/budget")
public class PageBudgetController {
    private BudgetConfigService budgetConfigService;

    @Autowired
    public void setBudgetConfigService(BudgetConfigService budgetConfigService) {
        this.budgetConfigService = budgetConfigService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * 动态汇总
     * @return
     */
    @RequestMapping("/dynamicsummary")
    @RequiresPermissions("/app/core/budget/dynamicsummary")
    public ModelAndView goDynamicsummary() {
        try {
            return new ModelAndView("app/core/budget/cost/dynamicsummary");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 预算费用支出查询
     * @return
     */
    @RequestMapping("/costquery")
    @RequiresPermissions("/app/core/budget/costquery")
    public ModelAndView goCostquery() {
        try {
            return new ModelAndView("app/core/budget/cost/costquery");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 预算费用审批
     * @param view
     * @return
     */
    @RequestMapping("/costapproval")
    @RequiresPermissions("/app/core/budget/costapproval")
    public ModelAndView goCostapproval(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/budget/cost/costapproval");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/budget/cost/costapprovalmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 费用上报详情
     * @return
     */
    @RequestMapping("/costdetails")
    @RequiresPermissions("/app/core/budget/costdetails")
    public ModelAndView goCostDetails() {
        try {
            return new ModelAndView("app/core/budget/cost/costdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 预算费用申请详情
     * @return
     */
    @RequestMapping("/costapplydetails")
    @RequiresPermissions("/app/core/budget/costapplydetails")
    public ModelAndView goCostApplydetails() {
        ModelAndView mv = null;
        try {
            mv = new ModelAndView("app/core/budget/cost/costapplydetails");
            return mv;
        } catch (Exception e) {
            mv = new ModelAndView("titps");
            return mv;
        }
    }

    /**
     * 预算用申请审批
     * @param view
     * @return
     */
    @RequestMapping("/costapply")
    @RequiresPermissions("/app/core/budget/costapply")
    public ModelAndView goCostapply(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/budget/cost/costapplymanage");
            } else {
                if (view.equals("create")) {
                    Account account = accountService.getRedisAUserInfoToAccount();
                    BudgetConfig budgetConfig = new BudgetConfig();
                    budgetConfig.setOrgId(account.getOrgId());
                    budgetConfig = budgetConfigService.selectOneBudgetConfig(budgetConfig);
                    if (StringUtils.isNotBlank(budgetConfig.getCostApplayType())) {
                        if (budgetConfig.getCostApplayType().equals("2")) {
                            mv = new ModelAndView("app/core/budget/cost/costapplybpm");
                            mv.addObject("flowId", budgetConfig.getFlowIdCost());
                            mv.addObject("accountId", account.getAccountId());
                        } else {
                            mv = new ModelAndView("app/core/budget/cost/costapply");
                        }
                    } else {
                        mv = new ModelAndView("app/core/budget/cost/costapply");
                    }

                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 预算调整审批
     * @param view
     * @return
     */
    @RequestMapping("/adjustmentapproval")
    @RequiresPermissions("/app/core/budget/adjustmentapproval")
    public ModelAndView goAdjustmentapproval(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/budget/project/adjustmentapproval");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("app/core/budget/project/adjustmentapprovalmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * 预算调整申请详情页
     * @return
     */
    @RequestMapping("/adjustmentdetails")
    @RequiresPermissions("/app/core/budget/adjustmentdetails")
    public ModelAndView goAdjustmentdetails() {
        try {
            return new ModelAndView("app/core/budget/project/adjustmentdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 预算调整申请
     * @param view
     * @return
     */
    @RequestMapping("/adjustment")
    @RequiresPermissions("/app/core/budget/adjustment")
    public ModelAndView goAdjustment(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/budget/project/adjustmentmanage");
            } else {
                if (view.equals("create")) {
                    mv = new ModelAndView("app/core/budget/project/adjustment");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 预算设置
     * @return
     */
    @RequestMapping("/setconfig")
    @RequiresPermissions("/app/core/budget/setconfig")
    public ModelAndView goSetConfig() {
        try {
            return new ModelAndView("app/core/budget/param/setconfig");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 费用上报
     * @param request
     * @param view
     * @return
     */
    @RequestMapping("/costreporting")
    @RequiresPermissions("/app/core/budget/costreporting")
    public ModelAndView goCostrePorting(HttpServletRequest request, String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                Account account = accountService.getRedisAUserInfoToAccount();
                BudgetConfig budgetConfig = new BudgetConfig();
                budgetConfig.setOrgId(account.getOrgId());
                budgetConfig = budgetConfigService.selectOneBudgetConfig(budgetConfig);
                if (budgetConfig == null) {
                    mv = new ModelAndView("app/core/budget/cost/index");
                } else {
                    if (StringUtils.isNotBlank(budgetConfig.getCostUpdateType())) {
                        if (budgetConfig.getCostUpdateType().equals("2")) {
                            mv = new ModelAndView("app/core/budget/cost/costbpm");
                            mv.addObject("flowId", budgetConfig.getFlowIdUpdate());
                            mv.addObject("accountId", account.getAccountId());
                        } else {
                            mv = new ModelAndView("app/core/budget/cost/index");
                        }
                    } else {
                        mv = new ModelAndView("app/core/budget/cost/index");
                    }
                }
            } else {
                mv = new ModelAndView("app/core/budget/cost/manage");
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 预算项目详情
     * @return
     */
    @RequestMapping("/projectdetails")
    @RequiresPermissions("/app/core/budget/projectdetails")
    public ModelAndView goProjectDetails() {
        try {
            return new ModelAndView("app/core/budget/project/projectdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 预算科目设置
     * @return
     */
    @RequestMapping("/setaccount")
    @RequiresPermissions("/app/core/budget/setaccount")
    public ModelAndView goSetAccount() {
        try {
            return new ModelAndView("app/core/budget/param/setaccount");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goProjectbudget
     * @Description:  创建预算
     */
    @RequestMapping("/projectbudget")
    @RequiresPermissions("/app/core/budget/projectbudget")
    public ModelAndView goProjectbudget(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/budget/project/projectbudget");
            } else {
                if (view.equals("child")) {
                    mv = new ModelAndView("app/core/budget/project/createchildproject");
                } else {
                    mv = new ModelAndView("app/core/budget/project/createproject");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    /**
     * @return ModelAndView
     * @Title: goUpbudget
     * @Description:  上报预算
     */
    @RequestMapping("/upbudget")
    @RequiresPermissions("/app/core/budget/upbudget")
    public ModelAndView goUpbudget() {
        try {
            return new ModelAndView("app/core/budget/project/upbudget");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goSetbudgettype
     * @Description:  创建预算分类
     */
    @RequestMapping("/setbudgettype")
    @RequiresPermissions("/app/core/budget/setbudgettype")
    public ModelAndView goSetbudgettype() {
        ModelAndView mv = null;
        try {
            return new ModelAndView("app/core/budget/param/setbudgettype");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

}
