package com.core136.controller.budget;

import com.core136.bean.account.Account;
import com.core136.bean.budget.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.budget.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ret/budgetget")
public class RouteGetBudgetController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private BudgetAccountService budgetAccountService;

    @Autowired
    public void setBudgetAccountService(BudgetAccountService budgetAccountService) {
        this.budgetAccountService = budgetAccountService;
    }

    private BudgetTypeService budgetTypeService;

    @Autowired
    public void setBudgetTypeService(BudgetTypeService budgetTypeService) {
        this.budgetTypeService = budgetTypeService;
    }

    private BudgetProjectService budgetProjectService;

    @Autowired
    public void setBudgetProjectService(BudgetProjectService budgetProjectService) {
        this.budgetProjectService = budgetProjectService;
    }

    private BudgetCostService budgetCostService;

    @Autowired
    public void setBudgetCostService(BudgetCostService budgetCostService) {
        this.budgetCostService = budgetCostService;
    }

    private BudgetConfigService budgetConfigService;

    @Autowired
    public void setBudgetConfigService(BudgetConfigService budgetConfigService) {
        this.budgetConfigService = budgetConfigService;
    }

    private BudgetAdjustmentService budgetAdjustmentService;

    @Autowired
    public void setBudgetAdjustmentService(BudgetAdjustmentService budgetAdjustmentService) {
        this.budgetAdjustmentService = budgetAdjustmentService;
    }

    private BudgetCostApplyService budgetCostApplyService;

    @Autowired
    public void setBudgetCostApplyService(BudgetCostApplyService budgetCostApplyService) {
        this.budgetCostApplyService = budgetCostApplyService;
    }

    /**
     * @param pageParam
     * @param parentId
     * @return RetDataBean
     * @Title: getBudgetCostList
     * @Description:  获取项目预算费用支出列表
     */
    @RequestMapping(value = "/getBudgetCostList", method = RequestMethod.POST)
    public RetDataBean getBudgetCostList(PageParam pageParam, String parentId
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = budgetCostService.getBudgetCostList(pageParam, parentId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param projectId
     * @param beginTime
     * @param endTime
     * @param status
     * @return RetDataBean
     * @Title: getBudgetCostApprpvalOldList
     * @Description:  获取预算费用历史审批记录
     */
    @RequestMapping(value = "/getBudgetCostApprpvalOldList", method = RequestMethod.POST)
    public RetDataBean getBudgetCostApprpvalOldList(PageParam pageParam, String projectId, String beginTime, String endTime, String status
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = budgetCostApplyService.getBudgetCostApprpvalOldList(pageParam, projectId, beginTime, endTime, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param projectId
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getBudgetCostApprpvalList
     * @Description:  获取预算费用审批记录
     */
    @RequestMapping(value = "/getBudgetCostApprpvalList", method = RequestMethod.POST)
    public RetDataBean getBudgetCostApprpvalList(PageParam pageParam, String projectId, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = budgetCostApplyService.getBudgetCostApprpvalList(pageParam, projectId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param projectId
     * @param beginTime
     * @param endTime
     * @param status
     * @return RetDataBean
     * @Title: getBudgetCostApplyList
     * @Description:  获取预算申请记录列表
     */
    @RequestMapping(value = "/getBudgetCostApplyList", method = RequestMethod.POST)
    public RetDataBean getBudgetCostApplyList(PageParam pageParam, String projectId, String beginTime, String endTime, String status
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = budgetCostApplyService.getBudgetCostApplyList(pageParam, projectId, beginTime, endTime, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetCostApply
     * @return RetDataBean
     * @Title: getBudgetCostApplyById
     * @Description:  获取预算费用申请详情
     */
    @RequestMapping(value = "/getBudgetCostApplyById", method = RequestMethod.POST)
    public RetDataBean getBudgetCostApplyById(BudgetCostApply budgetCostApply) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetCostApply.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetCostApplyService.selectOneBudgetCostApply(budgetCostApply));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param projectId
     * @param beginTime
     * @param endTime
     * @param applyUser
     * @param status
     * @return RetDataBean
     * @Title: getAdjustmentApprovalOldList
     * @Description:  获取等审批记录查询
     */
    @RequestMapping(value = "/getAdjustmentApprovalOldList", method = RequestMethod.POST)
    public RetDataBean getAdjustmentApprovalOldList(PageParam pageParam, String projectId, String beginTime, String endTime, String applyUser, String status
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = budgetAdjustmentService.getAdjustmentApprovalOldList(pageParam, applyUser, projectId, beginTime, endTime, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @param projectId
     * @param beginTime
     * @param endTime
     * @param applyUser
     * @return RetDataBean
     * @Title: getAdjustmentApprovalList
     * @Description:  获取等审批列表
     */
    @RequestMapping(value = "/getAdjustmentApprovalList", method = RequestMethod.POST)
    public RetDataBean getAdjustmentApprovalList(PageParam pageParam, String projectId, String beginTime, String endTime, String applyUser
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = budgetAdjustmentService.getAdjustmentApprovalList(pageParam, applyUser, projectId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param projectId
     * @param beginTime
     * @param endTime
     * @param status
     * @param approvalUser
     * @return RetDataBean
     * @Title: getAdjustmentApplayList
     * @Description:  获取预算调整申请列表
     */
    @RequestMapping(value = "/getAdjustmentApplayList", method = RequestMethod.POST)
    public RetDataBean getAdjustmentApplayList(PageParam pageParam, String projectId, String beginTime, String endTime, String status, String approvalUser
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.create_time");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = budgetAdjustmentService.getAdjustmentApplayList(pageParam, status, approvalUser, projectId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getAdjustmentApprovalUser
     * @Description:  获取预算调整审批人员列表
     */
    @RequestMapping(value = "/getAdjustmentApprovalUser", method = RequestMethod.POST)
    public RetDataBean getAdjustmentApprovalUser() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetConfigService.getAdjustmentApprovalUser(account));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getCostApprovalUser
     * @Description:  获取费用预算申请审批人
     */
    @RequestMapping(value = "/getCostApprovalUser", method = RequestMethod.POST)
    public RetDataBean getCostApprovalUser() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetConfigService.getCostApprovalUser(account));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetAdjustment
     * @return RetDataBean
     * @Title: getBudgetAdjustmentById
     * @Description:  获取费用调整申请详情
     */
    @RequestMapping(value = "/getBudgetAdjustmentById", method = RequestMethod.POST)
    public RetDataBean getBudgetAdjustmentById(BudgetAdjustment budgetAdjustment) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetAdjustment.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetAdjustmentService.selectOneBudgetAdjustment(budgetAdjustment));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param projectId
     * @param beginTime
     * @param endTime
     * @return RetDataBean
     * @Title: getChildBudgetCostList
     * @Description:  获取子项目费用支出列表
     */
    @RequestMapping(value = "/getChildBudgetCostList", method = RequestMethod.POST)
    public RetDataBean getChildBudgetCostList(PageParam pageParam, String projectId, String beginTime, String endTime
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.sort_no");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = budgetCostService.getChildBudgetCostList(pageParam, projectId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param parentId
     * @return RetDataBean
     * @Title: getBudgetChildProjectListForProject
     * @Description:  获取子项目列表
     */
    @RequestMapping(value = "/getBudgetChildProjectListForProject", method = RequestMethod.POST)
    public RetDataBean getBudgetChildProjectListForProject(String parentId) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("budget:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetProjectService.getBudgetChildProjectListForProject(account.getOrgId(), account.getOpFlag(), account.getAccountId(), parentId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBudgetProjectListForProject
     * @Description:  获取主项目下拉列表
     */
    @RequestMapping(value = "/getBudgetProjectListForProject", method = RequestMethod.POST)
    public RetDataBean getBudgetProjectListForProject() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetProjectService.getBudgetProjectListForProject(account.getOrgId(), account.getOpFlag(), account.getAccountId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetCost
     * @return RetDataBean
     * @Title: getBudgetCostById
     * @Description:  获取费用支出详情
     */
    @RequestMapping(value = "/getBudgetCostById", method = RequestMethod.POST)
    public RetDataBean getBudgetCostById(BudgetCost budgetCost) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetCost.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetCostService.selectOneBudgetCost(budgetCost));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param budgetConfig
     * @return RetDataBean
     * @Title: getBudgetConfigById
     * @Description:  获取费用预算配置详情
     */
    @RequestMapping(value = "/getBudgetConfigById", method = RequestMethod.POST)
    public RetDataBean getBudgetConfigById(BudgetConfig budgetConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetConfigService.selectOneBudgetConfig(budgetConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param chargeUser
     * @param projectType
     * @param budgetAccount
     * @return RetDataBean
     * @Title: getBudgetProjectList
     * @Description:  获取项目列表
     */
    @RequestMapping(value = "/getBudgetProjectList", method = RequestMethod.POST)
    public RetDataBean getBudgetProjectList(PageParam pageParam, String chargeUser, String projectType, String budgetAccount
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("budget:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = budgetProjectService.getBudgetProjectList(pageParam, projectType, chargeUser, budgetAccount);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param parentId
     * @return RetDataBean
     * @Title: getBudgetChildProjectList
     * @Description:  获取子项目列表
     */
    @RequestMapping(value = "/getBudgetChildProjectList", method = RequestMethod.POST)
    public RetDataBean getBudgetChildProjectList(PageParam pageParam, String parentId) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("budget:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = budgetProjectService.getBudgetChildProjectList(pageParam, parentId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @return RetDataBean
     * @Title: getProjectTreeList
     * @Description:  获取项目列表
     */
    @RequestMapping(value = "/getProjectTreeList", method = RequestMethod.POST)
    public List<Map<String, Object>> getProjectTreeList(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return budgetProjectService.getProjectTreeList(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param budgetProject
     * @return RetDataBean
     * @Title: getBudgetProjectById
     * @Description:  获取预算项目详情
     */
    @RequestMapping(value = "/getBudgetProjectById", method = RequestMethod.POST)
    public RetDataBean getBudgetProjectById(BudgetProject budgetProject) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetProject.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetProjectService.selectOneBudgetProject(budgetProject));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getBudgetTypeListForSelect
     * @Description:  获取预算公类
     */
    @RequestMapping(value = "/getBudgetTypeListForSelect", method = RequestMethod.POST)
    public RetDataBean getBudgetTypeListForSelect() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetTypeService.getBudgetTypeListForSelect(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param pageParam
     * @return RetDataBean
     * @Title: getBudgetTypeList
     * @Description:  获取分类列表
     */
    @RequestMapping(value = "/getBudgetTypeList", method = RequestMethod.POST)
    public RetDataBean getBudgetTypeList(
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = budgetTypeService.getBudgetTypeList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param budgetType
     * @return RetDataBean
     * @Title: getBudgetTypeById
     * @Description:  获取分类详情
     */
    @RequestMapping(value = "/getBudgetTypeById", method = RequestMethod.POST)
    public RetDataBean getBudgetTypeById(BudgetType budgetType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetType.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetTypeService.selectOneBudgetType(budgetType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, Object>>
     * @Title getErpBomSortTree
     * @Description  获取BOM 分类树结构
     */
    @RequestMapping(value = "/getBudgetAccountTree", method = RequestMethod.POST)
    public List<Map<String, String>> getBudgetAccountTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return budgetAccountService.getBudgetAccountTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param @param  request
     * @param @param  contractSort
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getBudgetAccountById
     * @Description:  获取预算科目详情
     */
    @RequestMapping(value = "/getBudgetAccountById", method = RequestMethod.POST)
    public RetDataBean getBudgetAccountById(BudgetAccount budgetAccount) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            budgetAccount.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, budgetAccountService.selectOneBudgetAccount(budgetAccount));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
