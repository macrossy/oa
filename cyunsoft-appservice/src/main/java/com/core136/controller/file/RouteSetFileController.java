package com.core136.controller.file;

import com.alibaba.fastjson.JSON;
import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.file.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserInfoService;
import com.core136.service.file.*;
import com.core136.unit.fileutils.FileTools;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

/**
 * @author lsq
 * @ClassName: RoutSetFileController
 * @Description: 系统文件系统操作CONTROLLER
 * @author: 稠云信息
 * @date: 2019年5月27日 下午4:23:56
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/set/fileset")
public class RouteSetFileController {
    private NetDiskService netDiskService;

    @Autowired
    public void setNetDiskService(NetDiskService netDiskService) {
        this.netDiskService = netDiskService;
    }

    private PersonalFileFolderService personalFileFolderService;

    @Autowired
    public void setPersonalFileFolderService(PersonalFileFolderService personalFileFolderService) {
        this.personalFileFolderService = personalFileFolderService;
    }

    private PersonalFileService personalFileService;

    @Autowired
    public void setPersonalFileService(PersonalFileService personalFileService) {
        this.personalFileService = personalFileService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private PublicFileFolderService publicFileFolderService;

    @Autowired
    public void setPublicFileFolderService(PublicFileFolderService publicFileFolderService) {
        this.publicFileFolderService = publicFileFolderService;
    }

    private PublicFileService publicFileService;

    @Autowired
    public void setPublicFileService(PublicFileService publicFileService) {
        this.publicFileService = publicFileService;
    }

    private AttachService attachService;

    @Autowired
    public void setAttachService(AttachService attachService) {
        this.attachService = attachService;
    }

    private PhotoService phoroService;

    @Autowired
    public void setPhotoService(PhotoService phoroService) {
        this.phoroService = phoroService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Value("${app.notallow}")
    private String notallow;

    /**
     * @param type
     * @param fileId
     * @param fileName
     * @return RetDataBean
     * @Title: updatePublicFileFolder
     * @Description:  公共文件柜重命名
     */
    @RequestMapping(value = "/updatePublicFileFolder", method = RequestMethod.POST)
    public RetDataBean updatePublicFileFolder(String type, String fileId, String fileName) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("publicfile:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(fileId) && StringUtils.isNotBlank(fileName)) {
                if (type.equals("file")) {
                    Example example = new Example(PublicFile.class);
                    example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("fileId", fileId);
                    PublicFile publicFile = new PublicFile();
                    publicFile.setOrgId(account.getOrgId());
                    publicFile.setFileId(fileId);
                    publicFile = publicFileService.selectOnePublicFile(publicFile);
                    String extName = publicFile.getFileName().split("\\.")[1];
                    publicFile.setFileName(fileName + "." + extName);
                    return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, publicFileService.updatePublicFile(publicFile, example));
                } else {
                    Example example = new Example(PublicFileFolder.class);
                    example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("folderId", fileId);
                    PublicFileFolder publicFileFolder = new PublicFileFolder();
                    publicFileFolder.setFolderName(fileName);
                    return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, publicFileFolderService.updatePublicFileFolder(publicFileFolder, example));
                }
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param fileId
     * @param currentLocation
     * @param type
     * @return RetDataBean
     * @Title: shearPublicFile
     * @Description:  公共文件柜剪切
     */
    @RequestMapping(value = "/shearPublicFile", method = RequestMethod.POST)
    public RetDataBean shearPublicFile(String fileId, String currentLocation, String type) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("publicfile:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(fileId) && StringUtils.isNotBlank(currentLocation) && StringUtils.isNotBlank(type)) {
                if (type.equals("file")) {
                    PublicFile publicFile = new PublicFile();
                    publicFile.setFileId(fileId);
                    publicFile.setOrgId(account.getOrgId());
                    publicFile = publicFileService.selectOnePublicFile(publicFile);
                    return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, publicFileService.shearPublicFile(publicFile, currentLocation));
                } else {
                    PublicFileFolder publicFileFolder = new PublicFileFolder();
                    publicFileFolder.setFolderId(fileId);
                    publicFileFolder.setOrgId(account.getOrgId());
                    publicFileFolder = publicFileFolderService.selectOnePublicFileFolder(publicFileFolder);
                    return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, publicFileFolderService.shearPublicFileFolder(publicFileFolder, currentLocation));
                }
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param currentLocation
     * @param fileId
     * @param type
     * @return RetDataBean
     * @Title: pastePublicFile
     * @Description:  复制粘贴
     */
    @RequestMapping(value = "/pastePublicFile", method = RequestMethod.POST)
    public RetDataBean pastePublicFile(String currentLocation, String fileId, String type) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("publicfile:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(currentLocation) && StringUtils.isNotBlank(fileId) && StringUtils.isNotBlank(type)) {
                if (type.equals("file")) {
                    PublicFile publicFile = new PublicFile();
                    publicFile.setOrgId(account.getOrgId());
                    publicFile.setFileId(fileId);
                    return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, publicFileService.pastePublicFile(publicFile, currentLocation));
                } else {
                    PublicFileFolder publicFileFolder = new PublicFileFolder();
                    publicFileFolder.setOrgId(account.getOrgId());
                    publicFileFolder.setFolderId(fileId);
                    return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, publicFileFolderService.pastePublicFileFolder(publicFileFolder, currentLocation));
                }
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param publicFileFolder
     * @return RetDataBean
     * @Title: deletePublicFileFolder
     * @Description:  删除共公文件柜文件夹
     */
    @RequestMapping(value = "/deletePublicFileFolder", method = RequestMethod.POST)
    public RetDataBean deletePublicFileFolder(PublicFileFolder publicFileFolder) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(publicFileFolder.getFolderId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            publicFileFolder.setOrgId(account.getOrgId());
            return publicFileFolderService.deleteFolder(publicFileFolder);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: insertPhoto
     * @Description:  创建相册
     * @param: request
     * @param: photo
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertPhoto", method = RequestMethod.POST)
    public RetDataBean insertPhoto(Photo photo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            photo.setPhotoId(SysTools.getGUID());
            photo.setCreateUser(account.getAccountId());
            photo.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            photo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, phoroService.insertPhoto(photo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updatePhoto
     * @Description:  更新相册
     * @param: request
     * @param: photo
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updatePhoto", method = RequestMethod.POST)
    public RetDataBean updatePhoto(Photo photo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            if (StringUtils.isBlank(photo.getPhotoId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(Photo.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("photoId", photo.getPhotoId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, phoroService.updatePhoto(example, photo));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    @RequestMapping(value = "/delPhoto", method = RequestMethod.POST)
    public RetDataBean delPhoto(Photo photo) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            if (StringUtils.isBlank(photo.getPhotoId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            photo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, phoroService.deletePhoto(photo));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param personalFileFolder
     * @return RetDataBean
     * @Title insertPersonalFileFolder
     * @Description  创建个人文件柜
     */
    @RequestMapping(value = "/insertPersonalFileFolder", method = RequestMethod.POST)
    public RetDataBean insertPersonalFileFolder(PersonalFileFolder personalFileFolder) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(personalFileFolder.getFolderLevel())) {
                personalFileFolder.setFolderLevel("0");
            }
            personalFileFolder.setFolderId(SysTools.getGUID());
            personalFileFolder.setCreateUser(account.getAccountId());
            personalFileFolder.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            personalFileFolder.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, personalFileFolderService.insert(personalFileFolder));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  type
     * @param @param  fileId
     * @param @param  fileName
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: updatePersonalFileFolder
     * @Description:  更新个人文件柜文件的名称
     */
    @RequestMapping(value = "/updatePersonalFileFolder", method = RequestMethod.POST)
    public RetDataBean updatePersonalFileFolder(String type, String fileId, String fileName) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("personalfile:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(fileId) && StringUtils.isNotBlank(fileName)) {
                if (type.equals("file")) {
                    Example example = new Example(PersonalFile.class);
                    example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("fileId", fileId);
                    PersonalFile personalFile = new PersonalFile();
                    personalFile.setOrgId(account.getOrgId());
                    personalFile.setFileId(fileId);
                    personalFile = personalFileService.selectOnePersonalFile(personalFile);
                    String extName = personalFile.getFileName().split("\\.")[1];
                    personalFile.setFileName(fileName + "." + extName);
                    return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, personalFileService.updatePersonFile(personalFile, example));
                } else {
                    Example example = new Example(PersonalFileFolder.class);
                    example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("folderId", fileId);
                    PersonalFileFolder personalFileFolder = new PersonalFileFolder();
                    personalFileFolder.setFolderName(fileName);
                    return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, personalFileFolderService.updatePersonFileFolder(personalFileFolder, example));
                }
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param personalFile
     * @return RetDataBean
     * @Title insertPersonalFile
     * @Description  添加文件
     */
    @RequestMapping(value = "/insertPersonalFile", method = RequestMethod.POST)
    public RetDataBean insertPersonalFile(PersonalFile personalFile) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("personalfile:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(personalFile.getAttach())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Attach attach = new Attach();
            attach.setOrgId(account.getOrgId());
            attach.setAttachId(personalFile.getAttach());
            attach = attachService.selectOne(attach);
            personalFile.setFileId(SysTools.getGUID());
            personalFile.setCreateUser(account.getAccountId());
            personalFile.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            personalFile.setOrgId(account.getOrgId());
            personalFile.setFileName(attach.getOldName());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, personalFileService.insertPersonalFile(personalFile));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param personalFileFolder
     * @return RetDataBean
     * @Title: updatePersonalFileFolderPriv
     * @Description:  更新个人文件柜
     */
    @RequestMapping(value = "/updatePersonalFileFolderPriv", method = RequestMethod.POST)
    public RetDataBean updatePersonalFileFolderPriv(PersonalFileFolder personalFileFolder) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(personalFileFolder.getFolderId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            personalFileFolder.setOrgId(account.getOrgId());
            Example example = new Example(PersonalFileFolder.class);
            example.createCriteria().andEqualTo("orgId", personalFileFolder.getOrgId()).andEqualTo("folderId", personalFileFolder.getFolderId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, personalFileFolderService.updatePersonFileFolder(personalFileFolder, example));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  fileId
     * @param @param  currentLocation
     * @param @param  fType
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: shearPersonalFile
     * @Description:  剪切个人文件柜
     */
    @RequestMapping(value = "/shearPersonalFile", method = RequestMethod.POST)
    public RetDataBean shearPersonalFile(String fileId, String currentLocation, String fType) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("personalfile:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(fileId) && StringUtils.isNotBlank(currentLocation) && StringUtils.isNotBlank(fType)) {
                if (fType.equals("file")) {
                    PersonalFile personalFile = new PersonalFile();
                    personalFile.setFileId(fileId);
                    personalFile.setOrgId(account.getOrgId());
                    personalFile = personalFileService.selectOnePersonalFile(personalFile);
                    return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, personalFileService.shearPersonalFile(personalFile, currentLocation));
                } else {
                    PersonalFileFolder personalFileFolder = new PersonalFileFolder();
                    personalFileFolder.setFolderId(fileId);
                    personalFileFolder.setOrgId(account.getOrgId());
                    personalFileFolder = personalFileFolderService.selectOnePersonalFileFolder(personalFileFolder);
                    return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, personalFileFolderService.shearPersonalFileFolder(personalFileFolder, currentLocation));
                }
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  personalFileFolder
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: delPersonalFolder
     * @Description: 删除个人文件柜中的文件夹
     */
    @RequestMapping(value = "/delPersonalFolder", method = RequestMethod.POST)
    public RetDataBean delPersonalFolder(PersonalFileFolder personalFileFolder) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("personalfile:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            personalFileFolder.setOrgId(account.getOrgId());
            if (personalFileService.isExistChild(personalFileFolder.getFolderId(), account.getAccountId(), account.getOrgId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            }
            if (StringUtils.isBlank(personalFileFolder.getFolderId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, personalFileFolderService.delete(personalFileFolder));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  personalFile
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: delPersonalFile
     * @Description:  删除个人文件柜文件
     */
    @RequestMapping(value = "/delPersonalFile", method = RequestMethod.POST)
    public RetDataBean delPersonalFile(PersonalFile personalFile) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("personalfile:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            personalFile.setOrgId(account.getOrgId());
            if (StringUtils.isBlank(personalFile.getFileId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, personalFileService.deletePersonalFile(personalFile));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  currentLocation
     * @param @param  fileId
     * @param @param  type
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: pastePersonalFile
     * @Description:  粘贴个人文件
     */
    @RequestMapping(value = "/pastePersonalFile", method = RequestMethod.POST)
    public RetDataBean pastePersonalFile(String currentLocation, String fileId, String type) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("personalfile:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(currentLocation) && StringUtils.isNotBlank(fileId) && StringUtils.isNotBlank(type)) {
                if (type.equals("file")) {
                    PersonalFile personalFile = new PersonalFile();
                    personalFile.setOrgId(account.getOrgId());
                    personalFile.setFileId(fileId);
                    return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, personalFileService.pastePersonalFile(personalFile, currentLocation));
                } else {
                    PersonalFileFolder personFileFolder = new PersonalFileFolder();
                    personFileFolder.setOrgId(account.getOrgId());
                    personFileFolder.setFolderId(fileId);
                    return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, personalFileFolderService.pastePersonalFileFolder(personFileFolder, currentLocation));
                }
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param netDisk
     * @return RetDataBean
     * @Title createNetDisk
     * @Description  创建网盘
     */
    @RequestMapping(value = "/createNetDisk", method = RequestMethod.POST)
    public RetDataBean createNetDisk(NetDisk netDisk) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            netDisk.setNetDiskId(SysTools.getGUID());
            netDisk.setDiskCreateUser(account.getAccountId());
            netDisk.setDiskCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            netDisk.setOrgId(account.getOrgId());
            if(netDisk.getSpaceLimit()==null)
            {
                netDisk.setSpaceLimit(0);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, netDiskService.insertNetDisk(netDisk));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 网络硬盘得命名
     *
     * @param netDisk
     * @param rootPath
     * @param newName
     * @return
     */
    @RequestMapping(value = "/renameFileFolderForNetDisk", method = RequestMethod.POST)
    public RetDataBean renameFileFolderForNetDisk(NetDisk netDisk, String rootPath, String newName) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            netDisk.setOrgId(account.getOrgId());
            netDisk.setRootPath(null);
            netDisk = netDiskService.selectOneNetDisk(netDisk);
            String root = "";
            if (netDisk.getRootPath().endsWith("/") || netDisk.getRootPath().endsWith("\\")) {
                root = netDisk.getRootPath().substring(0, netDisk.getRootPath().length() - 1);
            } else {
                root = netDisk.getRootPath();
            }
            String sourcePath = (root + rootPath).replace("/", File.separator).replace("\\", File.separator);
            String targetPath = sourcePath.substring(0, sourcePath.lastIndexOf(File.separator)) + File.separator + newName;
            FileTools.renameFileName(sourcePath, targetPath);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 删除网络硬盘文件夹
     *
     * @param netDisk
     * @param rootPath
     * @param newName
     * @return
     */
    @RequestMapping(value = "/delFileFolderForNetDisk", method = RequestMethod.POST)
    public RetDataBean delFileFolderForNetDisk(NetDisk netDisk, String rootPath, String newName) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            netDisk.setOrgId(account.getOrgId());
            netDisk.setRootPath(null);
            netDisk = netDiskService.selectOneNetDisk(netDisk);
            String root = "";
            if (netDisk.getRootPath().endsWith("/") || netDisk.getRootPath().endsWith("\\")) {
                root = netDisk.getRootPath().substring(0, netDisk.getRootPath().length() - 1);
            }
            String sourcePath = root + rootPath;
            FileTools.deleteDirectory(sourcePath);
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  folderName
     * @param @param  netDisk
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: createNetDiskFile
     * @Description:  网络硬盘创建文件夹
     */
    @RequestMapping(value = "/createNetDiskFolder", method = RequestMethod.POST)
    public RetDataBean createNetDiskFolder(String sourcePath, String folderName, NetDisk netDisk) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            if (StringUtils.isBlank(folderName) || StringUtils.isBlank(sourcePath)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            netDisk.setOrgId(account.getOrgId());
            UserInfo userInfo = userInfoService.getUserInfoByAccountId(account.getAccountId(), account.getOrgId());
            if (account.getOpFlag().equals("1")) {
                netDisk = netDiskService.selectOneNetDisk(netDisk);
                return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, netDiskService.createNetDiskFolder(sourcePath, folderName, netDisk));
            } else {
                netDisk = netDiskService.selectOneNetDisk(netDisk);
                int flag = netDiskService.getCreateFilePriv(account.getOrgId(), netDisk.getNetDiskId(), account.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel());
                if (flag > 0) {
                    return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, netDiskService.createNetDiskFolder(sourcePath, folderName, netDisk));
                } else {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
                }
            }

        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  sourcePath
     * @param @param  netDisk
     * @param @return UploadException 设定文件
     * @return RetDataBean 返回类型
     * @Title: createNetDiskFile
     * @Description:  在网络硬盘中创建文件
     */
    @RequestMapping("/createNetDiskFile")
    public void createNetDiskFile(HttpServletRequest request, HttpServletResponse response, String sourcePath, NetDisk netDisk) {
        RetDataBean retDataBean = new RetDataBean();
        PrintWriter out = null;
        Subject subject = SecurityUtils.getSubject();
        if (!subject.isPermitted("netdisk:insert")) {
            try {
                retDataBean = RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
                String s = JSON.toJSONString(retDataBean);
                response.setContentType("text/html; charset=utf-8");
                out = response.getWriter().append(s);
                out.close();
            } catch (Exception e) {
            } finally {
                if (out != null) {
                    out.close();
                }
            }
        } else {
            Account account = accountService.getRedisAUserInfoToAccount();
            netDisk.setOrgId(account.getOrgId());
            netDisk = netDiskService.selectOneNetDisk(netDisk);
            Collection<Part> parts = null;
            try {
                parts = request.getParts();
            } catch (IOException e) {
                RetDataTools.Error(e.getMessage());
            } catch (ServletException e) {
                RetDataTools.Error(e.getMessage());
            }
            try {
                int code = netDiskService.createNetDiskFile(parts, sourcePath, notallow, netDisk);
                String msg = "";
                if (code == 1) {
                    msg = MessageCode.MESSAGE_SUCCESS;
                } else if (code == 2) {
                    msg = MessageCode.MESSAGE_FILE_EXIST;
                } else if (code == 3) {
                    msg = MessageCode.MESSAGE_FILE_NAME_EMPTY;
                } else if (code == 4) {
                    msg = MessageCode.MESSAGE_FILE_EXTNAME_ERROR;
                }
                retDataBean = RetDataTools.Ok(msg);
                String s = JSON.toJSONString(retDataBean);
                response.setContentType("text/html; charset=utf-8");
                out = response.getWriter().append(s);
                out.close();
            } catch (Exception e) {
            } finally {
                if (out != null) {
                    out.close();
                }
            }
        }
    }

    /**
     * @param @param  request
     * @param @param  sourcePath
     * @param @param  netDisk
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: delNetDiskfile
     * @Description:  删除文件
     */
    @RequestMapping(value = "/delNetDiskfile", method = RequestMethod.POST)
    public RetDataBean delNetDiskfile(String sourcePath, NetDisk netDisk) {
        Subject subject = SecurityUtils.getSubject();
        if (!subject.isPermitted("netdisk:delete")) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
        }
        try {
            if (StringUtils.isBlank(sourcePath)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            netDisk.setOrgId(account.getOrgId());
            UserInfo userInfo = userInfoService.getUserInfoByAccountId(account.getAccountId(), account.getOrgId());
            if (account.getOpFlag().equals("1")) {
                netDisk = netDiskService.selectOneNetDisk(netDisk);
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, netDiskService.delNetDiskFile(sourcePath, netDisk));
            } else {
                netDisk = netDiskService.selectOneNetDisk(netDisk);
                int flag = netDiskService.getCreateFilePriv(account.getOrgId(), netDisk.getNetDiskId(), account.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel());
                if (flag > 0) {
                    return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, netDiskService.delNetDiskFile(sourcePath, netDisk));
                } else {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
                }
            }

        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param netDisk
     * @return RetDataBean
     * @Title deleteNetDisk
     * @Description  删除网盘
     */
    @RequestMapping(value = "/deleteNetDisk", method = RequestMethod.POST)
    public RetDataBean deleteNetDisk(NetDisk netDisk) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(netDisk.getNetDiskId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            netDisk.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, netDiskService.deleteNetDisk(netDisk));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  netDisk
     * @param @param  sourcePath
     * @param @param  newName
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: renameNetDiskFileName
     * @Description:  文件重命名
     */
    @RequestMapping(value = "/renameNetDiskFileName", method = RequestMethod.POST)
    public RetDataBean renameNetDiskFileName(NetDisk netDisk, String sourcePath, String newFileName) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(netDisk.getNetDiskId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            netDisk.setOrgId(account.getOrgId());
            netDisk = netDiskService.selectOneNetDisk(netDisk);
            if (netDiskService.renameNetDiskFileName(sourcePath, netDisk, newFileName)) {
                return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
            }
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param netDisk
     * @return RetDataBean
     * @Title updateNetDisk
     * @Description  更新网盘
     */
    @RequestMapping(value = "/updateNetDisk", method = RequestMethod.POST)
    public RetDataBean updateNetDisk(NetDisk netDisk) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(netDisk.getNetDiskId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            netDisk.setOrgId(account.getOrgId());
            Example example = new Example(NetDisk.class);
            example.createCriteria().andEqualTo("orgId", netDisk.getOrgId()).andEqualTo("netDiskId", netDisk.getNetDiskId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, netDiskService.updateNetDisk(netDisk, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @param  request
     * @param @param  sourcePath
     * @param @param  targetPath
     * @param @param  sourceNetDiskId
     * @param @param  targetNetDiskId
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: copyFile
     * @Description:  文件复制
     */
    @RequestMapping(value = "/copyFile", method = RequestMethod.POST)
    public RetDataBean copyFile(String sourcePath, String targetPath, String sourceNetDiskId, String targetNetDiskId) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(sourceNetDiskId) || StringUtils.isBlank(targetNetDiskId)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                NetDisk sourceNetDisk = new NetDisk();
                sourceNetDisk.setNetDiskId(sourceNetDiskId);
                sourceNetDisk.setOrgId(account.getOrgId());
                sourceNetDisk = netDiskService.selectOneNetDisk(sourceNetDisk);
                NetDisk targetNetDiks = new NetDisk();
                targetNetDiks.setNetDiskId(sourceNetDiskId);
                targetNetDiks.setOrgId(account.getOrgId());
                targetNetDiks = netDiskService.selectOneNetDisk(targetNetDiks);
                if (netDiskService.copyFile(sourcePath, sourceNetDisk, targetPath, targetNetDiks)) {
                    return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
                } else {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
                }
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 网络硬盘文件剪切
     *
     * @param sourcePath
     * @param targetPath
     * @param sourceNetDiskId
     * @param targetNetDiskId
     * @return
     */
    @RequestMapping(value = "/shearFile", method = RequestMethod.POST)
    public RetDataBean shearFile(String sourcePath, String targetPath, String sourceNetDiskId, String targetNetDiskId, String fileType) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(sourceNetDiskId) || StringUtils.isBlank(targetNetDiskId)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                NetDisk sourceNetDisk = new NetDisk();
                sourceNetDisk.setNetDiskId(sourceNetDiskId);
                sourceNetDisk.setOrgId(account.getOrgId());
                sourceNetDisk = netDiskService.selectOneNetDisk(sourceNetDisk);
                NetDisk targetNetDiks = new NetDisk();
                targetNetDiks.setNetDiskId(sourceNetDiskId);
                targetNetDiks.setOrgId(account.getOrgId());
                targetNetDiks = netDiskService.selectOneNetDisk(targetNetDiks);
                if (netDiskService.copyFile(sourcePath, sourceNetDisk, targetPath, targetNetDiks)) {
                    if (StringUtils.isNotBlank(fileType)) {
                        if (fileType.equals("file")) {
                            netDiskService.delNetDiskFile(sourcePath, sourceNetDisk);
                        } else if (fileType.equals("folder")) {
                            netDiskService.delNetDiskFolder(sourcePath, sourceNetDisk);
                        }
                    }
                    return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
                } else {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
                }
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  user
     * @param @param  dept
     * @param @param  level
     * @param @param  range
     * @param @param  netDiskId
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: addNetDiskPriv
     * @Description:  批量更新权限
     */

    @RequestMapping(value = "/addNetDiskPriv", method = RequestMethod.POST)
    public RetDataBean addNetDiskPriv(String user,
                                      String dept,
                                      String level,
                                      String range,
                                      String netDiskId
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(netDiskId)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, netDiskService.addNetDiskPriv(account, netDiskId, user, dept, level, range));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @param  request
     * @param @param  user
     * @param @param  dept
     * @param @param  level
     * @param @param  range
     * @param @param  netDiskId
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: removeNetDiskPriv
     * @Description:  批量移除权限
     */
    @RequestMapping(value = "/removeNetDiskPriv", method = RequestMethod.POST)
    public RetDataBean removeNetDiskPriv(String user,
                                         String dept,
                                         String level,
                                         String range,
                                         String netDiskId
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("netdisk:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(netDiskId)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, netDiskService.removeNetDiskPriv(account, netDiskId, user, dept, level, range));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  publicFileFolder
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: createPublicFile
     * @Description:  创建公共文件柜文件
     */
    @RequestMapping(value = "/createPublicFile", method = RequestMethod.POST)
    public RetDataBean createPublicFile(PublicFile publicFile, String msgType) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("publicfile:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            if (StringUtils.isBlank(publicFile.getAttach())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Attach attach = new Attach();
            attach.setOrgId(account.getOrgId());
            attach.setAttachId(publicFile.getAttach());
            attach = attachService.selectOne(attach);
            publicFile.setFileId(SysTools.getGUID());
            publicFile.setCreateUser(account.getAccountId());
            publicFile.setFileName(attach.getOldName());
            publicFile.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            publicFile.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, publicFileService.createPublicFile(account, userInfo, publicFile, msgType));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deletePublicFile
     * @Description:  删除公共文件柜文件
     * @param: request
     * @param: publicFile
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deletePublicFile", method = RequestMethod.POST)
    public RetDataBean deletePublicFile(PublicFile publicFile) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("publicfile:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(publicFile.getFileId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            publicFile.setOrgId(userInfo.getOrgId());
            publicFile = publicFileService.selectOnePublicFile(publicFile);
            if (account.getOpFlag().equals("1")) {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, publicFileService.deletePublicFile(publicFile));
            } else {
                boolean flag = publicFileFolderService.haveManagePriv(userInfo.getOrgId(), publicFile.getFolderId(), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel());
                if (flag) {
                    return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, publicFileService.deletePublicFile(publicFile));
                } else {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
                }
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  publicFileFolder
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: createPublicFileFolder
     * @Description:  创建公共文件夹
     */
    @RequestMapping(value = "/createPublicFileFolder", method = RequestMethod.POST)
    public RetDataBean createPublicFileFolder(PublicFileFolder publicFileFolder) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            publicFileFolder.setFolderId(SysTools.getGUID());
            publicFileFolder.setCreateUser(account.getAccountId());
            publicFileFolder.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            publicFileFolder.setOrgId(account.getOrgId());
            publicFileFolder.setStatus("0");
            if (StringUtils.isBlank(publicFileFolder.getSpaceLimit())) {
                publicFileFolder.setSpaceLimit("0");
            }
            if (StringUtils.isBlank(publicFileFolder.getOwner())) {
                publicFileFolder.setOwner(account.getAccountId());
            }
            if (StringUtils.isBlank(publicFileFolder.getFolderLevel())) {
                publicFileFolder.setFolderLevel("0");
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, publicFileFolderService.insertPublicFileFolder(publicFileFolder));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: setPublicFilePriv
     * @Description:  设置公共文件夹权限
     * @param: request
     * @param: publicFileFolder
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/setPublicFilePriv", method = RequestMethod.POST)
    public RetDataBean setPublicFilePriv(PublicFileFolder publicFileFolder) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(publicFileFolder.getFolderId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            if (!account.getOpFlag().equals("1")) {
                PublicFileFolder newPublicFileFolder = new PublicFileFolder();
                newPublicFileFolder.setFolderId(publicFileFolder.getFolderId());
                newPublicFileFolder.setOrgId(account.getOrgId());
                newPublicFileFolder = publicFileFolderService.selectOnePublicFileFolder(newPublicFileFolder);
                if (!newPublicFileFolder.getCreateUser().equals(account.getAccountId())) {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
                }
            }
            Example example = new Example(PublicFileFolder.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("folderId", publicFileFolder.getFolderId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, publicFileFolderService.updatePublicFileFolder(publicFileFolder, example));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  user
     * @param @param  dept
     * @param @param  level
     * @param @param  range
     * @param @param  folderId
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: addPublicFolderPriv
     * @Description:  批量添加公共文件柜权限
     */
    @RequestMapping(value = "/addPublicFolderPriv", method = RequestMethod.POST)
    public RetDataBean addPublicFolderPriv(String user,
                                           String dept,
                                           String level,
                                           String range,
                                           String folderId
    ) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(folderId)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, publicFileFolderService.addPublicFileFolderPriv(account, folderId, user, dept, level, range));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  user
     * @param @param  dept
     * @param @param  level
     * @param @param  range
     * @param @param  folderId
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: removePublicFolderPriv
     * @Description:  批量删除权限
     */
    @RequestMapping(value = "/removePublicFolderPriv", method = RequestMethod.POST)
    public RetDataBean removePublicFolderPriv(String user,
                                              String dept,
                                              String level,
                                              String range,
                                              String folderId
    ) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(folderId)) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, publicFileFolderService.removePublicFileFolderPriv(account, folderId, user, dept, level, range));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


}
