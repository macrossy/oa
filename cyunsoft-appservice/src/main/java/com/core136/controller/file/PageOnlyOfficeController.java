package com.core136.controller.file;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.file.Attach;
import com.core136.bean.file.NetDisk;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserInfoService;
import com.core136.service.file.AttachService;
import com.core136.service.file.NetDiskService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.net.URLDecoder;

@Controller
@RequestMapping("/app/core")
public class PageOnlyOfficeController {

    @Value("${onlyoffice.service.host}")
    private String onlyofficeService;

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private AttachService attachService;

    @Autowired
    public void setAttachService(AttachService attachService) {
        this.attachService = attachService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private NetDiskService netDiskService;

    @Autowired
    public void setNetDiskService(NetDiskService netDiskService) {
        this.netDiskService = netDiskService;
    }


    @RequestMapping("/onlyoffice")
    public ModelAndView goEditView(HttpServletRequest request, String openModeType, String netDiskId, String isNetDisk, String attachId) {
        ModelAndView mv = null;
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            Attach attach = new Attach();
            if (StringUtils.isNotBlank(isNetDisk)) {
                NetDisk netDisk = new NetDisk();
                netDisk.setNetDiskId(netDiskId);
                netDisk.setOrgId(account.getOrgId());
                netDisk = netDiskService.selectOneNetDisk(netDisk);
                attach.setOrgId(netDisk.getOrgId());
                attach.setUpTime(netDisk.getDiskCreateTime());
                String fileName = URLDecoder.decode(attachId,"utf-8");
                fileName = fileName.substring(fileName.lastIndexOf(File.separator) + 1);
                attach.setOldName(fileName);
                attach.setExtName(fileName.substring(fileName.lastIndexOf(".")).toLowerCase());
                UserInfo userInfo = userInfoService.getUserInfoByAccountId(netDisk.getDiskCreateUser(), attach.getOrgId());
                attach.setCreateAccount(userInfo.getUserName());
            } else {
                account.setOrgId(account.getOrgId());
                attach.setAttachId(attachId);
                attach = attachService.selectOne(attach);
                UserInfo userInfo = userInfoService.getUserInfoByAccountId(attach.getCreateAccount(), attach.getOrgId());
                attach.setCreateAccount(userInfo.getUserName());
            }
            if (attach.getExtName().equals(".doc") || attach.getExtName().equals(".docx")
                    || attach.getExtName().equals(".docm") || attach.getExtName().equals(".docxf")
                    || attach.getExtName().equals(".dot") || attach.getExtName().equals(".dotm")
                    || attach.getExtName().equals(".dotx") || attach.getExtName().equals(".epub")
                    || attach.getExtName().equals(".htm") || attach.getExtName().equals(".html")
                    || attach.getExtName().equals(".mht") || attach.getExtName().equals(".odt")
                    || attach.getExtName().equals(".oform") || attach.getExtName().equals(".ott")
                    || attach.getExtName().equals(".oxps") || attach.getExtName().equals(".pdf")
                    || attach.getExtName().equals(".rtf") || attach.getExtName().equals(".txt")
                    || attach.getExtName().equals(".djvu") || attach.getExtName().equals(".xml")
                    || attach.getExtName().equals(".xps")) {
                mv = new ModelAndView("app/core/onlyoffice/word");
                String extName = attach.getExtName().substring(1);
                mv.addObject("extName", extName);
                mv.addObject("documentType", "word");
            } else if (attach.getExtName().equals(".xls") || attach.getExtName().equals(".xlsx")
                    || attach.getExtName().equals(".csv") || attach.getExtName().equals(".fods")
                    || attach.getExtName().equals(".ods") || attach.getExtName().equals(".ots")
                    || attach.getExtName().equals(".xlsm") || attach.getExtName().equals(".xlt")
                    || attach.getExtName().equals(".xltm") || attach.getExtName().equals(".xltx")) {
                String extName = attach.getExtName().substring(1);
                mv = new ModelAndView("app/core/onlyoffice/excel");
                mv.addObject("extName", extName);
                mv.addObject("documentType", "cell");
            } else if (attach.getExtName().equals(".ppt") || attach.getExtName().equals(".pptx")
                    || attach.getExtName().equals(".fodp") || attach.getExtName().equals(".odp")
                    || attach.getExtName().equals(".otp") || attach.getExtName().equals(".pot")
                    || attach.getExtName().equals(".potx") || attach.getExtName().equals(".pps")
                    || attach.getExtName().equals(".ppsm") || attach.getExtName().equals(".ppsx")
                    || attach.getExtName().equals(".pptm")) {
                String extName = attach.getExtName().substring(1);
                mv = new ModelAndView("app/core/onlyoffice/ppt");
                mv.addObject("extName", extName);
                mv.addObject("documentType", "slide");
            }
            mv.addObject("fileName", attach.getOldName());
            mv.addObject("orgId", account.getOrgId());
            mv.addObject("attachId", attachId);
            mv.addObject("accountId", account.getAccountId());
            mv.addObject("onlyofficeService", onlyofficeService);
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
            mv.addObject("basePath", basePath);
            if (StringUtils.isBlank(openModeType)) {
                mv.addObject("openModeType", "view");
            } else {
                if (openModeType.equals("4") || openModeType.equals("5")) {
                    mv.addObject("openModeType", "edit");
                    mv.addObject("print", true);
                    mv.addObject("copy", true);
                    mv.addObject("download", true);
                    mv.addObject("edit", true);
                } else {
                    mv.addObject("edit", false);
                    if (openModeType.equals("3")) {
                        mv.addObject("print", true);
                        mv.addObject("download", true);
                    } else if (openModeType.equals("2")) {
                        mv.addObject("print", true);
                        mv.addObject("download", false);
                    } else {
                        mv.addObject("print", false);
                        mv.addObject("download", false);
                    }
                    mv.addObject("copy", false);
                    mv.addObject("openModeType", "view");
                }
            }
            String type = "desktop";
            if (SysTools.isMobileDevice(request)) {
                type = "mobile";
            }
            mv.addObject("type", type);
            mv.addObject("netDiskId", netDiskId);
            mv.addObject("isNetDisk", isNetDisk);
            mv.addObject("owner", attach.getCreateAccount());
            mv.addObject("uploaded", attach.getUpTime());
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
