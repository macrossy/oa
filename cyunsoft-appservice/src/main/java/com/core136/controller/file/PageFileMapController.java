package com.core136.controller.file;

import com.core136.bean.account.Account;
import com.core136.service.account.AccountService;
import com.core136.service.file.FileMapItemService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
@RequestMapping("/app/core")
public class PageFileMapController {
    private FileMapItemService fileMapItemService;

    @Autowired
    public void setFileMapItemService(FileMapItemService fileMapItemService) {
        this.fileMapItemService = fileMapItemService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * 文档目录
     *
     * @return
     */
    @RequestMapping("/filemap/filemapsort")
    public ModelAndView goFileMapSort() {
        try {
            return new ModelAndView("app/core/fileshare/filemap/sort");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 文档分类
     *
     * @return
     */
    @RequestMapping("/filemap/filemapitem")
    public ModelAndView goFileMapItem() {
        try {
            return new ModelAndView("app/core/fileshare/filemap/fileitem");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    @RequestMapping("/filemap/filemapsearch")
    public ModelAndView goFileMapSearch(String itemId) {
        try {
            if(StringUtils.isNotBlank(itemId))
            {
                return new ModelAndView("app/core/fileshare/filemap/search");
            }else {
                return new ModelAndView("app/core/fileshare/filemap/searchquery");
            }
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


    @RequestMapping("/filemap/filemaprecord")
    public ModelAndView goFileMapRecord() {
        try {
            return new ModelAndView("app/core/fileshare/filemap/filerecord");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    @RequestMapping("/filemap/filelist")
    public ModelAndView goFileMapFileList(String itemId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            Map<String, String> map = fileMapItemService.getManagePriv(account.getOrgId(), itemId, account.getAccountId());
            return new ModelAndView("app/core/fileshare/filemap/filelist").addObject("managePriv", map);
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 我的文档目录
     *
     * @return
     */
    @RequestMapping("/filemap/myfilelist")
    public ModelAndView goMyFileList() {
        try {
            return new ModelAndView("app/core/fileshare/filemap/myfilelist");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


}
