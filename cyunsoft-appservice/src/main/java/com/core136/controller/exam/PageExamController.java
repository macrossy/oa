package com.core136.controller.exam;

import com.core136.bean.account.Account;
import com.core136.bean.exam.ExamGrade;
import com.core136.service.account.AccountService;
import com.core136.service.exam.ExamGradeService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/app/core/exam")
public class PageExamController {
    private ExamGradeService examGradeService;

    @Autowired
    public void setExamGradeService(ExamGradeService examGradeService) {
        this.examGradeService = examGradeService;
    }

    private AccountService accountService;

    @Autowired
    private void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * 考试成绩管理
     *
     * @return
     */
    @RequestMapping("/grademanage")
    @RequiresPermissions("/app/core/exam/grademanage")
    public ModelAndView goGradeManage(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/exam/grademanage");
            } else {
                if (view.equals("old")) {
                    mv = new ModelAndView("/app/core/exam/gradeoldmanage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 阅卷
     *
     * @return
     */
    @RequestMapping("/approvalexamtest")
    @RequiresPermissions("/app/core/exam/approvalexamtest")
    public ModelAndView goApprovalExamTest() {
        try {
            return new ModelAndView("/app/core/exam/approvalexamtest");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 试卷详情
     *
     * @return
     */
    @RequestMapping("/examquestionrecorddetails")
    @RequiresPermissions("/app/core/exam/examquestionrecorddetails")
    public ModelAndView goExamQuestionRecordDetails() {
        try {
            return new ModelAndView("/app/core/exam/examquestionrecorddetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 开始考试
     *
     * @return
     */
    @RequestMapping("/begintest")
    @RequiresPermissions("/app/core/exam/beginTest")
    public ModelAndView goBeginTest(String recordId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            ExamGrade examGrade = new ExamGrade();
            examGrade.setOrgId(account.getOrgId());
            examGrade.setExamTestId(recordId);
            examGrade = examGradeService.selectOneExamGrade(examGrade);
            String flagStr = "";
            if (examGrade == null) {
                flagStr = "1";
            }
            return new ModelAndView("/app/core/exam/begintest").addObject("flagStr", flagStr);
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 获取个人考试表表
     *
     * @return
     */
    @RequestMapping("/myexamtest")
    @RequiresPermissions("/app/core/exam/myexamtest")
    public ModelAndView goMyExamTest() {
        try {
            return new ModelAndView("/app/core/exam/myexamtest");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 试卷管理
     *
     * @return
     */
    @RequestMapping("/examquestionsrecord")
    @RequiresPermissions("/app/core/exam/examquestionsrecord")
    public ModelAndView goExamQuestionsRecord(String view) {
        try {
            if (StringUtils.isBlank(view)) {
                return new ModelAndView("/app/core/exam/examquestionsrecordmanage");
            } else {
                if (view.equals("create")) {
                    return new ModelAndView("/app/core/exam/examquestionsrecord");
                }
            }
            return null;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 试管详情
     *
     * @return
     */
    @RequestMapping("/examtestdetails")
    @RequiresPermissions("/app/core/exam/examtestdetails")
    public ModelAndView goExamTestDetails() {
        try {
            return new ModelAndView("/app/core/exam/examtestdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 测试管理
     *
     * @param view
     * @return
     */
    @RequestMapping("/setexamtest")
    @RequiresPermissions("/app/core/exam/setexamtest")
    public ModelAndView goSetExamTest(String view) {
        try {
            if (StringUtils.isBlank(view)) {
                return new ModelAndView("/app/core/exam/setexamtestmanage");
            } else {
                if (view.equals("create")) {
                    return new ModelAndView("/app/core/exam/setexamtest");
                }
            }
            return null;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 试题详情
     *
     * @return
     */
    @RequestMapping("/questionsdetails")
    @RequiresPermissions("/app/core/exam/questionsdetails")
    public ModelAndView goExamQuestionsDetails() {
        try {
            return new ModelAndView("/app/core/exam/questionsdetails");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 题库管理
     *
     * @return
     */
    @RequestMapping("/setquestions")
    @RequiresPermissions("/app/core/exam/setquestions")
    public ModelAndView goExamQuestions() {
        try {
            return new ModelAndView("/app/core/exam/setquestions");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 考试分类
     *
     * @return
     */
    @RequestMapping("/examtestsort")
    @RequiresPermissions("/app/core/exam/examtestsort")
    public ModelAndView goExamTestSort() {
        try {
            return new ModelAndView("/app/core/exam/examtestsort");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: goExamSort
     * @Description:  试题分类设置
     */
    @RequestMapping("/examsort")
    @RequiresPermissions("/app/core/exam/examsort")
    public ModelAndView goExamSort() {
        try {
            return new ModelAndView("/app/core/exam/examsort");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }


}
