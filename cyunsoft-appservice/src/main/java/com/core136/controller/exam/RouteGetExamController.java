package com.core136.controller.exam;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.exam.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.exam.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 考试API接口
 */
@RestController
@RequestMapping("/ret/examget")
public class RouteGetExamController {
    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private ExamSortService examSortService;

    @Autowired
    public void setExamSortService(ExamSortService examSortService) {
        this.examSortService = examSortService;
    }

    private ExamQuestionsService examQuestionsService;

    @Autowired
    public void setExamQuestionsService(ExamQuestionsService examQuestionsService) {
        this.examQuestionsService = examQuestionsService;
    }

    private ExamTestService examTestService;

    @Autowired
    public void setExamTestService(ExamTestService examTestService) {
        this.examTestService = examTestService;
    }

    private ExamQuestionRecordService examQuestionRecordService;

    @Autowired
    public void setExamQuestionRecordService(ExamQuestionRecordService examQuestionRecordService) {
        this.examQuestionRecordService = examQuestionRecordService;
    }

    private ExamTestSortService examTestSortService;

    @Autowired
    public void setExamTestSortService(ExamTestSortService examTestSortService) {
        this.examTestSortService = examTestSortService;
    }

    private ExamGradeService examGradeService;

    @Autowired
    public void setExamGradeService(ExamGradeService examGradeService) {
        this.examGradeService = examGradeService;
    }

    /**
     * 查分
     *
     * @param examTestId
     * @return
     */
    @RequestMapping(value = "/getMyGrade", method = RequestMethod.POST)
    public RetDataBean getMyGrade(String examTestId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            ExamGrade examGrade = new ExamGrade();
            examGrade.setOrgId(account.getOrgId());
            examGrade.setAccountId(account.getAccountId());
            examGrade.setExamTestId(examTestId);
            examGrade = examGradeService.selectOneExamGrade(examGrade);
            if (examGrade == null) {
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, "没有参考记录");
            } else {
                if (examGrade.getStatus().equals("0")) {
                    return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, "判卷未结束！");
                } else {
                    return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examGrade.getGrade() + "分");
                }
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取考试成绩列表
     *
     * @param pageParam
     * @param accountId
     * @param examTestId
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getExamGradeList", method = RequestMethod.POST)
    public RetDataBean getExamGradeList(PageParam pageParam, String accountId, String examTestId, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("g.begin_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(accountId);
            PageInfo<Map<String, String>> pageInfo = examGradeService.getExamGradeList(pageParam, examTestId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 历史成绩管理
     *
     * @param pageParam
     * @param accountId
     * @param examTestId
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getExamGradeOldList", method = RequestMethod.POST)
    public RetDataBean getExamGradeOldList(PageParam pageParam, String accountId, String examTestId, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("g.begin_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setAccountId(accountId);
            PageInfo<Map<String, String>> pageInfo = examGradeService.getExamGradeOldList(pageParam, examTestId, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取个人考试表表
     *
     * @param pageParam
     * @return
     */
    @RequestMapping(value = "/getMyExamTestList", method = RequestMethod.POST)
    public RetDataBean getMyExamTestList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("t.begin_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setDeptId(userInfo.getDeptId());
            pageParam.setLevelId(userInfo.getLeadLevel());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = examTestService.getMyExamTestList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取试卷列表
     *
     * @param recordIds
     * @return
     */
    @RequestMapping(value = "/getExamQuestionRecordForSelect", method = RequestMethod.POST)
    public RetDataBean getExamQuestionRecordForSelect(String recordIds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examQuestionRecordService.getExamQuestionRecordForSelect(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getExamTestListForSelect", method = RequestMethod.POST)
    public RetDataBean getExamTestListForSelect() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examTestService.getExamTestListForSelect(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取试题列表
     *
     * @param recordIds
     * @return
     */
    @RequestMapping(value = "/getExamQuestionListByIds", method = RequestMethod.POST)
    public RetDataBean getExamQuestionListByIds(String recordIds) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examQuestionsService.getExamQuestionListByIds(account.getOrgId(), recordIds));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取试卷列表
     *
     * @param pageParam
     * @return
     */
    @RequestMapping(value = "/getExamQuestionsRecordList", method = RequestMethod.POST)
    public RetDataBean getExamQuestionsRecordList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = examQuestionRecordService.getExamQuestionsRecordList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 按分类获取试题列表
     *
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getExamQuestListForSelectBySortId", method = RequestMethod.POST)
    public RetDataBean getExamQuestListForSelectBySortId(String sortId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examQuestionsService.getExamQuestListForSelectBySortId(account.getOrgId(), sortId));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, String>>
     * @Title: getExamTestSortTree
     * @Description:  获取测试分类树结构
     */
    @RequestMapping(value = "/getExamTestSortTree", method = RequestMethod.POST)
    public List<Map<String, String>> getExamTestSortTree(String sortId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            return examTestSortService.getExamTestSortTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, String>>
     * @Title: getExamSortTree
     * @Description:  获取试题分类树结构
     */
    @RequestMapping(value = "/getExamSortTree", method = RequestMethod.POST)
    public List<Map<String, String>> getExamSortTree(String sortId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            return examSortService.getExamSortTree(account.getOrgId(), levelId);
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * @param request
     * @param examQuestions
     * @return RetDataBean
     * @Title: getExamQuestionsById
     * @Description:  获取试题详情
     */
    @RequestMapping(value = "/getExamQuestionsById", method = RequestMethod.POST)
    public RetDataBean getExamQuestionsById(HttpServletRequest request, ExamQuestions examQuestions) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            examQuestions.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examQuestionsService.selectOneExamQuestions(examQuestions));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param examTest
     * @return RetDataBean
     * @Title: getExamTestById
     * @Description:  获取试卷详情
     */
    @RequestMapping(value = "/getExamTestById", method = RequestMethod.POST)
    public RetDataBean getExamTestById(ExamTest examTest) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            examTest.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examTestService.selectOneExamTest(examTest));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取试卷管理列表
     *
     * @param pageParam
     * @param beginTime
     * @param endTime
     * @return
     */
    @RequestMapping(value = "/getExamTestManageList", method = RequestMethod.POST)
    public RetDataBean getExamTestManageList(PageParam pageParam, String beginTime, String endTime) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("t.sort_no");
            } else {
                pageParam.setSort("t." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = examTestService.getExamTestManageList(pageParam, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取考试成绩
     *
     * @param examGrade
     * @return
     */
    @RequestMapping(value = "/getExamGradeById", method = RequestMethod.POST)
    public RetDataBean getExamGradeById(ExamGrade examGrade) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            examGrade.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examGradeService.selectOneExamGrade(examGrade));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getExamQuestionRecordById", method = RequestMethod.POST)
    public RetDataBean getExamQuestionRecordById(ExamQuestionRecord examQuestionRecord) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            examQuestionRecord.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examQuestionRecordService.selectOneExamQuestionRecord(examQuestionRecord));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param examSort
     * @return RetDataBean
     * @Title: getExamSortById
     * @Description:  获取试卷类型
     */
    @RequestMapping(value = "/getExamSortById", method = RequestMethod.POST)
    public RetDataBean getExamSortById(ExamSort examSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            examSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examSortService.selectOneExamSort(examSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    @RequestMapping(value = "/getExamTestSortById", method = RequestMethod.POST)
    public RetDataBean getExamTestSortById(ExamTestSort examTestSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            examTestSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, examTestSortService.selectOneExamTestSort(examTestSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 按分类获取试题列表
     *
     * @param pageParam
     * @param sortId
     * @return
     */
    @RequestMapping(value = "/getExamQuestionsListBySortId", method = RequestMethod.POST)
    public RetDataBean getExamQuestionsListBySortId(PageParam pageParam, String sortId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("q.sort_no");
            } else {
                pageParam.setSort("q." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = examQuestionsService.getExamQuestionsListBySortId(pageParam, sortId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
