package com.core136.controller.news;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/app/core")
public class PageNewsController {
    /***
     * 新闻查看情况
     * @return
     */
    @RequestMapping("/news/readstatus")
    public ModelAndView goNewsReadStatus() {
        try {
            return new ModelAndView("app/core/news/readstatus");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @Title: goNews
     * @author:刘绍全
     * @Description: 新闻管理
     * @param: @return
     * @return: String
     */
    @RequestMapping("/news")
    @RequiresPermissions("/app/core/news")
    public ModelAndView goNews(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/news/index");
            } else {
                if (view.equals("edit")) {
                    mv = new ModelAndView("app/core/news/editnews");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param @return 设定文件
     * @return ModelAndView 返回类型
     * @Title: goMyNews
     * @Description:  跳转到我的新闻
     */
    @RequestMapping("/news/mynews")
    @RequiresPermissions("/app/core/news/mynews")
    public ModelAndView goMyNews() {
        try {
            return new ModelAndView("app/core/news/mynews");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    @RequestMapping("/news/readnews")
    @RequiresPermissions("/app/core/news/readnews")
    public ModelAndView goReadNews(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/news/readnews");
            } else {
                if (view.equals("mobile")) {
                    mv = new ModelAndView("app/mobile/main/news/details");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title goNewsManage
     * @Description  新闻维护
     */
    @RequestMapping("/news/manage")
    @RequiresPermissions("/app/core/news/manage")
    public ModelAndView goNewsManage() {
        try {
            return new ModelAndView("app/core/news/manage");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
