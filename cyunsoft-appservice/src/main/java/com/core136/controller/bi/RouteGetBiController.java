package com.core136.controller.bi;

import com.core136.bean.account.Account;
import com.core136.bean.bi.BiSort;
import com.core136.bean.bi.BiTemplate;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.service.account.AccountService;
import com.core136.service.bi.BiSortService;
import com.core136.service.bi.BiTemplateService;
import com.core136.service.bi.BiTypeService;
import com.core136.service.bi.JasperreportsUnit;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ret/biget")
public class RouteGetBiController {
    private BiSortService biSortService;

    @Autowired
    public void setBiSortService(BiSortService biSortService) {
        this.biSortService = biSortService;
    }

    private BiTypeService biTypeService;

    @Autowired
    public void setBiTypeService(BiTypeService biTypeService) {
        this.biTypeService = biTypeService;
    }

    private BiTemplateService biTemplateService;

    @Autowired
    public void setBiTemplateService(BiTemplateService biTemplateService) {
        this.biTemplateService = biTemplateService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private JasperreportsUnit jasperreportsUnit;

    @Autowired
    public void setJasperreportsUnit(JasperreportsUnit jasperreportsUnit) {
        this.jasperreportsUnit = jasperreportsUnit;
    }

    /**
     * @param request
     * @param response
     * @param biTemplate void
     * @Title: getJasReportForPdf
     * @Description:  获取pdf格式报表
     */
    @RequestMapping(value = "/getJasReportForPdf", method = RequestMethod.GET)
    public void getJasReportForPdf(HttpServletRequest request, HttpServletResponse response, BiTemplate biTemplate) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            biTemplate.setOrgId(account.getOrgId());
            jasperreportsUnit.getRepForPdf(request, response, biTemplate);
        } catch (Exception e) {
        }
    }

    /**
     * @param sortId
     * @return List<Map < String, Object>>
     * @Title: getBiSortTreeForParent
     * @Description:  获取BI分类结构
     */
    @RequestMapping(value = "/getBiSortTreeForParent", method = RequestMethod.POST)
    public List<Map<String, Object>> getBiSortTreeForParent(String sortId) {
        String levelId = "0";
        try {
            List<Map<String, Object>> tmplist1 = new ArrayList<Map<String, Object>>();
            List<Map<String, Object>> tmplist2 = new ArrayList<Map<String, Object>>();
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
                if (!levelId.equals("0")) {
                    tmplist1 = biTemplateService.getBiTemplateTree(account.getOrgId(), levelId, account.getAccountId());
                }
            }
            tmplist2 = biSortService.getBiSortTreeForParent(levelId, account.getOrgId());
            tmplist2.addAll(tmplist1);
            return tmplist2;
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * @param sortId
     * @return List<Map < String, Object>>
     * @Title getBiSortTree
     * @Description  获取BI分类结构
     */
    @RequestMapping(value = "/getBiSortTree", method = RequestMethod.POST)
    public List<Map<String, Object>> getBiSortTree(String sortId) {
        try {
            String levelId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                levelId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return biSortService.getBiSortTree(levelId, account.getOrgId());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @param biSort
     * @return RetDataBean
     * @Title getBiSortById
     * @Description  获取分类信息
     */
    @RequestMapping(value = "/getBiSortById", method = RequestMethod.POST)
    public RetDataBean getBiSortById(BiSort biSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            biSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, biSortService.selectOne(biSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getBiType
     * @Description:  获取BI类型
     */
    @RequestMapping(value = "/getBiType", method = RequestMethod.POST)
    public RetDataBean getBiType() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, biTypeService.getAllBiType(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  pageNumber
     * @param @param  pageSize
     * @param @param  search
     * @param @param  sort
     * @param @param  sortOrder
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getBiTemplateList
     * @Description:  按分类获取报表模版列表
     */
    @RequestMapping(value = "/getBiTemplateList", method = RequestMethod.POST)
    public RetDataBean getBiTemplateList(
            Integer pageNumber,
            Integer pageSize,
            String search,
            String sort,
            String sortOrder,
            String levelId
    ) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "b.sort_no";
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            String orderBy = sort + " " + sortOrder;
            Account account = accountService.getRedisAUserInfoToAccount();
            PageInfo<Map<String, Object>> pageInfo = biTemplateService.getBiTemplateList(pageNumber, pageSize, orderBy, levelId, account.getOrgId(), "%" + search + "%");
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @param  request
     * @param @param  biTemplate
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getBiTemplateById
     * @Description:  获取BI模版详情
     */
    @RequestMapping(value = "/getBiTemplateById", method = RequestMethod.POST)
    public RetDataBean getBiTemplateById(BiTemplate biTemplate) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            biTemplate.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, biTemplateService.selectOne(biTemplate));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }
}
