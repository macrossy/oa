package com.core136.controller.dataupload;

import com.core136.bean.account.Account;
import com.core136.bean.dataupload.DataUploadHandle;
import com.core136.bean.dataupload.DataUploadInfo;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.dataupload.DataUploadHandleService;
import com.core136.service.dataupload.DataUploadInfoService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/ret/datauploadget")
public class RouteGetDataInfoController {
    private DataUploadInfoService dataUploadInfoService;

    @Autowired
    public void setDataUploadInfoService(DataUploadInfoService dataUploadInfoService) {
        this.dataUploadInfoService = dataUploadInfoService;
    }

    private DataUploadHandleService dataUploadHandleService;

    @Autowired
    public void setDataUploadHandleService(DataUploadHandleService dataUploadHandleService) {
        this.dataUploadHandleService = dataUploadHandleService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param dataUploadHandle
     * @return RetDataBean
     * @Title: getDataUploadHandleById
     * @Description:  获取信息处理详情
     */
    @RequestMapping(value = "/getDataUploadHandleById", method = RequestMethod.POST)
    public RetDataBean getDataUploadHandleById(DataUploadHandle dataUploadHandle) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            dataUploadHandle.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, dataUploadHandleService.selectOneDataUploadHandle(dataUploadHandle));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param dataUploadInfo
     * @return RetDataBean
     * @Title: getDataUploadInfoById
     * @Description:  获取上报信息详情
     */
    @RequestMapping(value = "/getDataUploadInfoById", method = RequestMethod.POST)
    public RetDataBean getDataUploadInfoById(DataUploadInfo dataUploadInfo) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("dataupload:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            dataUploadInfo.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, dataUploadInfoService.selectOneDataUploadInfo(dataUploadInfo));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param deptId
     * @param fromAccountId
     * @param beginTime
     * @param endTime
     * @param dataType
     * @param approvedType
     * @return RetDataBean
     * @Title: getDataUploadInfoList
     * @Description:  获取上报信息列表
     */
    @RequestMapping(value = "/getDataUploadInfoList", method = RequestMethod.POST)
    public RetDataBean getDataUploadInfoList(PageParam pageParam, String deptId, String fromAccountId,
                                             String beginTime, String endTime, String dataType, String approvedType
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("dataupload:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = dataUploadInfoService.getDataUploadInfoList(pageParam, deptId, fromAccountId, beginTime, endTime, dataType, approvedType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param deptId
     * @param fromAccountId
     * @param beginTime
     * @param endTime
     * @param dataType
     * @param approvedType
     * @return RetDataBean
     * @Title: getToProcessInfoList
     * @Description:  获取持处理的信息列表
     */
    @RequestMapping(value = "/getToProcessInfoList", method = RequestMethod.POST)
    public RetDataBean getToProcessInfoList(PageParam pageParam, String deptId, String fromAccountId,
                                            String beginTime, String endTime, String dataType, String approvedType
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("dataupload:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = dataUploadInfoService.getToProcessInfoList(pageParam, deptId, fromAccountId, beginTime, endTime, dataType, approvedType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param deptId
     * @param fromAccountId
     * @param beginTime
     * @param endTime
     * @param dataType
     * @param approvedType
     * @return RetDataBean
     * @Title: getOldProcessInfoList
     * @Description:  信息处理历史列表
     */
    @RequestMapping(value = "/getOldProcessInfoList", method = RequestMethod.POST)
    public RetDataBean getOldProcessInfoList(PageParam pageParam, String deptId, String fromAccountId,
                                             String beginTime, String endTime, String dataType, String approvedType
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("dataupload:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("desc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = dataUploadInfoService.getOldProcessInfoList(pageParam, deptId, fromAccountId, beginTime, endTime, dataType, approvedType);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
