package com.core136.controller.discuss;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/app/core")
public class PageDiscussController {
    /**
     * 回帖
     *
     * @return
     */
    @RequestMapping("/discuss/revdiscuss")
    @RequiresPermissions("/app/core/discuss/revdiscuss")
    public ModelAndView revDiscuss() {
        try {
            return new ModelAndView("app/core/discuss/revdiscuss");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @return ModelAndView
     * @Title: setDiscussApproval
     * @Description:  讨论区主题审批
     */
    @RequestMapping("/discuss/approval")
    @RequiresPermissions("/app/core/discuss/approval")
    public ModelAndView setDiscussApproval() {
        try {
            return new ModelAndView("app/core/discuss/discussapproval");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: goDiscussIndex
     * @Description:  讨论区
     */
    @RequestMapping("/discuss/index")
    @RequiresPermissions("/app/core/discuss/index")
    public ModelAndView goDiscussIndex(HttpServletRequest request, String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("app/core/discuss/index");
            } else {
                if (view.equals("record")) {
                    mv = new ModelAndView("app/core/discuss/discussrecord");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * @param request
     * @return ModelAndView
     * @Title: setDiscuss
     * @Description:  讨论区设置
     */
    @RequestMapping("/discuss/setdiscuss")
    @RequiresPermissions("/app/core/discuss/setdiscuss")
    public ModelAndView setDiscuss(HttpServletRequest request) {
        try {
            return new ModelAndView("app/core/discuss/setdiscuss");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }
}
