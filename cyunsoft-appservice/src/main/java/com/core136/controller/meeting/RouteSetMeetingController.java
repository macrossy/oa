/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutSetMeetingController.java
 * @Package com.core136.controller.meeting
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月22日 上午10:11:35
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.controller.meeting;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.meeting.Meeting;
import com.core136.bean.meeting.MeetingDevice;
import com.core136.bean.meeting.MeetingNotes;
import com.core136.bean.meeting.MeetingRoom;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.meeting.MeetingDeviceService;
import com.core136.service.meeting.MeetingNotesService;
import com.core136.service.meeting.MeetingRoomService;
import com.core136.service.meeting.MeetingService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.mapper.entity.Example;

/**
 * @author lsq
 */
@RestController
@RequestMapping("/set/meetingset")
public class RouteSetMeetingController {
    private MeetingService meetingService;

    @Autowired
    public void setMeetingService(MeetingService meetingService) {
        this.meetingService = meetingService;
    }

    private MeetingRoomService meetingRoomService;

    @Autowired
    public void setMeetingRoomService(MeetingRoomService meetingRoomService) {
        this.meetingRoomService = meetingRoomService;
    }

    private MeetingDeviceService meetingDeviceService;

    @Autowired
    public void setMeetingDeviceService(MeetingDeviceService meetingDeviceService) {
        this.meetingDeviceService = meetingDeviceService;
    }

    private MeetingNotesService meetingNotesService;

    @Autowired
    public void setMeetingNotesService(MeetingNotesService meetingNotesService) {
        this.meetingNotesService = meetingNotesService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @param meeting
     * @return RetDataBean
     * @Title: cancelmeeting
     * @Description:  会议取消通知
     */
    @RequestMapping(value = "/cancelmeeting", method = RequestMethod.POST)
    public RetDataBean cancelmeeting(Meeting meeting) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("meeting:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            meeting.setOrgId(userInfo.getOrgId());
            meeting = meetingService.selectOneMeeting(meeting);
            meeting.setStatus("3");
            Example example = new Example(Meeting.class);
            example.createCriteria().andEqualTo("orgId", meeting.getOrgId()).andEqualTo("meetingId", meeting.getMeetingId());
            meetingService.sendCancelMeetingMsg(userInfo, meeting);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS, meetingService.updateMeeting(example, meeting));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param meeting
     * @return RetDataBean
     * @Title: sendMeetingMsg
     * @Description:  发送会议通知
     */
    @RequestMapping(value = "/sendMeetingMsg", method = RequestMethod.POST)
    public RetDataBean sendMeetingMsg(Meeting meeting) {
        try {
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            meeting.setOrgId(userInfo.getOrgId());
            meeting = meetingService.selectOneMeeting(meeting);
            meetingService.sendMeetingMsg(userInfo, meeting);
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: insertMeetingRoom
     * @Description:  添加会议室
     * @param: request
     * @param: meetingRoom
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertMeetingRoom", method = RequestMethod.POST)
    public RetDataBean insertMeetingRoom(MeetingRoom meetingRoom) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            meetingRoom.setRoomId(SysTools.getGUID());
            meetingRoom.setStatus("0");
            meetingRoom.setCreateUser(account.getAccountId());
            meetingRoom.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            meetingRoom.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, meetingRoomService.insertMeetingRoom(meetingRoom));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteMeetingRoom
     * @Description:  删除会议室
     * @param: request
     * @param: meetingRoom
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteMeetingRoom", method = RequestMethod.POST)
    public RetDataBean deleteMeetingRoom(MeetingRoom meetingRoom) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(meetingRoom.getRoomId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            meetingRoom.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, meetingRoomService.deleteMeetingRoom(meetingRoom));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateMeetingRoom
     * @Description:  更新会议室
     * @param: request
     * @param: meetingRoom
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateMeetingRoom", method = RequestMethod.POST)
    public RetDataBean updateMeetingRoom(MeetingRoom meetingRoom) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(meetingRoom.getRoomId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(MeetingRoom.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("roomId", meetingRoom.getRoomId());
            meetingRoom.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, meetingRoomService.updateMeetingRoom(example, meetingRoom));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: insertMeetingDevice
     * @Description:  添加会议室设备
     * @param: request
     * @param: meetingRoom
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertMeetingDevice", method = RequestMethod.POST)
    public RetDataBean insertMeetingDevice(MeetingDevice meetingDevice) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            meetingDevice.setDeviceId(SysTools.getGUID());
            meetingDevice.setStatus("0");
            meetingDevice.setCreateUser(account.getAccountId());
            meetingDevice.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            meetingDevice.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, meetingDeviceService.insertMeetingDevice(meetingDevice));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteMeetingDevice
     * @Description:  删除会议室
     * @param: request
     * @param: meetingRoom
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteMeetingDevice", method = RequestMethod.POST)
    public RetDataBean deleteMeetingDevice(MeetingDevice meetingDevice) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(meetingDevice.getDeviceId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            meetingDevice.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, meetingDeviceService.deleteMeetingDevice(meetingDevice));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateMeetingRoom
     * @Description:  更新设备信息
     * @param: request
     * @param: meetingRoom
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateMeetingDevice", method = RequestMethod.POST)
    public RetDataBean updateMeetingDevice(MeetingDevice meetingDevice) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(meetingDevice.getDeviceId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(MeetingDevice.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("deviceId", meetingDevice.getDeviceId());
            meetingDevice.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, meetingDeviceService.updateMeetingDevice(example, meetingDevice));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: insertMeeting
     * @Description:  申请会议
     * @param: request
     * @param: meeting
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertMeeting", method = RequestMethod.POST)
    public RetDataBean insertMeeting(Meeting meeting) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("meeting:insert")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_INSERT_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            meeting.setMeetingId(SysTools.getGUID());
            meeting.setCreateUser(account.getAccountId());
            meeting.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            meeting.setOrgId(account.getOrgId());
            meeting.setStatus("0");
            return meetingService.addMeeting(meeting);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteMeeting
     * @Description:  撤销会议
     * @param: request
     * @param: meeting
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteMeeting", method = RequestMethod.POST)
    public RetDataBean deleteMeeting(Meeting meeting) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("meeting:delete")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_DELETE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(meeting.getMeetingId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            meeting.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, meetingService.deleteMeeting(meeting));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateMeeting
     * @Description:  更新会议
     * @param: request
     * @param: meeting
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateMeeting", method = RequestMethod.POST)
    public RetDataBean updateMeeting(Meeting meeting) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("meeting:update")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_UPDATE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(meeting.getMeetingId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(Meeting.class);
            if (account.getOpFlag().equals("1")) {
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("meetingId", meeting.getMeetingId());
            } else {
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("meetingId", meeting.getMeetingId()).andEqualTo("createUser", meeting.getCreateUser());
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, meetingService.updateMeeting(example, meeting));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: insertMeetingNotes
     * @Description:  添加会议记要
     * @param: request
     * @param: meetingNotes
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertMeetingNotes", method = RequestMethod.POST)
    public RetDataBean insertMeetingNotes(MeetingNotes meetingNotes) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            meetingNotes.setNotesId(SysTools.getGUID());
            meetingNotes.setCreateUser(account.getAccountId());
            meetingNotes.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            meetingNotes.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, meetingNotesService.insertMeetingNotes(meetingNotes));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: deleteMeetingNotes
     * @Description:  删除会议记要
     * @param: request
     * @param: meetingNotes
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/deleteMeetingNotes", method = RequestMethod.POST)
    public RetDataBean deleteMeetingNotes(MeetingNotes meetingNotes) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(meetingNotes.getNotesId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            meetingNotes.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, meetingNotesService.deleteMeetingNotes(meetingNotes));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateMeetingNotes
     * @Description:  更新会议记要
     * @param: meetingNotes
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateMeetingNotes", method = RequestMethod.POST)
    public RetDataBean updateMeetingNotes(MeetingNotes meetingNotes) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(meetingNotes.getNotesId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Example example = new Example(MeetingNotes.class);
            if (account.getOpFlag().equals("1")) {
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("notesId", meetingNotes.getNotesId());
            } else {
                example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("notesId", meetingNotes.getNotesId()).andEqualTo("createUser", meetingNotes.getCreateUser());
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, meetingNotesService.updateMeetingNotes(example, meetingNotes));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
