package com.core136.controller.mobile;

import com.core136.bean.account.Account;
import com.core136.bean.sys.DdConfig;
import com.core136.common.dingding.DdUtils;
import com.core136.common.dingding.entity.DUserInfo;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.service.account.AccountService;
import com.core136.service.dingding.DingDingLoginService;
import com.core136.service.sys.DdConfigService;
import com.core136.service.sys.WxAndDdUtilsService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName: RoutGetErpController
 * @Description ERP数据获取接口
 * @author: 稠云信息
 * @date: 2018年12月7日 下午1:36:14
 * @Copyright: 2018 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/dingding/dingdingget")
public class RouteGetDingdingController {
    private WxAndDdUtilsService wxAndDdUtilsService;
    @Autowired
    public void setWxUtilsService(WxAndDdUtilsService wxAndDdUtilsService)
    {
        this.wxAndDdUtilsService = wxAndDdUtilsService;
    }
    private DingDingLoginService dingDingLoginService;

    @Autowired
    public void setDingDingLoginService(DingDingLoginService dingDingLoginService) {
        this.dingDingLoginService = dingDingLoginService;
    }

    private DdConfigService ddConfigService;

    @Autowired
    public void setDdConfigService(DdConfigService ddConfigService) {
        this.ddConfigService = ddConfigService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * @Title: getUserInfo
     * @Description:  获取用户信息
     * @param: request
     * @param: code
     * @param: unit
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getUserInfo", method = RequestMethod.GET)
    public RetDataBean getUserInfo(HttpServletRequest request, String code, String orgId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (account != null) {
                if (StringUtils.isNotBlank(account.getAccountId())) {
                    return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS);
                } else {
                    DdConfig ddConfig = new DdConfig();
                    ddConfig.setOrgId(orgId);
                    ddConfig = ddConfigService.selectOneDdConfig(ddConfig);
                    String accessToken = wxAndDdUtilsService.getDdAccessToken(ddConfig.getDingdingAppKey(), ddConfig.getDingdingAppSecret());
                    DUserInfo dUserInfo = DdUtils.getUserInfo(code, accessToken);
                    dingDingLoginService.getDUserinfo(request, dUserInfo.getUserId(), orgId);
                    return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS);
                }
            } else {
                DdConfig ddConfig = new DdConfig();
                ddConfig.setOrgId(orgId);
                ddConfig = ddConfigService.selectOneDdConfig(ddConfig);
                String accessToken = wxAndDdUtilsService.getDdAccessToken(ddConfig.getDingdingAppKey(), ddConfig.getDingdingAppSecret());
                DUserInfo dUserInfo = DdUtils.getUserInfo(code, accessToken);
                dingDingLoginService.getDUserinfo(request, dUserInfo.getUserId(), orgId);
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
