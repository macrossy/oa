/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutGetNoticeController.java
 * @Package com.core136.controller.notice
 * @Description: (用一句话描述该文件做什么)
 * @author: 稠云信息
 * @date: 2019年7月20日 上午10:17:20
 * @version V1.0
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
package com.core136.controller.notice;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.file.Attach;
import com.core136.bean.notice.Notice;
import com.core136.bean.notice.NoticeTemplate;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserInfoService;
import com.core136.service.file.AttachService;
import com.core136.service.notice.NoticeConfigService;
import com.core136.service.notice.NoticeService;
import com.core136.service.notice.NoticeTemplateService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author lsq
 * @ClassName: RoutGetNoticeController
 * @Description: 通知公告接口Controller
 * @author: 稠云信息
 * @date: 2019年7月20日 上午10:17:20
 * @Copyright: 2019 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/ret/noticeget")
public class RouteGetNoticeController {
    @Value("${app.attachpath}")
    private String attachpath;
    private NoticeTemplateService noticeTemplateService;

    @Autowired
    public void setNoticeTemplateService(NoticeTemplateService noticeTemplateService) {
        this.noticeTemplateService = noticeTemplateService;
    }

    private NoticeConfigService noticeConfigService;

    @Autowired
    public void setNoticeConfigService(NoticeConfigService noticeConfigService) {
        this.noticeConfigService = noticeConfigService;
    }

    private NoticeService noticeService;

    @Autowired
    public void setNoticeService(NoticeService noticeService) {
        this.noticeService = noticeService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }
    private AttachService attachService;

    @Autowired
    public void setAttachService(AttachService attachService)
    {
        this.attachService = attachService;
    }
    /**
     * 获取通知公告人员查看状态
     *
     * @param notice
     * @return
     */
    @RequestMapping(value = "/getNoticeReadStatus", method = RequestMethod.POST)
    public RetDataBean getNoticeReadStatus(Notice notice) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            notice.setOrgId(account.getOrgId());
            notice = noticeService.selectOneNotice(notice);
            List<Map<String, String>> accountIdList = userInfoService.getAccountIdInPriv(notice.getOrgId(), notice.getUserPriv(), notice.getDeptPriv(), notice.getLevelPriv());
            List<String> list = new ArrayList<String>();
            for (Map<String, String> map : accountIdList) {
                list.add(map.get("accountId"));
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, noticeService.getNoticeReadStatus(account.getOrgId(), notice.getNoticeId(), list));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getMyNoticeListForDesk
     * @Description:  获取桌面的通知消息
     * @param: request
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMyNoticeListForDesk", method = RequestMethod.POST)
    public RetDataBean getMyNoticeListForDesk() {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("notice:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, noticeService.getMyNoticeListForDesk(userInfo.getOrgId(), SysTools.getTime("yyyy-MM-dd"), userInfo.getAccountId(), userInfo.getDeptId(), userInfo.getLeadLevel()));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getMobileNoticeInfo
     * @Description:  获取移动端通知公告详情
     * @param: request
     * @param: notice
     * @param: dingUserId
     * @param: dingDeviceId
     * @param: orgId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMobileNoticeInfo", method = RequestMethod.POST)
    public RetDataBean getMobileNoticeInfo(Notice notice) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("notice:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(notice.getNoticeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            notice.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, noticeService.getNoticeInfo(account, notice));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param noticeType
     * @param beginTime
     * @param endTime
     * @param @return    设定文件
     * @return RetDataBean 返回类型
     * @Title: getMyNoticeList
     * @Description:  获取个人的通知公告
     */
    @RequestMapping(value = "/getMyNoticeList", method = RequestMethod.POST)
    public RetDataBean getMyNoticeList(
            PageParam pageParam,
            String noticeType,
            String beginTime,
            String endTime,
            String readStatus
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("notice:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("n.send_time");
            } else {
                pageParam.setSort("n." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            pageParam.setAccountId(userInfo.getAccountId());
            pageParam.setDeptId(userInfo.getDeptId());
            pageParam.setLevelId(userInfo.getLeadLevel());
            pageParam.setOrgId(userInfo.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = noticeService.getMyNoticeList(pageParam, noticeType, beginTime, endTime, readStatus);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param notice
     * @param @return 设定文件
     * @return RetDataBean 返回类型
     * @Title: getNoticeInfo
     * @Description:  获取通知公告的具体内容
     */
    @RequestMapping(value = "/getNoticeInfo", method = RequestMethod.POST)
    public RetDataBean getNoticeInfo(Notice notice) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("notice:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(notice.getNoticeId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            notice.setOrgId(account.getOrgId());
            notice = noticeService.getNoticeInfo(account, notice);
            if(StringUtils.isNotBlank(notice.getRedHead()))
            {
                Attach attach = new Attach();
                attach.setOrgId(account.getOrgId());
                attach.setAttachId(notice.getRedHead());
                attach = attachService.selectOne(attach);
                File file = new File(attachpath+attach.getPath());
                //String html = FileUtils.fileRead(file);
                //notice.setContent(html+notice.getContent());
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, notice);
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param noticeType
     * @param @return    设定文件
     * @return RetDataBean 返回类型
     * @Title: getRedHeadListByType
     * @Description:  按分类获取红头列表
     */
    @RequestMapping(value = "/getRedHeadListByType", method = RequestMethod.POST)
    public RetDataBean getRedHeadListByType(String noticeType) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, noticeTemplateService.getRedHeadListByType(account.getOrgId(), noticeType));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param @return   设定文件
     * @return RetDataBean 返回类型
     * @Title: getNoticeTemplateList
     * @Description:  获取通知公告模版
     */
    @RequestMapping(value = "/getNoticeTemplateList", method = RequestMethod.POST)
    public RetDataBean getNoticeTemplateList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("n.sort_no");
            } else {
                pageParam.setSort("n." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            pageParam.setOrderBy(orderBy);
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, String>> pageInfo = noticeTemplateService.getNoticeTemplateList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param @return    设定文件
     * @return RetDataBean 返回类型
     * @Title: getNoticeManageList
     * @Description: 获取通知公告维护列表
     */
    @RequestMapping(value = "/getNoticeManageList", method = RequestMethod.POST)
    public RetDataBean getNoticeManageList(
            PageParam pageParam,
            String noticeType,
            String beginTime,
            String endTime
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("notice:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("n.create_time");
            } else {
                pageParam.setSort("n." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOpFlag(account.getOpFlag());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, Object>> pageInfo = noticeService.getNoticeManageList(pageParam, noticeType, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param @return   设定文件
     * @return RetDataBean 返回类型
     * @Title: getNoticeApproverList
     * @Description:  获取审批列表
     */
    @RequestMapping(value = "/getNoticeApproverList", method = RequestMethod.POST)
    public RetDataBean getNoticeApproverList(
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("n.create_time");
            } else {
                pageParam.setSort("n" + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            PageInfo<Map<String, Object>> pageInfo = noticeService.getNoticeApproverList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param noticeTemplate
     * @param @return        设定文件
     * @return RetDataBean 返回类型
     * @Title: getNoticeTemplate
     * @Description:  获取红头模版信息
     */
    @RequestMapping(value = "/getNoticeTemplate", method = RequestMethod.POST)
    public RetDataBean getNoticeTemplate(NoticeTemplate noticeTemplate) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            noticeTemplate.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, noticeTemplateService.selectOneNoticeTemplate(noticeTemplate));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageNumber
     * @param pageSize
     * @param sort
     * @param sortOrder
     * @param @return    设定文件
     * @return RetDataBean 返回类型
     * @Title: getApproverUserList
     * @Description:  审批人员列表
     */
    @RequestMapping(value = "/getApproverUserList", method = RequestMethod.POST)
    public RetDataBean getApproverUserList(Integer pageNumber,
                                           Integer pageSize, String sort, String sortOrder) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "sort_no";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            String orderBy = sort + " " + sortOrder;
            PageInfo<Map<String, String>> pageInfo = noticeConfigService.getApproverUserList(pageNumber, pageSize,
                    orderBy, account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageNumber
     * @param pageSize
     * @param sort
     * @param sortOrder
     * @param @return    设定文件
     * @return RetDataBean 返回类型
     * @Title: getApproverUserList
     * @Description:  免审人员列表
     */
    @RequestMapping(value = "/getNotApproverUserList", method = RequestMethod.POST)
    public RetDataBean getNotApproverUserList(Integer pageNumber,
                                              Integer pageSize, String sort, String sortOrder) {
        try {
            if (StringUtils.isBlank(sort)) {
                sort = "sort_no";
            } else {
                sort = StrTools.lowerCharToUnderLine(sort);
            }
            if (StringUtils.isBlank(sortOrder)) {
                sortOrder = "asc";
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            String orderBy = sort + " " + sortOrder;
            PageInfo<Map<String, String>> pageInfo = noticeConfigService.getNotApproverUserList(pageNumber, pageSize,
                    orderBy, account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }
}
