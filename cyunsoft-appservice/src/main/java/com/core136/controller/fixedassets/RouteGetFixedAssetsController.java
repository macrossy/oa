/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutGetFixedassetController.java
 * @Package com.core136.controller.fixedassets
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月25日 下午5:18:44
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.controller.fixedassets;

import com.core136.bean.account.Account;
import com.core136.bean.fixedassets.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.fixedassets.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@RestController
@RequestMapping("/ret/fixedassetsget")
public class RouteGetFixedAssetsController {
    private FixedAssetsSortService fixedAssetsSortService;

    @Autowired
    public void setFixedAssetsSortService(FixedAssetsSortService fixedAssetsSortService) {
        this.fixedAssetsSortService = fixedAssetsSortService;
    }

    private FixedAssetsService fixedAssetsService;

    @Autowired
    public void setFixedAssetsService(FixedAssetsService fixedAssetsService) {
        this.fixedAssetsService = fixedAssetsService;
    }

    private FixedAssetsStorageService fixedAssetsStorageService;

    @Autowired
    public void setFixedAssetsStorageService(FixedAssetsStorageService fixedAssetsStorageService) {
        this.fixedAssetsStorageService = fixedAssetsStorageService;
    }

    private FixedAssetsApplyService fixedAssetApplayService;

    @Autowired
    public void setFixedAssetsApplyService(FixedAssetsApplyService fixedAssetApplayService) {
        this.fixedAssetApplayService = fixedAssetApplayService;
    }

    private FixedAssetsApprovalService fixedAssetsApprovalService;

    @Autowired
    public void setFixedAssetsApprovalService(FixedAssetsApprovalService fixedAssetsApprovalService) {
        this.fixedAssetsApprovalService = fixedAssetsApprovalService;
    }

    private FixedAssetsRepairService fixedAssetsRepairService;

    @Autowired
    public void setFixedAssetsRepairService(FixedAssetsRepairService fixedAssetsRepairService) {
        this.fixedAssetsRepairService = fixedAssetsRepairService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private FixedAssetsAllocationService fixedAssetsAllocationService;

    @Autowired
    public void setFixedAssetsAllocationService(FixedAssetsAllocationService fixedAssetsAllocationService) {
        this.fixedAssetsAllocationService = fixedAssetsAllocationService;
    }

    /**
     * @param pageParam
     * @param deptId
     * @return RetDataBean
     * @Title: getFixedAssetsAllocationOldList
     * @Description:  历史调拨记录
     */
    @RequestMapping(value = "/getFixedAssetsAllocationOldList", method = RequestMethod.POST)
    public RetDataBean getFixedAssetsAllocationOldList(PageParam pageParam, String deptId, String sortId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(orderBy);
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOpFlag(account.getOpFlag());
            PageInfo<Map<String, String>> pageInfo = fixedAssetsAllocationService.getFixedAssetsAllocationOldList(pageParam, deptId, sortId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getApplyAndApproveInfo
     * @Description:  申请与审批详情
     * @param: request
     * @param: applyId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getApplyAndApproveInfo", method = RequestMethod.POST)
    public RetDataBean getApplyAndApproveInfo(String applyId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fixedAssetApplayService.getApplyAndApproveInfo(account.getOrgId(), applyId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getFixedAssetsApplyList
     * @Description:  获取申请列表
     * @param: request
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: status
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getFixedAssetsApplyList", method = RequestMethod.POST)
    public RetDataBean getFixedAssetsApplyList(
            PageParam pageParam,
            String beginTime, String endTime, String status, String assetsSortId
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(orderBy);
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOpFlag(account.getOpFlag());
            PageInfo<Map<String, String>> pageInfo = fixedAssetApplayService.getFixedAssetsApplyList(pageParam, status, beginTime, endTime, assetsSortId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getFixedAssetsRepairList
     * @Description:  获取修改列表
     * @param: request
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: assetsSortId
     * @param: status
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getFixedAssetsRepairList", method = RequestMethod.POST)
    public RetDataBean getFixedAssetsRepairList(
            PageParam pageParam,
            String beginTime, String endTime, String assetsSortId, String status
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            String orderBy = pageParam.getSort() + " " + pageParam.getSortOrder();
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrderBy(orderBy);
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOpFlag(account.getOpFlag());
            PageInfo<Map<String, String>> pageInfo = fixedAssetsRepairService.getFixedAssetsRepairList(pageParam, beginTime, endTime, assetsSortId, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getFixedAssetsRepairById
     * @Description:  获取修改详情
     * @param: request
     * @param: fixedAssetsRepair
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getFixedAssetsRepairById", method = RequestMethod.POST)
    public RetDataBean getFixedAssetsRepairById(FixedAssetsRepair fixedAssetsRepair) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fixedAssetsRepair.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fixedAssetsRepairService.selectOneFixedAssetsRepair(fixedAssetsRepair));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getFixedAssetsApprovalById
     * @Description:  获取审批详情
     * @param: request
     * @param: fixedAssetsApproval
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getFixedAssetsApprovalById", method = RequestMethod.POST)
    public RetDataBean getFixedAssetsApprovalById(FixedAssetsApproval fixedAssetsApproval) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fixedAssetsApproval.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fixedAssetsApprovalService.selectOneFixedAssetsApproval(fixedAssetsApproval));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getFixedAssetsApplyById
     * @Description:  获取申请详情
     * @param: request
     * @param: fixedAssetsApply
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getFixedAssetsApplyById", method = RequestMethod.POST)
    public RetDataBean getFixedAssetsApplyById(FixedAssetsApply fixedAssetsApply) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fixedAssetsApply.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fixedAssetApplayService.selectOneFixedAssetsApply(fixedAssetsApply));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getApplyFixedAssetsList
     * @Description:  获取可申请的固定资产的列表
     * @param: request
     * @param: pageParam
     * @param: sortId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getApplyFixedAssetsList", method = RequestMethod.POST)
    public RetDataBean getApplyFixedAssetsList(
            PageParam pageParam,
            String sortId
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("fixedassets:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = fixedAssetsService.getApplyFixedAssetsList(pageParam, sortId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: queryFixedAssetsList
     * @Description:  查询固定资产列表
     * @param: request
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: sortId
     * @param: ownDept
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/queryFixedAssetsList", method = RequestMethod.POST)
    public RetDataBean queryFixedAssetsList(
            PageParam pageParam,
            String beginTime,
            String endTime,
            String sortId,
            String ownDept,
            String status
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("fixedassets:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = fixedAssetsService.queryFixedAssetsList(pageParam, beginTime, endTime, sortId, ownDept, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getAllocationList
     * @Description:  获取调拨列表
     * @param: request
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: sortId
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getAllocationList", method = RequestMethod.POST)
    public RetDataBean getAllocationList(
            PageParam pageParam,
            String beginTime,
            String endTime,
            String sortId,
            String applyUser
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("a.create_time");
            } else {
                pageParam.setSort("a." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = fixedAssetsService.getAllocationList(pageParam, beginTime, endTime, sortId, applyUser);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getFixedAssetsList
     * @Description:  获取固定资产列表
     * @param: request
     * @param: pageParam
     * @param: beginTime
     * @param: endTime
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getFixedAssetsList", method = RequestMethod.POST)
    public RetDataBean getFixedAssetsList(
            PageParam pageParam,
            String beginTime,
            String endTime,
            String sortId
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("fixedassets:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = fixedAssetsService.getFixedAssetsList(pageParam, beginTime, endTime, sortId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @Title: getFixedAssetsSortById
     * @Description:  获取固定资产分类详情
     * @param: request
     * @param: fixedAssetsSort
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getFixedAssetsSortById", method = RequestMethod.POST)
    public RetDataBean getFixedAssetsSortById(FixedAssetsSort fixedAssetsSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fixedAssetsSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fixedAssetsSortService.selectOneFixedAssetsSort(fixedAssetsSort));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getFixedAssetsStorageById
     * @Description:  获取仓库详情
     * @param: fixedAssetsStorage
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getFixedAssetsStorageById", method = RequestMethod.POST)
    public RetDataBean getFixedAssetsStorageById(FixedAssetsStorage fixedAssetsStorage) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fixedAssetsStorage.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fixedAssetsStorageService.selectOneFixedAssetsStorage(fixedAssetsStorage));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getAllFixedAssetsStorageList
     * @Description:  获取仓库列表
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getAllFixedAssetsStorageList", method = RequestMethod.POST)
    public RetDataBean getAllFixedAssetsStorageList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fixedAssetsStorageService.getAllFixedAssetsStorageList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getFixedAssetsById
     * @Description:  获取固定资产详情
     * @param: fixedAssets
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getFixedAssetsById", method = RequestMethod.POST)
    public RetDataBean getFixedAssetsById(FixedAssets fixedAssets) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            fixedAssets.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, fixedAssetsService.selectOneFixedAssets(fixedAssets));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: getFixedAssetSortTree
     * @Description:  获取固定资产的分类
     * @param: sortId
     * @param: @return
     * @return: List<Map < String, String>>
     */
    @RequestMapping(value = "/getFixedAssetSortTree", method = RequestMethod.POST)
    public List<Map<String, String>> getFixedAssetSortTree(String sortId) {
        try {
            String parentId = "0";
            if (StringUtils.isNotBlank(sortId)) {
                parentId = sortId;
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return fixedAssetsSortService.getFixedAssetSortTree(account.getOrgId(), parentId);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * @Title: getFixedAssetsStorageList
     * @Description:  获取仓库列表
     * @param: pageParam
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getFixedAssetsStorageList", method = RequestMethod.POST)
    public RetDataBean getFixedAssetsStorageList(PageParam pageParam) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("f.sort_no");
            } else {
                pageParam.setSort("f." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = fixedAssetsStorageService.getFixedAssetsStorageList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
