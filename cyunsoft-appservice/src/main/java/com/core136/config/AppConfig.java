package com.core136.config;


import com.core136.unit.fileutils.FileTools;
import com.zhuozhengsoft.pageoffice.poserver.AdminSeal;
import com.zhuozhengsoft.pageoffice.poserver.Server;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.sql.DataSource;
import java.util.Locale;


@Configuration
public class AppConfig implements WebMvcConfigurer {
    @Value("${pageOffice.seal.adminPwd}")
    private String sealPassword;
    @Value("${pageOffice.lic.path}")
    private String licPath;
    @Value("${app.language}")
    String language;
    @Value("${app.country}")
    String country;

    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        AntPathMatcher matcher = new AntPathMatcher();
        matcher.setCaseSensitive(false);
        configurer.setPathMatcher(matcher);
    }


    /**
     * Durid 数据源事务管理定义
     * @param dataSource
     * @return
     */
    @Bean(name = "generalTM") // 给事务管理器命名
    public PlatformTransactionManager generalTM(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }


    /**
     * 添加PageOffice的服务器端授权程序Servlet（必须）
     * @return
     */
    @Bean
    public ServletRegistrationBean<Server> servletRegistrationBean() {
        com.zhuozhengsoft.pageoffice.poserver.Server poserver = new com.zhuozhengsoft.pageoffice.poserver.Server();
        FileTools.isPathExistAndCreate(licPath);
        poserver.setSysPath(licPath);//设置PageOffice注册成功后,license.lic文件存放的目录
        ServletRegistrationBean<Server> srb = new ServletRegistrationBean<Server>(poserver);
        srb.addUrlMappings("/poserver.zz");
        srb.addUrlMappings("/posetup.exe");
        srb.addUrlMappings("/pageoffice.js");
        srb.addUrlMappings("/jquery.min.js");
        srb.addUrlMappings("/pobstyle.css");
        srb.addUrlMappings("/sealsetup.exe");
        return srb;
    }


    /**
     * 添加印章管理程序Servlet（可选）
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean<AdminSeal> servletRegistrationBean2() {
        com.zhuozhengsoft.pageoffice.poserver.AdminSeal adminSeal = new com.zhuozhengsoft.pageoffice.poserver.AdminSeal();
        adminSeal.setAdminPassword(sealPassword);//设置印章管理员admin的登录密码
        adminSeal.setSysPath(licPath);//设置印章数据库文件poseal.db存放的目录
        ServletRegistrationBean<AdminSeal> srb = new ServletRegistrationBean<AdminSeal>(adminSeal);
        srb.addUrlMappings("/adminseal.zz");
        srb.addUrlMappings("/alimage.zz");
        srb.addUrlMappings("/loginseal.zz");
        srb.addUrlMappings("/sealimage.zz");
        return srb;
    }

//注入金格控件
/*
    @Bean
    public ServletRegistrationBean<OfficeServer> servletRegistrationBean3() {
        OfficeServer officeServer = new OfficeServer();
        ServletRegistrationBean<OfficeServer> srb = new ServletRegistrationBean<OfficeServer>(officeServer);
        srb.addUrlMappings("/OfficeServer");
        srb.addUrlMappings("/FileDownload");
        return srb;
    }
*/


    /**
     * 设置拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }

    /**
     * 国际化切换拦截器
     *
     * @return 国际化切换拦截器
     */
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
        interceptor.setParamName("lang");
        return interceptor;
    }

    /**
     * 国际化处理器
     *
     * @return 国际化处理器
     */
    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        //设置默认区域,
        if (StringUtils.isNotBlank(language) && StringUtils.isNotBlank(country)) {
            Locale locale = new Locale(language, country);
            slr.setDefaultLocale(locale);
        } else {
            slr.setDefaultLocale(Locale.CHINA);
        }
        return slr;
    }
}
