let ue = UE.getEditor('content');
$(function () {
    getsuperversionLead();
    ue.addListener("ready", function () {
        $.ajax({
            url: "/ret/superversionget/getSuperversionById",
            type: "post",
            dataType: "json",
            data: {superversionId: superversionId},
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    for (var name in data.list) {
                        if (name == "attach") {
                            $("#superversionattach").attr("data_value", data.list[name]);
                            createAttach("superversionattach");
                        } else if (name == "handedUser") {
                            $("#handedUser").attr("data-value", data.list.handedUser);
                            $("#handedUser").val(getUserNameByStr(data.list.handedUser));
                        } else if (name == "joinUser") {
                            $("#joinUser").attr("data-value", data.list.joinUser);
                            $("#joinUser").val(getUserNameByStr(data.list.joinUser));
                        } else if (name == "content") {
                            ue.setContent(data.list.content);
                        } else {
                            if (name != "leadId") {
                                $("#" + name).val(data.list[name]);
                            }
                        }
                    }
                }
            }
        })
    })
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    $("#updatebtn").unbind("click").click(function () {
        updatesuperversion();
    });
    getSmsConfig("msgType", "supperversion");

});
function getsuperversionLead() {
    $.ajax({
        url: "/ret/superversionget/getSuperversionLeadList",
        type: "post",
        dataType: "json",
        data: {configId: configId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
                $("#leadId").html("");
            } else {
                var html = "";
                for (var i = 0; i < data.list.length; i++) {
                    html += "<option value='" + data.list[i].accountId + "'>" + data.list[i].userName + "</option>"
                }
                $("#leadId").html(html);
            }
        }
    })
}
function updatesuperversion() {
    if ($("#joinUser").attr("data-value") == "@all") {
        layer.msg("督查参与人员不能为全体！");
        return;
    } else {
        $.ajax({
            url: "/set/superversionset/updateSuperversion",
            type: "post",
            dataType: "json",
            data: {
                superversionId: superversionId,
                type: configId,
                leadId: $("#leadId").val(),
                beginTime: $("#beginTime").val(),
                endTime: $("#endTime").val(),
                handedUser: $("#handedUser").attr("data-value"),
                joinUser: $("#joinUser").attr("data-value"),
                content: ue.getContent(),
                attach: $("#superversionattach").attr("data_value"),
                msgType: getCheckBoxValue("msgType")
            },
            success: function (data) {
                if (data.status == "500") {
                    console.log(data.msg);
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    layer.msg(sysmsg[data.msg]);
                }
            }
        })
    }
}
