$(function () {
    query("");
    jeDate("#beginTime", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTime", {
        format: "YYYY-MM-DD"
    });
    $(".js-query-but").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    });
    $.fn.zTree.init($("#tree"), setting);
})

function query(superversionId) {
    $("#myTable").bootstrapTable({
        url: '/ret/superversionget/getControlProcessList?superversionId=' + superversionId,
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'processId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        },
            {
                field: 'num',
                title: '序号',//标题  可不加
                width: '50px',
                formatter: function (value, row, index) {
                    return index + 1;
                }
            },
            {
                field: 'typeName',
                width: '100px',
                title: '事件类型'
            },
            {
                field: 'title',
                title: '任务标题',
                sortable: true,
                width: '100px',
                formatter: function (value, row, index) {
                    return "<a href=\"javascript:void(0);readdetails('" + row.processId + "','" + row.superversionId + "')\">" + value + "</a>";
                }
            },
            {
                field: 'holderUserName',
                width: '100px',
                title: '主办人'
            }, {
                field: 'prcsValue',
                width: '100px',
                title: '进度',
                formatter: function (value, row, index) {
                    if (value) {
                        return value + "%";
                    } else {
                        return "0%";
                    }
                }
            }, {
                field: 'finishTime',
                width: '100px',
                title: '完成时间'
            },
            {
                field: 'content',
                width: '200px',
                title: '任务详情'
            },
            {
                field: 'attach',
                width: '200px',
                title: '相关附件',
                formatter: function (value, row, index) {
                    return createTableAttach(value);
                }
            },
            {
                field: 'opt',
                width: '50px',
                align: 'center',
                title: '操作',
                formatter: function (value, row, index) {
                    return createOptBtn(row.processId);
                }
            }],
        onClickCell: function (field, value, row, $element) {
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list //行数据，前面的key要与之前设置的dataField的值一致.
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        beginTime: $("#beginTime").val(),
        endTime: $("#endTime").val()
    };
    return temp;
};

function createOptBtn(processId) {
    var html = "<a href=\"javascript:void(0);getworkprocess('" + processId + "')\" class=\"btn btn-success btn-xs\" >过程</a>";
    return html;
}

function readdetails(processId, superversionId) {
    window.open("/app/core/superversion/processdetails?processId=" + processId + "&superversionId=" + superversionId);
}

function getworkprocess(processId) {
    window.open("/app/core/superversion/processresultdetails?processId=" + processId);
}

var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/superversionget/getSuperverionConfigTree",// Ajax 获取数据的 URL 地址
        autoParam: ["id"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "id",
            pIdKey: "parentId",
            rootPId: "0"
        },
        key: {
            name: "name"
        }
    }
};

function zTreeOnClick(event, treeId, treeNode) {
    if (treeNode.parentId != "0") {
        $("#myTable").bootstrapTable('destroy');
        query(treeNode.id);
    }
}
