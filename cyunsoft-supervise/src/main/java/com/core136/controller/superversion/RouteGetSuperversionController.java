/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: RoutGetSuperversionController.java
 * @Package com.core136.controller.superversion
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月14日 下午1:18:15
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.controller.superversion;


import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.superversion.*;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.StrTools;
import com.core136.service.account.AccountService;
import com.core136.service.account.UserInfoService;
import com.core136.service.superversion.*;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author lsq
 */
@RestController
@RequestMapping("/ret/superversionget")
public class RouteGetSuperversionController {
    private SuperversionService superversionService;

    @Autowired
    public void setSuperversionService(SuperversionService superversionService) {
        this.superversionService = superversionService;
    }

    private SuperversionConfigService superversionConfigService;

    @Autowired
    public void setSuperversionConfigService(SuperversionConfigService superversionConfigService) {
        this.superversionConfigService = superversionConfigService;
    }

    private SuperversionDelayService superversionDelayService;

    @Autowired
    public void setSuperversionDelayService(SuperversionDelayService superversionDelayService) {
        this.superversionDelayService = superversionDelayService;
    }

    private SuperversionProcessService superversionProcessService;

    @Autowired
    public void setSuperversionProcessService(SuperversionProcessService superversionProcessService) {
        this.superversionProcessService = superversionProcessService;
    }

    private SuperversionScoreService superversionScoreService;

    @Autowired
    public void setSuperversionScoreService(SuperversionScoreService superversionScoreService) {
        this.superversionScoreService = superversionScoreService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private SuperversionBpmConfigService superversionBpmConfigService;

    @Autowired
    public void setSuperversionBpmConfigService(SuperversionBpmConfigService superversionBpmConfigService) {
        this.superversionBpmConfigService = superversionBpmConfigService;
    }

    private UserInfoService userInfoService;

    @Autowired
    public void setUserInfoService(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    private SuperversionIntegralService superversionIntegralService;

    @Autowired
    public void setSuperversionIntegralService(SuperversionIntegralService superversionIntegralService) {
        this.superversionIntegralService = superversionIntegralService;
    }

    private SuperversionResultService superversionResultService;

    @Autowired
    public void setSuperversionResultService(SuperversionResultService superversionResultService) {
        this.superversionResultService = superversionResultService;
    }

    private SuperversionProblemService superversionProblemService;

    @Autowired
    public void setSuperversionProblemService(SuperversionProblemService superversionProblemService) {
        this.superversionProblemService = superversionProblemService;
    }

    private SuperversionBpmListService superversionBpmListService;

    @Autowired
    public void setSuperversionBpmListServer(SuperversionBpmListService superversionBpmListService) {
        this.superversionBpmListService = superversionBpmListService;
    }

    /**
     * 移动端督查督办列表
     *
     * @param page
     * @return
     */
    @RequestMapping(value = "/getMobileMySuperversionList", method = RequestMethod.POST)
    public RetDataBean getMobileMySuperversionList(Integer page) {
        try {
            page = (page - 1) * 10;
            UserInfo userInfo = accountService.getRedisAUserInfoToUserInfo();
            List<Map<String, String>> pageInfo = superversionService.getMobileMySuperversionList(userInfo.getOrgId(), userInfo.getAccountId(), page);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取督查督办事件列表
     *
     * @param beginTime
     * @param endTime
     * @param type
     * @param handedUser
     * @param pageParam
     * @return
     */
    @RequestMapping(value = "/getQueryOldSupperversionList", method = RequestMethod.POST)
    public RetDataBean getQueryOldSupperversionList(
            String beginTime,
            String endTime,
            String type,
            String handedUser,
            PageParam pageParam
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("superverion:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = superversionService.getQueryOldSupperversionList(pageParam, type, handedUser, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取延时任务列表
     *
     * @return
     */
    @RequestMapping(value = "/getAllSuperversionProcessForDesk", method = RequestMethod.POST)
    public RetDataBean getAllSuperversionProcessForDesk() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionProcessService.getAllSuperversionProcessForDesk(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取所有督查事件
     *
     * @return
     */
    @RequestMapping(value = "/getAllFinishSuperversionForDesk", method = RequestMethod.POST)
    public RetDataBean getAllFinishSuperversionForDesk() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionService.getAllFinishSuperversionForDesk(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取所有事件列表前5
     *
     * @return
     */
    @RequestMapping(value = "/getAllSuperversionForDesk", method = RequestMethod.POST)
    public RetDataBean getAllSuperversionForDesk() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionService.getAllSuperversionForDesk(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getSuperversionScoreList", method = RequestMethod.POST)
    public RetDataBean getSuperversionScoreList(PageParam pageParam, String processId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("c.create_time");
            } else {
                pageParam.setSort("c." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = superversionScoreService.getSuperversionScoreList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取问题回复
     *
     * @param recordId
     * @param processId
     * @return
     */
    @RequestMapping(value = "/getMySuperversionAnswerList", method = RequestMethod.POST)
    public RetDataBean getMySuperversionAnswerList(String recordId, String processId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionProblemService.getMySuperversionAnswerList(account.getOrgId(), recordId, processId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取俱个人的任务列表
     *
     * @return
     */
    @RequestMapping(value = "/getSuperversionProcessListForSelect", method = RequestMethod.POST)
    public RetDataBean getSuperversionProcessListForSelect() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionProcessService.getSuperversionProcessListForSelect(account.getOrgId(), account.getAccountId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取问题列表
     *
     * @param processId
     * @return
     */
    @RequestMapping(value = "/getMySuperversionProblemList", method = RequestMethod.POST)
    public RetDataBean getMySuperversionProblemList(PageParam pageParam, String processId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("p.create_time");
            } else {
                pageParam.setSort("p." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = superversionProblemService.getMySuperversionProblemList(pageParam, processId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 督查督办历史流程
     *
     * @param processId
     * @return
     */
    @RequestMapping(value = "/getSupversionBpmRecordList", method = RequestMethod.POST)
    public RetDataBean getSupversionBpmRecordList(String processId) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionBpmListService.getSupversionBpmRecordList(account.getOrgId(), processId));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取督查列表
     *
     * @return
     */
    @RequestMapping(value = "/getSuperverionConfigTree", method = RequestMethod.POST)
    public List<Map<String, String>> getSuperverionConfigTree(String id) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(id)) {
                return superversionConfigService.getSuperverionConfigTree(account.getOrgId());
            } else {
                return superversionService.getSupserversionForTree(account.getOrgId(), id);
            }
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * 获取流程详情
     *
     * @param superversionBpmList
     * @return
     */
    @RequestMapping(value = "/getSuperversionBpmListById", method = RequestMethod.POST)
    public RetDataBean getSuperversionBpmListById(SuperversionBpmList superversionBpmList) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionBpmList.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionBpmListService.selectOneSuperversionBpmList(superversionBpmList));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取任务处理结果
     *
     * @param pageParam
     * @param processId
     * @return
     */
    @RequestMapping(value = "/getSuperversionResultList", method = RequestMethod.POST)
    public RetDataBean getSuperversionResultList(PageParam pageParam, String processId) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("r.create_time");
            } else {
                pageParam.setSort("r." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = superversionResultService.getSuperversionResultList(pageParam, processId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 获取问题详情
     *
     * @param superversionProblem
     * @return
     */
    @RequestMapping(value = "/getSuperversionProblemById", method = RequestMethod.POST)
    public RetDataBean getSuperversionProblemById(SuperversionProblem superversionProblem) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionProblem.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionProblemService.selectOneSuperversionProblem(superversionProblem));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取督查督办流程设置列表
     *
     * @param pageParam
     * @return
     */
    @RequestMapping(value = "/getAllSuperversionBpmConfigList", method = RequestMethod.POST)
    public RetDataBean getAllSuperversionBpmConfigList(
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("b.create_time");
            } else {
                pageParam.setSort("b." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = superversionBpmConfigService.getAllSuperversionBpmConfigList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 获取任务处理详情
     *
     * @param superversionResult
     * @return
     */
    @RequestMapping(value = "/getSuperversionResultById", method = RequestMethod.POST)
    public RetDataBean getSuperversionResultById(SuperversionResult superversionResult) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionResult.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionResultService.selectOneSuperversionResult(superversionResult));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * 获取积分规则
     *
     * @param superversionIntegral
     * @return
     */
    @RequestMapping(value = "/getSuperversionIntegralById", method = RequestMethod.POST)
    public RetDataBean getSuperversionIntegralById(SuperversionIntegral superversionIntegral) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionIntegral.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionIntegralService.selectOneSuperversionIntegral(superversionIntegral));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    @RequestMapping(value = "/getSuperversionLeadList", method = RequestMethod.POST)
    public RetDataBean getSuperversionLeadList(SuperversionConfig superversionConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionConfig.setOrgId(account.getOrgId());
            superversionConfig = superversionConfigService.selectOneSuperversionConfig(superversionConfig);
            String accountStrs = superversionConfig.getLeadId();
            if (StringUtils.isNotBlank(accountStrs)) {
                List<String> list = new ArrayList<String>();
                String[] aArr = null;
                if (accountStrs.indexOf(",") > 0) {
                    aArr = accountStrs.split(",");
                } else {
                    aArr = new String[]{accountStrs};
                }
                list = new ArrayList<String>(Arrays.asList(aArr));
                return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, userInfoService.getAllUserInfoByAccountList(account.getOrgId(), list));
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param superversionBpmConfig
     * @return RetDataBean
     * @Title: getSuperversionBpmConfigById
     * @Description:  获取督查督办流程设置详情
     */
    @RequestMapping(value = "/getSuperversionBpmConfigById", method = RequestMethod.POST)
    public RetDataBean getSuperversionBpmConfigById(SuperversionBpmConfig superversionBpmConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionBpmConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionBpmConfigService.selectOneSuperversionBpmConfig(superversionBpmConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title: getSupperversionPorcessListForDesk
     * @Description:  获取督查督办待办记录
     */
    @RequestMapping(value = "/getSupperversionPorcessListForDesk", method = RequestMethod.POST)
    public RetDataBean getSupperversionPorcessListForDesk() {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("superverion:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionProcessService.getSupperversionPorcessListForDesk(account.getOrgId(), account.getAccountId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getManageSuperversionListForDesk", method = RequestMethod.POST)
    public RetDataBean getManageSuperversionListForDesk() {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("superverion:manage")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_MANAGE_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionService.getManageSuperversionListForDesk(account.getOrgId(), account.getAccountId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getSuperversionScoreById", method = RequestMethod.POST)
    public RetDataBean getSuperversionScoreById(SuperversionScore superversionScore) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionScore.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionScoreService.selectOneSuperversionScore(superversionScore));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/getSuperversionProcessById", method = RequestMethod.POST)
    public RetDataBean getSuperversionProcessById(SuperversionProcess superversionProcess) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionProcess.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionProcessService.selectOneSuperversionProcess(superversionProcess));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    @RequestMapping(value = "/getSuperversionDelayById", method = RequestMethod.POST)
    public RetDataBean getSuperversionDelayById(SuperversionDelay superversionDelay) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionDelay.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionDelayService.selectOneSuperversionDelay(superversionDelay));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param superversionConfig
     * @param @return
     * @Title: getSuperversionConfigById
     * @Description:  获取分类详情
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getSuperversionConfigById", method = RequestMethod.POST)
    public RetDataBean getSuperversionConfigById(SuperversionConfig superversionConfig) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            superversionConfig.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionConfigService.selectOneSuperversionConfig(superversionConfig));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param superversion
     * @param @return
     * @Title: getSuperversionById
     * @Description:  获取督查项目详情
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getSuperversionById", method = RequestMethod.POST)
    public RetDataBean getSuperversionById(Superversion superversion) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("superverion:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            superversion.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionService.selectOneSuperversion(superversion));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @return
     * @Title: getAllSuperversionConfigList
     * @Description:  获取类型与领导列表
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getAllSuperversionConfigList", method = RequestMethod.POST)
    public RetDataBean getAllSuperversionConfigList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionConfigService.getAllSuperversionConfigList(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @return
     * @Title: getMySuperversionConfigList
     * @Description:  与我有关的分类
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getMySuperversionConfigList", method = RequestMethod.POST)
    public RetDataBean getMySuperversionConfigList() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionConfigService.getMySuperversionConfigList(account.getOrgId(), account.getAccountId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param @return
     * @Title: getSuperversionConfigList
     * @Description:  获取类型列表
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getSuperversionConfigList", method = RequestMethod.POST)
    public RetDataBean getSuperversionConfigList(
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("sort_no");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<SuperversionConfig> pageInfo = superversionConfigService.getSuperversionConfigList(pageParam);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param beginTime
     * @param endTime
     * @param type
     * @param handedUser
     * @param status
     * @param pageParam
     * @param @return
     * @Title: getLeadManageSupperversionList
     * @Description:  获取当前用户管控的事件列表
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getLeadManageSupperversionList", method = RequestMethod.POST)
    public RetDataBean getLeadManageSupperversionList(
            String beginTime,
            String endTime,
            String type,
            String handedUser,
            String status,
            PageParam pageParam
    ) {
        try {
            Subject subject = SecurityUtils.getSubject();
            if (!subject.isPermitted("superverion:query")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_NOT_QUERY_PERMISSIONS);
            }
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("create_time");
            } else {
                pageParam.setSort(StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = superversionService.getLeadManageSupperversionList(pageParam, type, handedUser, beginTime, endTime, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param beginTime
     * @param endTime
     * @param type
     * @param handedUser
     * @param status
     * @param pageParam
     * @param @return
     * @Title: getSupperversionList
     * @Description:  获取我创建的督查事件
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getSupperversionList", method = RequestMethod.POST)
    public RetDataBean getSupperversionList(
            String beginTime,
            String endTime,
            String type,
            String handedUser,
            String status,
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("tmp.create_time");
            } else {
                pageParam.setSort("tmp." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = superversionService.getSupperversionList(pageParam, type, handedUser, beginTime, endTime, status);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取完成督查督办列表
     *
     * @param beginTime
     * @param endTime
     * @param type
     * @param handedUser
     * @param pageParam
     * @return
     */
    @RequestMapping(value = "/getSupperversionFinishList", method = RequestMethod.POST)
    public RetDataBean getSupperversionFinishList(
            String beginTime,
            String endTime,
            String type,
            String handedUser,
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("tmp.create_time");
            } else {
                pageParam.setSort("tmp." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = superversionService.getSupperversionFinishList(pageParam, type, handedUser, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param beginTime
     * @param endTime
     * @param createUser
     * @param pageParam
     * @param @return
     * @Title: getDelayApplyList
     * @Description:  获取延期审批列表
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getDelayApplyList", method = RequestMethod.POST)
    public RetDataBean getDelayApplyList(
            String beginTime,
            String endTime,
            String createUser,
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("d.create_time");
            } else {
                pageParam.setSort("d." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = superversionDelayService.getDelayApplyList(pageParam, beginTime, endTime, createUser);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 延期审批历史记录
     *
     * @param beginTime
     * @param endTime
     * @param createUser
     * @param status
     * @param pageParam
     * @return
     */
    @RequestMapping(value = "/getDelayOldApplyList", method = RequestMethod.POST)
    public RetDataBean getDelayApplyList(
            String beginTime,
            String endTime,
            String createUser,
            String status,
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("d.create_time");
            } else {
                pageParam.setSort("d." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = superversionDelayService.getDelayOldApplyList(pageParam, status, beginTime, endTime, createUser);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * 获取转办给我的列表
     *
     * @param beginTime
     * @param endTime
     * @param type
     * @param pageParam
     * @return
     */
    @RequestMapping(value = "/getSupperversionPorcessForChangeList", method = RequestMethod.POST)
    public RetDataBean getSupperversionPorcessForChangeList(
            String beginTime,
            String endTime,
            String type,
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("tmp.create_time");
            } else {
                pageParam.setSort("tmp." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = superversionProcessService.getSupperversionPorcessForChangeList(pageParam, type, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param beginTime
     * @param endTime
     * @param type
     * @param pageParam
     * @param @return
     * @Title: getSupperversionPorcessList
     * @Description:  获取待处理的督查列表
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getSupperversionPorcessList", method = RequestMethod.POST)
    public RetDataBean getSupperversionPorcessList(
            String beginTime,
            String endTime,
            String type,
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("tmp.create_time");
            } else {
                pageParam.setSort("tmp." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = superversionProcessService.getSupperversionPorcessList(pageParam, type, beginTime, endTime);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param pageParam
     * @param @return
     * @Title: getOldProcessList
     * @Description:  获取事件处理过程列表
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getOldProcessList", method = RequestMethod.POST)
    public RetDataBean getOldProcessList(
            String beginTime,
            String endTime,
            String type,
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("tmp.finish_time");
            } else {
                pageParam.setSort("tmp." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = superversionProcessService.getOldProcessList(pageParam, beginTime, endTime, type);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param beginTime
     * @param endTime
     * @param pageParam
     * @param @return
     * @Title: getControlProcessList
     * @Description:  获取我所管控的任务列表
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getControlProcessList", method = RequestMethod.POST)
    public RetDataBean getControlProcessList(
            String beginTime,
            String endTime,
            String superversionId,
            PageParam pageParam
    ) {
        try {
            if (StringUtils.isBlank(pageParam.getSort())) {
                pageParam.setSort("p.create_time");
            } else {
                pageParam.setSort("p." + StrTools.lowerCharToUnderLine(pageParam.getSort()));
            }
            if (StringUtils.isBlank(pageParam.getSortOrder())) {
                pageParam.setSortOrder("asc");
            }

            Account account = accountService.getRedisAUserInfoToAccount();
            pageParam.setAccountId(account.getAccountId());
            pageParam.setOrgId(account.getOrgId());
            pageParam.setOrderBy(pageParam.getSort() + " " + pageParam.getSortOrder());
            PageInfo<Map<String, String>> pageInfo = superversionProcessService.getControlProcessList(pageParam, beginTime, endTime, superversionId);
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, pageInfo);
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @return
     * @Title: getQuerySuperversionForDept
     * @Description:  按部门汇总
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getQuerySuperversionForDept", method = RequestMethod.POST)
    public RetDataBean getQuerySuperversionForDept() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionProcessService.getQuerySuperversionForDept(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param @return
     * @Title: getQuerySuperversionForType
     * @Description:  按类型汇总
     * @return: RetDataBean
     */
    @RequestMapping(value = "/getQuerySuperversionForType", method = RequestMethod.POST)
    public RetDataBean getQuerySuperversionForType() {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, superversionConfigService.getQuerySuperversionForType(account.getOrgId()));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
