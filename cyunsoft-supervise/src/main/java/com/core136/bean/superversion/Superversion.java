/**
 * All rights Reserved, Designed By www.tydic.com
 *
 * @Title: Superversion.java
 * @Package com.core136.bean.superversion
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月17日 下午12:37:58
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.superversion;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 */
@Table(name = "superversion")
public class Superversion implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String superversionId;
    private String title;
    //副标题
    private String subheading;
    private String type;
    private String beginTime;
    private String endTime;
    private String status;
    private String content;
    /**
     * 督查领导
     */
    private String leadId;
    /**
     * 参与人员
     */
    private String joinUser;
    private String handedUser;
    private String attach;
    private String createTime;
    private String createUser;
    private String msgType;
    private String finishTime;
    private String orgId;

    public String getSuperversionId() {
        return superversionId;
    }

    public void setSuperversionId(String superversionId) {
        this.superversionId = superversionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public String getJoinUser() {
        return joinUser;
    }

    public void setJoinUser(String joinUser) {
        this.joinUser = joinUser;
    }

    public String getHandedUser() {
        return handedUser;
    }

    public void setHandedUser(String handedUser) {
        this.handedUser = handedUser;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getMsgType() {
        return msgType;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getSubheading() {
        return subheading;
    }

    public void setSubheading(String subheading) {
        this.subheading = subheading;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }
}
