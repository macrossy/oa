/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: SuperversionScore.java
 * @Package com.core136.bean.superversion
 * @Description: 描述
 * @author: lsq
 * @date: 2019年11月18日 上午9:08:50
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.bean.superversion;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author lsq
 *
 */
@Table(name = "superversion_score")
public class SuperversionScore implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String scoreId;
    private String superversionId;
    private String remark;
    private Double sysScore;
    private Double userScore;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getScoreId() {
        return scoreId;
    }

    public void setScoreId(String scoreId) {
        this.scoreId = scoreId;
    }

    public String getSuperversionId() {
        return superversionId;
    }

    public void setSuperversionId(String superversionId) {
        this.superversionId = superversionId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Double getSysScore() {
        return sysScore;
    }

    public void setSysScore(Double sysScore) {
        this.sysScore = sysScore;
    }

    public Double getUserScore() {
        return userScore;
    }

    public void setUserScore(Double userScore) {
        this.userScore = userScore;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
