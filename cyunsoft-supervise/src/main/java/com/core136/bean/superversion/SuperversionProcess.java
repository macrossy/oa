
package com.core136.bean.superversion;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * 督查督办子任务
 */
@Table(name = "superversion_process")
public class SuperversionProcess implements Serializable {
    private static final long serialVersionUID = 1L;
    private String processId;
    private String superversionId;
    private String title;
    private Integer prcsValue;
    private String holder;
    private String operator;
    private String beginTime;
    private String endTime;
    private String content;
    private String attach;
    private String status;
    private String finishTime;
    private String changeUser;
    private String changeTime;
    private String changeStatus;
    private String createTime;
    private String createUser;
    private String orgId;

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSuperversionId() {
        return superversionId;
    }

    public void setSuperversionId(String superversionId) {
        this.superversionId = superversionId;
    }

    public Integer getPrcsValue() {
        return prcsValue;
    }

    public void setPrcsValue(Integer prcsValue) {
        this.prcsValue = prcsValue;
    }

    public String getHolder() {
        return holder;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getChangeUser() {
        return changeUser;
    }

    public void setChangeUser(String changeUser) {
        this.changeUser = changeUser;
    }

    public String getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(String changeTime) {
        this.changeTime = changeTime;
    }

    public String getChangeStatus() {
        return changeStatus;
    }

    public void setChangeStatus(String changeStatus) {
        this.changeStatus = changeStatus;
    }

}
