package com.core136.bean.superversion;

import javax.persistence.Table;
import java.io.Serializable;

/**
 * 任务处理结果
 */
@Table(name = "superversion_result")
public class SuperversionResult implements Serializable {
    private static final long serialVersionUID = 1L;
    private String recordId;
    private String processId;
    private Integer prcsValue;
    private String finishTime;
    private String content;
    private String attach;
    private String createUser;
    private String createTime;
    private String orgId;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public Integer getPrcsValue() {
        return prcsValue;
    }

    public void setPrcsValue(Integer prcsValue) {
        this.prcsValue = prcsValue;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
