package com.core136.service.superversion;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.core136.bean.account.Account;
import com.core136.bean.superversion.SuperversionBpmConfig;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.superversion.SuperversionBpmConfigMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SuperversionBpmConfigService {
    private SuperversionBpmConfigMapper superversionBpmConfigMapper;

    @Autowired
    public void setSuperversionBpmConfigMapper(SuperversionBpmConfigMapper superversionBpmConfigMapper) {
        this.superversionBpmConfigMapper = superversionBpmConfigMapper;
    }

    public int insertSuperversionBpmConfig(SuperversionBpmConfig superversionBpmConfig) {
        return superversionBpmConfigMapper.insert(superversionBpmConfig);
    }

    public int deleteSuperversionBpmConfig(SuperversionBpmConfig superversionBpmConfig) {
        return superversionBpmConfigMapper.delete(superversionBpmConfig);
    }

    public int updateSuperversionBpmConfig(Example example, SuperversionBpmConfig superversionBpmConfig) {
        return superversionBpmConfigMapper.updateByExampleSelective(superversionBpmConfig, example);
    }

    public SuperversionBpmConfig selectOneSuperversionBpmConfig(SuperversionBpmConfig superversionBpmConfig) {
        return superversionBpmConfigMapper.selectOne(superversionBpmConfig);
    }

    public List<Map<String, String>> getAllSuperversionBpmConfigList(String orgId) {
        return superversionBpmConfigMapper.getAllSuperversionBpmConfigList(orgId);
    }


    /**
     * 获取督查督办流程设置列表
     *
     * @param pageParam
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getAllSuperversionBpmConfigList(PageParam pageParam) throws Exception {
        Example example = new Example(SuperversionBpmConfig.class);
        example.createCriteria().andEqualTo("orgId", pageParam.getOrgId());
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getAllSuperversionBpmConfigList(pageParam.getOrgId());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * 设置督查流程
     *
     * @param account
     * @param configStr
     * @return
     */
    public RetDataBean setBpmConfig(Account account, String configStr) {
        if (StringUtils.isBlank(configStr)) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
        } else {
            JSONArray jsonArray = JSONArray.parseArray(configStr);
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String configId = jsonObject.getString("configId");
                String flowIds = jsonObject.getString("flowIds");
                SuperversionBpmConfig superversionBpmConfig = new SuperversionBpmConfig();
                superversionBpmConfig.setOrgId(account.getOrgId());
                superversionBpmConfig.setConfigId(configId);
                superversionBpmConfig = selectOneSuperversionBpmConfig(superversionBpmConfig);
                if (superversionBpmConfig == null) {
                    superversionBpmConfig = new SuperversionBpmConfig();
                    superversionBpmConfig.setRecordId(SysTools.getGUID());
                    superversionBpmConfig.setConfigId(configId);
                    superversionBpmConfig.setFlowIds(flowIds);
                    superversionBpmConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                    superversionBpmConfig.setCreateUser(account.getAccountId());
                    superversionBpmConfig.setOrgId(account.getOrgId());
                    insertSuperversionBpmConfig(superversionBpmConfig);
                } else {
                    superversionBpmConfig = new SuperversionBpmConfig();
                    superversionBpmConfig.setFlowIds(flowIds);
                    superversionBpmConfig.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                    superversionBpmConfig.setCreateUser(account.getAccountId());
                    Example example = new Example(SuperversionBpmConfig.class);
                    example.createCriteria().andEqualTo("orgId", account.getOrgId()).andEqualTo("configId", configId);
                    updateSuperversionBpmConfig(example, superversionBpmConfig);
                }
            }
            return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
        }
    }


}
