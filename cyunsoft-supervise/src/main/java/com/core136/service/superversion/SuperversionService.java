/**
 * All rights Reserved, Designed By www.cyunsoft.com
 *
 * @Title: SuperversionService.java
 * @Package com.core136.service.superversion
 * @Description: 描述
 * @author: lsq
 * @date: 2019年10月28日 下午4:44:21
 * @version V1.0
 * @Copyright:江苏稠云 www.cyunsoft.com
 */
package com.core136.service.superversion;

import com.core136.bean.account.Account;
import com.core136.bean.account.UserInfo;
import com.core136.bean.superversion.Superversion;
import com.core136.bean.sys.MsgBody;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.GobalConstant;
import com.core136.common.utils.SysTools;
import com.core136.mapper.superversion.SuperversionMapper;
import com.core136.service.account.AccountService;
import com.core136.service.sys.MessageUnitService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/**
 * @author lsq
 */
@Service
public class SuperversionService {
    private SuperversionMapper superversionMapper;

    @Autowired
    public void setSuperversionMapper(SuperversionMapper superversionMapper) {
        this.superversionMapper = superversionMapper;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private MessageUnitService messageUnitService;

    @Autowired
    public void setMessageUnitService(MessageUnitService messageUnitService) {
        this.messageUnitService = messageUnitService;
    }

    public int insertSuperversion(Superversion superversion) {
        return superversionMapper.insert(superversion);
    }


    public int deleteSuperversion(Superversion superversion) {
        return superversionMapper.delete(superversion);
    }

    public int updateSuperversion(Example example, Superversion superversion) {
        return superversionMapper.updateByExampleSelective(superversion, example);
    }

    public Superversion selectOneSuperversion(Superversion superversion) {
        return superversionMapper.selectOne(superversion);
    }

    public List<Map<String, String>> getSupserversionForTree(String orgId, String type) {
        return superversionMapper.getSupserversionForTree(orgId, type);
    }

    /**
     * @param userInfo
     * @param superversion
     * @return int
     * @Title: createSuperversion
     * @Description:  创建督查督办
     */
    public int createSuperversion( UserInfo userInfo, Superversion superversion) {
        if (StringUtils.isNotBlank(superversion.getMsgType())) {
            String leadId = superversion.getLeadId();
            String handedUser = superversion.getHandedUser();
            String joinUser = superversion.getJoinUser();
            List<String> userList = new ArrayList<String>();
            List<String> arr2 = new ArrayList<String>();
            List<String> arr3 = new ArrayList<String>();
            if (StringUtils.isNotBlank(leadId)) {
                userList = new ArrayList<String>(Arrays.asList(leadId.split(",")));
            }
            if (StringUtils.isNotBlank(handedUser)) {
                arr2 = new ArrayList<String>(Arrays.asList(handedUser.split(",")));
            }
            if (StringUtils.isNotBlank(joinUser)) {
                arr3 = new ArrayList<String>(Arrays.asList(joinUser.split(",")));
            }
            userList.addAll(arr2);
            userList.addAll(arr3);
            Set<String> set = new HashSet<String>();
            set.addAll(userList);     // 将list所有元素添加到set中    set集合特性会自动去重复
            userList.clear();
            userList.addAll(set);
            List<MsgBody> msgBodyList = new ArrayList<MsgBody>();
            for (int i = 0; i < userList.size(); i++) {
                Account account2 = new Account();
                account2.setAccountId(userList.get(i));
                account2.setOrgId(userInfo.getOrgId());
                account2 = accountService.selectOneAccount(account2);
                MsgBody msgBody = new MsgBody();
                msgBody.setTitle("督查督办提醒");
                msgBody.setContent("任务标题为：" + superversion.getTitle() + "的查看提醒！");
                msgBody.setSendTime(superversion.getCreateTime());
                msgBody.setAccount(account2);
                msgBody.setFromAccountId(userInfo.getAccountId());
                msgBody.setFormUserName(userInfo.getUserName());
                msgBody.setRedirectUrl("/app/core/superversion/superversiondetails?superversionId==" + superversion.getSuperversionId());
                msgBody.setOrgId(userInfo.getOrgId());
                msgBodyList.add(msgBody);
            }
            String smsStatus = superversion.getMsgType();
            messageUnitService.sendMessage(smsStatus, GobalConstant.MSG_TYPE_SUPERVERSION, msgBodyList);
        }
        return superversionMapper.insert(superversion);
    }


    public int updateSuperversion(UserInfo userInfo, Example example, Superversion superversion) {
        if (StringUtils.isNotBlank(superversion.getMsgType())) {
            String leadId = superversion.getLeadId();
            String handedUser = superversion.getHandedUser();
            String joinUser = superversion.getJoinUser();
            List<String> userList = new ArrayList<String>();
            List<String> arr2 = new ArrayList<String>();
            List<String> arr3 = new ArrayList<String>();
            if (StringUtils.isNotBlank(leadId)) {
                userList = new ArrayList<String>(Arrays.asList(leadId.split(",")));
            }
            if (StringUtils.isNotBlank(handedUser)) {
                arr2 = new ArrayList<String>(Arrays.asList(handedUser.split(",")));
            }
            if (StringUtils.isNotBlank(joinUser)) {
                arr3 = new ArrayList<String>(Arrays.asList(joinUser.split(",")));
            }
            userList.addAll(arr2);
            userList.addAll(arr3);
            Set<String> set = new HashSet<String>();
            set.addAll(userList);     // 将list所有元素添加到set中    set集合特性会自动去重复
            userList.clear();
            userList.addAll(set);
            List<MsgBody> msgBodyList = new ArrayList<MsgBody>();
            for (int i = 0; i < userList.size(); i++) {
                Account account2 = new Account();
                account2.setAccountId(userList.get(i));
                account2.setOrgId(userInfo.getOrgId());
                account2 = accountService.selectOneAccount(account2);
                MsgBody msgBody = new MsgBody();
                msgBody.setTitle("督查督办提醒");
                msgBody.setContent("任务标题为：" + superversion.getTitle() + "的查看提醒！");
                msgBody.setSendTime(superversion.getCreateTime());
                msgBody.setAccount(account2);
                msgBody.setFromAccountId(userInfo.getAccountId());
                msgBody.setFormUserName(userInfo.getUserName());
                msgBody.setRedirectUrl("/app/core/superversion/superversiondetails?superversionId==" + superversion.getSuperversionId());
                msgBody.setOrgId(userInfo.getOrgId());
                msgBodyList.add(msgBody);
            }
            String smsStatus = superversion.getMsgType();
            messageUnitService.sendMessage(smsStatus, GobalConstant.MSG_TYPE_SUPERVERSION, msgBodyList);
        }
        return superversionMapper.updateByExampleSelective(superversion, example);
    }

    /**
     * @Title: getSupperversionList
     * @Description:  我的督查列表
     * @param: orgId
     * @param: accountId
     * @param: type
     * @param: handedUser
     * @param: beginTime
     * @param: endTime
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getSupperversionList(String orgId, String accountId, String type, String handedUser, String beginTime, String endTime, String status, String search) {
        return superversionMapper.getSupperversionList(orgId, accountId, type, handedUser, beginTime, endTime, status, "%" + search + "%");
    }

    /**
     * 获取督查督办事件列表
     *
     * @param orgId
     * @param accountId
     * @param type
     * @param handedUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getQueryOldSupperversionList(String orgId, String accountId, String type, String handedUser, String beginTime, String endTime, String search) {
        return superversionMapper.getQueryOldSupperversionList(orgId, accountId, type, handedUser, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取督查督办事件列表
     *
     * @param pageParam
     * @param type
     * @param handedUser
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getQueryOldSupperversionList(PageParam pageParam, String type, String handedUser, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getQueryOldSupperversionList(pageParam.getOrgId(), pageParam.getAccountId(), type, handedUser, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @Title: getLeadManageSupperversionList
     * @Description:  获取当前用户管控的事件列表
     * @param: orgId
     * @param: accountId
     * @param: type
     * @param: handedUser
     * @param: beginTime
     * @param: endTime
     * @param: status
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getLeadManageSupperversionList(String orgId, String accountId, String type, String handedUser, String beginTime, String endTime, String status, String search) {
        return superversionMapper.getLeadManageSupperversionList(orgId, accountId, type, handedUser, beginTime, endTime, status, "%" + search + "%");
    }

    /**
     * @throws Exception
     * @Title: getLeadManageSupperversionList
     * @Description:  获取当前用户管控的事件列表
     * @param: pageParam
     * @param: type
     * @param: handedUser
     * @param: beginTime
     * @param: endTime
     * @param: status
     * @param: @return
     * @return: PageInfo<Map < String, String>>
     */
    public PageInfo<Map<String, String>> getLeadManageSupperversionList(PageParam pageParam, String type, String handedUser, String beginTime, String endTime, String status) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getLeadManageSupperversionList(pageParam.getOrgId(), pageParam.getAccountId(), type, handedUser, beginTime, endTime, status, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @throws Exception
     * @Title: getSupperversionList
     * @Description:  我的督查列表
     * @param: pageParam
     * @param: type
     * @param: handedUser
     * @param: beginTime
     * @param: endTime
     * @param: @return
     * @return: PageInfo<Map < String, String>>
     */
    public PageInfo<Map<String, String>> getSupperversionList(PageParam pageParam, String type, String handedUser, String beginTime, String endTime, String status) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSupperversionList(pageParam.getOrgId(), pageParam.getAccountId(), type, handedUser, beginTime, endTime, status, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * 获取完成督查督办列表
     *
     * @param orgId
     * @param accountId
     * @param type
     * @param handedUser
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getSupperversionFinishList(String orgId, String accountId, String type, String handedUser, String beginTime, String endTime, String search) {
        return superversionMapper.getSupperversionFinishList(orgId, accountId, type, handedUser, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取完成督查督办列表
     *
     * @param pageParam
     * @param type
     * @param handedUser
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getSupperversionFinishList(PageParam pageParam, String type, String handedUser, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getSupperversionFinishList(pageParam.getOrgId(), pageParam.getAccountId(), type, handedUser, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    public List<Map<String, String>> getManageSuperversionListForDesk(String orgId, String accountId) {
        return superversionMapper.getManageSuperversionListForDesk(orgId, accountId);
    }

    /**
     * 移动端督查督办列表
     *
     * @param orgId
     * @param accountId
     * @param page
     * @return
     */
    public List<Map<String, String>> getMobileMySuperversionList(String orgId, String accountId, Integer page) {
        return superversionMapper.getMobileMySuperversionList(orgId, accountId, page);
    }

    public List<Map<String, String>> getAllSuperversionForDesk(String orgId) {
        return superversionMapper.getAllSuperversionForDesk(orgId);
    }

    public List<Map<String, String>> getAllFinishSuperversionForDesk(String orgId) {
        return superversionMapper.getAllFinishSuperversionForDesk(orgId);
    }

}
