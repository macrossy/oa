package com.core136.service.superversion;

import com.core136.bean.superversion.SuperversionProblem;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.superversion.SuperversionProblemMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SuperversionProblemService {
    private SuperversionProblemMapper superversionProblemMapper;

    @Autowired
    public void setSuperversionProblemMapper(SuperversionProblemMapper superversionProblemMapper) {
        this.superversionProblemMapper = superversionProblemMapper;
    }

    public int insertSuperversionProblem(SuperversionProblem superversionProblem) {
        return superversionProblemMapper.insert(superversionProblem);
    }

    public int deleteSuperversionProblem(SuperversionProblem superversionProblem) {
        return superversionProblemMapper.delete(superversionProblem);
    }

    public int updateSuperversionProblem(Example example, SuperversionProblem superversionProblem) {
        return superversionProblemMapper.updateByExampleSelective(superversionProblem, example);
    }

    public SuperversionProblem selectOneSuperversionProblem(SuperversionProblem superversionProblem) {
        return superversionProblemMapper.selectOne(superversionProblem);
    }

    /**
     * 获取问题列表
     *
     * @param orgId
     * @param processId
     * @param accountId
     * @param search
     * @return
     */
    public List<Map<String, String>> getMySuperversionProblemList(String orgId, String processId, String accountId, String search) {
        return superversionProblemMapper.getMySuperversionProblemList(orgId, processId, accountId, "%" + search + "%");
    }

    /**
     * 获取问题列表
     *
     * @param pageParam
     * @param processId
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getMySuperversionProblemList(PageParam pageParam, String processId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMySuperversionProblemList(pageParam.getOrgId(), processId, pageParam.getAccountId(), pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    public List<Map<String, String>> getMySuperversionAnswerList(String orgId, String recordId, String processId) {
        return superversionProblemMapper.getMySuperversionAnswerList(orgId, recordId, processId);
    }


}
