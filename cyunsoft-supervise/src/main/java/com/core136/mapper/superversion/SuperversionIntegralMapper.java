package com.core136.mapper.superversion;

import com.core136.bean.superversion.SuperversionIntegral;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SuperversionIntegralMapper extends MyMapper<SuperversionIntegral> {
}
