package com.core136.mapper.superversion;

import com.core136.bean.superversion.SuperversionBpmConfig;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SuperversionBpmConfigMapper extends MyMapper<SuperversionBpmConfig> {

    public List<Map<String, String>> getAllSuperversionBpmConfigList(@Param(value = "orgId") String orgId);


}
