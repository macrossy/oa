package com.core136.service.erp;

import com.core136.bean.erp.ErpBom;
import com.core136.bean.erp.ErpMateriel;
import com.core136.mapper.erp.ErpBomMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: ErpBomService
 * @Description: BOM清单操作服务类
 * @author: 稠云信息
 * @date: 2018年12月14日 下午2:45:30
 * @Copyright: 2018 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Service
public class ErpBomService {
    private ErpBomMapper erpBomMapper;

    @Autowired
    public void setErpBomMapper(ErpBomMapper erpBomMapper) {
        this.erpBomMapper = erpBomMapper;
    }

    /**
     * @param example
     * @param pageNumber
     * @param pageSize
     * @return PageInfo<ErpBom>
     * @Title getErpErpBomBySort
     * @Description  按条件获取BOM表单
     */
    public PageInfo<ErpBom> getErpBomBySort(Example example, int pageNumber, int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<ErpBom> datalist = erpBomMapper.selectByExample(example);
        PageInfo<ErpBom> pageInfo = new PageInfo<ErpBom>(datalist);
        return pageInfo;
    }

    /**
     * @param erpBom
     * @return int
     * @Title insertErpBom
     * @Description  创建BOM清单
     */
    public int insertErpBom(ErpBom erpBom) {
        return erpBomMapper.insert(erpBom);
    }

    /**
     * @param erpBom
     * @param example
     * @return int
     * @Title updateErpBom
     * @Description  更新BOM清单信息
     */
    public int updateErpBom(ErpBom erpBom, Example example) {
        return erpBomMapper.updateByExample(erpBom, example);
    }

    /**
     * @param erpBom
     * @return int
     * @Title delErpBom
     * @Description  删除BOM清单
     */
    public int delErpBom(ErpBom erpBom) {
        return erpBomMapper.delete(erpBom);
    }

    /**
     * @param erpBom
     * @return ErpBom
     * @Title selectOne
     * @Description  按条件查询一条BOM
     */
    public ErpBom selectOne(ErpBom erpBom) {
        return erpBomMapper.selectOne(erpBom);
    }

    /**
     * 按分类获取BOM树结构
     */

    public List<Map<String, Object>> getErpBomTreeBySortId(String sortId, String orgId) {
        //  Auto-generated method stub
        return erpBomMapper.getErpBomTreeBySortId(sortId, orgId);
    }

    /**
     * 获取BOM清单用于SELECT2插件
     */

    public List<ErpMateriel> selectBomList2ById(String bomName, String orgId) {
        //  Auto-generated method stub
        return erpBomMapper.selectBomList2ById(bomName, orgId);
    }

}
