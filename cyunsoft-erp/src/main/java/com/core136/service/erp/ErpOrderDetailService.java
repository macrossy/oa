package com.core136.service.erp;

import com.core136.bean.erp.ErpOrderDetail;
import com.core136.mapper.erp.ErpOrderDetailMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ErpOrderDetailService {
    private ErpOrderDetailMapper erpOrderDetailMapper;

    @Autowired
    public void setErpOrderDetailMapper(ErpOrderDetailMapper erpOrderDetailMapper) {
        this.erpOrderDetailMapper = erpOrderDetailMapper;
    }
    /**
     *
     */
    /**
     * 获取订单产品列表
     */
    public PageInfo<Map<String, Object>> getErpOrderDetailList(String orderId, String search, String orgId, Integer pageNumber,
                                                               Integer pageSize, String sortStr) {
        PageHelper.startPage(pageNumber, pageSize);
        List<Map<String, Object>> datalist = erpOrderDetailMapper.getErpOrderDetail(orderId, search, orgId);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<Map<String, Object>>(datalist);
        return pageInfo;
    }


    public List<Map<String, Object>> getErpOrderDetail(String orderId, String search, String orgId) {
        //  Auto-generated method stub
        return erpOrderDetailMapper.getErpOrderDetail(orderId, search, orgId);
    }

    /**
     * @param erpOrderDetail
     * @return int
     * @Title insertErpOrderDetail
     * @Description  添加订单中的产品
     */
    public int insertErpOrderDetail(ErpOrderDetail erpOrderDetail) {
        return erpOrderDetailMapper.insert(erpOrderDetail);
    }

    /**
     * @param erpOrderDetail
     * @return int
     * @Title deleteErpOrderDetail
     * @Description  删除订中的产品
     */
    public int deleteErpOrderDetail(ErpOrderDetail erpOrderDetail) {
        return erpOrderDetailMapper.delete(erpOrderDetail);
    }

    /**
     * @param erpOrderDetail
     * @return List<ErpOrderDetail>
     * @Title getErpProductList
     * @Description  获取订单中的产品列表
     */
    public List<ErpOrderDetail> getOrderDetailList(ErpOrderDetail erpOrderDetail) {
        return erpOrderDetailMapper.select(erpOrderDetail);
    }
}
