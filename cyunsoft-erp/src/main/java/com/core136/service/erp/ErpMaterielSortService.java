package com.core136.service.erp;

import com.core136.bean.account.Account;
import com.core136.bean.erp.ErpMaterielSort;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.mapper.erp.ErpMaterielSortMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

/**
 * @ClassName: ErpMaterielSortService
 * @Description: ERP物料分类数据库操作服务类
 * @author: 稠云信息
 * @date: 2018年12月7日 上午10:47:56
 * @Copyright: 2018 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@Service
public class ErpMaterielSortService {
    private ErpMaterielSortMapper erpMaterielSortMapper;

    @Autowired
    public void setErpMaterielSortMapper(ErpMaterielSortMapper erpMaterielSortMapper) {
        this.erpMaterielSortMapper = erpMaterielSortMapper;
    }

    /**
     * @Title: insertMaterielSort
     * @Description:  添加ERP物料分类
     * @param: erpMaterielSort
     * @param: @return
     * @return: int
     */
    public int insertErpMaterielSort(ErpMaterielSort erpMaterielSort) {
        return erpMaterielSortMapper.insert(erpMaterielSort);
    }

    /**
     * @Title: selectOne
     * @Description:  按条件查询一个分类信息
     * @param: erpMaterielSort
     * @param: @return
     * @return: ErpMaterielSort
     */
    public ErpMaterielSort selectOne(ErpMaterielSort erpMaterielSort) {
        return erpMaterielSortMapper.selectOne(erpMaterielSort);
    }

    /**
     * 获取物料分类的顶层分类
     */

    public List<Map<String, Object>> getErpMaterielSortTree(String sortLevel, String orgId) {
        //  Auto-generated method stub
        return erpMaterielSortMapper.getErpMaterielSortTree(sortLevel, orgId);
    }

    /**
     * 判断当前分类下是否还有子分类，大于0再说明下面有子分类,小于0再分类下没有子分类
     */

    public int isExistChild(String sortId, String orgId) {
        //  Auto-generated method stub
        return erpMaterielSortMapper.isExistChild(sortId, orgId);
    }

    /**
     * @Title: delErpMaterielSort
     * @Description:  删除分类
     * @param: erpMaterielSort
     * @param: @return
     * @return: int
     */
    public int delErpMaterielSort(ErpMaterielSort erpMaterielSort) {
        return erpMaterielSortMapper.delete(erpMaterielSort);
    }

    /**
     * @Title: updateErpMaterielSort
     * @Description:  更新物料分类
     * @param: erpMaterielSort
     * @param: example
     * @param: @return
     * @return: int
     */
    public int updateErpMaterielSort(ErpMaterielSort erpMaterielSort, Example example) {
        return erpMaterielSortMapper.updateByExample(erpMaterielSort, example);
    }


    /**
     * @return int
     * ServletException
     * FileUploadException
     * @Title importMaterielSort
     * @Description  物料分类导入
     */
    public RetDataBean importMaterielSort(MultipartFile file, Account account) {
//		try {
//			//List<String[]> list=ExcelUtil.readExcel(file);
//			//jdbcTemplate.execute("insert into erp_test(dm1,mc1,materiel_code) values('"+list.get(0)[0]+"','"+list.get(0)[1]+"','"+list.get(0)[14]+"')");
//			//System.out.println(list.size());
//		} catch (IOException e) {
//			//  Auto-generated catch block
//
//		}
        return null;
    }
}
