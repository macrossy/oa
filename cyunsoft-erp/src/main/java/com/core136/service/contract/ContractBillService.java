package com.core136.service.contract;

import com.core136.bean.contract.ContractBill;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.contract.ContractBillMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ContractBillService {

    private ContractBillMapper contractBillMapper;

    @Autowired
    public void setContractBillMapper(ContractBillMapper contractBillMapper) {
        this.contractBillMapper = contractBillMapper;
    }

    public int insertContractBill(ContractBill contractBill) {
        return contractBillMapper.insert(contractBill);
    }

    public int deleteContractBill(ContractBill contractBill) {
        return contractBillMapper.delete(contractBill);
    }

    public int updateContractBill(ContractBill contractBill, Example example) {
        return contractBillMapper.updateByExampleSelective(contractBill, example);
    }

    public ContractBill selectOneContractBill(ContractBill contractBill) {
        return contractBillMapper.selectOne(contractBill);
    }

    /**
     * @Title: getContractBillList
     * @Description: 获取发票列表
     * @param: orgId
     * @param: opFlag
     * @param: accountId
     * @param: userPriv
     * @param: beginTime
     * @param: endTime
     * @param: search
     * @param: @return
     * @return: List<Map < String, String>>
     */
    public List<Map<String, String>> getContractBillList(String orgId, String opFlag, String accountId, String isOpen, String status, String billType, String beginTime, String endTime, String search) {
        return contractBillMapper.getContractBillList(orgId, opFlag, accountId, isOpen, status, billType, beginTime, endTime, "%" + search + "%");
    }

    /**
     * @throws Exception
     * @Title: getContractBillList
     * @Description:  获取发票列表
     * @param: pageParam
     * @param: userPriv
     * @param: beginTime
     * @param: endTime
     * @param: @return
     * @return: PageInfo<Map < String, String>>
     */
    public PageInfo<Map<String, String>> getContractBillList(PageParam pageParam, String isOpen, String status, String billType, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getContractBillList(pageParam.getOrgId(), pageParam.getOpFlag(), pageParam.getAccountId(), isOpen, status, billType, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getDeskBillList
     * @Description:  获取财务门户发票列表
     */
    public List<Map<String, String>> getDeskBillList(String orgId) {
        return contractBillMapper.getDeskBillList(orgId);
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getContractBillTop
     * @Description:  获取近期发票列表
     */
    public List<Map<String, String>> getContractBillTop(String orgId) {
        return contractBillMapper.getContractBillTop(orgId);
    }
}
