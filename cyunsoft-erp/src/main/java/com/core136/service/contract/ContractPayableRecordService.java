package com.core136.service.contract;

import com.core136.bean.contract.ContractPayable;
import com.core136.bean.contract.ContractPayableRecord;
import com.core136.bean.sys.PageParam;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.contract.ContractPayableRecordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ContractPayableRecordService {
    private ContractPayableRecordMapper contractPayableRecordMapper;

    @Autowired
    public void setContractPayableRecordMapper(ContractPayableRecordMapper contractPayableRecordMapper) {
        this.contractPayableRecordMapper = contractPayableRecordMapper;
    }

    private ContractPayableService contractPayableService;

    @Autowired
    public void setContractPayableService(ContractPayableService contractPayableService) {
        this.contractPayableService = contractPayableService;
    }

    /**
     * @param contractPayableRecord
     * @return RetDataBean
     * @Title: payableAmount
     * @Description:  添加付款记录
     */
    @Transactional(value = "generalTM")
    public RetDataBean payableAmount(ContractPayableRecord contractPayableRecord) {
        ContractPayable contractPayable = new ContractPayable();
        contractPayable.setPayableId(contractPayableRecord.getPayableId());
        contractPayable.setOrgId(contractPayableRecord.getOrgId());
        contractPayable = contractPayableService.selectOneContractPayable(contractPayable);
        Double payabled = contractPayable.getPayabled();
        Double unPayabled = contractPayable.getUnPayabled();
        if (unPayabled < contractPayableRecord.getAmount()) {
            return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
        } else {
            unPayabled = unPayabled - contractPayableRecord.getAmount();
            payabled = payabled + contractPayableRecord.getAmount();
            ContractPayable contractPayable1 = new ContractPayable();
            contractPayable1.setPayableId(contractPayable.getPayableId());
            contractPayable1.setUnPayabled(unPayabled);
            contractPayable1.setPayabled(payabled);
            contractPayable1.setOrgId(contractPayable.getOrgId());
            Example example = new Example(ContractPayable.class);
            example.createCriteria().andEqualTo("orgId", contractPayable.getOrgId()).andEqualTo("payableId", contractPayable1.getPayableId());
            contractPayableService.updateContractPayable(contractPayable1, example);
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, insertContractPayableRecord(contractPayableRecord));
        }
    }


    public int insertContractPayableRecord(ContractPayableRecord contractPayableRecord) {
        return contractPayableRecordMapper.insert(contractPayableRecord);
    }

    public int deleteContractPayableRecord(ContractPayableRecord contractPayableRecord) {
        return contractPayableRecordMapper.delete(contractPayableRecord);
    }

    public int updateContractPayableRecord(Example example, ContractPayableRecord contractPayableRecord) {
        return contractPayableRecordMapper.updateByExampleSelective(contractPayableRecord, example);
    }

    public ContractPayableRecord selectOneContractPayableRecord(ContractPayableRecord contractPayableRecord) {
        return contractPayableRecordMapper.selectOne(contractPayableRecord);
    }

    public List<Map<String, String>> getContractPayableRecordList(String orgId, String payableId) {
        return contractPayableRecordMapper.getContractPayableRecordList(orgId, payableId);
    }

    /**
     * @param pageParam
     * @return PageInfo<Map < String, String>>
     * @throws Exception
     * @Title: getContractPayableRecordList
     * @Description:  获取付款记录
     */
    public PageInfo<Map<String, String>> getContractPayableRecordList(PageParam pageParam, String payableId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSql(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getContractPayableRecordList(pageParam.getOrgId(), payableId);
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getPayableRecordTop
     * @Description:  近期付款记录
     */
    public List<Map<String, String>> getPayableRecordTop(String orgId) {
        return contractPayableRecordMapper.getPayableRecordTop(orgId);
    }

}
