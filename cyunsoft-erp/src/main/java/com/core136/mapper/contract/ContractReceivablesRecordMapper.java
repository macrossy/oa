package com.core136.mapper.contract;

import com.core136.bean.contract.ContractReceivablesRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ContractReceivablesRecordMapper extends MyMapper<ContractReceivablesRecord> {

    /**
     * @param orgId
     * @param receivablesId
     * @return List<Map < String, String>>
     * @Title: getContractReceivablesRecordList
     * @Description:  获取收款记录
     */
    public List<Map<String, String>> getContractReceivablesRecordList(@Param(value = "orgId") String orgId, @Param(value = "receivablesId") String receivablesId);

    /**
     * @param orgId
     * @return List<Map < String, String>>
     * @Title: getReceivRecordTop
     * @Description:  收款记录
     */
    public List<Map<String, String>> getReceivRecordTop(@Param(value = "orgId") String orgId);
}
