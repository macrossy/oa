package com.core136.mapper.crm;

import com.core136.bean.crm.CrmLinkMan;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface CrmLinkManMapper extends MyMapper<CrmLinkMan> {

    /**
     * @param orgId
     * @param customerId
     * @return List<Map < String, Object>>
     * @Title getCrmLinkManList
     * @Description  获取客户联系人列表
     */
    public List<Map<String, Object>> getCrmLinkManList(@Param(value = "orgId") String orgId, @Param(value = "customerId") String customerId);


    /**
     * @param orgId
     * @param search
     * @return List<Map < String, Object>>
     * @Title getCrmLinkManAllList
     * @Description  获取CRM联系人列表
     */
    public List<Map<String, Object>> getCrmLinkManAllList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);

    /**
     * @param @param  orgId
     * @param @param  keepUser
     * @param @param  search
     * @param @return 设定文件
     * @return List<Map < String, Object>> 返回类型
     * @Title: getMyCrmLinkManAllList
     * @Description:  业务员客户联系人
     */
    public List<Map<String, Object>> getMyCrmLinkManAllList(@Param(value = "orgId") String orgId, @Param(value = "keepUser") String keepUser, @Param(value = "search") String search);

    /**
     * @param orgId
     * @param customerId
     * @param linkManId
     * @return List<Map < String, Object>>
     * @Title getCrmLinkManInfo
     * @Description  联系人基本信息
     */
    public Map<String, Object> getCrmLinkManInfo(@Param(value = "orgId") String orgId, @Param(value = "linkManId") String linkManId);


}
