package com.core136.mapper.crm;

import com.core136.bean.crm.CrmContractRecord;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface CrmContractRecordMapper extends MyMapper<CrmContractRecord> {
    /**
     * @param orgId
     * @param customerId
     * @return List<Map < String, Object>>
     * @Title getRecordListByCustomerId
     * @Description  获取客户所有联系记录
     */
    public List<Map<String, Object>> getRecordListByCustomerId(@Param(value = "orgId") String orgId, @Param(value = "customerId") String customerId);
}
