package com.core136.mapper.crm;

import com.core136.bean.crm.CrmIndustry;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface CrmIndustryMapper extends MyMapper<CrmIndustry> {
    /**
     * @param @param  orgId
     * @param @param  search
     * @param @return 设定文件
     * @return List<Map < String, Object>> 返回类型
     * @Title: getAllIndustryList
     * @Description:  获取企业分类列表
     */
    public List<Map<String, Object>> getAllIndustryList(@Param(value = "orgId") String orgId, @Param(value = "search") String search);
}
