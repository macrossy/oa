package com.core136.mapper.erp;

import com.core136.bean.erp.ErpOrder;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ErpOrderMapper extends MyMapper<ErpOrder> {

}
