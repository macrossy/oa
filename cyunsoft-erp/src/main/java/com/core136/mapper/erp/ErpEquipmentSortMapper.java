package com.core136.mapper.erp;

import com.core136.bean.erp.ErpEquipmentSort;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ErpEquipmentSortMapper extends MyMapper<ErpEquipmentSort> {

    /**
     * @param sortLevel
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title getErpEquipmentSortTree
     * @Description  获取设备分类树结构
     */
    public List<Map<String, Object>> getErpEquipmentSortTree(@Param(value = "sortLevel") String sortLevel, @Param(value = "orgId") String orgId);

    /**
     * @param sortId
     * @param orgId
     * @return int
     * @Title isExistChild
     * @Description  判断分类下是否有子分类
     */
    public int isExistChild(@Param(value = "sortId") String sortId, @Param(value = "orgId") String orgId);
}
