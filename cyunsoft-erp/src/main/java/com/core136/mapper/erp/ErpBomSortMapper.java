package com.core136.mapper.erp;

import com.core136.bean.erp.ErpBomSort;
import com.core136.common.dbutils.MyMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ErpBomSortMapper extends MyMapper<ErpBomSort> {
    /**
     * @param sortLevel
     * @param orgId
     * @return List<Map < String, Object>>
     * @Title getErpBomSortTree
     * @Description  获取Bom分类父级Id
     */
    public List<Map<String, Object>> getErpBomSortTree(@Param(value = "sortLevel") String sortLevel, @Param(value = "orgId") String orgId);

    /**
     * @param sortId
     * @param orgId
     * @return int
     * @Title isExistChild
     * @Description  判断当前分类下面是否还有子分类
     */
    public int isExistChild(@Param(value = "sortId") String sortId, @Param(value = "orgId") String orgId);
}
