package com.core136.controller.erp;

import com.core136.bean.account.Account;
import com.core136.bean.erp.*;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.service.account.AccountService;
import com.core136.service.erp.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * @ClassName: RoutSetErpController
 * @Description: ERP数据更新接口
 * @author: 稠云信息
 * @date: 2018年12月14日 下午2:39:31
 * @Copyright: 2018 www.cyunsoft.com Inc. All rights reserved.
 * 注意：本内容仅限于江苏稠云信息技术有限公司内部传阅，禁止外泄以及用于其他的商业目
 */
@RestController
@RequestMapping("/set/erpset")
public class RouteSetErpController {
    private ErpMaterielSortService erpMaterielSortService;

    @Autowired
    public void setErpMaterielSortService(ErpMaterielSortService erpMaterielSortService) {
        this.erpMaterielSortService = erpMaterielSortService;
    }

    private ErpProductSortService erpProductSortService;

    @Autowired
    public void setErpProductSortService(ErpProductSortService erpProductSortService) {
        this.erpProductSortService = erpProductSortService;
    }

    private ErpUnitService erpUnitService;

    @Autowired
    public void setErpUnitService(ErpUnitService erpUnitService) {
        this.erpUnitService = erpUnitService;
    }

    private ErpMaterielService erpMaterielService;

    @Autowired
    public void setErpMaterielService(ErpMaterielService erpMaterielService) {
        this.erpMaterielService = erpMaterielService;
    }

    private ErpBomSortService erpBomSortService;

    @Autowired
    public void setErpBomSortService(ErpBomSortService erpBomSortService) {
        this.erpBomSortService = erpBomSortService;
    }

    private ErpBomDetailService erpBomDetailService;

    @Autowired
    public void setErpBomDetailService(ErpBomDetailService erpBomDetailService) {
        this.erpBomDetailService = erpBomDetailService;
    }

    private ErpBomService erpBomService;

    @Autowired
    public void setErpBomService(ErpBomService erpBomService) {
        this.erpBomService = erpBomService;
    }

    private ErpProductService erpProductService;

    @Autowired
    public void setErpProductService(ErpProductService erpProductService) {
        this.erpProductService = erpProductService;
    }

    private ErpOrderService erpOrderService;

    @Autowired
    public void setErpOrderService(ErpOrderService erpOrderService) {
        this.erpOrderService = erpOrderService;
    }

    private ErpOrderDetailService erpOrderDetailService;

    @Autowired
    public void setErpOrderDetailService(ErpOrderDetailService erpOrderDetailService) {
        this.erpOrderDetailService = erpOrderDetailService;
    }

    private DoCostService doCostService;

    @Autowired
    public void setDoCostService(DoCostService doCostService) {
        this.doCostService = doCostService;
    }

    private ErpEquipmentService erpEquipmentService;

    @Autowired
    public void setErpEquipmentService(ErpEquipmentService erpEquipmentService) {
        this.erpEquipmentService = erpEquipmentService;
    }

    private ErpEquipmentSortService erpEquipmentSortService;

    @Autowired
    public void setErpEquipmentSortService(ErpEquipmentSortService erpEquipmentSortService) {
        this.erpEquipmentSortService = erpEquipmentSortService;
    }

    private AccountService accountService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }


    /**
     * @param erpMaterielSort
     * @param
     * @Title: addMaterielSort
     * @Description:  创建ERP物料分类
     * @return: RetDataBean      数据返回类
     */
    @RequestMapping(value = "/insertErpMaterielSort", method = RequestMethod.POST)
    public RetDataBean insertErpMaterielSort(ErpMaterielSort erpMaterielSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(erpMaterielSort.getSortLevel())) {
                erpMaterielSort.setSortLevel("0");
            }
            erpMaterielSort.setSortId(SysTools.getGUID());
            erpMaterielSort.setCreateUser(account.getAccountId());
            erpMaterielSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            erpMaterielSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, erpMaterielSortService.insertErpMaterielSort(erpMaterielSort));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: delErpMaterielSort
     * @Description:  删除物料分类
     * @param: request
     * @param: erpMaterielSort
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/delErpMaterielSort", method = RequestMethod.POST)
    public RetDataBean delErpMaterielSort(ErpMaterielSort erpMaterielSort) {
        try {
            if (StringUtils.isBlank(erpMaterielSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            if (erpMaterielSortService.isExistChild(erpMaterielSort.getSortId(), account.getOrgId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                erpMaterielSort.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, erpMaterielSortService.delErpMaterielSort(erpMaterielSort));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateErpMaterielSort
     * @Description:  更新物料分类
     * @param: request
     * @param: erpMaterielSort
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateErpMaterielSort", method = RequestMethod.POST)
    public RetDataBean updateErpMaterielSort(ErpMaterielSort erpMaterielSort) {
        try {
            if (StringUtils.isBlank(erpMaterielSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                if (StringUtils.isBlank(erpMaterielSort.getSortLevel())) {
                    erpMaterielSort.setSortLevel("0");
                }
                erpMaterielSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                erpMaterielSort.setCreateUser(account.getAccountId());
                erpMaterielSort.setOrgId(account.getOrgId());
                Example example = new Example(ErpMaterielSort.class);
                Criteria criteria = example.createCriteria();
                criteria.andEqualTo("sortId", erpMaterielSort.getSortId()).andEqualTo("orgId", account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, erpMaterielSortService.updateErpMaterielSort(erpMaterielSort, example));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param erpProductSort
     * @return
     * @Title: updateErpProductSort
     * @Description:  更新产品分类
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateErpProductSort", method = RequestMethod.POST)
    public RetDataBean updateErpProductSort(ErpProductSort erpProductSort) {
        try {
            if (StringUtils.isBlank(erpProductSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                if (StringUtils.isBlank(erpProductSort.getSortLevel())) {
                    erpProductSort.setSortLevel("0");
                }
                erpProductSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                erpProductSort.setCreateUser(account.getAccountId());
                erpProductSort.setOrgId(account.getOrgId());
                Example example = new Example(ErpProductSort.class);
                Criteria criteria = example.createCriteria();
                criteria.andEqualTo("sortId", erpProductSort.getSortId()).andEqualTo("orgId", account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, erpProductSortService.updateErpProductSort(erpProductSort, example));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: insertErpProductSort
     * @Description:  创建产品分类
     * @param: request
     * @param: erpProductSort
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertErpProductSort", method = RequestMethod.POST)
    public RetDataBean insertErpProductSort(ErpProductSort erpProductSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(erpProductSort.getSortLevel())) {
                erpProductSort.setSortLevel("0");
            }
            erpProductSort.setSortId(SysTools.getGUID());
            erpProductSort.setCreateUser(account.getAccountId());
            erpProductSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            erpProductSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_REQUEST_SUCCESS, erpProductSortService.insert(erpProductSort));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: delErpProductSort
     * @Description:  删除产品分类
     * @param: erpProductSort
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/delErpProductSort", method = RequestMethod.POST)
    public RetDataBean delErpProductSort(ErpProductSort erpProductSort) {
        try {
            if (StringUtils.isBlank(erpProductSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            if (erpMaterielSortService.isExistChild(erpProductSort.getSortId(), account.getOrgId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                if (StringUtils.isNotBlank(erpProductSort.getSortId())) {

                    erpProductSort.setOrgId(account.getOrgId());
                    return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, erpProductSortService.delErpProductSort(erpProductSort));
                } else {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
                }
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpProduct
     * @return RetDataBean
     * @Title delErpProduct
     * @Description  删除产品
     */
    @RequestMapping(value = "/delErpProduct", method = RequestMethod.POST)
    public RetDataBean delErpProduct(ErpProduct erpProduct) {
        try {
            if (StringUtils.isBlank(erpProduct.getProductId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                if (StringUtils.isBlank(erpProduct.getProductId())) {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
                } else {
                    erpProduct.setOrgId(account.getOrgId());
                    return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, erpProductService.delErpProduct(erpProduct));
                }
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: insertErpMateriel
     * @Description:  添加新物料
     * @param: request
     * @param: erpMateriel
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/insertErpMateriel", method = RequestMethod.POST)
    public RetDataBean insertErpMateriel(ErpMateriel erpMateriel) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            erpMateriel.setMaterielId(SysTools.getGUID());
            erpMateriel.setCreateUser(account.getAccountId());
            erpMateriel.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            erpMateriel.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, erpMaterielService.insert(erpMateriel));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: delErpMateriel
     * @Description:  删除提定的物料
     * @param: request
     * @param: erpMateriel
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/delErpMateriel", method = RequestMethod.POST)
    public RetDataBean delErpMateriel(ErpMateriel erpMateriel) {
        try {
            if (StringUtils.isBlank(erpMateriel.getMaterielId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                if (StringUtils.isNotBlank(erpMateriel.getMaterielId())) {
                    erpMateriel.setOrgId(account.getOrgId());
                    return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, erpMaterielService.delete(erpMateriel));
                } else {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
                }

            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @Title: updateErpMateriel
     * @Description:  更新指定的物料信息
     * @param: request
     * @param: erpMateriel
     * @param: @return
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateErpMateriel", method = RequestMethod.POST)
    public RetDataBean updateErpMateriel(ErpMateriel erpMateriel) {
        try {
            if (StringUtils.isBlank(erpMateriel.getMaterielId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            erpMateriel.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            erpMateriel.setCreateUser(account.getAccountId());
            erpMateriel.setOrgId(account.getOrgId());
            Example example = new Example(ErpMateriel.class);
            Criteria criteria = example.createCriteria();
            criteria.andEqualTo("materielId", erpMateriel.getMaterielId()).andEqualTo("orgId", account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, erpMaterielService.updateErpMateriel(erpMateriel, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param ErpBomSort
     * @return RetDataBean
     * @Title insertErpBomSort
     * @Description  新建BOM分类
     */
    @RequestMapping(value = "/insertErpBomSort", method = RequestMethod.POST)
    public RetDataBean insertErpBomSort(ErpBomSort ErpBomSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(ErpBomSort.getSortLevel())) {
                ErpBomSort.setSortLevel("0");
            }
            ErpBomSort.setSortId(SysTools.getGUID());
            ErpBomSort.setCreateUser(account.getAccountId());
            ErpBomSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            ErpBomSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, erpBomSortService.insertErpBomSort(ErpBomSort));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpBomSort
     * @return RetDataBean
     * @Title updateErpBomSort
     * @Description  按条件更新BOM分类
     */
    @RequestMapping(value = "/updateErpBomSort", method = RequestMethod.POST)
    public RetDataBean updateErpBomSort(ErpBomSort erpBomSort) {
        try {
            if (StringUtils.isBlank(erpBomSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                if (StringUtils.isBlank(erpBomSort.getSortLevel())) {
                    erpBomSort.setSortLevel("0");
                }
                erpBomSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                erpBomSort.setCreateUser(account.getAccountId());
                erpBomSort.setOrgId(account.getOrgId());
                Example example = new Example(ErpBomSort.class);
                Criteria criteria = example.createCriteria();
                criteria.andEqualTo("sortId", erpBomSort.getSortId()).andEqualTo("orgId", account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, erpBomSortService.updateErpBomSort(erpBomSort, example));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpBomSort
     * @return RetDataBean
     * @Title delErpBomSort
     * @Description  删除BOM分类
     */
    @RequestMapping(value = "/delErpBomSort", method = RequestMethod.POST)
    public RetDataBean delErpBomSort(ErpBomSort erpBomSort) {
        try {
            if (StringUtils.isBlank(erpBomSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            if (erpMaterielSortService.isExistChild(erpBomSort.getSortId(), account.getOrgId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                if (StringUtils.isNotBlank(erpBomSort.getSortId())) {
                    erpBomSort.setOrgId(account.getOrgId());
                    return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, erpBomSortService.delErpBomSort(erpBomSort));
                } else {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
                }

            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpBom
     * @return RetDataBean
     * @Title insertErpBom
     * @Description  创建BOM清单
     */
    @RequestMapping(value = "/insertErpBom", method = RequestMethod.POST)
    public RetDataBean insertErpBom(ErpBom erpBom) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            erpBom.setBomId(SysTools.getGUID());
            erpBom.setCreateUser(account.getAccountId());
            erpBom.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            erpBom.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, erpBomService.insertErpBom(erpBom));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpBom
     * @return RetDataBean
     * @Title updateErpBom
     * @Description  更新BOM清单
     */
    @RequestMapping(value = "/updateErpBom", method = RequestMethod.POST)
    public RetDataBean updateErpBom(ErpBom erpBom) {
        try {
            if (StringUtils.isBlank(erpBom.getBomId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                erpBom.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                erpBom.setCreateUser(account.getAccountId());
                erpBom.setOrgId(account.getOrgId());
                Example example = new Example(ErpBom.class);
                Criteria criteria = example.createCriteria();
                criteria.andEqualTo("bomId", erpBom.getBomId()).andEqualTo("orgId", account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, erpBomService.updateErpBom(erpBom, example));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpBom
     * @return RetDataBean
     * @Title delErpBom
     * @Description  按指定的条件删除BOM清单
     */
    @RequestMapping(value = "/delErpBom", method = RequestMethod.POST)
    public RetDataBean delErpBom(ErpBom erpBom) {
        try {
            if (StringUtils.isBlank(erpBom.getBomId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            if (StringUtils.isNotBlank(erpBom.getBomId())) {
                erpBom.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, erpBomService.delErpBom(erpBom));
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpBomDetail
     * @return RetDataBean
     * @Title insertErpBomDetail
     * @Description  添加BOM清单详情
     */
    @RequestMapping(value = "/insertErpBomDetail", method = RequestMethod.POST)
    public RetDataBean insertErpBomDetail(ErpBomDetail erpBomDetail) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(erpBomDetail.getMaterielCode())) {
                if (erpBomDetailService.isExistMaterielCode(erpBomDetail.getBomId(), erpBomDetail.getMaterielCode(), account.getOrgId()) > 0) {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
                }
            } else if (StringUtils.isNotBlank(erpBomDetail.getChildBomId())) {
                if (erpBomDetailService.isExistChildBomIdCode(erpBomDetail.getBomId(), erpBomDetail.getChildBomId(), account.getOrgId()) > 0) {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
                }
            }

            if (StringUtils.isBlank(erpBomDetail.getChildBomId()) && StringUtils.isBlank(erpBomDetail.getMaterielCode())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            } else {
                erpBomDetail.setBomDetailId(SysTools.getGUID());
                erpBomDetail.setCreateUser(account.getAccountId());
                erpBomDetail.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                erpBomDetail.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, erpBomDetailService.insertErpBomDetail(erpBomDetail));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpBomDetail
     * @return RetDataBean
     * @Title deleteErpBomDetail
     * @Description  删除BOM清单详情中的物料
     */
    @RequestMapping(value = "/deleteErpBomDetail", method = RequestMethod.POST)
    public RetDataBean deleteErpBomDetail(ErpBomDetail erpBomDetail) {
        try {
            if (StringUtils.isBlank(erpBomDetail.getBomDetailId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(erpBomDetail.getBomDetailId())) {
                erpBomDetail.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, erpBomDetailService.deleteErpBomDetail(erpBomDetail));
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }

        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }


    /**
     * @param erpBomDetail
     * @return RetDataBean
     * @Title updateErpBomDetail
     * @Description  更新 ErpBomDetail
     */
    @RequestMapping(value = "/updateErpBomDetail", method = RequestMethod.POST)
    public RetDataBean updateErpBomDetail(ErpBomDetail erpBomDetail) {
        try {
            if (StringUtils.isBlank(erpBomDetail.getBomDetailId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                erpBomDetail.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                erpBomDetail.setCreateUser(account.getAccountId());
                erpBomDetail.setOrgId(account.getOrgId());
                Example example = new Example(ErpBomDetail.class);
                Criteria criteria = example.createCriteria();
                criteria.andEqualTo("bomDetailId", erpBomDetail.getBomDetailId()).andEqualTo("bomId", erpBomDetail.getBomId()).andEqualTo("orgId", account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, erpBomDetailService.updateErpBomDetail(erpBomDetail, example));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpProduct
     * @return RetDataBean
     * @Title insertErpProduct
     * @Description  添加新产品
     */
    @RequestMapping(value = "/insertErpProduct", method = RequestMethod.POST)
    public RetDataBean insertErpProduct(ErpProduct erpProduct) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            erpProduct.setProductId(SysTools.getGUID());
            erpProduct.setCreateUser(account.getAccountId());
            erpProduct.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            erpProduct.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, erpProductService.insertErpProduct(erpProduct));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpOrder
     * @return RetDataBean
     * @Title insertErpOrder
     * @Description  创建订单
     */
    @RequestMapping(value = "/insertErpOrder", method = RequestMethod.POST)
    public RetDataBean insertErpOrder(ErpOrder erpOrder) {
        try {
            if (StringUtils.isNotBlank(erpOrder.getOrderCode())) {
                Account account = accountService.getRedisAUserInfoToAccount();
                erpOrder.setOrderId(SysTools.getGUID());
                erpOrder.setCreateUser(account.getAccountId());
                erpOrder.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                erpOrder.setOrgId(account.getOrgId());
                erpOrderService.insertErpOrder(erpOrder);
                return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, "/app/core/erp/cost/orderproduct?orderId=" + erpOrder.getOrderId());
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_FAILED);
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpOrder
     * @return RetDataBean
     * @Title updateErpOrder
     * @Description 更新订单信息
     */
    @RequestMapping(value = "/updateErpOrder", method = RequestMethod.POST)
    public RetDataBean updateErpOrder(ErpOrder erpOrder) {
        try {
            if (StringUtils.isBlank(erpOrder.getOrderId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            erpOrder.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            erpOrder.setCreateUser(account.getAccountId());
            erpOrder.setOrgId(account.getOrgId());
            Example example = new Example(ErpOrder.class);
            Criteria criteria = example.createCriteria();
            criteria.andEqualTo("orderId", erpOrder.getOrderId()).andEqualTo("orgId", account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, erpOrderService.updateErpOrder(erpOrder, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    @RequestMapping(value = "/deleteErpOrder", method = RequestMethod.POST)
    public RetDataBean deleteErpOrder(ErpOrder erpOrder) {
        try {
            if (StringUtils.isBlank(erpOrder.getOrderId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                erpOrder.setCreateUser(account.getAccountId());
            } else {
                erpOrder.setOrgId(account.getOrgId());
            }
            if (StringUtils.isNotBlank(erpOrder.getOrderId())) {
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, erpOrderService.deleteErpOrder(erpOrder));
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }

        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpOrderDetail
     * @return RetDataBean
     * @Title Detail
     * @Description  添加订单产品
     */
    @RequestMapping(value = "/insertErpOrderDetail", method = RequestMethod.POST)
    public RetDataBean insertErpOrderDetail(ErpOrderDetail erpOrderDetail) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            erpOrderDetail.setDetailId(SysTools.getGUID());
            erpOrderDetail.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, erpOrderDetailService.insertErpOrderDetail(erpOrderDetail));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpOrderDetail
     * @return RetDataBean
     * @Title deleteErpOrderDetail
     * @Description  删除订单中的产品，若订单已核成本成本，则不能删除
     */
    @RequestMapping(value = "/deleteErpOrderDetail", method = RequestMethod.POST)
    public RetDataBean deleteErpOrderDetail(ErpOrderDetail erpOrderDetail) {
        try {
            if (StringUtils.isBlank(erpOrderDetail.getDetailId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isNotBlank(erpOrderDetail.getOrderId())) {
                erpOrderDetail.setOrgId(account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, erpOrderDetailService.deleteErpOrderDetail(erpOrderDetail));
            } else {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }

        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpProduct
     * @return
     * @Title: updateErpProduct
     * @Description:  更新产品信息
     * @return: RetDataBean
     */
    @RequestMapping(value = "/updateErpProduct", method = RequestMethod.POST)
    public RetDataBean updateErpProduct(ErpProduct erpProduct) {
        try {
            if (StringUtils.isBlank(erpProduct.getProductId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            erpProduct.setOrgId(account.getOrgId());
            Example example = new Example(ErpProduct.class);
            example.createCriteria().andEqualTo("productId", erpProduct.getProductId()).andEqualTo("orgId", account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, erpProductService.updateErpProduct(erpProduct, example));
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @return RetDataBean
     * @Title importmaterielsort
     * @Description  导入物料分类
     */
    @RequestMapping(value = "/importmaterielsort", method = RequestMethod.POST)
    public RetDataBean importmaterielsort(@RequestParam MultipartFile file) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, erpMaterielSortService.importMaterielSort(file, account));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpEquipmentSort
     * @return RetDataBean
     * @Title insertErpEquipmentSort
     * @Description  创建设备分类
     */
    @RequestMapping(value = "/insertErpEquipmentSort", method = RequestMethod.POST)
    public RetDataBean insertErpEquipmentSort(ErpEquipmentSort erpEquipmentSort) {
        try {
            Account account = accountService.getRedisAUserInfoToAccount();
            if (StringUtils.isBlank(erpEquipmentSort.getSortLevel())) {
                erpEquipmentSort.setSortLevel("0");
            }
            erpEquipmentSort.setSortId(SysTools.getGUID());
            erpEquipmentSort.setCreateUser(account.getAccountId());
            erpEquipmentSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            erpEquipmentSort.setOrgId(account.getOrgId());
            return RetDataTools.Ok(MessageCode.MESSAGE_INSERT_SUCCESS, erpEquipmentSortService.insertErpEquipmentSort(erpEquipmentSort));
        } catch (Exception e) {

            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpEquipmentSort
     * @return RetDataBean
     * @Title updateErpEquipmentSort
     * @Description  更新设备分类
     */
    @RequestMapping(value = "/updateErpEquipmentSort", method = RequestMethod.POST)
    public RetDataBean updateErpEquipmentSort(ErpEquipmentSort erpEquipmentSort) {
        try {
            if (StringUtils.isBlank(erpEquipmentSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            } else {
                if (StringUtils.isBlank(erpEquipmentSort.getSortLevel())) {
                    erpEquipmentSort.setSortLevel("0");
                }
                erpEquipmentSort.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
                erpEquipmentSort.setCreateUser(account.getAccountId());
                erpEquipmentSort.setOrgId(account.getOrgId());
                Example example = new Example(ErpEquipmentSort.class);
                Criteria criteria = example.createCriteria();
                criteria.andEqualTo("sortId", erpEquipmentSort.getSortId()).andEqualTo("orgId", account.getOrgId());
                return RetDataTools.Ok(MessageCode.MESSAGE_UPDATE_SUCCESS, erpEquipmentSortService.updateErpEquipmentSort(erpEquipmentSort, example));
            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

    /**
     * @param erpEquipmentSort
     * @return RetDataBean
     * @Title delErpEquipmentSort
     * @Description  删除设备分类
     */
    @RequestMapping(value = "/delErpEquipmentSort", method = RequestMethod.POST)
    public RetDataBean delErpEquipmentSort(ErpEquipmentSort erpEquipmentSort) {
        try {
            if (StringUtils.isBlank(erpEquipmentSort.getSortId())) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
            }
            Account account = accountService.getRedisAUserInfoToAccount();
            if (!account.getOpFlag().equals("1")) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_YOUR_NOT_ADMIN);
            }
            if (erpMaterielSortService.isExistChild(erpEquipmentSort.getSortId(), account.getOrgId()) > 0) {
                return RetDataTools.NotOk(MessageCode.MESSAGE_HAVE_CHILDREN_ITEM);
            } else {
                if (StringUtils.isNotBlank(erpEquipmentSort.getSortId())) {
                    erpEquipmentSort.setOrgId(account.getOrgId());
                    return RetDataTools.Ok(MessageCode.MESSAGE_DELETE_SUCCESS, erpEquipmentSortService.deleteErpEquipmentSort(erpEquipmentSort));
                } else {
                    return RetDataTools.NotOk(MessageCode.MESSAGE_REQUEST_ERROR);
                }

            }
        } catch (Exception e) {
            return RetDataTools.Error(e.getMessage());
        }
    }

}
