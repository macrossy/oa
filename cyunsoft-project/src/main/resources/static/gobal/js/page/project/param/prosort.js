var zTree;
var setting = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/proget/getProSortTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    callback: {
        onClick: zTreeOnClick
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    }
};

var setting1 = {
    async: {
        enable: true,// 设置 zTree 是否开启异步加载模式
        url: "/ret/proget/getProSortTree",// Ajax 获取数据的 URL 地址
        autoParam: ["sortId"],// 异步加载时需要自动提交父节点属性的参数
    },
    view: {
        dblClickExpand: false,
        selectedMulti: false
        //禁止多选
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "sortId",
            pIdKey: "levelId",
            rootPId: "0"
        },
        key: {
            name: "sortName"
        }
    },
    callback: {
        onClick: function (e, treeId, treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("menuTree"), nodes = zTree.getSelectedNodes(), v = "";
            vid = "";
            nodes.sort(function compare(a, b) {
                return a.id - b.id;
            });
            for (var i = 0, l = nodes.length; i < l; i++) {
                v += nodes[i].sortName + ",";
                vid += nodes[i].sortId + ",";
            }
            if (v.length > 0)
                v = v.substring(0, v.length - 1);
            if (vid.length > 0)
                vid = vid.substring(0, vid.length - 1);
            var idem = $("#levelId");
            idem.val(v);
            idem.attr("data-value", vid);
        }
    }
};

$(function () {
    $.ajax({
        url: "/ret/proget/getProSortTree",
        type: "post",
        dataType: "json",
        success: function (data) {
            zTree = $.fn.zTree.init($("#tree"), setting, data);// 初始化树节点时，添加同步获取的数据
            var topNode = [{
                sortName: "TOP分类",
                isParent: "false",
                sortId: ""
            }];
            var newTreeNodes = topNode.concat(data);
            $.fn.zTree.init($("#menuTree"), setting1, newTreeNodes);
        }
    });
    $("#createbut").unbind("click").click(function () {
        addsort();
    });
    $("#cbut").unbind("click").click(function () {
        document.getElementById("from").reset();
        $("#flowdiv").empty();
        $("#flowdiv").attr("data-value", "");
        $("#createbut").show();
        $("#updatabut").hide();
        $("#delbut").hide();
    });
    $("#delbut").unbind("click").click(function () {
        delProSort();
    });
    $("#updatabut").unbind("click").click(function () {
        updateProSort();
    });

    $("#levelId").unbind("click").click(function (e) {
        e.stopPropagation();
        $("#menuContent").css({
            "width": $(this).outerWidth() + "px"
        }).slideDown(200);
    });
    $("body").unbind("click").click(function () {
        $("#menuContent").hide();
    });

    $("#menuContent").unbind("click").click(function (e) {
        e.stopPropagation();
    });
    $.ajax({
        url: "/ret/bpmget/getAllBpmFlowListByManage",
        type: "post",
        dataType: "json",
        success: function (data) {
            if (data.status == "200") {
                var html = "";
                for (var i = 0; i < data.list.length; i++) {
                    html += "<option value='" + data.list[i].flowId + "'>" + data.list[i].flowName + "</option>"
                }
                $("#bpmlist").html(html);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
});

function zTreeOnClick(event, treeId, treeNode) {
    $("#flowdiv").empty();
    $("#flowdiv").attr("data-value", "");
    document.getElementById("from").reset();
    $.ajax({
        url: "/ret/proget/getProSortById",
        type: "post",
        dataType: "json",
        data: {
            sortId: treeNode.sortId
        },
        success: function (data) {
            if (data.status == "200") {
                let v = data.list;
                for (let name in v) {
                    if (name == "levelId") {
                        $.ajax({
                            url: "/ret/proget/getProSortById",
                            type: "post",
                            dataType: "json",
                            data: {
                                sortId: v["levelId"]
                            },
                            success: function (data) {
                                if (data.status == "200") {
                                    if (data.list) {
                                        $("#levelId").val(data.list.sortName);
                                        $("#levelId").attr("data-value", data.list.sortId);
                                    } else {
                                        $("#levelId").attr("data-value", "");
                                        $("#levelId").val("");
                                    }
                                }
                            }
                        });
                    } else if (name == "flowIds") {
                        $.ajax({
                            url: "/ret/bpmget/getFlowNameByFlowIds",
                            type: "post",
                            dataType: "json",
                            async: false,
                            data: {flowIds: v[name]},
                            success: function (res) {
                                if (res.status == "200") {
                                    if (data.list) {
                                        for (let i = 0; i < res.list.length; i++) {
                                            $("#flowdiv").append("<div class='js-" + res.list[i].flowId + "'>" + res.list[i].flowName + "<a onclick='clearBpmOpt(\"" + res.list[i].flowId + "\");' style='float: right;cursor: pointer'>×</a></div>");
                                        }
                                    }
                                } else if (res.status == "100") {
                                    layer.msg(sysmsg[res.msg]);
                                } else {
                                    console.log(res.list);
                                }
                            }
                        })
                    } else {
                        $("#" + name).val(v[name]);
                    }
                }
            } else {
                console.log(data.msg);
            }

        }
    });
    $("#createbut").hide();
    $("#updatabut").show();
    $("#delbut").show();
}

function delProSort() {
    if (confirm("确定删除当前分类吗？")) {
        $.ajax({
            url: "/set/proset/deleteProSort",
            type: "post",
            dataType: "json",
            data: {
                sortId: $("#sortId").val()
            },
            success: function (data) {
                if (data.status == 200) {
                    layer.msg(sysmsg[data.msg]);
                    location.reload();
                } else {
                    console.log(data.msg);
                }
            }
        });
    } else {
        return;
    }
}

function addsort() {
    if ($("#sortName").val() == "") {
        layer.msg("分类名称不能为空！");
        return;
    }
    $.ajax({
        url: "/set/proset/insertProSort",
        type: "post",
        dataType: "json",
        data: {
            sortNo: $("#sortNo").val(),
            sortName: $("#sortName").val(),
            flowIds: $("#flowdiv").attr("data-value"),
            levelId: $("#levelId").attr("data-value"),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else {
                console.log(data.msg);
            }
        }
    });
}

function updateProSort() {
    if ($("#sortName").val() == "") {
        layer.msg("分类名称不能为空！");
        return;
    }
    $.ajax({
        url: "/set/proset/updateProSort",
        type: "post",
        dataType: "json",
        data: {
            sortId: $("#sortId").val(),
            sortNo: $("#sortNo").val(),
            sortName: $("#sortName").val(),
            flowIds: $("#flowdiv").attr("data-value"),
            levelId: $("#levelId").attr("data-value"),
            remark: $("#remark").val()
        },
        success: function (data) {
            if (data.status == 200) {
                layer.msg(sysmsg[data.msg]);
                location.reload();
            } else {
                console.log(data.msg);
            }
        }
    });
}

function setBpm() {
    $("#bpmModal").modal("show");
    $(".js-add-bpm").unbind("click").click(function () {
        let flowId = $("#bpmlist").val();
        let flowName = $("#bpmlist").find("option:selected").text();
        $("#flowdiv").append("<div class='js-" + flowId + "'>" + flowName + "<a onclick='clearBpmOpt(\"" + flowId + "\");' style='float: right;cursor: pointer'>×</a></div>");
        let tmpValue = $("#flowdiv").attr("data-value");
        let flowArr = [];
        if (tmpValue) {
            flowArr = tmpValue.split(",");
        }
        flowArr.push(flowId);
        $("#flowdiv").attr("data-value", flowArr.join(","));
        $("#bpmModal").modal("hide");
    })
}

function clearBpmOpt(flowId) {
    let tmpValue = $("#flowdiv").attr("data-value");
    tmpValue = tmpValue.replaceAll(flowId + ",", "");
    tmpValue = tmpValue.replaceAll(flowId, "");
    let flowArr = [];
    if (tmpValue) {
        flowArr = tmpValue.split(",");
    }
    $("#flowdiv").attr("data-value", flowArr.join(","));
    $(".js-" + flowId).remove();
}
