$(function () {
    $.ajax({
        url: "/ret/proget/getProRecordById",
        type: "post",
        dataType: "json",
        data: {proId: proId},
        success: function (data) {
            if (data.status == "200") {
                let info = data.list;
                for (let id in info) {
                    if (id == "attach") {
                        $("#attach").attr("data_value", info.attach);
                        createAttach("attach", 1);
                    } else if (id == "manager" || id == "approvalUser") {
                        $("#" + id).html(getUserNameByStr(info[id]));
                    } else if (id == "joinDept") {
                        $("#" + id).html(getDeptNameByDeptIds(info[id]));
                    } else if (id == "taskUsers") {
                        $("#" + id).html(getUserNameByStr(info[id]));
                    } else if (id == "isCreateFile") {
                        if (info[id] == "1") {
                            $("#" + id).html("创建项目文档目录");
                        } else {
                            $("#" + id).html("不创建项目文档目录");
                        }
                    } else if (id == "proSort") {
                        $.ajax({
                            url: "/ret/proget/getProSortById",
                            type: "post",
                            dataType: "json",
                            data: {sortId: info[id]},
                            success: function (data) {
                                if (data.status = "200") {
                                    $("#" + id).html(data.list.sortName);
                                } else if (data.status == "100") {
                                    layer.msg(sysmsg[data.msg]);
                                } else {
                                    console.log(data.msg);
                                }
                            }
                        });
                    } else {
                        $("#" + id).html(info[id]);
                    }
                }
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    });
    getProRecordById();
    gantt.config.xml_date = "%Y-%m-%d %H:%i";
    gantt.addMarker({
        start_date: new Date(),
        css: "today",
        text: "今日",
        title: "日期:" + getSysDate()
    });
    gantt.config.readonly = true;
    gantt.init("gantt_here");
    gantt.attachEvent("onGanttReady", function () {
        var tooltips = gantt.ext.tooltips;
        tooltips.tooltip.setViewport(gantt.$task_data);
    });
    gantt.parse(getProTaskInfo());
    $("#funds").click(function () {
        getProCostTypePie();
        getProCostTypeBar();
    })
})

function getProRecordById() {
    $.ajax({
        url: "/ret/proget/getProRecordById",
        type: "post",
        dataType: "json",
        data: {proId: proId},
        success: function (data) {
            if (data.status == "500") {
                console.log(data.msg);
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                $("#title1").html(data.list.title);
                $("#manager1").html("项目经理：【" + getUserNameByStr(data.list.manager) + "】");
                $("#prodate").html("开始日期：" + data.list.beginTime + "&nbsp;&nbsp;结束日期：" + data.list.endTime + "")
            }
        }
    });
}

function getProTaskInfo() {
    var returnData;
    $.ajax({
        url: "/ret/proget/getProTaskInfo",
        type: "POST",
        dataType: "json",
        async: false,
        data: {
            proId: proId
        },
        success: function (data) {
            if (data.status == "200") {
                returnData = data.list;
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.msg);
            }
        }
    })
    return returnData;
}

var myChart;

function getProCostTypePie() {
    $.ajax({
        url: "/ret/echartsprojectget/getProCostTypePie",
        type: "post",
        dataType: "json",
        data: {proId: proId},
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                myChart = echarts.init(document.getElementById('main1'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}

function getProCostTypeBar() {
    $.ajax({
        url: "/ret/echartsprojectget/getProCostTypeBar",
        type: "post",
        dataType: "json",
        data: {proId: proId},
        success: function (data) {
            if (data.status == "200") {
                var option = data.list;
                myChart = echarts.init(document.getElementById('main2'));
                myChart.setOption(option);
                window.addEventListener("resize", function () {
                    myChart.resize();
                });
            } else if (data.status == "100") {
                layer.msg(sysmsg[data.msg]);
            } else {
                console.log(data.list);
            }
        }
    })
}
