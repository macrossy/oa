$(function () {
    jeDate("#beginTimeQuery", {
        format: "YYYY-MM-DD"
    });
    jeDate("#endTimeQuery", {
        format: "YYYY-MM-DD"
    });
    $(".js-simple-query").unbind("click").click(function () {
        $("#myTable").bootstrapTable("refresh");
    })
    query();
})

function query() {
    $("#myTable").bootstrapTable({
        url: '/ret/proget/getProRecordListForApproval',
        method: 'post',
        contentType: 'application/x-www-form-urlencoded',
        toolbar: '#toobar',//工具列
        striped: true,//隔行换色
        cache: false,//禁用缓存
        pagination: true,//启动分页
        sidePagination: 'server',//分页方式
        pageNumber: 1,//初始化table时显示的页码
        pageSize: 10,//每页条目
        showFooter: false,//是否显示列脚
        showPaginationSwitch: true,//是否显示 数据条数选择框
        sortOrder: "DESC",
        sortable: true,//排序
        search: true,//启用搜索
        showColumns: true,//是否显示 内容列下拉框
        showRefresh: true,//显示刷新按钮
        idField: 'proId',//key值栏位
        clickToSelect: true,//点击选中checkbox
        pageList: [10, 20, 30, 50],//可选择单页记录数
        queryParams: queryParams,
        columns: [{
            checkbox: true
        }, {
            field: 'num',
            title: '序号',//标题  可不加
            width: '50px',
            formatter: function (value, row, index) {
                return index + 1;
            }
        }, {
            field: 'proCode',
            width: '50px',
            title: '项目编号'
        }, {
            field: 'title',
            width: '150px',
            title: '项目名称'
        }, {
            field: 'sortName',
            width: '50px',
            title: '项目分类'
        }, {
            field: 'proLevel',
            width: '50px',
            title: '项目等级',
            formatter: function (value, row, index) {
                if (value == "0") {
                    return "一般";
                } else if (value == "1") {
                    return "普通";
                } else if (value == "2") {
                    return "重点";
                }
            }
        }, {
            field: 'beginTime',
            width: '50px',
            title: '开始时间'
        }, {
            field: 'endTime',
            width: '50px',
            title: '结束时间'
        }, {
            field: 'managerUserName',
            width: '50px',
            title: '项目经理'
        }, {
            field: 'opt',
            title: '操作',
            align: 'center',
            width: '100px',
            formatter: function (value, row, index) {
                return createOptBtn(row.proId);
            }
        }],
        onClickCell: function (field, value, row, $element) {
            //alert(row.SystemDesc);
        },
        responseHandler: function (res) {
            if (res.status == "500") {
                console.log(res.msg);
            } else if (res.status == "100") {
                layer.msg(sysmsg[res.msg]);
            } else {
                return {
                    total: res.list.total, //总页数,前面的key必须为"total"
                    rows: res.list.list
                };
            }
        }
    });
}

function queryParams(params) {
    var temp = {
        search: params.search,
        pageSize: this.pageSize,
        pageNumber: this.pageNumber,
        sort: params.sort,
        sortOrder: params.order,
        proLevel: $("#proLevelQuery").val(),
        endTime: $("#endTimeQuery").val(),
        beginTime: $("#beginTimeQuery").val()
    };
    return temp;
};

function createOptBtn(proId) {
    let html = "<a href=\"javascript:void(0);doapproval('" + proId + "')\" class=\"btn btn-darkorange btn-xs\">审批</a>&nbsp;&nbsp;" +
        "<a href=\"javascript:void(0);details('" + proId + "')\" class=\"btn btn-sky btn-xs\" >详情</a>";
    return html;
}

function details(proId) {
    window.open("/app/core/project/projectdetails?view=approval&proId=" + proId);
}

function doapproval(proId) {
    $("#approvalModal").modal("show");
    document.getElementById("form1").reset();
    $(".js-approval").unbind("click").click(function () {
        $.ajax({
            url: "/set/proset/setApprovalProRecrod",
            type: "post",
            dataType: "json",
            data: {
                proId: proId,
                ideaText: $("#ideaText").val(),
                status: $("#status").val()
            },
            success: function (data) {
                if (data.status == "200") {
                    layer.msg(sysmsg[data.msg]);
                    $("#myTable").bootstrapTable("refresh");
                    $("#approvalModal").modal("hide");
                } else if (data.status == "100") {
                    layer.msg(sysmsg[data.msg]);
                } else {
                    console.log(data.msg);
                }
            }
        });
    })
}
