package com.core136.controller.project;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/app/core/project")
public class PageProController {
    /**
     * 项目经费
     *
     * @param view
     * @return
     */
    @RequestMapping("/funds")
    @RequiresPermissions("/app/core/project/funds")
    public ModelAndView goProFunds(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/project/funds/index");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("/app/core/project/funds/manage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 项目问题
     *
     * @return
     */
    @RequestMapping("/problem")
    @RequiresPermissions("/app/core/project/problem")
    public ModelAndView goProblem() {
        try {
            return new ModelAndView("/app/core/project/problem/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 项目查询
     *
     * @return
     */
    @RequestMapping("/query")
    @RequiresPermissions("/app/core/project/query")
    public ModelAndView goProQuery() {
        try {
            return new ModelAndView("/app/core/project/query/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 项目审批
     *
     * @param view
     * @return
     */
    @RequestMapping("/proapproval")
    @RequiresPermissions("/app/core/project/proapproval")
    public ModelAndView goProApproval(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/project/approval/index");
            } else {
                if (view.equals("manage")) {
                    mv = new ModelAndView("/app/core/project/approval/manage");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 问题详情
     *
     * @return
     */
    @RequestMapping("/problemdetails")
    @RequiresPermissions("/app/core/project/problemdetails")
    public ModelAndView goProblemDetails() {
        try {
            return new ModelAndView("/app/core/project/problem/details");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 问题回复
     *
     * @return
     */
    @RequestMapping("/problemreply")
    @RequiresPermissions("/app/core/project/problemreply")
    public ModelAndView goProblemReply() {
        try {
            return new ModelAndView("/app/core/project/problem/reply");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 项目文档
     *
     * @return
     */
    @RequestMapping("/profile")
    @RequiresPermissions("/app/core/project/profile")
    public ModelAndView goProFile() {
        try {
            return new ModelAndView("/app/core/project/file/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 获取我的待办任务
     *
     * @param view
     * @return
     */
    @RequestMapping("/myprotask")
    @RequiresPermissions("/app/core/project/myprotask")
    public ModelAndView goMyProTask(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/project/task/index");
            } else {
                if (view.equals("process")) {
                    mv = new ModelAndView("/app/core/project/process/index");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 项目任务详情
     *
     * @return
     */
    @RequestMapping("/protaskdetails")
    @RequiresPermissions("/app/core/project/protaskdetails")
    public ModelAndView goProTaskDetails() {
        try {
            return new ModelAndView("/app/core/project/task/details");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 项目任务分解
     *
     * @return
     */
    @RequestMapping("/decompose")
    @RequiresPermissions("/app/core/project/decompose")
    public ModelAndView goDecompose() {
        try {
            return new ModelAndView("/app/core/project/pro/decompose");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 项目详情
     *
     * @return
     */
    @RequestMapping("/projectdetails")
    @RequiresPermissions("/app/core/project/projectdetails")
    public ModelAndView goTrainingDetails(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/project/pro/details");
            } else {
                if (view.equals("approval")) {
                    mv = new ModelAndView("/app/core/project/approval/details");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 项目立项
     *
     * @param view
     * @return
     */
    @RequestMapping("/createproject")
    @RequiresPermissions("/app/core/project/createproject")
    public ModelAndView goCreateProject(String view) {
        try {
            return new ModelAndView("/app/core/project/pro/index");
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 项目权限设置
     *
     * @param view
     * @return
     */
    @RequestMapping("/param/setpriv")
    @RequiresPermissions("/app/core/project/param/setpriv")
    public ModelAndView goMyPartyNews(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/project/param/createpriv");
            } else {
                if (view.equals("approval")) {
                    mv = new ModelAndView("/app/core/project/param/approvalpriv");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

    /**
     * 项目代码设置
     *
     * @param view
     * @return
     */
    @RequestMapping("/param/setparam")
    @RequiresPermissions("/app/core/project/param/setparam")
    public ModelAndView goSetParam(String view) {
        ModelAndView mv = null;
        try {
            if (StringUtils.isBlank(view)) {
                mv = new ModelAndView("/app/core/project/param/prosort");
            } else {
                if (view.equals("role")) {
                    mv = new ModelAndView("/app/core/project/param/prorole");
                } else if (view.equals("cost")) {
                    mv = new ModelAndView("/app/core/project/param/procost");
                } else if (view.equals("exp")) {
                    mv = new ModelAndView("/app/core/project/param/proexpaccount");
                }
            }
            return mv;
        } catch (Exception e) {
            return new ModelAndView("titps");
        }
    }

}
