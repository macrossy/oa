package com.core136.service.project;

import com.core136.bean.project.ProTask;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.project.ProTaskMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProTaskService {
    private ProTaskMapper proTaskMapper;

    @Autowired
    public void setProTaskMapper(ProTaskMapper proTaskMapper) {
        this.proTaskMapper = proTaskMapper;
    }

    private ProTaskLinkService proTaskLinkService;

    @Autowired
    public void setProTaskLinkService(ProTaskLinkService proTaskLinkService) {
        this.proTaskLinkService = proTaskLinkService;
    }

    public int insertProTask(ProTask proTask) {
        return proTaskMapper.insert(proTask);
    }

    public int deleteProTask(ProTask proTask) {
        return proTaskMapper.delete(proTask);
    }

    public int updateProTask(Example example, ProTask proTask) {
        return proTaskMapper.updateByExampleSelective(proTask, example);
    }

    public ProTask selectOneProTask(ProTask proTask) {
        return proTaskMapper.selectOne(proTask);
    }

    /**
     * 获取子任甘特图信息
     *
     * @param orgId
     * @param proId
     * @return
     */
    public Map<String, List<Map<String, String>>> getProTaskInfo(String orgId, String proId) {
        Map<String, List<Map<String, String>>> map = new HashMap<String, List<Map<String, String>>>();
        map.put("data", getProTaskList(orgId, proId));
        map.put("links", proTaskLinkService.getProTaskLinkList(orgId, proId));
        return map;
    }


    /**
     * 获取项目子任务列表
     *
     * @param orgId
     * @param proId
     * @return
     */
    public List<Map<String, String>> getProTaskList(String orgId, String proId) {
        return proTaskMapper.getProTaskList(orgId, proId);
    }

    /**
     * 获取项目子任务列表
     *
     * @param orgId
     * @param accountId
     * @param proSort
     * @param beginTime
     * @param endTime
     * @param search
     * @return
     */
    public List<Map<String, String>> getMyTaskWorkList(String orgId, String accountId, String proSort, String beginTime, String endTime, String search) {
        return proTaskMapper.getMyTaskWorkList(orgId, accountId, proSort, beginTime, endTime, "%" + search + "%");
    }

    /**
     * 获取项目子任务列表
     *
     * @param pageParam
     * @param proSort
     * @param beginTime
     * @param endTime
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getMyTaskWorkList(PageParam pageParam, String proSort, String beginTime, String endTime) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getMyTaskWorkList(pageParam.getOrgId(), pageParam.getAccountId(), proSort, beginTime, endTime, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }


    public List<Map<String, String>> getTaskByProForSelect(@Param(value = "orgId") String orgId, @Param(value = "proId") String proId) {
        return proTaskMapper.getTaskByProForSelect(orgId, proId);
    }

}
