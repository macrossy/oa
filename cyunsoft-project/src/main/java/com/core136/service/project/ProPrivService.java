package com.core136.service.project;

import com.core136.bean.account.Account;
import com.core136.bean.project.ProPriv;
import com.core136.common.enums.MessageCode;
import com.core136.common.retdataunit.RetDataBean;
import com.core136.common.retdataunit.RetDataTools;
import com.core136.common.utils.SysTools;
import com.core136.mapper.project.ProPrivMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

@Service
public class ProPrivService {
    private ProPrivMapper proPrivMapper;

    @Autowired
    public void setProPrivMapper(ProPrivMapper proPrivMapper) {
        this.proPrivMapper = proPrivMapper;
    }

    public int insertProPriv(ProPriv proPriv) {
        return proPrivMapper.insert(proPriv);
    }

    public int deleteProPriv(ProPriv proPriv) {
        return proPrivMapper.delete(proPriv);
    }

    public int updateProPriv(Example example, ProPriv proPriv) {
        return proPrivMapper.updateByExampleSelective(proPriv, example);
    }

    public ProPriv selectOneProPriv(ProPriv proPriv) {
        return proPrivMapper.selectOne(proPriv);
    }

    /**
     * 设置项止权限
     *
     * @param account
     * @param proPriv
     * @return
     */
    public RetDataBean setProPriv(Account account, ProPriv proPriv) {
        ProPriv tmpProPriv = new ProPriv();
        tmpProPriv.setOrgId(account.getOrgId());
        tmpProPriv = selectOneProPriv(tmpProPriv);
        if (tmpProPriv == null) {
            proPriv.setPrivId(SysTools.getGUID());
            proPriv.setCreateUser(account.getAccountId());
            proPriv.setCreateTime(SysTools.getTime("yyyy-MM-dd HH:mm:ss"));
            proPriv.setOrgId(account.getOrgId());
            insertProPriv(proPriv);
        } else {
            Example example = new Example(ProPriv.class);
            example.createCriteria().andEqualTo("orgId", account.getOrgId());
            updateProPriv(example, proPriv);
        }
        return RetDataTools.Ok(MessageCode.MESSAGE_SUCCESS);
    }

    /**
     * 判读人员是否在立项权限内
     *
     * @param orgId
     * @param accountId
     * @param deptId
     * @param levelId
     * @return
     */
    public Integer isInPriv(String orgId, String accountId, String deptId, String levelId) {
        return proPrivMapper.isInPriv(orgId, accountId, deptId, levelId);
    }


}
