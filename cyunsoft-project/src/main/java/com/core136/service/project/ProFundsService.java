package com.core136.service.project;

import com.core136.bean.project.ProFunds;
import com.core136.bean.sys.PageParam;
import com.core136.common.utils.SysTools;
import com.core136.mapper.project.ProFundsMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ProFundsService {
    private ProFundsMapper proFundsMapper;

    @Autowired
    private void setProFundsMapper(ProFundsMapper proFundsMapper) {
        this.proFundsMapper = proFundsMapper;
    }

    public int insertProFunds(ProFunds proFunds) {
        return proFundsMapper.insert(proFunds);
    }

    public int deleteProFunds(ProFunds proFunds) {
        return proFundsMapper.delete(proFunds);
    }

    public int updateProFunds(Example example, ProFunds proFunds) {
        return proFundsMapper.updateByExampleSelective(proFunds, example);
    }

    public ProFunds selectOneProFunds(ProFunds proFunds) {
        return proFundsMapper.selectOne(proFunds);
    }


    /**
     * 项目预算列表
     *
     * @param orgId
     * @param proId
     * @param search
     * @return
     */
    public List<Map<String, String>> getProFundsListByProId(String orgId, String proId, String search) {
        return proFundsMapper.getProFundsListByProId(orgId, proId, search);
    }

    /**
     * 项目预算列表
     *
     * @param pageParam
     * @param proId
     * @return
     * @throws Exception
     */
    public PageInfo<Map<String, String>> getProFundsListByProId(PageParam pageParam, String proId) throws Exception {
        PageHelper.startPage(pageParam.getPageNumber(), pageParam.getPageSize(), SysTools.escapeSqlStr(pageParam.getOrderBy()));
        List<Map<String, String>> datalist = getProFundsListByProId(pageParam.getOrgId(), proId, pageParam.getSearch());
        PageInfo<Map<String, String>> pageInfo = new PageInfo<Map<String, String>>(datalist);
        return pageInfo;
    }

}
