package com.core136.service.project;

import com.core136.bean.project.ProExpAccount;
import com.core136.mapper.project.ProExpAccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ProExpAccountService {
    private ProExpAccountMapper proExpAccountMapper;

    @Autowired
    public void setProExpAccountMapper(ProExpAccountMapper proExpAccountMapper) {
        this.proExpAccountMapper = proExpAccountMapper;
    }

    public int insertProExpAccount(ProExpAccount proExpAccount) {
        return proExpAccountMapper.insert(proExpAccount);
    }

    public int deleteProExpAccount(ProExpAccount proExpAccount) {
        return proExpAccountMapper.delete(proExpAccount);
    }

    public int updateProExpAccount(Example example, ProExpAccount proExpAccount) {
        return proExpAccountMapper.updateByExampleSelective(proExpAccount, example);
    }

    public ProExpAccount selectOneProExpAccount(ProExpAccount proExpAccount) {
        return proExpAccountMapper.selectOne(proExpAccount);
    }

    /**
     * 获取项目费用科目树结构
     *
     * @param orgId
     * @param levelId
     * @return
     */
    public List<Map<String, String>> getProExpAccountTree(String orgId, String levelId) {
        return proExpAccountMapper.getProExpAccountTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     *
     * @param orgId
     * @param sortId
     * @return
     */
    public int isExistChild(String orgId, String sortId) {
        return proExpAccountMapper.isExistChild(orgId, sortId);
    }

}
