package com.core136.service.project;

import com.core136.bean.project.ProSort;
import com.core136.mapper.project.ProSortMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ProSortService {
    private ProSortMapper proSortMapper;

    @Autowired
    public void setProSortMapper(ProSortMapper proSortMapper) {
        this.proSortMapper = proSortMapper;
    }

    public int insertProSort(ProSort proSort) {
        return proSortMapper.insert(proSort);
    }

    public int deleteProSort(ProSort proSort) {
        return proSortMapper.delete(proSort);
    }

    public int updateProSort(Example example, ProSort proSort) {
        return proSortMapper.updateByExampleSelective(proSort, example);
    }

    public ProSort selectOneProSort(ProSort proSort) {
        return proSortMapper.selectOne(proSort);
    }

    /**
     * 获取项目分类树结构
     *
     * @param orgId
     * @param levelId
     * @return
     */
    public List<Map<String, String>> getProSortTree(String orgId, String levelId) {
        return proSortMapper.getProSortTree(orgId, levelId);
    }

    /**
     * 获取项目分类树结构
     *
     * @param orgId
     * @param levelId
     * @return
     */
    public List<Map<String, String>> getProSortForProTree(String orgId, String levelId) {
        return proSortMapper.getProSortForProTree(orgId, levelId);
    }

    /**
     * 判断是否还有子集
     *
     * @param orgId
     * @param sortId
     * @return
     */
    public int isExistChild(String orgId, String sortId) {
        return proSortMapper.isExistChild(orgId, sortId);
    }


}
